export {}
declare global{
    interface Array<T>{
        /**
         * Loads an enum into an array of { key, value }
         * @param enumType the enum source to map from
         * @param convertToSpaceCasing if `true`, converts the keys from 'camelCase' to 'space Case'. defaults to `true`
         * @param excludeValues indicates an array of enum items to exclude by value
         */
        loadFromEnum(enumType:any, convertToSpaceCasing?:boolean, excludeValues?:number[]):void
        /**
         * shorthand for array.slice, removes all items in the array that satisfies the predicate callback
         * 
         * returns the number of items removed
         * @param predicate callback to use for to tell the function what items we want to remove
         */
        removeWhere(predicate: (value: T, index: number, obj: T[]) => boolean):number
        /**
         * 
         * @param predicate predicate function for finding the correct item
         * @param childrenPropertyName name of childArray property
         */
        findRecursive(predicate: (value: T, index: number, obj: T[]) => boolean, childrenPropertyName:string): T;
        /**
         * 
         * @param callbackfn forEachItem callback
         * @param childrenPropertyName name of childArray property
         */
        forEachRecursive(callbackfn: (value: T, index: number, array: T[]) => void, childrenPropertyName:string);
        /**
         * recompute the .SortIndex of items base on the current sort order of the array
         * @param startIndex starting index to use for .SortIndex, defaults to `0`
         * @param filterPredicate  filter predicate to determine which items need .SortIndex recomputing, recomputes every item in the array if no filter is applied
         * @param SortIndexProperty  name of the SortIndex property, defaults to `"SortIndex"`
         */
        recomputeSortIndex(startIndex?:number, filterPredicate?: (value: T, index: number, obj: T[]) => boolean, SortIndexProperty?:string);
        /**
         * similar to array.push but inserts the item at the specified index
         * @param index insertion index
         * @param items the items to insert in the array
         */
        insertAt(index:number, ...items:T[]);
        /**
         * 
         * @param sorting Sort Type: ASC / DESC, defaults to `"ASC"`
         * @param sortBy name of the property to be sorted by, defaults to `"ID"` 
         * @param ParentProperty name of the parent property, defaults to `"ID"`
         * @param ChildProperty name of the child property, defaults to `"ParentID"`
         */
        sortWithParentChild(sorting?: 'ASC' | 'DESC', sortBy?: string, ParentProperty?: string, ChildProperty?: string);
        /**
         * returns false if no two children in array have the same value of the given property name
         * returns the string value of the duplicate if it does have duplicate
         * @param propertyName 
         */
        hasChildWithDuplicateProperty(propertyName: string): boolean|string;

        /**
         * strip out duplicate items in the array
         * @param compareFn function to use for comparing if two items are equal. defaults to `x==y`
         */
        filterDuplicates(compareFn?:(x:T,y:T)=>boolean):T[];
    }
}
Array.prototype.loadFromEnum = function(enumType:any, convertToSpaceCasing:boolean=true, excludeValues:number[]=[]){
    /*
        Example usage:
        var someArray:any[] = [];
        someArray.loadFromEnum(someEnum);
    */
    this.length = 0;
    var keys = Object.keys(enumType).filter(key => !isNaN(Number(enumType[key])));
    keys.forEach(key=>{
        this.push({
            key: convertToSpaceCasing? key.replace(/([A-Z]+)*([A-Z][a-z])/g, "$1 $2").trim():key,
            value: enumType[key]
        });
    });
    excludeValues.forEach(excluded=>{
        let itemToRemove = this.find(item=>item.value==excluded);
        let rmIndex = this.indexOf(itemToRemove);
        if(rmIndex>-1){
            this.splice(rmIndex, 1);
        }
    });
}
Array.prototype.removeWhere = function(predicate: (value: any, index: number, obj: any[]) => boolean){
    let removed = 0;
    let array = this as Array<any>;
    let itemsToRemove = array.filter(predicate);

    itemsToRemove.forEach(itemToRemove=>{
        let rmIndex = array.findIndex(item=>item==itemToRemove);
        if(rmIndex > -1){
            array.splice(rmIndex, 1);
            removed++;
        }
    });

    return removed;
}
Array.prototype.findRecursive = function(predicate:(value: any, index: number, obj: any[])=>boolean, childrenPropertyName:string){
    if(!childrenPropertyName){
        throw "findRecursive requires parameter `childrenPropertyName`";
    }
    let array = [];
    array = this;
    let initialFind =  array.find(predicate);
    let elementsWithChildren  = array.filter(x=>x[childrenPropertyName]);
    if(initialFind){
        return initialFind;
    }else if(elementsWithChildren.length){
        let childElements = [];
        elementsWithChildren.forEach(x=>{
            childElements.push(...x[childrenPropertyName]);
        });
        return childElements.findRecursive(predicate, childrenPropertyName);
    }else{
        return undefined;
    }
}
Array.prototype.forEachRecursive = function(callbackfn: (value: any, index: number, array: any[]) => void, childrenPropertyName:string){
    let array = this as Array<any>;
    let doRecursive = (items:any[])=>{
        items.forEach(callbackfn);
        let itemsWithChildren = items.filter(x=>x[childrenPropertyName]);
        if(itemsWithChildren.length){
            let children = [];
            itemsWithChildren.forEach(x=>children.push(...x[childrenPropertyName]));
            doRecursive(children);
        }
    }
    doRecursive(array);
}
Array.prototype.recomputeSortIndex = function(startIndex:number = 0,
                                             filterPredicate:(value: any, index: number, obj: any[]) => boolean = null,
                                             SortIndexProperty:string = "SortIndex" ){
    let index = startIndex;
    let actualArray = this as any[];
    let objectsToRecompute = [...actualArray];
    if(filterPredicate){
        objectsToRecompute = objectsToRecompute.filter(filterPredicate);
    }
    objectsToRecompute.forEach(obj=>obj[SortIndexProperty]=index++);
}
Array.prototype.insertAt =  function(index:number, ...items:any[]){
    let arr = this as any[];
    arr.splice(index, 0, ...items);
}
Array.prototype.sortWithParentChild = function(sorting: 'ASC' | 'DESC' = 'ASC', sortBy: string = 'ID', ParentProperty: string = "ID", ChildProperty: string = "ParentID"){
    let actualArray = this as any[];

    actualArray.sort((a,b) => {
        let compareA = a[sortBy].toString();
        let compareB = b[sortBy].toString();

        if (sorting == 'ASC') return compareA.localeCompare(compareB)
        if (sorting == 'DESC') return compareB.localeCompare(compareA);
        
        return 0;
    });

    let childrens = actualArray.filter(object => object[ChildProperty] != null && object[ChildProperty] != undefined);
    actualArray.removeWhere(object => object[ChildProperty] != null && object[ChildProperty] != undefined);

    if (childrens && childrens.length > 0) {
        let childrensWithParentIndex = {};
        childrens.forEach(child => {
            if (childrensWithParentIndex[child[ChildProperty]] == null && childrensWithParentIndex[child[ChildProperty]] == undefined) {
                childrensWithParentIndex[child[ChildProperty]] = [];
            }
            childrensWithParentIndex[child[ChildProperty]].push(child);
        });

        while(Object.keys(childrensWithParentIndex).length != 0) {
            for (let key in childrensWithParentIndex) {
                if (childrensWithParentIndex.hasOwnProperty(key)) {
                    let insertAtIndex = actualArray.findIndex(object => object[ParentProperty] == key);
                    if (insertAtIndex != null && insertAtIndex > -1) {
                        insertAtIndex++;
                        actualArray.insertAt(insertAtIndex, ...childrensWithParentIndex[key]);
                        delete childrensWithParentIndex[key];
                    }
                }
            }
        }
    }
}
Array.prototype.hasChildWithDuplicateProperty = function(propertyName: string){
    const visitedValueMap = {};
    for (let i = 0; i < this.length; i++) {
      const child = this[i];
      if (visitedValueMap[child[propertyName]] != null){
        return child[propertyName];
      } else {
        visitedValueMap[child[propertyName]] = true;
      }
    }
    return false;
}
Array.prototype.filterDuplicates = function(compareFn = (x,y)=>x == y){
    const arr:any[] = this;
    const newArr:any[] = [];
    for (const arrItem of arr) {
        const exists = newArr.some(newArrItem => compareFn(newArrItem, arrItem));
        !exists && newArr.push(arrItem);
    }
    return newArr;
}