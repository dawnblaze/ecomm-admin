import { Routes, RouterModule } from '@angular/router';

import { LogoutComponent } from "./modules/corebridge/components/logout/logout.component";
import { CompaniesComponent } from "./modules/ecommerce/pages/customers/companies/companies.component";
import { ContactsComponent } from './modules/ecommerce/pages/customers/contacts/contacts.component';
import { ContactDetailsComponent } from './modules/ecommerce/pages/customers/contacts/details/contact-details.component';
import { CompanyDetailsComponent } from "./modules/ecommerce/pages/customers/companies/details/company-details.component";
import { CatalogsComponent } from "./modules/ecommerce/pages/catalogs/catalogs.component";
import { CatalogDetailsComponent } from './modules/ecommerce/pages/catalog-details/catalog-details.component';
import { CatalogNewComponent } from './modules/ecommerce/pages/catalog-new/catalog-new.component';
import { CatalogAssociateComponent } from './modules/ecommerce/pages/catalog-associate/catalog-associate.component';
import { CategoryNodeDetailsComponent } from './modules/ecommerce/pages/category-node-details/category-node-details.component';
import { CategoriesComponent } from "./modules/ecommerce/pages/categories/categories.component";
import { CategoryDetailComponent } from "./modules/ecommerce/pages/category-detail/category-detail.component";
import { CategoryNewComponent } from './modules/ecommerce/pages/category-new/category-new.component';
import { AddonsComponent } from './modules/ecommerce/pages/addons/addons.component';
import { AddonDetailsComponent } from './modules/ecommerce/pages/addon-details/addon-details.component';
import { AddonNewComponent } from './modules/ecommerce/pages/addon-new/addon-new.component';
import { AddonValueNewComponent } from './modules/ecommerce/pages/addon-value-new/addon-value-new.component';
import { AddonValueDetailsComponent } from './modules/ecommerce/pages/addon-value-details/addon-value-details.component';
import { ProductsComponent } from './modules/ecommerce/pages/products/products.component';
import { ProductDetailsComponent } from './modules/ecommerce/pages/product-details/product-details.component';
import { ProductAssociateComponent } from './modules/ecommerce/pages/product-associate/product-associate.component';
import { ProductNewComponent } from './modules/ecommerce/pages/product-new/product-new.component';
import { StorefrontsComponent } from './modules/ecommerce/pages/storefronts/storefronts.component';
import { StorefrontDetailsComponent } from './modules/ecommerce/pages/storefront-details/storefront-details.component';
import { StorefrontNewComponent } from './modules/ecommerce/pages/storefront-new/storefront-new.component';
import { StylesComponent } from './modules/ecommerce/pages/styles/styles.component';
import { StyleNewComponent } from './modules/ecommerce/pages/style-new/style-new.component';
import { StyleDetailsComponent } from './modules/ecommerce/pages/style-details/style-details.component';
import { OrdersComponent } from './modules/ecommerce/pages/orders/orders.component';
import { EmailTemplatesComponent } from './modules/ecommerce/pages/email-templates/email-templates.component';
import { EmailTemplateDetailsComponent } from './modules/ecommerce/pages/email-template-details/email-template-details.component';
import { EmailTemplateNewComponent } from './modules/ecommerce/pages/email-template-new/email-template-new.component';
import { ProfilesComponent } from './modules/ecommerce/pages/profiles/profiles.component';
import { ProfileDetailsComponent } from './modules/ecommerce/pages/profile-details/profile-details.component';
import { ProfileNewComponent } from './modules/ecommerce/pages/profile-new/profile-new.component';
import { ReportingComponent } from './modules/ecommerce/pages/reporting/reporting.component';
import { ReportingCompanyFinancialComponent } from './modules/ecommerce/pages/reporting/company_financial/company_financial.component';
import { ReportingCustomersReceivablesComponent } from './modules/ecommerce/pages/reporting/customers_receivables/customers_receivables.component';
import { ReportingSalesComponent } from './modules/ecommerce/pages/reporting/sales/sales.component';
import { ReportingProductionComponent } from './modules/ecommerce/pages/reporting/production/production.component';
import { ReportingFranchiseComponent } from './modules/ecommerce/pages/reporting/franchise/franchise.component';
import { ComingSoonComponent } from "./modules/ecommerce/pages/coming-soon/coming-soon.component";
import { ProfileComponent } from './modules/ecommerce/pages/profile/profile.component';
import { ShippingBoxesComponent } from './modules/ecommerce/pages/shipping-boxes/shipping-boxes.component';
import { ShippingBoxDetailsComponent } from './modules/ecommerce/pages/shipping-box-details/shipping-box-details.component';
import { ShippingBoxNewComponent } from './modules/ecommerce/pages/shipping-box-new/shipping-box-new.component';
import { ShippingMethodsComponent } from './modules/ecommerce/pages/shipping-methods/shipping-methods.component';
import { ShippingMethodDetailsComponent } from './modules/ecommerce/pages/shipping-method-details/shipping-method-details.component';
import { ShippingMethodNewComponent } from './modules/ecommerce/pages/shipping-method-new/shipping-method-new.component';
import { ReviewsComponent } from './modules/ecommerce/pages/reviews/reviews.component';
import { ReviewDetailsComponent } from './modules/ecommerce/pages/review-details/review-details.component';
import { FontsComponent } from './modules/ecommerce/pages/settings/fonts/fonts.component';
import { FontNewComponent } from './modules/ecommerce/pages/settings/font-new/font-new.component';
import { FontDetailsComponent } from './modules/ecommerce/pages/settings/font-details/font-details.component';
import { FontGroupsComponent } from './modules/ecommerce/pages/settings/font-groups/font-groups.component';
import { FontGroupNewComponent } from './modules/ecommerce/pages/settings/font-group-new/font-group-new.component';
import { FontGroupDetailsComponent } from './modules/ecommerce/pages/settings/font-group-details/font-group-details.component';
import { SettingsComponent } from './modules/ecommerce/pages/settings/settings.component';
import { ColorsComponent } from './modules/ecommerce/pages/settings/colors/colors.component';
import { ColorNewComponent } from './modules/ecommerce/pages/settings/color-new/color-new.component';
import { ColorDetailsComponent } from './modules/ecommerce/pages/settings/color-details/color-details.component';
import { ColorGroupsComponent } from './modules/ecommerce/pages/settings/color-groups/color-groups.component';
import { ColorGroupNewComponent } from './modules/ecommerce/pages/settings/color-group-new/color-group-new.component';
import { ColorGroupDetailsComponent } from './modules/ecommerce/pages/settings/color-group-details/color-group-details.component';
import { NotFoundComponent } from './modules/ecommerce/pages/not-found/not-found.component';
import { ProductDefaultsComponent } from "./modules/ecommerce/pages/product-defaults/product-defaults.component";
import { DevSupportComponent } from './modules/ecommerce/pages/developer/dev-support.component';
import { FeaturesComponent } from './modules/ecommerce/pages/features/features.component';
import { EventLogComponent } from './modules/ecommerce/pages/eventlog/event-log.component';
import { EditorPresetsComponent } from './modules/ecommerce/pages/editor-presets/editor-presets.component';
import { EditorPresetNewComponent } from './modules/ecommerce/pages/editor-preset-new/editor-preset-new.component';
import { SurfacePresetsComponent } from './modules/ecommerce/pages/surface-presets/surface-presets.component';
import { SurfacePresetDetailsComponent } from './modules/ecommerce/pages/surface-preset-details/surface-preset-details.component';

import { CanDeactivateGuard } from './services/can-deactivate-guard.service';
import { AuthGuard } from './services/app-guard.service';
import { CustomersCanvasComponent } from './modules/ecommerce/components/customers-canvas/customers-canvas.component';


const appRoutes: Routes = [
  {
    path: '',
    component: ComingSoonComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'profile',
    component: ProfileComponent
  },
  {
    path: 'dashboard',
    redirectTo: 'ecommerce/storefronts',
    pathMatch: 'full'
  },
  {
    path: 'ecommerce',
    redirectTo: 'ecommerce/storefronts',
    pathMatch: 'full'
  },
  // Storefronts
  {
    path: 'ecommerce/storefronts',
    component: StorefrontsComponent
  },
  {
    path: 'ecommerce/storefronts/add',
    component: StorefrontNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/storefronts/:id',
    component: StorefrontDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/storefronts/:id/:tab',
    component: StorefrontDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/storefronts/:id/:tab/:new',
    component: StorefrontDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/storefronts/:id/:tab/:selected',
    component: StorefrontDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Styles
  {
    path: 'ecommerce/styles',
    component: StylesComponent
  },
  {
    path: 'ecommerce/styles/add',
    component: StyleNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/styles/:id',
    component: StyleDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Catalogs
  {
    path: 'ecommerce/merchandise/catalogs',
    component: CatalogsComponent
  },
  {
    path: 'ecommerce/merchandise/catalogs/add',
    component: CatalogNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/catalogs/:id',
    component: CatalogDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/catalogs/:id/:tab',
    component: CatalogDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/catalogs/:id/:tab/add',
    component: CatalogDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/catalogs/:id/:tab/:category',
    component: CatalogDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Catagories
  {
    path: 'ecommerce/merchandise/collections',
    component: CategoriesComponent
  },
  {
    path: 'ecommerce/merchandise/collections/add',
    component: CategoryNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/collections/:id/products/associate',
    component: ProductAssociateComponent
  },
  {
    path: 'ecommerce/merchandise/collections/:id',
    component: CategoryDetailComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/collections/:id/:tab',
    component: CategoryDetailComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Products
  {
    path: 'ecommerce/merchandise/products',
    component: ProductsComponent
  },
  {
    path: 'ecommerce/merchandise/products/add',
    component: ProductNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/products/:id',
    component: ProductDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/products/:id/:tab',
    component: ProductDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/products/:id/:tab/new',
    component: ProductDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/products/:id/:tab/:selected',
    component: ProductDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Customer's Canvas
  {
    path: 'canvas-editor',
    component: CustomersCanvasComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Addons
  {
    path: 'ecommerce/merchandise/addons',
    component: AddonsComponent
  },
  {
    path: 'ecommerce/merchandise/addons/add',
    component: AddonNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/addons/:addon',
    component: AddonDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/merchandise/addons/:addon/:tab',
    component: AddonDetailsComponent,
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: 'ecommerce/merchandise/addons/:addon/:tab/add',
    component: AddonDetailsComponent,
    canDeactivate: [CanDeactivateGuard],
  },
  {
    path: 'ecommerce/merchandise/addons/:addon/:tab/:id',
    component: AddonDetailsComponent,
    canDeactivate: [CanDeactivateGuard],
  },
  // Email
  {
    path: 'ecommerce/email-templates',
    component: EmailTemplatesComponent
  },
  {
    path: 'ecommerce/email-templates/add',
    component: EmailTemplateNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/email-templates/:name',
    component: EmailTemplateDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Shipping Boxes
  {
    path: 'ecommerce/shipping-boxes',
    component: ShippingBoxesComponent
  },
  {
    path: 'ecommerce/shipping-boxes/add',
    component: ShippingBoxNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/shipping-boxes/:id',
    component: ShippingBoxDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Shipping Methods
  {
    path: 'ecommerce/shipping-methods',
    component: ShippingMethodsComponent
  },
  {
    path: 'ecommerce/shipping-methods/add',
    component: ShippingMethodNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/shipping-methods/:id',
    component: ShippingMethodDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/shipping-methods/:id/:tab',
    component: ShippingMethodDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Orders
  {
    path: 'ecommerce/orders',
    component: OrdersComponent
  },
  // Customers
  {
    path: 'ecommerce/customers/companies',
    component: CompaniesComponent
  },
  {
    path: 'ecommerce/customers/companies/:accountId',
    component: CompanyDetailsComponent
  },
  {
    path: 'ecommerce/customers/contacts/:accountId',
    component: ContactDetailsComponent
  },
  {
    path: 'ecommerce/customers/contacts',
    component: ContactsComponent
  },
  // Profiles
  {
    path: 'ecommerce/profiles',
    component: ProfilesComponent
  },
  {
    path: 'ecommerce/profiles/add',
    component: ProfileNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/profiles/:profile',
    component: ProfileDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Reports
  {
    path: 'ecommerce/reports',
    component: ReportingCustomersReceivablesComponent
  },
  {
    path: 'ecommerce/reports/company_financial',
    component: ReportingCompanyFinancialComponent
  },
  {
    path: 'ecommerce/reports/customers_receivables',
    component: ReportingCustomersReceivablesComponent
  },
  {
    path: 'ecommerce/reports/sales',
    component: ReportingSalesComponent
  },
  {
    path: 'ecommerce/reports/production',
    component: ReportingProductionComponent
  },
  {
    path: 'ecommerce/reports/franchise',
    component: ReportingFranchiseComponent
  },
  // Reviews
  {
    path: 'ecommerce/reviews',
    component: ReviewsComponent
  },
  {
    path: 'ecommerce/reviews/:id',
    component: ReviewDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Settings
  {
    path: 'ecommerce/settings',
    component: SettingsComponent
  },
  {
    path: 'ecommerce/settings/colors',
    component: ColorsComponent
  },
  {
    path: 'ecommerce/settings/colors/add',
    component: ColorNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/colors/:id',
    component: ColorDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/colors/:id/:tab',
    component: ColorDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/color-groups',
    component: ColorGroupsComponent
  },
  {
    path: 'ecommerce/settings/color-groups/add',
    component: ColorGroupNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/color-groups/:id',
    component: ColorGroupDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/color-groups/:id/:tab',
    component: ColorGroupDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/fonts',
    component: FontsComponent
  },
  {
    path: 'ecommerce/settings/fonts/system',
    component: FontsComponent
  },
  {
    path: 'ecommerce/settings/fonts/custom',
    component: FontsComponent
  },
  {
    path: 'ecommerce/settings/fonts/add',
    component: FontNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/fonts/:fontFamilyName',
    component: FontDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/fonts/:fontFamilyName/:tab',
    component: FontDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/font-groups',
    component: FontGroupsComponent
  },
  {
    path: 'ecommerce/settings/font-groups/add',
    component: FontGroupNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/font-groups/:id',
    component: FontGroupDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/font-groups/:id/:tab',
    component: FontGroupDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/product-defaults',
    component: ProductDefaultsComponent
  },
  //design-editor
  {
    path: 'ecommerce/settings/design-editor-presets',
    component: EditorPresetsComponent
  },
  {
    path: 'ecommerce/settings/design-editor-presets/add',
    component: EditorPresetNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/design-editor-presets/:id',
    component: EditorPresetNewComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  //surface-preset
  {
    path: 'ecommerce/settings/surface-presets',
    component: SurfacePresetsComponent
  },
  {
    path: 'ecommerce/settings/surface-presets/add',
    component: SurfacePresetDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: 'ecommerce/settings/surface-presets/:id',
    component: SurfacePresetDetailsComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  // Add a dummy route so that we can guard multiple child routes
  {
    path: '',
    runGuardsAndResolvers: 'always',
    canActivate: [AuthGuard],
    children: [
      {
        path:'developer',
        component: DevSupportComponent
      },
      {
        path: 'developer/features',
        component: FeaturesComponent
      },
      {
        path: 'developer/eventlog',
        component: EventLogComponent
      }
    ]
  },
  // Catch-all
  {
    path: '**',
    component: NotFoundComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);
