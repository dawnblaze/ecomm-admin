import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-root',
    template: '<cb-page><router-outlet></router-outlet></cb-page>'
})
export class AppComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
