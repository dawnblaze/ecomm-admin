import { DomSanitizer } from '@angular/platform-browser'
import { Observable, forkJoin, of }   from 'rxjs';

import { ProductArtwork } from './models/product-artwork';
import { Product } from './models/product';

import { AlertService }      from '../../services/alert.service';
import { CoreBridgeService } from '../../services/corebridge.service';
import { ModalService }      from '../../services/modal.service';
import { ZnodeService }      from '../../services/znode.service';
import { DomainService }      from '../../services/domain.service';
import { CesaService, MisTypes } from "../../services/cesa.service";

export class ArtworkModule{
  public ModuleEnabled: boolean;
  public Entity: ProductArtwork;
  public PreviewUrl: string;
  public OpenModalButtonText: string;

  private product: Product;
  private boundReceiveMessage: EventListener = this.receiveMessage.bind(this);

  constructor(private sanitizer: DomSanitizer, private alertService: AlertService, private domainService: DomainService, private corebridgeService: CoreBridgeService, private modalService: ModalService, private znodeService: ZnodeService, cesaService: CesaService)
  {
    if(cesaService.getMisType() == MisTypes.Cyrious)
    {
      this.ModuleEnabled = cesaService.getECodEnabled();
    }
    else if (cesaService.getMisType() == MisTypes.CoreBridge)
    {
      this.corebridgeService.getZnodeOnlineDesignerEnabled().subscribe(enabled => {
        this.ModuleEnabled = enabled;
        sessionStorage.setItem(CesaService.ECodEnabledKey, enabled.toString());
      },
          err => this.alertService.error(err)
      );
    }
  }

  private refreshProductComputedFields(){
    //let's look for an Artwork.ID
    let hasExistingArtworkID = this.Entity.ID != null && this.Entity.ID != "";
    //and refresh the product's computed fields
    this.product.HadCustomizableArtwork = hasExistingArtworkID;
    this.product.UseCustomizableArtwork = hasExistingArtworkID && this.Entity.IsActive;
    this.OpenModalButtonText = hasExistingArtworkID ? "Edit Design" : "Add Artwork";
    this.PreviewUrl = this.getArtworkPreviewURL();
  }

  public OnProductLoad(product: Product, productArtwork: ProductArtwork) {
      this.product = product;

      if (productArtwork == null) {
          this.Entity = new ProductArtwork();

          if (this.product.ProductId != null && this.product.ProductId > 0) {
            this.Entity.ProductID = product.ProductId;
          }
      } else {
          this.Entity = productArtwork;
      }

      this.refreshProductComputedFields();
  }

  public OnProductSave(product: Product, useCustomizableArtwork: boolean|null) {
      //if we have customizable artwork
      if (useCustomizableArtwork === true)
      {
        //product saved with customizable selected
        this.OnProductLoad(this.product, this.Entity);
      }
      else if (this.Entity == null)
      {
        //ok this is hard to explain - OnProductLoad always creates an Entity to use
        //but here it's missing, so we'll re-call OnProductLoad with null
        console.warn("Artwork module assumption error: most likely Save called before Load (underlying working artwork Entity missing)");
        this.OnProductLoad(this.product, null);
      }
      else
      {
        //product saved with Standard image type
        this.product = product;
        this.refreshProductComputedFields();
      }
  }

  private getArtworkPreviewURL(): string {
    if (this.Entity != null && this.Entity.ID) {
      return this.domainService.ecod + '/daim/asset/' + this.Entity.ID + '/thumbnail?' + Date.now();
    } else {
      return "assets/img/noimage.png";
    }
  }

  /// returns UseCustomizableArtwork boolean, not success/failure
  public UpdateProductArtwork(useCustomizeableArtwork: boolean): Observable<boolean|null>{
    if (this.product == null)
    {
      console.warn("Artwork module not initialized with product, but is attempting to save.");
    }
    else //if (this.product.HadCustomizableArtwork)
    {
      if (this.Entity == null)
      {
        console.warn("Artwork module has detected artwork update, but has no artwork to update");
      }
      else
      {
        this.Entity.IsActive = useCustomizeableArtwork;
        this.znodeService.updateProductArtwork(this.Entity).subscribe();
        return of<boolean>(this.Entity.IsActive);
      }
    }

    return of<boolean>(null);
  }

  private artworkEditorAction: string;
  public OpenModal() {
    this.artworkEditorAction = (this.Entity != null && this.Entity.ID) ? "edit&artwork=" + this.Entity.ID : "new";

    let odecUrl = this.domainService.ecod + '?operation=' + this.artworkEditorAction + '&nqzva=true';

    if (this.product.Height && this.product.Width) {
      odecUrl = odecUrl + '&h=' + this.product.Height + '&w=' + this.product.Width;
    }

    this.modalService.iframe(this.sanitizer.bypassSecurityTrustResourceUrl(odecUrl));
    window.addEventListener("message", this.boundReceiveMessage, false);
  }

  public Dispose() {
      window.removeEventListener("message", this.boundReceiveMessage);
  }

  receiveMessage(event) {
    var origin = event.origin || event.originalEvent.origin; // For Chrome, the origin property is in the event.originalEvent object.
    if (origin !== this.domainService.ecod)
      return;

    if (event.data.action == "odec.cancel.click") {
      this.modalService.close();
    } else if (event.data.action == "odec.save.click") {
      if (event.data.id != null && this.artworkEditorAction == "new") {
          if (this.product != null && this.product.ProductId > 0) {
            this.Entity.ID = event.data.id;
            this.Entity.ProductID = this.product.ProductId;
            this.Entity.IsActive = true;
            this.znodeService.setProductArtwork(this.Entity).subscribe(
                res => console.log(res),
                err => this.alertService.error(err)
            );
          } else {
              this.Entity.ID = event.data.id;
              this.product.ArtworkID = event.data.id;
          }
      }
      this.PreviewUrl = this.getArtworkPreviewURL();
      this.modalService.close();
      this.OpenModalButtonText = "Edit Design";
    }
  }

  setRequireCustomization(value){
    this.Entity.RequireCustomization = value;
    let isNew = this.Entity.ID == undefined || this.Entity.ID == '00000000-0000-0000-0000-000000000000';

    if(isNew) {
        this.znodeService.setProductArtwork(this.Entity).subscribe(
            res => console.log(res),
            err => this.alertService.error(err)
        );
    } else {
        this.znodeService.updateProductArtwork(this.Entity).subscribe(
            res => console.log(res),
            err => this.alertService.error(err)
        );
    }

  }
}
