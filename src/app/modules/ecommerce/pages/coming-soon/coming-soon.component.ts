import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'app-coming-soon',
  templateUrl: './coming-soon.component.html'
})
export class ComingSoonComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Coming Soon'
    }
  ];

  constructor(private router: Router, private authService: AuthService) {}

  ngOnInit() {
    if(this.authService.isLoggedIn()){
      this.router.navigate(['/ecommerce/storefronts']);
    }
  }

}
