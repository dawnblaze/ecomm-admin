import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

import { CSS } from '../../models/css';

@Component({
  selector: 'app-style-details',
  templateUrl: './style-details.component.html'
})
export class StyleDetailsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Styles',
      path: '/ecommerce/styles/'
    }
  ];

  public css: CSS;
  private unchanged: string;
  private title: string;
  private loading: boolean = false;

  constructor(private route: ActivatedRoute, private router: Router, private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService, private themeService: ThemeService) {}

	ngOnInit() {
    var CSSID: number;

    this.route.params.forEach((params: Params) => {
      CSSID = +params['id'];
    });

    this.znodeService.getCss(CSSID).subscribe(
      stylesheet => {
        this.css = stylesheet;
        this.unchanged = JSON.stringify(this.css);

        this.title = this.css.Name;
        this.breadcrumbs.push({title: this.title, path:"/ecommerce/styles/" + this.css.CSSID});
      },
      err => this.alertService.error(err)
    );
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.css) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  private cancel() {
    this.router.navigate(['/ecommerce/styles']);
  }

  private save() {
    this.loading = true;
    this.znodeService.updateCss(1, this.css).subscribe(
      data => {

        this.css = data;
        this.unchanged = JSON.stringify(this.css);

        this.loading = false;
      },
      err => this.alertService.error(err)
    );
  }

  private setHexCode(code: string, keys: string[]) {
    for (var key in keys) {
      this.css.StyleDictionary[keys[key]] = code;
    }
  }

}
