import {
  Component,
  AfterViewInit,
  ViewChild,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";

import { untilDestroyed } from "ngx-take-until-destroy";
import { Observable, forkJoin } from "rxjs";
import { take } from 'rxjs/operators';
import "../../../../rxjs-extensions";

import { ZnodeService } from "../../../../services/znode.service";
import { ZnodeApiService } from "../../../../services/znode-api.service";
import { AlertService } from "../../../../services/alert.service";
import { ModalService } from "../../../../services/modal.service";
import { ThemeService } from "../../../../services/theme.service";

import { CategoryProfileListComponent } from "../../components/category-profile-list/category-profile-list.component";
import { RowAction } from "../../../corebridge/components/table2/table.component";

import { Catalog } from "../../models/catalog";
import { Category } from "../../models/category";
import { CategoryNode } from "../../models/category-node";
import { Product } from "../../models/product";
import { Theme } from "../../models/theme";
import { CSS } from "../../models/css";

@Component({
  selector: "cb-category-node-details",
  templateUrl: "./category-node-details.component.html"
})
export class CategoryNodeDetailsComponent implements AfterViewInit, OnDestroy {
  @Input() categoryNodeId: number;
  @Input() catalogId: number;
  @Input() isNew: boolean;
  @Output() onSave: EventEmitter<any> = new EventEmitter();
  @Output() onExit: EventEmitter<any> = new EventEmitter();
  @Output() onLoad: EventEmitter<string> = new EventEmitter();

  @ViewChild(CategoryProfileListComponent)
  public categoryProfileList: CategoryProfileListComponent;

  public categories: Category[];
  public parentCategory: Category;
  public parentNodes: CategoryNode[];
  public node: CategoryNode;
  private nodebase: string;
  private unchanged: string;
  private unchangedCategoryProfiles: string;
  public isProfileListChanged: boolean;
  public isActive: boolean = true;

  public saving: boolean = false;
  public loading: boolean = true;
  public displayDialog: boolean = true;
  public selectColor: string = "#e3f0c9";
  public recordActions: RowAction[] = [RowAction.Activate, RowAction.Remove];

  public categoryProducts: Product[] = [];
  public productColumns = [
    { field: "Thumbnail", header: "Thumbnail" },
    { field: "Name", header: "Name" },
    { field: "RetailPrice", header: "Retail Price", currency: "USD" }
  ];

  constructor(
    private znodeService: ZnodeService,
    private znodeApiService: ZnodeApiService,
    private alertService: AlertService,
    private modalService: ModalService
  ) {}

  ngAfterViewInit() {
    this.loading = true;

    if (this.isNew) {
      this.node = new CategoryNode();
      this.znodeService
        .getCatalog(this.catalogId)
        .pipe(untilDestroyed(this))
        .subscribe(
          catalog => {
            this.node.Catalog = catalog;
            this.node.CatalogId = catalog.CatalogId;
            this.setNode(this.node);
            forkJoin(
              this.znodeService.getCategories(),
              this.znodeService.getCatalogCategoryNodes(this.node.CatalogId)
            )
              .pipe(untilDestroyed(this))
              .subscribe(
                data => {
                  this.categories = data[0];
                  this.parentNodes = data[1];
                  this.setNodeSelect();
                },
                err => this.showAlertAfterLoad(err)
              );

            this.unchanged = JSON.stringify(this.node);
          },
          error => this.showAlertAfterLoad(error)
        );
    } else {
      forkJoin(
        this.znodeService.getCategoryNode(this.categoryNodeId),
        this.znodeService.getCatalog(this.catalogId),
        this.znodeService.getCategories(),
        this.znodeService.getCatalogCategoryNodes(this.catalogId)
      )
        .pipe(untilDestroyed(this))
        .subscribe(
          data => {
            this.setNode(data[0]);
            this.node.Catalog = data[1];
            this.categories = data[2];
            this.parentNodes = data[3];
            this.setNodeSelect();
            this.isActive = this.node.IsActive;
          },
          err => this.showAlertAfterLoad(err)
        );
    }
    this.selectColor = document
      .querySelector("body")
      .className.includes("misType-1")
      ? "#cae1ed"
      : "#e3f0c9";
  }

  public canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.node)) {
      return true;
    }

    // return this.modalService.warning(
    //   "You Have Unsaved Changes",
    //   "Leaving without saving will discard any changes you have made. Are you sure you want to leave?",
    //   "Discard and Leave",
    //   "Stay"
    // );
  }

  private setNodeSelect() {
    let root = JSON.parse(this.unchanged);
    root.CategoryNodeId = -1;
    root["DisplayText"] = "No Parent - Root Level";
    this.categories.forEach(category => {
      category["DisplayText"] = category.SeoUrl || category.Name;
      let parentNode = this.parentNodes.find(
        node => node.CategoryId == category.CategoryId
      );
      if (parentNode) parentNode["DisplayText"] = category["DisplayText"];
    });
    this.parentNodes = [root].concat(this.parentNodes);

    // Filter out "used" categories from the list
    this.categories = this.categories.filter(category => {
      if (!this.isNew && category.CategoryId == this.node.CategoryId) {
        return true;
      } else if (
        this.parentNodes.filter(node => node.CategoryId == category.CategoryId)
          .length > 0
      ) {
        return false;
      } else {
        return true;
      }
    });

    if (this.isNew) {
      this.selectCategory(this.categories[0]);
    } else {
      this.selectCategory(
        this.categories.find(c => c.CategoryId == this.node.CategoryId)
      );
      this.onLoad.emit(this.node.Category.Name);
    }
    if (!this.node.ParentCategoryNodeId) {
      this.node.ParentCategoryNodeId = this.parentNodes[0].CategoryNodeId;
    }

    this.unchanged = JSON.stringify(this.node);
  }

  private setNode(node: CategoryNode) {
    this.node = node;
    this.nodebase = JSON.stringify(this.node);

    if (this.node.ParentCategoryNodeId == null) {
      this.node.ParentCategoryNodeId = 0;
    }
    if (this.node.ThemeId == null) {
      this.node.ThemeId = 0;
    }
    if (this.node.CssId == null) {
      this.node.CssId = 0;
    }
    if(!this.isNew) {
      this.categoryProfileList.initCategory(node.CategoryId);
    }
    this.unchanged = JSON.stringify(this.node);
  }

  public save() {
    this.node.CategoryId = this.node.Category.CategoryId;

    if (this.node.ParentCategoryNodeId <= 0) {
      this.node.ParentCategoryNodeId = null;
    } else if (!this.isNew && this.node.ParentCategoryNodeId != null) {
      // parent cannot be a child of it's children
      let nodes = this.parentNodes.filter(
        node => node.CategoryNodeId == this.node.ParentCategoryNodeId
      );
      let node = nodes[0];

      if (node.ParentCategoryNodeId == this.node.CategoryNodeId) {
        this.alertService.error(
          "Parent Collection cannot be a child of selected Collection"
        );
        return false;
      }

      // category cannot be it's own parent
      if (this.node.CategoryNodeId == this.node.ParentCategoryNodeId) {
        this.alertService.error(
          "Collection cannot be it's own Parent Collection"
        );
        return false;
      }
    }
    if (this.node.ThemeId == 0) {
      this.node.ThemeId = null;
    }
    if (this.node.CssId == 0) {
      this.node.CssId = null;
    }

    this.loading = true;
    if (this.isNew) {
      this.znodeService
        .createCategoryNode(this.node)
        .pipe(untilDestroyed(this))
        .subscribe(
          res => this.saveSuccess(),
          err => this.showAlertAfterLoad(err)
        );
    } else {
      this.znodeService
        .updateCategoryNode(this.node)
        .pipe(untilDestroyed(this))
        .subscribe(
          res => {
            this.saveSuccess();
          },
          err => this.showAlertAfterLoad(err)
        );
    }
  }

  public saveSuccess() {
    this.loading = false;
    if(this.isNew ||  this.categoryProfileList.save()) {
      this.alertService.success("Saved!");
    }
    this.unchanged = JSON.stringify(this.node);
    this.isProfileListChanged = false;
    this.onSave.emit();
    this.close();
  }

  public showAlertAfterLoad(error: string) {
    this.loading = false;
    this.alertService.error(error);
  }

  public close(doSave: boolean = false) {
    if (doSave) {
      this.save();
    } else {
      let oldVal = this.unchanged.replace(/['"]+/g, '');
      let newVal = (JSON.stringify(this.node)).replace(/['"]+/g, '')
      if (oldVal != newVal || this.isProfileListChanged) {
        this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay').subscribe(res => {
          if (res) {
            this.displayDialog = false;
            setTimeout(() => {
              this.onExit.emit();
            }, 500);
          }
        })
      } else {
        this.displayDialog = false;
        setTimeout(() => {
          this.onExit.emit();
        }, 500);
      }
    }
  }

  selectCategory(category: Category) {
    if (category) {
      this.node.Category = category;
      this.node.CategoryId = category.CategoryId;
      this.categoryProducts = [];
      this.znodeApiService
        .getProductsByCategory(this.node.CategoryId)
        .pipe(untilDestroyed(this))
        .subscribe(
          res => {
            this.categoryProducts = res.data;
            this.categoryProducts.forEach(
              p => (p["Thumbnail"] = p.ArtworkThumbnailUri || p.ImageSmallPath)
            );
            setTimeout(() => { this.loading = false; }, 1000);
          },
          err => this.showAlertAfterLoad(err)
        );
    }
  }
  selectParent(node: CategoryNode) {
    this.node.ParentCategoryNodeId = node.CategoryNodeId;
  }

  toggleActive() {
    let node = JSON.parse(this.nodebase);
    node.IsActive = !this.node.IsActive;
    this.isActive = node.IsActive;
    this.znodeService
      .updateCategoryNode(node)
      .pipe(untilDestroyed(this))
      .subscribe(
        res => {
          this.node.IsActive = res.IsActive;
          this.onSave.emit();
        },
        err => this.alertService.error(err)
      );
  }

  remove() {
    var title =
      "Please confirm you would like to delete the selected Associated Collection. This change cannot be undone.";
    var content = "Deletion will permanently remove the Associated Collection.";
    this.modalService.warning(title, content);

    this.modalService.response.pipe(take(1)).subscribe(res => {
      if (!res) return;
      this.loading = true;
      this.znodeService
        .getCatalogCategoryNodes()
        .pipe(untilDestroyed(this))
        .subscribe(
          nodes => {
            var canDelete = true;
            nodes.forEach(categoryNode => {
              if (
                categoryNode.ParentCategoryNodeId == this.node.CategoryNodeId
              ) {
                canDelete = false;
              }
            });

            if (canDelete) {
              this.znodeService
                .deleteCategoryNode(this.node.CategoryNodeId)
                .pipe(untilDestroyed(this))
                .subscribe(
                  res => {
                    this.alertService.success(
                      "Collection Removed from Catalog"
                    );
                    this.loading = false;
                    this.onSave.emit();
                    this.close();
                  },
                  err => {
                    this.alertService.error(err);
                    this.close();
                  }
                );
            } else {
              this.alertService.error(
                "Cannot Remove Collection with Child Collections"
              );
              this.close();
            }
          },
          err => this.alertService.error(err)
        );
    });
  }

  checkProfileListChanges(status) {
    this.isProfileListChanged = status;
  }

  ngOnDestroy() {}
}
