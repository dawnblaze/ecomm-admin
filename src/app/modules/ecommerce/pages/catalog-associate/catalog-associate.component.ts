import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { Catalog } from '../../models/catalog';
import { Category } from '../../models/category';
import { CategoryNode } from '../../models/category-node';
import { Theme } from '../../models/theme';
import { CSS } from '../../models/css';

@Component({
  selector: 'cp-catalog-associate',
  templateUrl: './catalog-associate.component.html'
})
export class CatalogAssociateComponent implements OnInit {

  @Output() onExit: EventEmitter<any> = new EventEmitter();

  public categories: Category[];
  public parentCategory: Category;
  public parentNodes: CategoryNode[];
  public node: CategoryNode = new CategoryNode();
  private unchanged: string;

  constructor( private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService, private route: ActivatedRoute, private location: Location, private router: Router ) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];

      this.znodeService.getCatalog(id).subscribe(catalog => {
        this.node.Catalog = catalog;
        this.node.CatalogId = catalog.CatalogId;
        this.getCategories();

        this.unchanged = JSON.stringify(this.node);
      },
      error => {
        console.error(error);
        this.alertService.error(error);
      });
    });
  }

  public canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.node) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  private getCategories() {
    forkJoin(
      this.znodeService.getCategories(),
      this.znodeService.getCatalogCategoryNodes(this.node.CatalogId)
    ).subscribe(
      data => {
        this.categories = data[0];
        this.parentNodes = data[1];
        this.parentNodes.forEach(node => {
          let categories: Category[] = this.categories.filter(cat => cat.CategoryId==node.CategoryId);
          node.Name = categories[0].Name; 
        });
        this.node.ParentCategoryNodeId = 0;

        // Filter out "used" categories from the list
        this.categories = this.categories.filter((category) => {
          if ( (this.parentNodes.filter(node => node.CategoryId == category.CategoryId)).length > 0) {
            return false;
          } else {
            return true;
          }
        });

        this.node.Category = this.categories[0];
        this.node.CategoryId = this.categories[0].CategoryId;
        this.unchanged = JSON.stringify(this.node);
      },
      err => this.alertService.error(err)
    );
  }

  public save(){
    this.node.CategoryId = this.node.Category.CategoryId;

    this.znodeService.createCategoryNode(this.node).subscribe( res => {
      this.node = res;
      this.unchanged = JSON.stringify(this.node);

      this.alertService.success('Saved!');
      this.onExit.emit();
    },
    err => this.alertService.error(err))

  }

  public cancel(){
    this.onExit.emit();
  }
}
