import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { FluteDirection } from '../../models/flute-direction';
import { ShippingBox } from '../../models/shipping-box';
import { Profile } from '../../models/profile';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'cb-shipping-box-new',
  templateUrl: './shipping-box-new.component.html'
})
export class ShippingBoxNewComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Shipping Boxes',
      path: '/ecommerce/shipping-boxes'
    },
    {
      title: 'New'
    }
  ];

  public loading: boolean = true;
  public shippingBox: ShippingBox = new ShippingBox();
  private unchanged: string;
  public profiles: Profile[];
  public fluteDirections: FluteDirection[];

  constructor(private znodeService: ZnodeService
    , private alertService: AlertService
    , private modalService: ModalService
    , private router: Router
    , private themeService: ThemeService) { }

  ngOnInit() {
    forkJoin(
      this.znodeService.getProfiles(),
      this.znodeService.getFluteDirections()
    ).subscribe(
      data => {
        this.profiles = data[0];
        this.fluteDirections = data[1];
        this.unchanged = JSON.stringify(this.shippingBox);
        this.onLoaded();
      },
      err => this.alertService.error(err)
    );
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.shippingBox) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes'
    , 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  onLoaded(){
    this.loading = false;
  }

  public save() {
    this.loading = true;
    this.znodeService.createShippingBox(this.shippingBox).subscribe(
      data => {
        this.shippingBox = data;
        this.unchanged = JSON.stringify(this.shippingBox);

        this.alertService.success('Shipping Box Created Successfully');
        this.loading = false;
        this.cancel();
      },
      err => this.alertService.error(err)
    );
  }

  public cancel() {
    this.router.navigate(['/ecommerce/shipping-boxes']);
  }
}
