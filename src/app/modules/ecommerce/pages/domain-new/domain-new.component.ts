import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { Observable, forkJoin } from 'rxjs';

import { AlertService } from '../../../../services/alert.service';
import { ZnodeService } from '../../../../services/znode.service';

import { Domain } from '../../models/domain';
import { Portal } from '../../models/portal';

@Component({
  selector: 'cb-domain-new',
  templateUrl: './domain-new.component.html'
})
export class DomainNewComponent implements OnInit {

  @Input() portal: Portal;
  @Output() onExit: EventEmitter<any> = new EventEmitter();
  
  public domain: Domain = new Domain();
  public loading: boolean = false;

  constructor(private alertService: AlertService, private znodeService: ZnodeService) {}

  ngOnInit() {
    this.domain.PortalId = this.portal.PortalId;
  }

  save() {
    this.loading = true;
    this.domain.ApiKey = this.guid();
    this.znodeService.createDomain(this.domain).subscribe(
      res => {
        console.log(res);
        this.loading = false;
        this.alertService.success('Domain Saved');
        this.cancel();
      },
      err => {
        this.loading = false;
        this.alertService.error(err);
      }
    );
  }

  cancel() {
    this.onExit.emit();
  }

  S4() {
    return (((1+Math.random())*0x10000)|0).toString(16).substring(1); 
  }

  guid() {
    return (this.S4() + this.S4() + "-" + this.S4() + "-4" + this.S4().substr(0,3) + "-" + this.S4() + "-" + this.S4() + this.S4() + this.S4()).toLowerCase();
  }
}
