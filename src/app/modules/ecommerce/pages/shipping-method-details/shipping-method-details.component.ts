import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, forkJoin } from 'rxjs';

import { ShippingMethod } from '../../models/shipping-method';
import { ShippingRule } from '../../models/shipping-rule';
import { Profile } from '../../models/profile';
import { Country } from '../../models/country';
import { ShippingCarrier } from '../../models/shipping-carrier';
import { ShippingInsuranceType } from '../../models/shipping-insurance';
import { ShippingCarrierServiceType } from '../../models/shipping-carrier-service-type';
import { ShippingCarrierRateRule} from '../../models/shipping-carrier-rate-rule';

import { CesaService, MisTypes } from '../../../../services/cesa.service';
import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';
import { DomainService } from '../../../../services/domain.service';

import { TabsComponent } from '../../../corebridge/components/tabs/tabs.component';

@Component({
  selector: 'cb-shipping-method-details',
  templateUrl: './shipping-method-details.component.html'
})
export class ShippingMethodDetailsComponent implements OnInit {

  public shippingPath = '/ecommerce/shipping-methods/';
  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Shipping Methods',
      path: this.shippingPath
    }
  ];

  @ViewChild(TabsComponent) private tabs: TabsComponent;

  public loading: boolean = true;
  public saving: boolean = false;
  public misType: number;
  public ShippingMethod: ShippingMethod = new ShippingMethod();
  private unchanged: string;
  public profiles: Profile[];
  public countries: Country[];
  public shippingCarriers: ShippingCarrier[];
  public shippingInsurances: ShippingInsuranceType[];
  public shippingCarrierServiceTypes: ShippingCarrierServiceType[];
  public shippingCarrierRateRule: ShippingCarrierRateRule;
  public title: string;
  public flatRateShippingRule: ShippingRule = new ShippingRule();
  public fixedRateShippingRule: ShippingRule = new ShippingRule();
  public quantityRateShippingRule: ShippingRule = new ShippingRule();
  public weightRateShippingRule: ShippingRule = new ShippingRule();

  public selectedCarrier: number;
  public isCarrierRateEnabled: boolean = true;

  // Validations
  public shipCodeUnique: boolean = true;
  public shipCodeInvalid: boolean = false;

  private routerSub;

  constructor(private znodeService: ZnodeService, private domain: DomainService, private alertService: AlertService, private modalService: ModalService, private route: ActivatedRoute, private router: Router, private location: Location, private themeService: ThemeService, private cesaService: CesaService) {
     this.routerSub = router.events.subscribe(path => {
      // Since we use location.go() to navigate inside of the component and location.go() doesn't update the router with the new path,
      // we need to capture router path changes and handle them inside of the component.
      if (!this.location.isCurrentPathEqualTo(router.routerState.snapshot.url)) {
        this.setOpenTab();
      }
    });
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      var ShippingMethodId = +params['id'];

      this.misType = this.cesaService.getMisType();
      this.selectedCarrier = 0;

      forkJoin(
        this.znodeService.getShippingMethod(ShippingMethodId),
        this.znodeService.getProfiles(),
        this.znodeService.getCountries(),
        this.znodeService.getShippingRules(ShippingMethodId),
        this.znodeService.getShippingCarriers(),
        this.znodeService.getShippingInsurance(),
        this.znodeService.getShippingCarrierRateRuleByShippingID(ShippingMethodId),
        this.znodeService.getShippingRuleTypes(),
        this.znodeService.getConfigSettingByBID(this.domain.bid)
      ).subscribe(
        data => {
          this.ShippingMethod = data[0];

          this.isCarrierRateEnabled = data[8].EnableCarrierRate;

          this.profiles = data[1];

          if(this.ShippingMethod.ProfileId == null){
            this.ShippingMethod.ProfileId = -1;
          }

          if(this.ShippingMethod.CountryCode == null){
            this.ShippingMethod.CountryCode = 0;
          }

          this.countries = data[2];
          this.title = this.ShippingMethod.Description;

          this.flatRateShippingRule = data[3].filter(rule => rule.ShippingRuleTypeId == 0)[0];
          this.quantityRateShippingRule = data[3].filter(rule => rule.ShippingRuleTypeId == 1)[0];
          this.weightRateShippingRule = data[3].filter(rule => rule.ShippingRuleTypeId == 2)[0];
          this.fixedRateShippingRule = data[3].filter(rule => rule.ShippingRuleTypeId == 3)[0];

          // shipping carriers
          this.shippingCarriers = data[4];
          // shipping insurances
          this.shippingInsurances = data[5];
          // shipping carrier rate rule
          this.shippingCarrierRateRule = this.applyCarrierRateRuleDefaultValues(data[6]);
          // filter and set a reference for Carrier Rate Shipping Rule as a reference for the CarrierRateRule.ShippingRuleTypeId
          this.shippingCarrierRateRule.ShippingRuleTypeID = data[7].filter(
                shippingRule => shippingRule.Name === 'Carrier Rate')[0].ShippingRuleTypeId;

          if (this.shippingCarrierRateRule.ShippingCarrierID > 0) {
            this.getCarrierServiceTypeByCarrier(this.shippingCarrierRateRule.ShippingCarrierID);
          }

          this.unchanged = this.getSerialized();

          this.shippingPath = this.shippingPath+this.ShippingMethod.ShippingId+'/'
          let crumb = {title: this.ShippingMethod.Description, path:this.shippingPath};
          this.breadcrumbs.push(crumb);

          this.onLoaded();
        },
        err => this.alertService.error(err)
      );

    });
  }

  applyCarrierRateRuleDefaultValues(carrierRateRule: ShippingCarrierRateRule) {
    for (let key in carrierRateRule) {
      if (carrierRateRule.hasOwnProperty(key)) {
        let carrierRateItem = carrierRateRule[key];
        // this allows the select box to default to select value if current item is null
        carrierRateRule[key] = carrierRateItem ? carrierRateItem : '';
      }
    }

    return carrierRateRule;
  }

  private setOpenTab() {
    this.route.params.forEach((params: Params) => {
      let tab = params['tab'];
      let url = this.location.path();
      if(tab){
        this.tabs.openTab(tab);
        this.changedTab(tab);
      } else {
        this.tabs.openTab("general");
        this.changedTab("general");
      }
    });
  }

  private getSerialized(): string {
    var serialized = JSON.stringify(this.ShippingMethod);
    serialized += JSON.stringify(this.flatRateShippingRule);
    serialized += JSON.stringify(this.quantityRateShippingRule);
    serialized += JSON.stringify(this.weightRateShippingRule);
    serialized += JSON.stringify(this.fixedRateShippingRule);

    return serialized;
  }

  public changedTab(tabSlug: string){
    let tabPath: string = this.shippingPath+tabSlug;
    this.location.go(tabPath);

    while(this.breadcrumbs.length > 2){
      this.breadcrumbs.pop();
    }

    this.breadcrumbs.push({title: this.capitalizeFirst(tabSlug), path:tabPath});
  }

  private capitalizeFirst(str: string): string {
    str = str.replace('-', ' ');
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1);});
  }

  canDeactivate(): Observable<boolean> | boolean {
    var changed: string = this.getSerialized();

    if( this.unchanged == changed ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  onLoaded(){
    this.loading = false;
  }

  public save() {

    // If not validated don't allow save.
    if(!this.shipCodeUnique || this.shipCodeInvalid){
      return;
    }

    this.saving = true;

    forkJoin(
      this.znodeService.updateShippingMethod(this.ShippingMethod),
      this.znodeService.updateShippingRule(this.fixedRateShippingRule),
      this.znodeService.updateShippingRule(this.flatRateShippingRule),
      this.znodeService.updateShippingRule(this.quantityRateShippingRule),
      this.znodeService.updateShippingRule(this.weightRateShippingRule),
      this.znodeService.updateShippingCarrierRateRule(this.shippingCarrierRateRule)
    ).subscribe(
      data => {
        if(this.ShippingMethod.ProfileId == null){
          this.ShippingMethod.ProfileId = -1;
        }

        if(this.ShippingMethod.CountryCode == null){
          this.ShippingMethod.CountryCode = 0;
        }

        this.fixedRateShippingRule = data[1];
        this.flatRateShippingRule = data[2];
        this.quantityRateShippingRule = data[3];
        this.weightRateShippingRule = data[4];
        this.shippingCarrierRateRule = this.applyCarrierRateRuleDefaultValues(data[5]);

        this.unchanged = this.getSerialized();
        this.alertService.success('Shipping Method Updated Successfully');
        this.saving = false;
      },
      err => this.alertService.error(err)
    );
  }

  public cancel() {
    this.router.navigate(['/ecommerce/shipping-methods']);
  }

  ngOnDestroy() {
    if (this.routerSub != null) {
      this.routerSub.unsubscribe();
    }
  }

  isNumber(e){
    return (e != null && !isNaN(e) && e.toString() !== '');
  }

  public getCarrierServiceTypeByCarrier(carrierId: number) {
    if (carrierId > 0) {
      this.znodeService.getShippingCarrierServiceTypesByCarrierId(carrierId).subscribe(
        data => this.shippingCarrierServiceTypes = data,
        error => this.alertService.error(error)
      );
    }
  }

  
   /**
   * @method checkIfValidCharacters
   * @param stringToCheck target.value to test against.
   * @returns true/false
   */
  private checkIfValidCharacters(stringToCheck: string):boolean{
    if(!(/^[\w\d_.-]+$/.test(stringToCheck)) === true) {
      this.shipCodeInvalid = true;
      return true;
    }
    return false;
  }

  /**
   * @method checkIfUniqueValue
   * @param e e.target.value to check
   * 
   */
  public checkIfUniqueValue(e){
    
    // check Product Internal Code against other Values.
    console.log('Target: ', e.target.value)
    this.shipCodeUnique = true;
    this.shipCodeInvalid = false;

    if(!this.checkIfValidCharacters(e.target.value)){
      let shipCodeList = this.znodeService.getShippingMethods().subscribe((data: ShippingMethod[])=>{
        for(let sm of data){
          if(sm.ShippingCode === e.target.value){
            this.shipCodeUnique = false;
            return true;
          }
        }
      })
    }
  }

}
