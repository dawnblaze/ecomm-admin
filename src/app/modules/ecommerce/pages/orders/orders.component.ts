import { Component } from '@angular/core';

import { ThemeService } from '../../../../services/theme.service';

import { OrdersTableComponent } from '../../components/orders-table/orders-table.component';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html'
})
export class OrdersComponent {

  public loading: boolean = true;

  public breadcrumbs = [
    {
      title: 'Orders'
    }
  ];

  constructor(public themeService: ThemeService) {}

  onLoaded(){
    this.loading = false;
  }
}
