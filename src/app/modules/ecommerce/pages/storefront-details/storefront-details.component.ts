import { Component, AfterViewInit, ViewChild, OnDestroy, ElementRef, OnInit, ChangeDetectorRef } from "@angular/core";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { Location } from "@angular/common";
import { NgForm } from '@angular/forms';

import { Observable, forkJoin } from "rxjs";
import { mergeMap } from "rxjs/operators";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { untilDestroyed } from 'ngx-take-until-destroy';

import { ZnodeService } from "../../../../services/znode.service";
import { AlertService } from "../../../../services/alert.service";
import { ModalService } from "../../../../services/modal.service";
import { ThemeService } from "../../../../services/theme.service";
import { CesaService, MisTypes } from "../../../../services/cesa.service";
import { CoreBridgeService } from "../../../../services/corebridge.service";

import { Portal } from "../../models/portal";
import { Catalog } from "../../models/catalog";
import { PortalCatalog } from "../../models/portal-catalog";
import { Image } from "../../models/image";
import { CSS } from "../../models/css";
import { FooterDataModel } from "../../models/footer-data";
import { CurrencyType } from "../../models/currency-type";
import { ContentSection } from "../../models/content-section";
import { TabsComponent } from "../../../corebridge/components/tabs/tabs.component";
import { ContentPageDetailsComponent } from "../content-page-details";
import { ContentPageNewComponent } from "../content-page-new";
import { PortalProfileDetailsComponent } from "../portal-profile-details";
import { PortalProfileNewComponent } from "../portal-profile-new";
import { PaymentSettingsTableComponent } from "../../components/payment-settings-table/payment-settings-table.component";
import { OrderApprovalGroupComponent } from "../../components/order-approval-group/order-approval-group.component";
import { Profile } from "../../models/profile";
import { ControlStation } from "../../models/control-station";
import { isNullOrUndefined } from "util";
import { CesaStationMapping } from "../../models/cesa-station-mapping";
import { DomainService } from "../../../../services/domain.service";
import { DetailedError } from '../../../../modules/corebridge/models/alert';
import { AutoSaveBarState } from '../../../corebridge/components/autosave-bar/autosave-bar.component';

//Imported for the EnableFileUploads setting.
import { PortalSetting } from "../../models/portal-setting";

@Component({
  selector: "app-storefront-details",
  templateUrl: "./storefront-details.component.html",
  styleUrls: ["./storefront-details.component.less"]
})
export class StorefrontDetailsComponent implements OnInit, AfterViewInit, OnDestroy {
  private storePath: string = "/ecommerce/storefronts/";

  public breadcrumbs: any[] = [
    {
      title: "My Stores",
      path: this.storePath
    },
    {
      title: ""
    }
  ];

  @ViewChild(TabsComponent) public tabs: TabsComponent;
  @ViewChild(ContentPageNewComponent)
  private contentPageNewComponent: ContentPageNewComponent;
  @ViewChild(ContentPageDetailsComponent)
  private contentPageDetailsComponent: ContentPageDetailsComponent;
  @ViewChild(PortalProfileDetailsComponent)
  private portalProfileDetailsComponent: PortalProfileDetailsComponent;
  @ViewChild(PortalProfileNewComponent)
  private portalProfileNewComponent: PortalProfileNewComponent;
  @ViewChild(PaymentSettingsTableComponent)
  private paymentSettingsComponent: PaymentSettingsTableComponent;
  @ViewChild('approvals') approvals: OrderApprovalGroupComponent;

  @ViewChild('general') general: NgForm;
  @ViewChild('integration') integration: NgForm;
  @ViewChild('checkout') checkout: NgForm;
  @ViewChild('javascript') javascript: NgForm;

  public tabState: any = {
    domain: "list",
    content: "list",
    profiles: "list"
  };

  public portal: Portal;
  public unchanged: string;
  public unchangedCss: string;
  public unchangedContentSection: string;
  public catalogs: Catalog[];
  public contentSection: ContentSection;
  public contentSections: ContentSection[];
  public contentSectionsWhiteList = [
    "Home Page Banner",
    "Login Page Banner",
    "Cart Page Banner",
    "Home Page Lower Banner",
    "Catalog Page Lower Banner"
  ];

  public portalCatalog: PortalCatalog;
  public css: CSS;
  public footerDataModel = new FooterDataModel().footerModel;
  public title: string;
  public currencyTypes: CurrencyType[];
  public currency: CurrencyType;
  public profile: Profile;
  public loading: boolean = false;
  public misType: number;
  public misText: string;
  public misToolTipId: string;
  public enableOrderApprovals: boolean = false;

  private selectedObjectId: number = 0;

  //Start un-used variables
  private contentPageState: string = "list";
  private selectedContentPageId: number;
  private profileState: string = "list";
  private selectedProfileId: number;
  private domainState: string = "list";
  private selectedDomainId: number;
  private smtpFieldType: string = "text";
  //End un-used variables
  public imageRoot: string;
  public imageVersion: number = 0;
  public faviconVersion: number = 0;
  private routerSub;
  public orderStations: ControlStation[];
  public orderLineItemStations: ControlStation[];
  public selectedOrderStation: ControlStation;
  public selectedOrderLineItemStation: ControlStation;
  public cesaDefaultOrderStationMapping: CesaStationMapping;
  public cesaDefaultOrderLineItemStationMapping: CesaStationMapping;
  public orderStationId: number;
  public orderLineItemStationId: number;

  //Used for the EnableFileUploads setting.
  public portalSetting: PortalSetting = new PortalSetting();

  public showSupportedCreditCardTypes: boolean;

  /*
  Used to determine if the "Enable file uploads checkbox should be displayed.
  Value temporarily hard coded for dev purposes.
  */
  public fileManagementEnabled: boolean = false;
  public enableFileUploads: boolean;
  public ckeditorTermsAndConditionConfig = {
    toolbar: [{ name: "links", items: ["Link", "Unlink"] }],
    removePlugins: "elementspath",
    enterMode: 2
  };

  public onlyOnAccountPayment: boolean = false;

  public genericDomain: string = "";
  public customDomain: string = "";
  public showCheckoutTinyMCE: boolean = false;
  public showCheckoutError: boolean = false;

  public saveState: AutoSaveBarState = AutoSaveBarState.saved;
  public validationErrors: DetailedError[] = [];

  constructor(
    private domain: DomainService,
    private znodeService: ZnodeService,
    private alertService: AlertService,
    private modalService: ModalService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    public themeService: ThemeService,
    private cesaService: CesaService,
    private coreBridgeService: CoreBridgeService,
    private elementRef: ElementRef,
    private cdRef: ChangeDetectorRef
  ) {
    this.routerSub = router.events.subscribe(path => {
      // Since we use location.go() to navigate inside of the component and location.go() doesn't update the router with the new path,
      // we need to capture router path changes and handle them inside of the component.
      /*
      if (!this.location.isCurrentPathEqualTo(router.routerState.snapshot.url)) {
        var pattern = /storefronts\/[0-9]+/g;
        if( pattern.test(router.routerState.snapshot.url) ){
          // only set tabs if navigating to same page
          this.setOpenTab();
        }
      }
       */
    });
  }

  ngOnInit() {
    let portalId: number;
    this.imageRoot = this.znodeService.base;
    this.misType = this.cesaService.getMisType();
    this.route.params.forEach((params: Params) => {
      if (this.portal) {
        return;
      }

      let id = +params["id"];
      this.getPortal(id);
      portalId = id;

      //For: ROG-211 / ROG-222
      this.getIsFileManagementEnabled();
    });

    this.znodeService
      .getConfigSettingByBID(this.domain.bid)
      .subscribe(configSetting => {
        this.enableOrderApprovals = configSetting.EnableOrderApprovals;
      });

    if (this.misType == MisTypes.Cyrious) {
      this.misText = "Control";
      this.misToolTipId = "store_integration_toggle_control";

      //Load station mapping for Portal
      forkJoin(
        this.cesaService.getControlOrderStations(),
        this.cesaService.getControlOrderLineItemStations()
      ).subscribe(
        res => {
          let [orderStations, lineItemStations] = res;
          this.orderStations = orderStations;
          this.orderLineItemStations = lineItemStations;

          this.cesaService.getOrderStationMapping(portalId).subscribe(res => {
            this.cesaDefaultOrderStationMapping =
              res || new CesaStationMapping();
            this.selectedOrderStation = this.orderStations.filter(
              x => x.stationId == this.cesaDefaultOrderStationMapping.atomID
            )[0];
            this.orderStationId = this.selectedOrderStation ? this.selectedOrderStation.stationId : null;
          });

          this.cesaService
            .getOrderLineItemStationMapping(portalId)
            .subscribe(res => {
              this.cesaDefaultOrderLineItemStationMapping =
                res || new CesaStationMapping();
              this.selectedOrderLineItemStation = this.orderLineItemStations.filter(
                x =>
                  x.stationId ==
                  this.cesaDefaultOrderLineItemStationMapping.atomID
              )[0];
              this.orderLineItemStationId = this.selectedOrderLineItemStation ? this.selectedOrderLineItemStation.stationId : null;
            });
        },
        err => {
          this.alertService.error(err);
          this.loading = false;
        },
        () => (this.loading = false)
      );

      if (isNullOrUndefined(this.cesaDefaultOrderStationMapping))
        this.cesaDefaultOrderStationMapping = new CesaStationMapping();

      if (isNullOrUndefined(this.cesaDefaultOrderLineItemStationMapping))
        this.cesaDefaultOrderLineItemStationMapping = new CesaStationMapping();
    } else {
      this.misText = "CoreBridge";
      this.misToolTipId = "store_integration_toggle_corebridge";
    }

    this.loading = false;
  }

  ngAfterViewInit() {
    // p-inputmask label fix
    setTimeout(() => {
      const elements = document.querySelectorAll('p-inputmask');
      elements.forEach(el => {
        let parent = el;
        let label = el.nextElementSibling;
        label.remove();
        parent.appendChild(label);
      })
    }, 5000);
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  validate(values: any[]){
    this.validationErrors = [];
    if(this.general.valid && !this.showCheckoutError) {
      this.saveState = AutoSaveBarState.none;
      this.alertService.detailedError('general', this.validationErrors);
      // Sales Phone Number fix
      if (this.portal.SalesPhoneNumber) {
        let numArr = this.portal.SalesPhoneNumber.match(/\d/g);
        let numStr = ''
        if (numArr && numArr.length) numStr = numArr.join("");
        if (numStr.length < 10) {
          return;
        }
      }
      if (this.portal.CustomerServicePhoneNumber) {
        let numArr = this.portal.CustomerServicePhoneNumber.match(/\d/g);
        let numStr = ''
        if (numArr && numArr.length) numStr = numArr.join("");
        if (numStr.length < 10) {
          return;
        }
      }

      if(this.general.dirty || this.integration.dirty || this.checkout.dirty || this.javascript.dirty)
        this.save();
    } else if(this.general.dirty || this.general.valid == false) {
      this.saveState = AutoSaveBarState.invalid;
      this.elementRef.nativeElement.querySelectorAll('.ng-invalid:not(form)').forEach(element => {
        if(element.nextElementSibling || element.querySelector('.floating-label, .floating-label-empty')) {
          let label = element.nextElementSibling ? element.nextElementSibling.innerText.replace(' *','') : element.querySelector('.floating-label, .floating-label-empty').innerText.replace(' *','');
          let message = 'This field is required' + (label.includes('Email') ? ' and must be a valid email address.' : '.');
          if(label == 'Email Domains')
            message = 'This field is required and must have a valid domain format.';
          this.validationErrors.push({ ID: 0, Name: "", FieldName: label, ErrorMessage: message, TabSlug: 'general', TabTitle: 'Store Details',
            goToAction: () => {
              this.tabs.openTab('general');
              this.changedTab('general');
            }
          });
        }
      });
      this.alertService.detailedError('general', this.validationErrors);
    } 
  }

  validatePayment(paymentSaveState: AutoSaveBarState) {
    if(paymentSaveState == AutoSaveBarState.invalid) {
      this.saveState = AutoSaveBarState.invalid;
      if(!this.showCheckoutError) {
        this.alertService.detailedError('checkout', [{ ID: 0, Name: "", FieldName: "Payment Types",
          ErrorMessage: "When Approvals are enabled, only the “On Account” payment type can be enabled",
          TabSlug: 'checkout', TabTitle: 'Checkout',
          goToAction: () => {
            this.tabs.openTab('checkout');
            this.changedTab('checkout');
          }
        }]);
        this.showCheckoutError = true;
      }
    } else if(this.showCheckoutError) {
      this.showCheckoutError = false;
      this.alertService.detailedError('checkout', []);
    }
    this.checkout.controls['paymentSettings'].setValue(this.showCheckoutError);
    this.checkout.form.markAsDirty();
  }

  validateApprovals(approvalErrors: DetailedError[]) {
    approvalErrors.forEach(e => {
      e.TabSlug = 'approvals';
      e.TabTitle = 'Approvals';
      e.goToAction = () => {
        this.tabs.openTab('approvals');
        this.changedTab('approvals');
      };
    });
    if(approvalErrors.length) {
      this.alertService.detailedError('approvals', approvalErrors);
      this.saveState = AutoSaveBarState.invalid;
    }
  }

  setStateApprovals(state: AutoSaveBarState) {
    if(state == AutoSaveBarState.saved && this.tabs.tabs.find(tab => tab.hasError)) {
      this.saveState = AutoSaveBarState.invalid;
    } else {
      this.saveState = state;
    }
  }

  canDeactivate(): Observable<boolean> | boolean {
    let canDeactivate: boolean = true;
    // converts unchanged back to object then set Default profile ids to new ids because we aren't updating it here and we are comparing old data
    let unchangedPortal = JSON.parse(this.unchanged);
    unchangedPortal.DefaultAnonymousProfileId = this.portal.DefaultAnonymousProfileId;
    unchangedPortal.DefaultRegisteredProfileId = this.portal.DefaultRegisteredProfileId;

    this.unchanged = JSON.stringify(unchangedPortal);

    // ROG-952 change. This is needed due to ckeditor changing null values to an empty string
    // thus resulting to old data and compared data not equal
    if (this.portal.PortalSetting &&
      this.isNullOrEmpty(this.portal.PortalSetting.CheckoutTermsCond) &&
      this.isNullOrEmpty(unchangedPortal.CheckoutTermsCond)
    ) {
      this.portal.PortalSetting.CheckoutTermsCond = unchangedPortal.PortalSetting.CheckoutTermsCond;
    }

    if (
      this.unchanged == JSON.stringify(this.portal) &&
      this.unchangedCss &&
      this.unchangedCss.replace(/"\s+|\s+"/g, '"') ==
        JSON.stringify(this.css).replace(/"\s+|\s+"/g, '"') &&
      this.unchangedContentSection == JSON.stringify(this.contentSection)
    ) {
      if (this.tabState.content != "list") {
        if (this.contentPageDetailsComponent.canDeactivate()) {
          return true;
        } else {
          canDeactivate = false;
        }
      }


      if (this.tabState.profiles == "detail") {
        return this.portalProfileDetailsComponent.canDeactivate();
      }

      if (this.tabState.profiles == "new") {
        return this.portalProfileNewComponent.canDeactivate();
      }

      if (canDeactivate) {
        return true;
      }
    }

    return this.modalService.warning(
      "You Have Unsaved Changes",
      "Leaving without saving will discard any changes you have made. Are you sure you want to leave?",
      "Discard and Leave",
      "Stay"
    );
  }

  private isNullOrEmpty(value: string): Boolean {
    return isNullOrUndefined(value) || value == "";
  }

  setOpenTab() {
    this.route.params.forEach((params: Params) => {
      let tab = params["tab"];
      if (!tab) {
        tab = "general";
      }
      this.tabs.openTab(tab);
      this.changedTab(tab);

      let selected = +params["selected"];
      if (selected > 0) {
        this.selectedObjectId = selected;
        this.tabState[tab] = "detail";
      }

      let create = params["new"];
      if (create) {
        this.tabState[tab] = "new";
      }
    });
  }

  //For: ROG-211 / ROG-222
  getIsFileManagementEnabled() {
    if (this.misType !== 1) {
      this.coreBridgeService.getIsFileManagementEnabled().subscribe(result => {
        this.fileManagementEnabled = result;
      });
    } else {
      this.fileManagementEnabled = true;
    }
  }

  getPortal(id: number) {
    this.znodeService.getPortal(id).subscribe(portal => {
      this.portal = portal;
      this.enableFileUploads = this.portal.PortalSetting && this.portal.PortalSetting.EnableFileUploads;

      this.znodeService.isCreditCardActive$.subscribe(isActive => {
        this.showSupportedCreditCardTypes = isActive;
      });

      this.title = this.portal.StoreName;
      this.breadcrumbs[this.breadcrumbs.length - 1] = {
        title: this.title,
        path: this.storePath + this.portal.PortalId
      };

      if (this.portal.SmtpUsername === "" || this.portal.SmtpUsername == null) {
        this.portal.SmtpUsername = "********";
        this.portal.SmtpPassword = "********";
      }

      this.setOpenTab();
      this.getCatalog();
      this.getCss();
      this.getCurrencyTypes();
      this.getContentSections();
      this.getDomains();
      this.znodeService
        .getPortalProfiles(this.portal.PortalId)
        .pipe(
          mergeMap(portalProfiles => {
            if (portalProfiles[0]) {
              return this.znodeService.getProfile(portalProfiles[0].ProfileID);
            }
          })
        )
        .subscribe(profile => {
          this.profile = profile;
          /** temporary while api is not yet working **/
          setTimeout(() => {
            this.showCheckoutTinyMCE = true;
          }, 1000)
          this.profile.OrderApprovalsEnabled = false;
        });
      this.unchanged = JSON.stringify(this.portal);

      [this.general, this.integration, this.checkout, this.javascript].forEach(ngForm =>{
        ngForm.form.valueChanges.pipe(debounceTime(2000),distinctUntilChanged(),untilDestroyed(this)).subscribe(values => {
          this.validate(values);
        });
      });
    });
  }

  getCatalog() {
    this.znodeService.getPortalCatalogs(this.portal.PortalId).subscribe(res => {
      if (res.length > 0) {
        this.portalCatalog = res[0];
        let catId: number = this.portalCatalog.CatalogId;

        this.znodeService.getCatalog(catId).subscribe(catalog => {
          catalog.IsDefault = true;
          this.portal.Catalogs[0] = catalog;
          this.unchanged = JSON.stringify(this.portal);
        });
      } else {
        this.portalCatalog = new PortalCatalog();
      }

      this.getCatalogs();
    });
  }

  getCss() {
    this.znodeService.getCss(this.portal.PortalId).subscribe(css => {
      this.css = css;
      this.unchangedCss = JSON.stringify(this.css);

      this.css.StyleDictionary["base-font-family"] = this.css.StyleDictionary[
        "base-font-family"
      ].trim();
    },
    err => { this.css = new CSS(); });
  }

  getCatalogs() {
    this.znodeService.getCatalogs().subscribe(res => {
      this.catalogs = res;
      if (this.portal.Catalogs.length > 0) {
        for (let catalog of this.catalogs) {
          if (this.portal.Catalogs[0].CatalogId == catalog.CatalogId) {
            this.portal.Catalogs[0] = catalog;
          }
        }
      } else {
        this.portal.Catalogs.push(this.catalogs[0]);
      }

      this.portal.Catalogs[0].IsDefault = true;
      this.unchanged = JSON.stringify(this.portal);
    });
  }

  getCurrencyTypes() {
    this.znodeService.getCurrencyTypes().subscribe(res => {
      this.currencyTypes = res;

      let currency = this.currencyTypes.filter(c => {
        return c.CurrencyTypeID == this.portal.CurrencyTypeId;
      });

      this.currency = currency[0];
    });
  }

  getContentSections() {
    this.znodeService.getContentSections(this.portal.PortalId).subscribe(
      res => {
        this.contentSections = res;
        this.contentSection = this.contentSections[0];
        this.unchangedContentSection = JSON.stringify(this.contentSection);
      },
      err => console.log(err)
    );
  }

  getDomains() {
    let endName = this.misType == 1 ? ".cyrious.com" : ".corebridge.net";
    this.znodeService.getPortalDomains(this.portal.PortalId).subscribe(res => {
      let portalDomain = res.find(
        d => d.WebAppType == 3 && d.DomainName.endsWith(endName)
      );
      this.genericDomain = portalDomain ? portalDomain.DomainName : "";
      portalDomain = res.find(
        d => d.WebAppType == 3 && !d.DomainName.endsWith(endName)
      );
      this.customDomain = portalDomain ? portalDomain.DomainName : "";
    });
  }

  save() {
    if (!this.portal.SalesPhoneNumber) {
      this.portal.SalesPhoneNumber = "";
    }

    if (!this.portal.SalesPhoneNumber) {
      return false;
    }

    this.saveState = AutoSaveBarState.saving;
    this.loading = true;

    if(this.checkout.dirty) {
      this.paymentSettingsComponent.save();
      let changed = this.elementRef.nativeElement.querySelectorAll('.ng-dirty:not(form):not(.payment-type-toggle)');
      if(!changed.length) {
        this.loading = false;
        this.saveState = AutoSaveBarState.saved;
        this.checkout.form.markAsPristine();
        return;
      }
    }

    this.znodeService
      .getCatalogCategoryNodes(this.portalCatalog.CatalogId)
      .subscribe(
        res => {
          if (res.length > 0) {
            this.updatePortal();
          } else {
            this.loading = false;
            this.alertService.error(
              "The catalog selected doesn't contain categories."
            );
            this.saveState = AutoSaveBarState.error;
          }
        },
        err => {
          this.alertService.error(err);
          this.saveState = AutoSaveBarState.error;
        }
      );

    if (this.misType == MisTypes.Cyrious) {
      //For default order/line item stations

      if (
        this.selectedOrderStation &&
        this.selectedOrderStation.stationId &&
        this.selectedOrderStation.stationName
      ) {
        this.cesaDefaultOrderStationMapping.atomID = this.selectedOrderStation.stationId;
        this.cesaDefaultOrderStationMapping.atomName = this.selectedOrderStation.stationName;
        this.cesaService
          .setOrderStationMapping(
            this.portal.PortalId,
            this.cesaDefaultOrderStationMapping
          )
          .subscribe(res => {}, err => this.alertService.error(err));
      } else {
        this.cesaService
          .setOrderStationMapping(this.portal.PortalId, null)
          .subscribe(res => {}, err => this.alertService.error(err));
      }

      if (
        this.selectedOrderLineItemStation &&
        this.selectedOrderLineItemStation.stationId &&
        this.selectedOrderLineItemStation.stationName
      ) {
        this.cesaDefaultOrderLineItemStationMapping.atomID = this.selectedOrderLineItemStation.stationId;
        this.cesaDefaultOrderLineItemStationMapping.atomName = this.selectedOrderLineItemStation.stationName;

        this.cesaService
          .setOrderLineItemStationMapping(
            this.portal.PortalId,
            this.cesaDefaultOrderLineItemStationMapping
          )
          .subscribe(res => {}, err => this.alertService.error(err));
      } else {
        this.cesaService
          .setOrderLineItemStationMapping(this.portal.PortalId, null)
          .subscribe(res => {}, err => this.alertService.error(err));
      }
    }
  }

  private updatePortal() {
    this.portalCatalog.Portal = this.portal;
    this.portalCatalog.PortalId = this.portal.PortalId;
    this.css.StyleDictionary["base-color-primary"] = this.css.StyleDictionary[
      "heading-text-color"
    ];
    this.css.StyleDictionary["checkbox-radio-color"] = this.css.StyleDictionary[
      "heading-text-color"
    ];
    this.css.StyleDictionary["required-star"] = this.css.StyleDictionary[
      "heading-text-color"
    ];

    // Temporary code not implementing Use Company ID
    if(this.portal.PortalSetting)
      this.portal.PortalSetting.EnableRegisterCompanyID = false;

    forkJoin(
      this.znodeService.updatePortal(this.portal),
      this.znodeService.updatePortalCatalog(this.portalCatalog),
      this.znodeService.updateCss(this.portal.PortalId, this.css),
      this.znodeService.updateProfile(this.profile)
    ).subscribe(
      data => {
        let portalDataFromResponse = data[0];
        this.portal = portalDataFromResponse;
        if (this.contentSection) {
          this.znodeService
            .updateContentSection(this.contentSection)
            .subscribe(data => {
              this.unchangedContentSection = JSON.stringify(
                this.contentSection
              );
            });
        }

        this.alertService.success("Storefront Updated Successfully");
        this.breadcrumbs[
          this.breadcrumbs.length - 1
        ].title = this.portal.StoreName;

        this.loading = false;

        if (this.portal.SmtpUsername) {
          this.portal.SmtpUsername = "********";
        }

        if (this.portal.SmtpPassword) {
          this.portal.SmtpPassword = "********";
        }

        this.unchanged = JSON.stringify(data[0]);

        this.saveState = AutoSaveBarState.saved;
        this.general.form.markAsPristine();
        this.integration.form.markAsPristine();
        this.checkout.form.markAsPristine();
        this.javascript.form.markAsPristine();
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
        this.saveState = AutoSaveBarState.error;
      }
    );
  }

  cancel() {
    this.router.navigate(["/ecommerce/storefronts"]);
  }

  private uploadStoreLogo(data: Image) {
    this.portal.LogoPath = data.ImagePath;
    this.imageVersion++;
  }

  private uploadStoreFavicon(data: Image) {
    if(this.portal.PortalSetting)
      this.portal.PortalSetting.FaviconPath = data.ImagePath;
    this.faviconVersion++;
  }

  private uploadDefaultProductImage(data: Image) {
    this.portal.ImageNotAvailablePath = data.ImagePath;
  }

  private viewContentPage(id: number) {
    this.selectedObjectId = id;
    this.tabState.content = 'detail';
    // this.location.go(this.storePath+this.portal.PortalId+'/store-info/'+id);
    this.contentPageDetailsComponent.showDialog(id);
  }

  private listContent() {
    const floating_toolbar = <HTMLScriptElement>document.querySelector('.tox-toolbar__overflow')
    if (floating_toolbar) {
      floating_toolbar.style.display = 'none';
    }
    this.tabState.content = "list";
    // this.location.go(this.storePath + this.portal.PortalId + "/store-info");
    // this.breadcrumbs.pop();
  }

  private addContent() {
    this.tabState.content = "new";
    // this.location.go(this.storePath + this.portal.PortalId + "/store-info/new");
    // this.breadcrumbs.push({title: 'Add'});
    this.contentPageDetailsComponent.showDialog();
  }

  private viewProfile(id: number) {
    this.selectedObjectId = id;
    this.tabState.profiles = "detail";
    this.location.go(this.storePath + this.portal.PortalId + "/profiles/" + id);
  }

  private listProfiles() {
    this.tabState.profiles = "list";
    this.location.go(this.storePath + this.portal.PortalId + "/profiles");
    // this.breadcrumbs.pop();
  }

  private addProfile() {
    this.tabState.profiles = "new";
    this.location.go(this.storePath + this.portal.PortalId + "/profiles/new");
    // this.breadcrumbs.push({title: 'Add'});
  }

  public changedTab(tabSlug: string) {
    for (var table in this.tabState) {
      this.tabState[table] = "list";
    }

    // let tabPath: string = this.storePath + this.portal.PortalId + "/" + tabSlug;
    // this.router.navigate(tabPath.split("/"));

    // while(this.breadcrumbs.length > 2){
    //   this.breadcrumbs.pop();
    // }

    // this.breadcrumbs.push({title: this.capitalizeFirst(tabSlug), path:tabPath});
  }

  private capitalizeFirst(str: string): string {
    str = str.replace("-", " ");
    return str.replace(/\w\S*/g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1);
    });
  }

  private testEmail() {
    this.znodeService.CheckEmailAccount().subscribe(
      res => {
        this.alertService.success("SMTP Settings Successfully Configured");
      },
      err => this.alertService.error(err)
    );
  }

  private setHexCode(code: string, keys: string[]) {
    for (var key in keys) {
      this.css.StyleDictionary[keys[key]] = code;
    }
  }

  ngOnDestroy() {
    if (this.routerSub != null) {
      this.routerSub.unsubscribe();
    }
    this.alertService.detailedError('general', []);
    this.alertService.detailedError('checkout', []);
  }

  public onCurrencyChange() {
    let currency = this.currencyTypes.filter(c => {
      return c.CurrencyTypeID == this.portal.CurrencyTypeId;
    });

    this.currency = currency[0];
    this.portal.CurrencySuffix = this.currency.CurrencySuffix;
  }

  enablePublicSignUp(isEnabled: boolean) {
    this.portal.AccountCreationDisabled = isEnabled;
    if (isEnabled && this.portal.PortalSetting) {
      this.portal.PortalSetting.EnableRegisterCompanyID = false;
      this.portal.PortalSetting.EnableRegisterRestrictDomain = false;
      this.portal.PortalSetting.RegisterAllowedDomains = this.portal.PortalSetting.RegisterAllowedDomains;
    }
  }

  onOrderApprovalChange(enable: boolean) {
    this.portal.PortalSetting.EnableOrderApproval = enable;
    this.znodeService.updatePortal(this.portal).subscribe(portal => {
      this.unchanged = JSON.stringify(this.portal);
    });
  }

  onBlur(event: any) {
    event.target.value = event.target.value.trim();
  }

  checkOnAccountOnly(value: boolean) {
    this.onlyOnAccountPayment = value;
    if (!value && this.portal.PortalSetting && this.portal.PortalSetting.EnableOrderApproval) {
      this.portal.PortalSetting.EnableOrderApproval = false;
      setTimeout(() => {
        this.onOrderApprovalChange(false);
        // this.save();
      }, 1000);
    }
  }

  onOrderSyncToggle(value: boolean) {
    this.profile.OrderSyncDisabled = !value;
    this.general.form.markAsDirty();
  }

  selectOrderStation(station: ControlStation){
    this.selectedOrderStation = station;
    this.orderStationId = station.stationId;
    this.general.form.markAsDirty();
  }
  selectOrderLineItemStation(station: ControlStation){
    this.selectedOrderLineItemStation = station;
    this.orderLineItemStationId = station.stationId;
    this.general.form.markAsDirty();
  }

  selectCatalog(catalog: Catalog) {
    this.portalCatalog.CatalogId = catalog.CatalogId;
    this.general.form.markAsDirty();
  }
}
