import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

import { EmailTemplate } from '../../models/email-template';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

@Component({
  selector: 'app-email-template-new',
  templateUrl: './email-template-new.component.html'
})
export class EmailTemplateNewComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Email Templates',
      path: '/ecommerce/email-templates'
    },
    {
      title: 'New'
    }
  ];

  public template: EmailTemplate = new EmailTemplate();
  private unchanged: string;
  public loading: boolean = false;

  constructor( private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService, private router: Router ) {}

  ngOnInit() {
    this.unchanged = JSON.stringify(this.template);
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.template) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  save(){
    this.loading = true;

    if (!this.template.TemplateName.includes('.htm')) {
      this.template.TemplateName += '.html';
    }

    this.znodeService.createEmailTemplate(this.template).subscribe(
      res => {
        this.template = res;
        this.unchanged = JSON.stringify(this.template);

        this.loading = false;
        this.cancel();
      },
      err => {
        this.loading = false;
        this.alertService.error(err);
      }
    );
  }

  cancel() {
    this.router.navigate(['/ecommerce/email-templates']);
  }

}
