import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'app-addons',
  templateUrl: './addons.component.html'
})
export class AddonsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Add-Ons'
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) { }

  ngOnInit() { }

  onLoaded(){
    console.log('loaded');
    this.loading = false;
    console.log(this.loading);
  }

}
