import { Component, OnInit, OnDestroy, ElementRef } from "@angular/core";
import { Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
import { NgForm } from "@angular/forms";

import { Observable, forkJoin } from "rxjs";

import { ZnodeService } from "../../../../services/znode.service";
import { CoreBridgeService } from "../../../../services/corebridge.service";
import { AlertService } from "../../../../services/alert.service";
import { ModalService } from "../../../../services/modal.service";
import { DomainService } from "../../../../services/domain.service";
import { ThemeService } from "../../../../services/theme.service";

import { Product } from "../../models/product";
import { IncomeAccount } from "../../models/income-account";
import { Sku } from "../../models/sku";
import { TaxClass } from "../../models/tax-class";
import { ShippingRuleType } from "../../models/shipping-rule-type";
import { Supplier } from "../../models/supplier";
import { Manufacturer } from "../../models/manufacturer";
import { Image } from "../../models/image";

import { ArtworkModule } from "../../artwork.module";

import { AutoSaveBarState } from "../../../corebridge/components/autosave-bar/autosave-bar.component";
import { CoreBridgeProductSettings } from "../../models/corebridge-product-settings";
import { CesaService, MisTypes } from "../../../../services/cesa.service";
import { ControlProductCategory } from "../../../cesa/models/control/control-product-category";
import { ControlProduct } from "../../../cesa/models/control/control-product";
import { CesaProductMapping } from "../../../cesa/models/cesa-product-mapping";
import { isNullOrUndefined } from "util";

//Imported for the EnableFileUploads setting.
import { ProductSetting } from "../../models/product-setting";

import { ConfigSetting } from "../../models/config-setting";
import { ATEService } from "../../../../services/ate.service";

@Component({
  selector: "app-product-new",
  templateUrl: "./product-new.component.html"
})
export class ProductNewComponent implements OnInit, OnDestroy {
  public breadcrumbs = [
    {
      title: "Products",
      path: "/ecommerce/merchandise/products"
    },
    {
      title: "New"
    }
  ];

  public product: Product = new Product();
  private unchanged: string;
  public title: string;
  public sku: Sku = new Sku();
  public taxClasses: TaxClass[];
  public shippingRuleTypes: ShippingRuleType[] = [];
  public suppliers: Supplier[];
  public manufacturers: Manufacturer[];
  public incomeAccounts: IncomeAccount[];
  public artwork: ArtworkModule;
  public productSettings: any;
  public configSetting: ConfigSetting;

  public misType: number;
  public controlCategories: ControlProductCategory[] = [];
  public controlProducts: ControlProduct[] = [];
  public selectedControlProduct: ControlProduct;
  public cesaProductMapping: CesaProductMapping = new CesaProductMapping();
  public controlVariablesToSet: string;

  public ipc_checkbox: boolean = false;
  public productNumInvalid: boolean = false;
  public productNumUnique: boolean = true;
  public seoUrlInvalid: boolean;
  public saveState: AutoSaveBarState = AutoSaveBarState.none;
  public saveAttempt: boolean = false;

  //Used for the EnableFileUploads setting.
  public productSetting: ProductSetting = new ProductSetting();

  /*
  Used to determine if the "Enable file uploads checkbox should be displayed.
  Value temporarily hard coded for dev purposes.
  */
  public fileManagementEnabled: boolean = false;

  public productInfo: any = {
    cb_integration_category: "basic"
  };

  public ecommCategories: { CategoryName: string; CategoryId: number }[];
  public quickProducts: any[] = [];

  public categories: any[];

  public loading: boolean = false;

  public skuInvalid: boolean = false;

  public imageRoot: string;

  public carrierRateId: number;

  private imageVersion: number = 0;

  public settings: any = {};

  constructor(
    private sanitizer: DomSanitizer,
    private znodeService: ZnodeService,
    private corebridgeService: CoreBridgeService,
    private domainService: DomainService,
    private alertService: AlertService,
    private modalService: ModalService,
    private router: Router,
    public themeService: ThemeService,
    public cesaService: CesaService,
    private ateService: ATEService,
    private elementRef: ElementRef
  ) {}

  ngOnInit() {
    this.imageRoot = this.znodeService.base;
    this.misType = this.cesaService.getMisType();

    //For: ROG-211 / ROG-222
    this.getIsFileManagementEnabled();

    forkJoin(
      this.znodeService.getTaxClasses(),
      this.znodeService.getSuppliers(),
      this.znodeService.getManufacturers(),
      this.znodeService.getShippingRuleTypes(),
      this.znodeService.getConfigSettingByBID(this.domainService.bid)
    ).subscribe(data => {
      let that = this;

      this.taxClasses = data[0];
      this.product.TaxClassId = this.taxClasses[0].TaxClassId;
      this.unchanged = JSON.stringify(this.product);

      this.suppliers = data[1];
      this.manufacturers = data[2];

      this.configSetting = Object.assign(new ConfigSetting(), data[4]);

      if (!this.configSetting.EnableOnlineTaxLookup) {
        this.taxClasses = this.taxClasses
          .filter(x => x.ExternalId == null)
          .filter(y => y.TaxClassId == 1 || y.TaxClassId == 2);
      } else if (
        this.configSetting.EnableOnlineTaxLookup &&
        this.misType == MisTypes.CoreBridge
      ) {
        this.taxClasses = this.taxClasses.filter(x => x.ExternalId != null);
      }

      // ROG-392: Add the option Carrier Rate to the dropdown immediately above the Flat Rate Per Item option.
      // This function moves the option Carrier Rate at the first index of the array.
      data[3].forEach(function(shippingRuleType) {
        if (shippingRuleType.Name == "Carrier Rate") {
          let index = data[3].indexOf(shippingRuleType);
          data[3].splice(index, 1);
          if (that.configSetting.EnableCarrierRate)
            data[3].unshift(shippingRuleType);

          // Create a reference for the carrier rate ID for checking required product attributes fields
          that.carrierRateId = shippingRuleType.ShippingRuleTypeId;
        }
      });

      this.shippingRuleTypes = data[3];
    });

    if (this.misType == MisTypes.Cyrious) {
      forkJoin(
        this.cesaService.getControlProductCategories(),
        this.cesaService.getControlProducts()
      ).subscribe(
        data => {
          let [categories, products] = data;
          this.controlCategories = categories;
          this.controlProducts = products;

          this.product.CoreBridgeQuickProductCategoryId = "";
          this.product.CoreBridgeQuickProductId = "";
          this.product.CoreBridgeProductCategoryId = "";
          this.product.CoreBridgeIncomeAccountId = "";
        },
        err => {
          this.alertService.error(err);
        }
      );
    }
    // else if(this.misType == MisTypes.CoreBridge) {
    else {
      forkJoin(
        this.corebridgeService.getEcommProductCategories(),
        this.corebridgeService.getProductCategories(),
        this.corebridgeService.getIncomeAccounts(),
        this.corebridgeService.getProductSettingsByZnodeProductID(-1)
      ).subscribe(data => {
        let [
          ecommCategories,
          categories,
          incomeAccounts,
          productSettings
        ] = data;
        this.ecommCategories = ecommCategories;
        this.categories = categories;
        this.incomeAccounts = incomeAccounts;
        this.productSettings = productSettings;

        this.product.CoreBridgeQuickProductCategoryId =
          this.productSettings.CoreBridgeQuickProductCategoryId || "";
        this.product.CoreBridgeQuickProductId =
          this.productSettings.CoreBridgeQuickProductId || "";
        this.product.CoreBridgeProductCategoryId =
          this.productSettings.CoreBridgeProductCategoryId || "";
        this.product.CoreBridgeIncomeAccountId =
          this.productSettings.CoreBridgeIncomeAccountId || "";

        if (this.productSettings.IntegrationTypeId == 2) {
          this.productInfo.cb_integration_category = "advanced";

          if (this.product.CoreBridgeQuickProductCategoryId != null) {
            this.getQuickProducts(
              this.product.CoreBridgeQuickProductCategoryId
            );
          }
        } else if (this.productSettings.IntegrationTypeId == 3) {
          this.productInfo.cb_integration_category = "reporting";
        } else {
          this.productInfo.cb_integration_category = "basic";
        }
      });
    }

    this.product.AttributeIds = "";

    // this.product.SkuId = null;
    this.product.IsActive = true;
    this.product.AllowBackOrder = true;
    this.artwork = new ArtworkModule(
      this.sanitizer,
      this.alertService,
      this.domainService,
      this.corebridgeService,
      this.modalService,
      this.znodeService,
      this.cesaService
    );
    this.artwork.OnProductLoad(this.product, null);
    this.product.ProductSetting = new ProductSetting();

    this.product.QuantityOnHand = 0;
    this.product.OutOfStockMessage = "Out of Stock";
    this.product.BackOrderMessage = "On Backorder";
    this.unchanged = JSON.stringify(this.product);
  }

  //For: ROG-211 / ROG-222
  getIsFileManagementEnabled() {
    if (this.misType !== 1) {
      this.corebridgeService.getIsFileManagementEnabled().subscribe(result => {
        this.fileManagementEnabled = result;
      });
    } else {
      this.fileManagementEnabled = true;
    }
  }

  ngOnDestroy() {
    if (this.artwork != null) this.artwork.Dispose();
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.product)) {
      return true;
    }
    return this.modalService.warning(
      "You Have Unsaved Changes",
      "Leaving without saving will discard any changes you have made. Are you sure you want to leave?",
      "Discard and Leave",
      "Stay"
    );
  }

  private getQuickProducts(categoryId: number) {
    this.quickProducts = [];

    if (categoryId > 0) {
      this.corebridgeService.getProductsByCategory(categoryId).subscribe(
        data => (this.quickProducts = data),
        err => this.alertService.error(err)
      );
    }
  }

  public save() {
    if (this.skuInvalid) {
      return;
    }

    if (!this.productNumUnique || this.productNumInvalid) {
      return;
    }

    this.loading = true;
    this.saveState = AutoSaveBarState.saving;

    if (!this.product.ProductNumber) {
      this.product.ProductNumber = this.product.SkuName;
    }

    // `this.product` value will be replaced after `createProduct`, previous product integration will be erased.
    // Get the previous product integration
    let oldProduct: Product = this.product;

    // EC-1471 - When creating a new product, populate SeoTitle with Product Title (Product.Name)
    this.product.SeoTitle = this.product.Name;
    this.product.SeoUrl = this.product.Name.toLowerCase().replace(
      /[^a-zA-Z0-9-]/g,
      "-"
    );
    // Remove multiple occurence of dash in the Url Handle
    // after conversion.
    do {
      this.product.SeoUrl = this.product.SeoUrl.replace(/-{2}/g, "-");
    } while (this.product.SeoUrl.indexOf("--") > 0);

    this.znodeService.createProduct(this.product).subscribe(
      res => {
        this.product = <Product>res;

        if (this.misType == MisTypes.Cyrious) {
          if (!isNullOrUndefined(this.selectedControlProduct)) {
            this.cesaProductMapping.eCommID = this.product.ProductId;
            this.cesaProductMapping.atomID = this.selectedControlProduct.productId;
            this.cesaProductMapping.atomName = this.selectedControlProduct.productName;
            this.cesaService
              .setProductMapping(this.cesaProductMapping)
              .subscribe();
          }

          this.updateProduct();
        }
        // else if(this.misType == MisTypes.CoreBridge){
        else {
          // Recover previous integration settings and apply it in the new `this.product`
          this.product.CoreBridgeQuickProductCategoryId =
            oldProduct.CoreBridgeQuickProductCategoryId;
          this.product.CoreBridgeQuickProductId =
            oldProduct.CoreBridgeQuickProductId;
          this.product.CoreBridgeProductCategoryId =
            oldProduct.CoreBridgeProductCategoryId;
          this.product.CoreBridgeIncomeAccountId =
            oldProduct.CoreBridgeIncomeAccountId;

          let settings: CoreBridgeProductSettings = new CoreBridgeProductSettings();
          settings.ZnodeProductId = this.product.ProductId;

          if (this.productInfo.cb_integration_category == "basic") {
            this.product.CoreBridgeQuickProductCategoryId = null;
            this.product.CoreBridgeQuickProductCategoryName = "";
            this.product.CoreBridgeQuickProductId = null;
            this.product.CoreBridgeQuickProductName = "";
            this.product.CoreBridgeProductCategoryId = null;
            this.product.CoreBridgeProductCategoryName = "";
            this.product.CoreBridgeIncomeAccountId = null;
            this.product.CoreBridgeIncomeAccountName = "";

            settings.IntegrationTypeId = 1;
          }

          if (this.productInfo.cb_integration_category == "advanced") {
            this.product.CoreBridgeProductCategoryId = null;
            this.product.CoreBridgeProductCategoryName = "";
            this.product.CoreBridgeIncomeAccountId = null;
            this.product.CoreBridgeIncomeAccountName = "";

            settings.IntegrationTypeId = 2;

            settings.CoreBridgeQuickProductCategoryId = null;
            settings.CoreBridgeQuickProductId = null;

            if (this.product.CoreBridgeQuickProductCategoryId > 0) {
              settings.CoreBridgeQuickProductCategoryId = this.product.CoreBridgeQuickProductCategoryId;
            }
            if (this.product.CoreBridgeQuickProductId > 0) {
              settings.CoreBridgeQuickProductId = this.product.CoreBridgeQuickProductId;
            }
          }

          if (this.productInfo.cb_integration_category == "reporting") {
            this.product.CoreBridgeQuickProductCategoryId = null;
            this.product.CoreBridgeQuickProductCategoryName = "";
            this.product.CoreBridgeQuickProductId = null;
            this.product.CoreBridgeQuickProductName = "";

            settings.IntegrationTypeId = 3;

            settings.CoreBridgeProductCategoryId = null;
            settings.CoreBridgeIncomeAccountId = null;

            if (this.product.CoreBridgeProductCategoryId > 0) {
              settings.CoreBridgeProductCategoryId = this.product.CoreBridgeProductCategoryId;
            }
            if (this.product.CoreBridgeIncomeAccountId > 0) {
              settings.CoreBridgeIncomeAccountId = this.product.CoreBridgeIncomeAccountId;
            }
          }

          this.settings = settings;

          this.corebridgeService
            .createOrUpdateProductSettings(settings)
            .subscribe(
              res => {
                this.product.ExternalId = res.Id;
                console.log(res);
                this.updateProduct();
              },
              err => {
                this.alertService.error(err);
                this.saveState = AutoSaveBarState.error;
              }
            );
        }
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
        this.saveState = AutoSaveBarState.error;
      }
    );
  }

  private updateProduct() {
    if (!this.productNumUnique || this.productNumInvalid) {
      return;
    }
    console.log(this.product);
    forkJoin(
      this.znodeService.updateProduct(this.product),
      this.znodeService.updateProductSettings(this.product)
    ).subscribe(
      data => {
        console.log(data);
        this.product = data[0];
        this.unchanged = JSON.stringify(this.product);

        this.alertService.success("Product Saved!");
        this.saveState = AutoSaveBarState.saved;

        this.loading = false;
        this.router.navigate([
          "/ecommerce/merchandise/products",
          this.product.ProductId
        ]);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
        this.saveState = AutoSaveBarState.error;
      }
    );
  }

  public cancel() {
    this.router.navigate(["/ecommerce/merchandise/products"]);
  }

  public truncateShortDescription() {
    this.product.ShortDescription = this.product.ShortDescription.substring(
      0,
      100
    );
  }

  private uploadProductImage(image: Image) {
    this.imageVersion++;
    this.product.ImageFile = image.ImagePath;
  }

  public isNumber(e) {
    return e != null && !isNaN(e) && e.toString() !== "";
  }

  public onBlur(event: any, variableName: string = "", isVar: boolean = false) {
    event.target.value = event.target.value.trim();
    if (variableName == "SeoUrl") {
      event.target.value = event.target.value.replace(/\s+/g, "-");
      if (event.target.value !== "") {
        forkJoin(
          this.znodeService.isSeoUrlExist(0, event.target.value)
        ).subscribe(
          data => {
            this["seoUrlInvalid"] = data[0].IsExist;
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          }
        );
      }
    }
    if (isVar) this[variableName] = !/^[\w\d_.-]+$/.test(event.target.value);
  }

  validate(event: any, form: NgForm) {
    this.alertService.detailedError(event.tab, event.errors);
    this.saveState =
      form.invalid && this.saveAttempt
        ? AutoSaveBarState.invalid
        : AutoSaveBarState.none;
  }

  focusInvalid() {
    let invalidControl = this.elementRef.nativeElement.querySelector(
      "input.ng-invalid"
    );
    if (invalidControl) invalidControl.focus();
    this.saveState = AutoSaveBarState.invalid;
    this.saveAttempt = true;
  }
}
