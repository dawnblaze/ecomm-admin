import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgForm } from "@angular/forms";
import { untilDestroyed } from "ngx-take-until-destroy";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

import { EditorPreset } from '../../models/editor-preset';
import { AlertService } from '../../../../services/alert.service';
import { CodaService } from '../../../../services/coda.service';
import { AutoSaveBarState } from "../../../corebridge/components/autosave-bar/autosave-bar.component";
import { EditorPresetJsonModel } from '../../models/editor-preset-json';
import { DetailedError } from "../../../../modules/corebridge/models/alert";
import { INamedQuark } from "../../../corebridge/models/iatom";

import * as defaultPresetJson from './preset-json-data.json';

@Component({
  selector: 'app-editor-preset-new',
  templateUrl: './editor-preset-new.component.html',
  styleUrls: ['./editor-preset-new.component.less']
})
export class EditorPresetNewComponent implements OnInit, AfterViewInit, OnDestroy {

  public breadcrumbs = [
    {
      title: 'Settings',
      path: 'ecommerce/settings'
    },
    {
      title: 'Online Designer'
    },
    {
      title: 'Design Editor Presets',
      path: 'ecommerce/settings/design-editor-presets'
    }
  ];
  public title: string;
  public editorPreset: EditorPreset;
  public saveState: AutoSaveBarState = AutoSaveBarState.none;
  public isNew: boolean;
  public isStandard: boolean = false;
  public id: number;
  public presetJSON: EditorPresetJsonModel = (defaultPresetJson as any).default;
  public errors: DetailedError[] = [];

  public editorTypes = ['Canvas Only', 'Basic Only', 'Basic & Advanced'];
  public editMode: string;
  public fontModes = ['Basic', 'Advanced'];

  // dropdown options
  public initialModes: INamedQuark[] = [{ ID: 1, Name: 'Basic' }, { ID: 2, Name: 'Advanced' }];
  public units: INamedQuark[] = [{ ID: 1, Name: 'inch' }, { ID: 2, Name: 'mm' }, { ID: 3, Name: 'cm' }, { ID: 4, Name: 'point' }, { ID: 5, Name: 'custom' }];
  public proofFormats: INamedQuark[] = [{ ID: 1, Name: 'jpeg' }, { ID: 2, Name: 'png' }];
  public proofFlipModes: INamedQuark[] = [{ ID: 1, Name: 'none' }, { ID: 2, Name: 'vertical' }, { ID: 3, Name: 'horizontal' }, { ID: 4, Name: 'both' }];
  public hiResFormats: INamedQuark[] = [{ ID: 1, Name: 'pdf' }, { ID: 2, Name: 'jpeg' }, { ID: 3, Name: 'png' }, { ID: 4, Name: 'tiff' }];
  private hiResCompressions: INamedQuark[] = [{ ID: 1, Name: 'jpeg' }, { ID: 2, Name: 'zip' }, { ID: 3, Name: 'none' }, { ID: 4, Name: 'lzw' }];
  public outputCompressions: INamedQuark[];
  public hiResColorSpaces: INamedQuark[] = [{ ID: 1, Name: 'cmyk' }, { ID: 2, Name: 'rbg' }, { ID: 3, Name: 'grayscale' }];
  public hiResFlipModes: INamedQuark[] = [{ ID: 1, Name: 'none' }, { ID: 2, Name: 'vertical' }, { ID: 3, Name: 'horizontal' }, { ID: 4, Name: 'both' }];
  public initialMode: number;
  public unit: number;
  public proofImageFileFormat: number;
  public proofImageFlipMode: number;
  public hiResOutputFileFormat: number;
  public hiResOutputCompression: number;
  public hiResOutputColorSpace: number;
  public hiResOutputFlipMode: number;

  @ViewChild('cbForm') ngForm: NgForm;

  constructor(private route: ActivatedRoute, private router: Router, private codaService: CodaService, private alertService: AlertService, private elementRef: ElementRef) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.id = +params['id'];
      if (this.id) {
        this.codaService.getEditorPresetById(this.id).pipe(untilDestroyed(this)).subscribe(response => {
          this.editorPreset = response;
          this.setPresetJSON(response.presetJSON);
          this.title = response.name;
          this.breadcrumbs.push({ title: response.name });
          this.isStandard = this.editorPreset.name.trim().toUpperCase() == 'STANDARD';
          if (!this.isStandard) {
            this.ngForm.form.valueChanges.pipe(debounceTime(2000), untilDestroyed(this)).subscribe(values => {
              this.validate(values);
            });
          }
        });

        this.isNew = false;
      } else {
        this.editorPreset = this.setBasicDataForNewPreset();
        this.title = 'New Design Editor Preset';
        this.breadcrumbs.push({ title: 'New' });
        this.isNew = true;
        this.ngForm.form.valueChanges.pipe(debounceTime(2000), untilDestroyed(this)).subscribe(values => {
          this.validate(values);
        });
      }
    });
  }

  ngAfterViewInit() {
    let foundElement = setInterval(() => {
      let elements = document.querySelectorAll('cbx-dropdown')
      if (elements.length) {
        elements.forEach((el, index) => {
          let temp = el.children[1].innerHTML;
          let asterisk = document.createElement("span")
          asterisk.classList.add('required-label')
          asterisk.innerHTML = "*"
          el.children[1].appendChild(asterisk);
        })
        clearInterval(foundElement)
      }
    }, 100)
  }

  setBasicDataForNewPreset() {
    let newEditorPreset: EditorPreset = {
      id: 0,
      isDeleted: false,
      isPrimary: false,
      presetJSON: '',
      sortIndex: 99,
      name: ''
    };
    return newEditorPreset;
  }

  setPresetJSON(jsonString: string) {
    let presetJSON = JSON.parse(jsonString) || this.presetJSON;
    if (presetJSON.canvasOnlyMode) {
      this.editMode = this.editorTypes[0];
    } else if (presetJSON.initialMode == 'Simple Only') {
      this.editMode = this.editorTypes[1];
    } else {
      this.editMode = this.editorTypes[2];
      this.initialMode = presetJSON.initialMode == 'Advanced' ? 2 : 1;
    }

    this.unit = this.mapOptionData('units', presetJSON.canvas.rulers.unit);
    this.proofImageFileFormat = this.mapOptionData('proofFormats', presetJSON.rendering.proofImageFileFormat);
    this.proofImageFlipMode = this.mapOptionData('proofFlipModes', presetJSON.rendering.proofImageFlipMode);
    this.hiResOutputFileFormat = this.mapOptionData('hiResFormats', presetJSON.rendering.hiResOutputFileFormat);
    this.hiResOutputCompression = this.mapOptionData('hiResCompressions', presetJSON.rendering.hiResOutputCompression);
    this.hiResOutputColorSpace = this.mapOptionData('hiResColorSpaces', presetJSON.rendering.hiResOutputColorSpace);
    this.hiResOutputFlipMode = this.mapOptionData('hiResFlipModes', presetJSON.rendering.hiResOutputFlipMode);
    this.setHiResCompressions();
    this.presetJSON = presetJSON;
  }

  mapOptionData(options: string, value: string): number {
    if (value) {
      let match = this[options].find(data => data.Name == value);
      return match ? match.ID : 0;
    }
    return 0;
  }

  setEditMode() {
    this.presetJSON.canvasOnlyMode = this.editMode == this.editorTypes[0];
    if (this.editMode == this.editorTypes[1]) {
      this.presetJSON.initialMode = 'Simple Only';
    } else if (this.editMode == this.editorTypes[2]) {
      this.initialMode = 1;
      this.presetJSON.initialMode = 'Simple';
    }
  }

  setDefaultEditor(selected: INamedQuark) {
    this.initialMode = selected.ID;
    this.presetJSON.initialMode = selected.ID == 1 ? 'Simple' : selected.Name;

    this.save();
  }

  setUnit(selected: INamedQuark) {
    this.unit = selected.ID;
    this.presetJSON.canvas.rulers.unit = selected.Name;

    this.save();
  }

  setOutputFormat(selected: any) {
    this.presetJSON.rendering.proofImageFileFormat = selected.value;
  }

  setRenderOutput(selected: INamedQuark, variableName: string) {
    this[variableName] = selected.ID;
    this.presetJSON.rendering[variableName] = selected.Name;
    if (variableName == 'hiResOutputFileFormat') {
      this.setHiResCompressions(true);
    }

    this.save();
  }

  setHiResCompressions(isOutputFormatDropdownChanging: boolean = false) {
    switch (this.hiResOutputFileFormat) {
      case 1: this.outputCompressions = this.hiResCompressions.slice(0, 2); break;
      case 4: this.outputCompressions = this.hiResCompressions.slice(3); break;
      case 2: this.outputCompressions = this.hiResCompressions.slice(0, 1); break;
      default: this.outputCompressions = []; this.hiResOutputCompression = 0;
    }

    if (isOutputFormatDropdownChanging) {
      this.setHiResCompressionsDefaultValue();
    }
  }

  setHiResCompressionsDefaultValue() {
    if (this.outputCompressions.length) {
      this.hiResOutputCompression = this.outputCompressions[0].ID;
      this.presetJSON.rendering.hiResOutputCompression = this.outputCompressions[0].Name;
    }
  }

  setCanvasSnapLineColor(selected: any) {
    this.presetJSON.canvas.snapLines.color = selected.value;

    this.save();
  }

  validate(values: any) {
    if (this.ngForm.touched) {
      if (this.errors.length > 0) {
        let required = [].slice.call(this.elementRef.nativeElement.querySelectorAll('.required input'));
        this.errors.removeWhere(e => !required.find(el => el.name == e['FieldId']));
        this.elementRef.nativeElement.querySelectorAll('.hidden cbx-dropdown').forEach(element => {
          this.errors.removeWhere(e => element.id == e['FieldId']);
        });
      }
      this.elementRef.nativeElement.querySelectorAll('.required input').forEach(element => {
        if (element.name == 'name') {
          if (!this.isStandard && values.name.trim().toUpperCase() == 'STANDARD') {
            this.addDetailedError('Preset Name', 'Standard is a reserved name', element.name);
          } else if (values.name.trim().length == 0) {
            this.addDetailedError('Preset Name', 'This field is required', element.name);
          } else {
            this.errors.removeWhere(e => e.FieldName == 'Preset Name');
          }
        } else if (this.ngForm.controls[element.name] && this.ngForm.controls[element.name].touched && element.nextElementSibling) {
          let label = element.nextElementSibling.innerText.replace(' *', '');
          if (this.ngForm.controls[element.name].valid)
            this.errors.removeWhere(e => e.FieldName == label);
          else if (!this.errors.find(e => e.FieldName == label))
            this.addDetailedError(label, 'This field is required', element.name);
        }
      });
      this.elementRef.nativeElement.querySelectorAll('.cb_fieldset-field:not(.hidden) cbx-dropdown').forEach(element => {
        let label = element.querySelector('.cbx-label').innerText.replace('*', '').trim();
        let section = "";
        if (label == 'Output Format' || label == 'Flip Output Orientation')
          section = element.id.startsWith('proof') ? 'Proof Output' : 'High Resolution Output';
        if (values[element.id])
          this.errors.removeWhere(e => e.FieldName == label && e.Name == section);
        else if (!this.errors.find(e => e.FieldName == label))
          this.addDetailedError(label, 'This field is required', element.id, section);
      });

      if ((this.presetJSON.rendering.hiResOutputFileFormat == 'pdf' || this.presetJSON.rendering.hiResOutputFileFormat == 'jpeg')
        && this.presetJSON.rendering.hiResOutputCompression == 'jpeg') {
        
        let label = 'JPEG Compression';
        let inputName = 'jpegCompression';

        if (this.presetJSON.rendering.hiResOutputJpegCompressionQuality == null) {
          this.addDetailedError(label, 'JPEG Compression is required', inputName);
        } else {
          if (this.presetJSON.rendering.hiResOutputJpegCompressionQuality < 0 || this.presetJSON.rendering.hiResOutputJpegCompressionQuality > 100) {
            this.addDetailedError(label, 'JPEG Compression only accepts values between 0 and 100', inputName);
            this.ngForm.controls[inputName].setErrors({ 'incorrect': true });
          } else {
            this.errors.removeWhere(e => e.FieldName == label);
            if(this.ngForm.controls[inputName]) {
              this.ngForm.controls[inputName].setErrors(null);
            }
          }
        }
      }

      this.alertService.detailedError('EditorPreset', this.errors);
      if (this.errors.length == 0 && this.ngForm.valid) {
        if (this.isNew)
          this.saveState = AutoSaveBarState.none;
        else
          this.save();
      } else if (!this.isNew) {
        this.saveState = AutoSaveBarState.invalid;
      }
    }
  }

  addDetailedError(field: string, message: string, id: string, section: string = "") {
    let fieldError = this.errors.find(e => e.FieldName == field);
    if (!fieldError || fieldError.ErrorMessage != message) {
      if (fieldError)
        this.errors.removeWhere(e => e.FieldName == field);
      let detailedError = { ID: 0, Name: section, FieldName: field, ErrorMessage: message, FieldId: id };
      this.errors.push(detailedError);
    }
  }

  save() {
    if (this.errors.length > 0 || !this.ngForm.valid) {
      this.saveState = AutoSaveBarState.invalid;
      if (!this.ngForm.valid) {
        this.elementRef.nativeElement.querySelector('input.ng-invalid').focus();
      }
      if (this.isNew && this.errors.length == 0) {
        this.ngForm.form.markAsTouched();
        this.validate(this.ngForm.form.value);
      }
      return;
    }

    this.saveState = AutoSaveBarState.saving;

    //serialize the object with the preset settings and save on presetJSON property
    let serializedEditorPresetJson = JSON.stringify(this.presetJSON);
    this.editorPreset.presetJSON = serializedEditorPresetJson;

    if (this.isNew) {
      this.codaService.createEditorPreset(this.editorPreset).pipe(untilDestroyed(this)).subscribe(res => {
        this.handleEditorPresetAfterSave(res);
      });
    } else {
      this.codaService.updateEditorPreset(this.id, this.editorPreset).pipe(untilDestroyed(this)).subscribe(res => {
        this.handleEditorPresetAfterSave(res);
      });
    }
  }

  handleEditorPresetAfterSave(response: any) {
    console.log(response);

    this.saveState = AutoSaveBarState.saved;
    if (this.isNew) {
      this.router.navigate(['/ecommerce/settings/design-editor-presets']);
    } else {
      this.ngForm.form.markAsPristine();
      if (this.title != this.editorPreset.name) {
        this.title = this.editorPreset.name;
        this.breadcrumbs.pop();
        this.breadcrumbs.push({ title: this.title });
      }
    }
  }

  cancel() {
    this.router.navigate(['/ecommerce/settings/design-editor-presets']);
  }

  styleChange(event) {
    console.log('Event Triggered from inner child: ', event, event.value);
  }

  ngOnDestroy() {
    this.alertService.detailedError('EditorPreset', []);
  }
}
