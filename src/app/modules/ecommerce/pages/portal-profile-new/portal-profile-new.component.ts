import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Observable, forkJoin } from 'rxjs';

import { AlertService } from '../../../../services/alert.service';
import { ZnodeService } from '../../../../services/znode.service';
import { ModalService } from '../../../../services/modal.service';

import { Portal } from '../../models/portal';
import { Profile } from '../../models/profile';
import { PortalProfile } from '../../models/portal-profile';

@Component({
  selector: 'cb-portal-profile-new',
  templateUrl: './portal-profile-new.component.html'
})
export class PortalProfileNewComponent implements OnInit {

  @Input() portal: Portal;

  @Output() onExit: EventEmitter<any> = new EventEmitter();

  public profiles: Profile[];
  public portalProfile: PortalProfile = new PortalProfile();
  public portalProfiles: PortalProfile[] = [];
  private unchanged: string;

  public defaultAnonymous: boolean = false;
  public defaultRegistered: boolean = false;

  public loading: boolean = false;

  constructor(private alertService: AlertService, private znodeService: ZnodeService, private modalService: ModalService) {}

  ngOnInit() {
    forkJoin(
      this.znodeService.getProfiles(),
      this.znodeService.getPortalProfiles(this.portal.PortalId)
    ).subscribe(
      data => {
        this.portalProfile.PortalID = this.portal.PortalId;

        this.profiles = data[0];
        this.portalProfiles = data[1];

        this.portalProfile.ProfileID = this.profiles.filter(profile => !this.existingPortalProfile(profile.ProfileId))[0].ProfileId;
        this.unchanged = JSON.stringify(this.portalProfile);
      },
      err => this.alertService.error(err)
    )
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.portalProfile) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  public save(){
    this.loading = true;
    this.znodeService.createPortalProfile(this.portalProfile).subscribe(
      res => {
        this.portalProfile = res;
        this.unchanged = JSON.stringify(this.portalProfile);
        
        if(this.defaultAnonymous || this.defaultRegistered){
          this.updatePortal(res.ProfileID);
        }
        else{
          this.alertService.success('Profile Associated Successfully');
          this.cancel();
        }
        this.loading = false;
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    );
  }

  public updatePortal(portalProfileId: number) {

    var update: boolean = false;

    if(this.defaultAnonymous){
      // console.debug("Setting DefaultAnonymousProfileId to " + portalProfileId);
      this.portal.DefaultAnonymousProfileId = portalProfileId;
    }
    if(this.defaultRegistered){
      // console.debug("Setting DefaultRegisteredProfileId to " + portalProfileId);
      this.portal.DefaultRegisteredProfileId = portalProfileId;
    }

    this.znodeService.updatePortal(this.portal).subscribe(
      data => {
        this.portal = data;
        this.alertService.success('Profile Associated Successfully');
        this.cancel();
      },
      err => this.alertService.error(err)
    );
  }

  public cancel(){
    this.onExit.emit();
  }

  public existingPortalProfile(profileId: number) {
    let nodes = this.portalProfiles.filter( portalProfile => portalProfile.ProfileID == profileId);
    return nodes.length > 0;
  }

}
