import { Component } from '@angular/core';

import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'app-shipping-boxes',
  templateUrl: './shipping-boxes.component.html'
})
export class ShippingBoxesComponent {

  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Shipping Boxes'
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) { }

  onLoaded(){
    this.loading = false;
  }

}
