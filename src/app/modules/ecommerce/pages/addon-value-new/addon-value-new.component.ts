import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ThemeService } from '../../../../services/theme.service';
import { DomainService } from '../../../../services/domain.service';

import { Addon } from '../../models/addon';
import { AddonValue } from '../../models/addon-value';
import { TaxClass } from '../../models/tax-class';
import { Supplier } from '../../models/supplier';

import { ConfigSetting } from '../../models/config-setting';
import { CesaService, MisTypes } from '../../../../services/cesa.service';
import { ATEService } from '../../../../services/ate.service';

@Component({
  selector: 'cb-addon-value-new',
  templateUrl: './addon-value-new.component.html'
})
export class AddonValueNewComponent implements OnInit {
  @Input() AddonId: number;
  @Output() onExit: EventEmitter<any> = new EventEmitter();

  public addonValue: AddonValue = new AddonValue();
  public taxClasses: TaxClass[];
  public suppliers: Supplier[];
  public addon: Addon;
  public defaultAddonValue: AddonValue;
  public configSetting: ConfigSetting;

  public loading: boolean = true;
  public skuInvalid: boolean = false;

  constructor(private znodeService: ZnodeService, private alertService: AlertService, private route: ActivatedRoute, private router: Router, private themeService: ThemeService, private domainService: DomainService, 
    private cesaService:CesaService,
    private ateService:ATEService) { }

  ngOnInit() {

    forkJoin(
      this.znodeService.getTaxClasses(),
      this.znodeService.getSuppliers(),
      this.znodeService.getAddon(this.AddonId),
      this.znodeService.getDefaultAddonValue(this.AddonId),
      this.znodeService.getConfigSettingByBID(this.domainService.bid)
    ).subscribe(  
      data => {
        this.taxClasses = data[0];
        this.addonValue.TaxClassId = this.taxClasses[0].TaxClassId;
        this.suppliers = data[1];

        this.addon = data[2];
        this.addonValue.AddOnId = this.addon.AddOnId;

        this.configSetting = Object.assign(new ConfigSetting(), data[4]);

        if (!this.configSetting.EnableOnlineTaxLookup) {
          this.taxClasses = this.taxClasses.filter(x => x.ExternalId == null);
        }else if(this.cesaService.getMisType() == MisTypes.CoreBridge && this.ateService.isATEProcessorTypeIdEqualToFour()){
          
            this.taxClasses = this.taxClasses.filter(x => x.ExternalId != null && x.ExternalId.toString().startsWith('4'));
          
        }

        this.defaultAddonValue = data[3];
        this.loading = false;
      }
    );

  }

  public save() {
    if (this.skuInvalid) {
      return;
    }

    this.loading = true;

    this.znodeService.createAddonValue(this.addonValue).subscribe(
      res => {
        if (this.addonValue.IsDefault && this.defaultAddonValue && this.defaultAddonValue.AddOnValueId != this.addonValue.AddOnValueId) {
          this.defaultAddonValue.IsDefault = false;

          this.znodeService.updateAddonValue(this.defaultAddonValue).subscribe(
            response => {
              this.alertService.success('Add-On Value Created Successfully');
              this.loading = false;
              this.cancel();
            },
            error => {
              this.alertService.error(error);
              this.loading = false;
            });
        }
        else {
          this.loading = false;
          this.alertService.success('Add-On Value Created Successfully');
          this.cancel();
        }
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    )
  }

  public cancel() {
    this.onExit.emit();
  }

  isNumber(e){
    return (e != null && !isNaN(e) && e.toString() !== '');
  }
  
  public onBlur(event: any, isSKU: boolean = false) {
    event.target.value = event.target.value.trim();
    if (isSKU) this.skuInvalid = !(/^[\w\d_.-]+$/.test(event.target.value));
  }
}
