import {
  Component,
  AfterViewInit,
  ViewChild,
  OnDestroy,
  OnInit
} from "@angular/core";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { Location } from "@angular/common";

import { Observable, forkJoin } from "rxjs";
import { FileUploader } from "ng2-file-upload";
import { cbFileUploader } from "../../../corebridge/ng2-file-upload-overridden/cb-file-uploader.class";

import { ZnodeService } from "../../../../services/znode.service";
import { CoreBridgeService } from "../../../../services/corebridge.service";
import { AlertService } from "../../../../services/alert.service";
import { ModalService } from "../../../../services/modal.service";
import { DomainService } from "../../../../services/domain.service";
import { ThemeService } from "../../../../services/theme.service";
import { CesaService, MisTypes } from "../../../../services/cesa.service";

import { Product } from "../../models/product";
import { IncomeAccount } from "../../models/income-account";
import { Sku } from "../../models/sku";
import { TaxClass } from "../../models/tax-class";
import { ShippingRuleType } from "../../models/shipping-rule-type";
import { Supplier } from "../../models/supplier";
import { Manufacturer } from "../../models/manufacturer";
import { Facet } from "../../models/facet";
import { ProductTag } from "../../models/product-tag";
import { ProductTier } from "../../models/product-tier";
import { ProductHighlight } from "../../models/product-highlight";
import { Addon } from "../../models/addon";
import { DigitalAsset } from "../../models/digital-asset";
import { Image } from "../../models/image";
import { CoreBridgeProductSettings } from "../../models/corebridge-product-settings";
import { DetailedError } from "../../../../modules/corebridge/models/alert";

import { TabsComponent } from "../../../corebridge/components/tabs/tabs.component";
import { AutoSaveBarState } from "../../../corebridge/components/autosave-bar/autosave-bar.component";
import { SkuProfileListComponent } from "../../components/sku-profile-list/sku-profile-list.component";
import { ProductTabDetailComponent } from "../../components/product-tabs/product-tab-detail.component";
import { ProductAssetsTableComponent } from "../../components/product-assets-table/product-assets-table.component";
import { isNullOrUndefined } from "util";
import { MatDialog } from "@angular/material";

//Imported for the EnableFileUploads setting.
import { ProductSetting } from "../../models/product-setting";

// customs
import { ProductCustomImageValidation } from "./ProductCustomImageValidation";
import { ConfigSetting } from "../../models/config-setting";
import { ATEService } from "../../../../services/ate.service";

import { ProductAddonsTableComponent } from "../../../ecommerce/components/product-addons-table/product-addons-table.component";
import { ProductInformationInsertImageComponent } from "../../../ecommerce/components/product-information-insert-image/product-information-insert-image.component";

import { untilDestroyed } from "ngx-take-until-destroy";
import { NgForm } from "@angular/forms";
import { debounceTime, distinctUntilChanged, map } from "rxjs/operators";
import { ProductImage } from "../../models/product-image";
import { IimageGalleryImageItem } from "../../../corebridge/models/image_gallery_image";
import { ProductImageCustomComponent } from "../../components/product-image-custom/product-image-custom.component";

@Component({
  selector: "app-product-details",
  templateUrl: "./product-details.component.html",
  styleUrls: ["./product-details.component.less"]
})
export class ProductDetailsComponent
  implements AfterViewInit, OnDestroy, OnInit {
  public productsLink = "/ecommerce/merchandise/products/";
  public breadcrumbs = [
    {
      title: "Products",
      path: this.productsLink
    }
  ];

  public product: Product;
  public unchanged: string;
  public title: string;
  public sku: Sku;
  public taxClasses: TaxClass[] = [];
  public shippingRuleTypes: ShippingRuleType[] = [];
  public suppliers: Supplier[] = [];
  public manufacturers: Manufacturer[] = [];
  public bundles: any[];
  public facets: Facet[];
  public tags: ProductTag = new ProductTag();
  public addons: Addon[];
  public tiers: ProductTier[];
  public highlights: ProductHighlight[];
  public digitalAssets: DigitalAsset[];
  public configSetting: ConfigSetting;
  public ipc_checkbox: Boolean = false; // boolean for state of "different From Sku"
  public validationErrors: DetailedError[] = [];
  public saveState: AutoSaveBarState = AutoSaveBarState.saved;
  public hasDragFile: boolean = false;

  public productInfo: any = {
    cb_integration_category: "basic"
  };
  public uploader: FileUploader = new cbFileUploader({
    url: "",
    autoUpload: false
  });

  imageVersion: number = 0;

  @ViewChild(TabsComponent) tabs: TabsComponent;
  @ViewChild(ProductTabDetailComponent)
  private detailsTab: ProductTabDetailComponent;
  @ViewChild(SkuProfileListComponent)
  private skuProfileList: SkuProfileListComponent;
  @ViewChild(ProductAssetsTableComponent)
  productAssets: ProductAssetsTableComponent;
  @ViewChild(ProductImageCustomComponent) imageCustomArt: ProductImageCustomComponent;

  public tabState: any = {
    "tiered-pricing": "list",
    images: "list",
    "add-ons": "list"
  };
  public selectedObjectId: number = 0;

  public ecommCategories: { CategoryName: string; CategoryId: number }[];
  public categories: {
    CategoryName: string;
    CategoryId: number;
    IsActive: boolean;
  }[];
  public quickProducts: any[] = [];
  public incomeAccounts: IncomeAccount[];

  public misType: number;
  public settings: any;

  //Used for the EnableFileUploads setting.

  /*
  Used to determine if the "Enable file uploads checkbox should be displayed.
  Value temporarily hard coded for dev purposes.
  */
  public fileManagementEnabled: boolean = false;

  public loading: boolean = false;

  public productSettings: any;

  public imageRoot: string;

  private carrierRateId: number;

  private routerSub;
  private productCustomImageValidation: ProductCustomImageValidation;
  private castToNumberTypes = [
    //  product[key] => where key is equal to the items in this list.
    "WholesalePrice",
    "SalePrice",
    "RetailPrice"
  ];

  public infoTabs = [
    "Features",
    "Specifications",
    "Shipping Info",
    "Downloads"
  ];
  public activeInfoTab = this.infoTabs[0];
  public productImages: ProductImage[] = []; // first index is the featured image
  public origProductImages: any[] = [];

  @ViewChild("productAddons") productAddons: ProductAddonsTableComponent;

  @ViewChild("informationForm") infoForm: NgForm;
  public seoUrlInvalid: boolean;

  @ViewChild("insertImageModal")
  insertImageModal: ProductInformationInsertImageComponent;

  constructor(
    private znodeService: ZnodeService,
    private corebridgeService: CoreBridgeService,
    private alertService: AlertService,
    private domainService: DomainService,
    private modalService: ModalService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private themeService: ThemeService,
    private cesaService: CesaService,
    private ateService: ATEService,
    private dialog: MatDialog
  ) {
    this.routerSub = router.events
      .pipe(untilDestroyed(this))
      .subscribe(path => {
        // Since we use location.go() to navigate inside of the component and location.go() doesn't update the router with the new path,
        // we need to capture router path changes and handle them inside of the component.
        /*
      if (!this.location.isCurrentPathEqualTo(router.routerState.snapshot.url)) {
        var pattern = /products\/[0-9]+/g;
        if( pattern.test(router.routerState.snapshot.url) ){
          // only set tabs if navigating to same page
          this.setOpenTab();
        }
      }*/
      });
    this.productCustomImageValidation = new ProductCustomImageValidation(
      modalService
    );
  }

  ngOnInit() {
    this.infoForm.form.valueChanges
      .pipe(debounceTime(2000), distinctUntilChanged(), untilDestroyed(this))
      .subscribe(values => {
        this.onProductChange(values);
      });
    window.addEventListener(
      "dragover",
      e => {
        this.preventDropInImagesTab(e);
      },
      false
    );
    window.addEventListener(
      "drop",
      e => {
        this.preventDropInImagesTab(e);
      },
      false
    );
  }

  preventDropInImagesTab(event: any) {
    if (this.tabs.isActiveTab("images")) {
      event && event.preventDefault();
      this.hasDragFile = false;
    }
  }

  onProductChange(values: any) {
    // Product changes under the product details tab are
    // handled under product-tab-detail.ts component
    if (this.infoForm.dirty) {
      if (!values.seoTitle || !values.seoTitle.trim().length) {
        this.product.SeoTitle = this.product.Name;
      }
      if (!values.seoUrl || !values.seoUrl.trim().length) {
        this.product.SeoUrl = this.product.Name.toLowerCase().replace(
          /[^a-zA-Z0-9-]/g,
          "-"
        );
        // Remove multiple occurence of dash in the Url Handle
        // after conversion.
        do {
          this.product.SeoUrl = this.product.SeoUrl.replace(/-{2}/g, "-");
        } while (this.product.SeoUrl.indexOf("--") > 0);
      }

      // Check if the URL Handle is unique. We cannot have this
      // on the blur event since we need to prevent the auto-saving
      // function if the URL Handle is not unique and auto-saving
      // is done on this method.
      // We don't need to convert the URL Handle to use dash here
      // since that is already handled on the keyup event
      this.removeDetailedError("Url Handle", "information");
      this.znodeService
        .isSeoUrlExist(this.product.ProductId, this.product.SeoUrl)
        .pipe(untilDestroyed(this))
        .subscribe(
          res => {
            this.seoUrlInvalid = res.IsExist;
            if (this.seoUrlInvalid) {
              this.addDetailedError(
                "Url Handle",
                "Duplicate value exists.",
                "information",
                "Information"
              );
            } else {
              this.removeDetailedError("Url Handle", "information");
            }

            if (this.infoForm.invalid || this.validationErrors.length) {
              this.saveState = AutoSaveBarState.invalid;
            }
            else if (this.unchanged == JSON.stringify(this.product)) {
              this.saveState = AutoSaveBarState.saved;
            }
            else {
              this.alertService.clearDetailedError();
              this.save();
            }
          },
          err => this.alertService.error(err)
        );
    }
  }

  ngAfterViewInit() {
    var productId: number;
    this.misType = this.cesaService.getMisType();

    this.route.params.forEach((params: Params) => {
      productId = +params["id"];
    });

    //For: ROG-211 / ROG-222
    this.getIsFileManagementEnabled();

    this.imageRoot = this.znodeService.base;

    forkJoin(
      this.znodeService.getSuppliers(),
      this.znodeService.getManufacturers(),
      this.znodeService.getProductFacets(productId),
      this.znodeService.getProductTags(productId),
      this.znodeService.getProductAddons(productId)
    )
      .pipe(untilDestroyed(this))
      .subscribe(data => {
        let [suppliers, manufacturers, facets, tags, addons] = data;
        this.suppliers = suppliers;
        this.manufacturers = manufacturers;
        this.facets = facets;
        this.tags = tags;
        if (this.tags.ProductTags == null) {
          this.tags.ProductTags = "";
        }
        this.addons = addons;
      });

    forkJoin(
      this.znodeService.getTaxClasses(),
      this.znodeService.getProductTiers(productId),
      this.znodeService.getProductHighlights(productId),
      this.znodeService.getProductDigitalAssets(productId),
      this.znodeService.getShippingRuleTypes(),
      this.znodeService.getConfigSettingByBID(this.domainService.bid)
    )
      .pipe(untilDestroyed(this))
      .subscribe(data => {
        let that = this;
        let [
          taxClasses,
          tiers,
          highlights,
          digitalAssets,
          shippingRuleTypes,
          configSetting
        ] = data;
        this.taxClasses = taxClasses;
        this.tiers = tiers;
        this.highlights = highlights;
        this.digitalAssets = digitalAssets;
        this.configSetting = Object.assign(new ConfigSetting(), configSetting);

        if (!this.configSetting.EnableOnlineTaxLookup) {
          this.taxClasses = this.taxClasses
            .filter(x => x.ExternalId == null)
            .filter(y => y.TaxClassId == 1 || y.TaxClassId == 2);
        } else if (
          this.configSetting.EnableOnlineTaxLookup &&
          this.misType == MisTypes.CoreBridge
        ) {
          this.taxClasses = this.taxClasses.filter(x => x.ExternalId != null);
        }

        // ROG-392: Add the option Carrier Rate to the dropdown immediately above the Flat Rate Per Item option.
        // This function moves the option Carrier Rate at the first index of the array.
        shippingRuleTypes.forEach((shippingRuleType, index) => {
          if (shippingRuleType.Name == "Carrier Rate") {
            shippingRuleTypes.splice(index, 1);

            if (this.configSetting.EnableCarrierRate) {
              shippingRuleTypes.unshift(shippingRuleType);
            }

            that.carrierRateId = shippingRuleType.ShippingRuleTypeId;
          }
        });

        this.shippingRuleTypes = shippingRuleTypes;
        this.getProduct(productId);
      });

    if (this.misType != MisTypes.Cyrious) {
      forkJoin(
        this.corebridgeService.getEcommProductCategories(),
        this.corebridgeService.getProductCategories(),
        this.corebridgeService.getIncomeAccounts(),
        this.corebridgeService.getProductSettingsByZnodeProductID(productId)
      )
        .pipe(untilDestroyed(this))
        .subscribe(data => {
          let [
            ecommCategories,
            categories,
            incomeAccounts,
            productSettings
          ] = data;
          this.ecommCategories = ecommCategories;
          this.categories = categories;
          this.incomeAccounts = incomeAccounts;
          this.productSettings = productSettings;
        });
    }
  }

  //For: ROG-211 / ROG-222
  getIsFileManagementEnabled() {
    if (this.misType !== 1) {
      this.corebridgeService
        .getIsFileManagementEnabled()
        .pipe(untilDestroyed(this))
        .subscribe(result => {
          this.fileManagementEnabled = result;
        });
    } else {
      this.fileManagementEnabled = true;
    }
  }

  checkCustomForWidthHeight() {
    let product = this.product;
    let validWidthAndHeight = this.productCustomImageValidation.widthHeightValidate(
      product
    );

    if (!validWidthAndHeight) {
      this.tabs.openTab("general");
    }

    return validWidthAndHeight;
  }

  // need to transform the formatted prices (with decimal) for dirty data check.
  // by default, the prices from this.unchanged are prices without format
  transformPricesToNumberType(num) {
    if (typeof num === "number") {
      return num;
    }

    if (typeof num === "string") {
      return parseFloat(num.replace(/\,/g, ""));
    }

    return num;
  }

  // clone product before removing the formatted prices (with decimal) to real number type.
  // this will prevent from modifying and reflecting the changes on the product page
  cloneProductToCastPricesToNumber(product) {
    let cloneProduct = Object.assign({}, product);

    for (let priceKey of this.castToNumberTypes) {
      if (cloneProduct.hasOwnProperty(priceKey)) {
        cloneProduct[priceKey] = this.transformPricesToNumberType(
          cloneProduct[priceKey]
        );
      }
    }

    return cloneProduct;
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (!this.detailsTab.isAvailabilityChanged && !this.hasChanges()) {
      return true;
    }
    let result = this.modalService.warning(
      "You Have Unsaved Changes",
      "Leaving without saving will discard any changes you have made. Are you sure you want to leave?",
      "Discard and Leave",
      "Stay"
    );
    if (result) {
      this.alertService.clearDetailedError();
    }

    return result;
  }

  hasChanges(): boolean {
    if (!this.unchanged || (this.tabs.isActiveTab('images') && this.saveState == AutoSaveBarState.saved))
      return false;

    let cloneProduct = this.cloneProductToCastPricesToNumber(this.product);
    let parsedUnchanged: Product = JSON.parse(this.unchanged);

    // ROG-952 change. This is needed due to ckeditor changing null values to empty string
    // thus resulting to old data and compared data not equal

    // FeaturesDescription check
    if (
      this.isNullOrEmpty(cloneProduct.FeaturesDescription) &&
      this.isNullOrEmpty(parsedUnchanged.FeaturesDescription)
    ) {
      cloneProduct.FeaturesDescription = parsedUnchanged.FeaturesDescription;
    }
    // Specifications check
    if (
      this.isNullOrEmpty(cloneProduct.Specifications) &&
      this.isNullOrEmpty(parsedUnchanged.Specifications)
    ) {
      cloneProduct.Specifications = parsedUnchanged.Specifications;
    }
    // AdditionalInfo check
    if (
      this.isNullOrEmpty(cloneProduct.AdditionalInfo) &&
      this.isNullOrEmpty(parsedUnchanged.AdditionalInfo)
    ) {
      cloneProduct.AdditionalInfo = parsedUnchanged.AdditionalInfo;
    }

    let unchanged = this.unchanged == JSON.stringify(cloneProduct);
    this.saveState = unchanged ? AutoSaveBarState.saved : AutoSaveBarState.none;
    return !unchanged;
  }

  private isNullOrEmpty(value: string): Boolean {
    return isNullOrUndefined(value) || value == "";
  }

  setOpenTab() {
    this.route.params.forEach((params: Params) => {
      let tab = params["tab"];
      if (!tab) {
        tab = "general";
      }
      this.tabs.openTab(tab);
      this.changedTab(tab);

      let selected = +params["selected"];
      if (selected > 0) {
        this.selectedObjectId = selected;
        this.tabState[tab] = "detail";
      }
    });
  }

  private loadProductImages() {
    this.productImages = [];
    if (this.product.ImageFile) {
      var featured = new ProductImage();
      featured.ImageFile = this.product.ImageFile;
      // featured.ProductImageID = this.product.pro
      this.productImages.push(featured);
    }

    this.znodeService
      .getProductImages(this.product.ProductId)
      .subscribe(data => {
        let altImages = data.map(p => new ProductImage(p));
        this.productImages = this.productImages.concat(altImages);
        console.log("this.productImages", this.productImages);
        this.origProductImages = JSON.parse(JSON.stringify(this.productImages));
        if(this.productImages.length)
          this.imageCustomArt.setImage(this.productImages[0]);
      });
  }

  private getProduct(id: number) {
    forkJoin(
      this.znodeService.getProduct(id),
      this.znodeService.getProductSku(id)
    )
      .pipe(untilDestroyed(this))
      .subscribe(
        data => {
          if (data[1].length != 0) {
            let skus: Sku[] = data[1];
            this.sku = skus[0];
            this.znodeService
              .getSku(this.sku.SkuId)
              .pipe(untilDestroyed(this))
              .subscribe(sku => {
                this.sku = sku;
              });
          } else {
            this.sku = new Sku();
          }

          var product: Product = data[0];
          this.product = product;

          if (!this.product.OutOfStockMessage)
            this.product.OutOfStockMessage = "Out of Stock";
          if (this.product.AllowBackOrder && !this.product.BackOrderMessage)
            this.product.BackOrderMessage = "On Backorder";

          if (this.product.ProductSetting.ProductNumDifferent) {
            this.ipc_checkbox = true;
          }

          this.loadProductImages();

          this.product.SkuId = this.sku.SkuId;
          this.product.SkuName = this.sku.Sku;
          this.product.Skus = [this.sku];

          if (
            this.product.AttributeIds == null ||
            this.product.AttributeIds == undefined
          ) {
            this.product.AttributeIds = "";
          }

          if (isNullOrUndefined(this.product.AllowBackOrder)) {
            this.product.AllowBackOrder = true;
          }

          // this.product = product;

          if (!this.configSetting.EnableCarrierRate) {
            if (this.carrierRateId === this.product.ShippingRuleTypeId) {
              this.product.ShippingRuleTypeId =
                this.shippingRuleTypes.length &&
                this.shippingRuleTypes[0].ShippingRuleTypeId;
            }
          }

          /*
        Set EnableFileUploads to ProductSetting model.
        The setting is expected to be fetched from the new ProductSetting database table in Znode.
        Commented out for now. Reverted to bind to product.EnableFileUploads directly since this will
        require fewer changes to the rest of the component's code when communicating with API.

        this.productSetting.EnableFileUploads = this.product.EnableFileUploads;
        */

          if (this.product.TaxClassId < 1) {
            this.product.TaxClassId = this.taxClasses[0].TaxClassId;
          }

          this.title = this.product.Name;
          this.productsLink = this.productsLink + this.product.ProductId;
          let crumb = { title: this.product.Name, path: this.productsLink };
          this.breadcrumbs.push(crumb);

          if (this.misType == MisTypes.Cyrious) {
            this.unchanged = JSON.stringify(this.product);
            this.loading = false;
          }
          // else if(this.misType == MisTypes.CoreBridge){
          else {
            this.corebridgeService
              .getProductSettingsByZnodeProductID(this.product.ProductId)
              .pipe(untilDestroyed(this))
              .subscribe(
                settings => {
                  if (settings.IsSuccess) {
                    this.product.CoreBridgeQuickProductCategoryId =
                      settings.CoreBridgeQuickProductCategoryId || "";
                    this.product.CoreBridgeQuickProductId =
                      settings.CoreBridgeQuickProductId || "";
                    this.product.CoreBridgeProductCategoryId =
                      settings.CoreBridgeProductCategoryId || "";
                    this.product.CoreBridgeIncomeAccountId =
                      settings.CoreBridgeIncomeAccountId || "";

                    if (settings.IntegrationTypeId == 2) {
                      this.productInfo.cb_integration_category = "advanced";

                      if (
                        this.product.CoreBridgeQuickProductCategoryId != null
                      ) {
                        this.getQuickProducts(
                          this.product.CoreBridgeQuickProductCategoryId
                        );
                      }
                    } else if (settings.IntegrationTypeId == 3) {
                      this.productInfo.cb_integration_category = "reporting";
                    } else {
                      this.productInfo.cb_integration_category = "basic";
                    }

                    this.settings = settings;

                    this.unchanged = JSON.stringify(this.product);
                    this.setOpenTab();
                    this.loading = false;
                  } else {
                    this.alertService.error(settings.Message);
                    this.unchanged = JSON.stringify(this.product);
                    this.setOpenTab();
                    this.loading = false;
                  }
                },
                err => {
                  this.alertService.error(err);
                  this.unchanged = JSON.stringify(this.product);
                }
              );
          }

          this.imageCustomArt.onProductLoad(this.product);
        },
        err => console.error(err)
      );
  }

  public save() {
    if (this.tabs.isActiveTab("images")) {
      /** This saves the changes made by the Images & Art tab ->
       *  Standard/Customizable toggle, Customization Required
       * */
      this.imageCustomArt.save();
      this.znodeService.updateProduct(this.product).subscribe();
      return;
    }

    this.onSave(true);
    let validWidthAndHeight = this.checkCustomForWidthHeight();
    if (validWidthAndHeight) {
      forkJoin(
        this.znodeService.updateProduct(this.product),
        this.znodeService.updateProductSEOSettings(this.product)
      )
        .pipe(untilDestroyed(this))
        .subscribe(
          res => {
            console.log(res);
            this.onSave(false);

            this.unchanged = JSON.stringify(this.product);
          },
          err => {
            this.alertService.error(err);
            this.loading = false;
            this.saveState = AutoSaveBarState.error;
          }
        );
    }
  }

  private updateProduct() {
    this.product.SkuName = this.sku.Sku;

    forkJoin(
      this.znodeService.updateProduct(this.product),
      this.znodeService.saveProductTag(this.tags, this.product.ProductId),
      this.znodeService.updateSku(this.sku)
    )
      .pipe(untilDestroyed(this))
      .subscribe(
        data => {
          let [product, tag, sku] = data;
          product.CallForPricing = this.product.CallForPricing;

          // retain corebridge settings
          product.CoreBridgeQuickProductCategoryId = this.product.CoreBridgeQuickProductCategoryId;
          product.CoreBridgeQuickProductId = this.product.CoreBridgeQuickProductId;
          product.CoreBridgeProductCategoryId = this.product.CoreBridgeProductCategoryId;
          product.CoreBridgeIncomeAccountId = this.product.CoreBridgeIncomeAccountId;

          this.product = product;
          this.product.SkuName = this.sku.Sku;

          this.unchanged = JSON.stringify(this.product);

          this.alertService.success("Product Saved!");
          this.loading = false;
          this.detailsTab.ngForm.form.markAsPristine();
          this.saveState = AutoSaveBarState.saved;
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
          this.saveState = AutoSaveBarState.error;
        }
      );
  }

  public cancel() {
    this.alertService.clearDetailedError();
    this.router.navigate(["/ecommerce/merchandise/products"]);
  }

  private viewProductTier(id: number) {
    this.selectedObjectId = id;
    this.tabState["tiered-pricing"] = "detail";
    this.location.go(this.productsLink + "/tiered-pricing/" + id);
  }

  private listProductTiers() {
    this.tabState["tiered-pricing"] = "list";
    this.location.go(this.productsLink + "/tiered-pricing");
    this.breadcrumbs.pop();
  }

  private addProductTier() {
    this.tabState["tiered-pricing"] = "new";
    this.location.go(this.productsLink + "/tiered-pricing/new");
    this.breadcrumbs.push({ title: "Add", path: "" });
  }

  public viewProductImage(id: number) {
    this.selectedObjectId = id;
    this.tabState.images = "detail";
    this.location.go(this.productsLink + "/images/" + id);
  }

  private listProductImages() {
    this.tabState.images = "list";
    this.location.go(this.productsLink + "/images");
    this.breadcrumbs.pop();
  }

  private addProductImage() {
    this.tabState.images = "new";
    this.location.go(this.productsLink + "/images/new");
    this.breadcrumbs.push({ title: "Add", path: "" });
  }

  public goToAddOn(id: number) {
    this.router.navigate(["ecommerce/merchandise/addons/", id]);
  }

  private addBreadcrumb(title: string) {
    this.breadcrumbs.push({ title: title, path: "" });
  }

  public truncateShortDescription() {
    this.product.ShortDescription = this.product.ShortDescription.substring(
      0,
      100
    );
  }

  private uploadProductImage(image: Image) {
    this.product.ImageFile = image.ImagePath;
    this.product.ImageLargePath = image.ImagePathLarge;
    this.product.ImageMediumPath = image.ImagePathMedium;
    this.product.ImageSmallPath = image.ImagePathSmall;
    this.product.ImageSmallThumbnailPath = image.ImagePathThumbnail;
    this.product.ImageThumbnailPath = image.ImagePathThumbnail;

    this.imageVersion++;
  }

  private getQuickProducts(categoryId: number) {
    this.quickProducts = [];

    if (categoryId > 0) {
      this.corebridgeService
        .getProductsByCategory(categoryId)
        .pipe(untilDestroyed(this))
        .subscribe(
          data => (this.quickProducts = data),
          err => this.alertService.error(err)
        );
    }
  }

  public changedTab(tabSlug: string) {
    for (var table in this.tabState) {
      this.tabState[table] = "list";
    }

    // let tabPath: string = this.productsLink+'/'+tabSlug;
    // this.router.navigate(tabPath.split('/'));

    // Admin UI Overhaul: Do not update breadcrumbs on tab change
    // while(this.breadcrumbs.length > 2){
    //   this.breadcrumbs.pop();
    // }

    // this.breadcrumbs.push({title: this.capitalizeFirst(tabSlug), path: this.productsLink+'/'+tabSlug});
  }

  ngOnDestroy() {
    if (this.routerSub != null) {
      this.routerSub.unsubscribe();
    }
  }

  IsMisTypeCoreBridge(): boolean {
    return this.misType === MisTypes.CoreBridge;
  }

  validate(event: any) {
    this.validationErrors.removeWhere(e => e.TabSlug == event.tab);
    this.alertService.detailedError(event.tab, event.errors);
    this.validationErrors = this.validationErrors.concat(event.errors);
    if (this.validationErrors.length > 0)
      this.saveState = AutoSaveBarState.invalid;
  }

  onSave(event: boolean) {
    this.loading = event;
    this.saveState = event ? AutoSaveBarState.saving : AutoSaveBarState.saved;
    if (!event && this.tabs.tabs.find(tab => tab.hasError))
      this.saveState = AutoSaveBarState.invalid;
    else if (this.saveState == AutoSaveBarState.saved)
      this.unchanged = JSON.stringify(this.product);
  }

  onSaveProductDetails(state: AutoSaveBarState) {
    this.loading = state == AutoSaveBarState.saving;
    this.saveState = state;
    if (state == AutoSaveBarState.saved)
      this.unchanged = JSON.stringify(this.product);
  }

  setErrorState() {
    this.saveState = AutoSaveBarState.error;
  }

  tabHasError(tab: string): boolean {
    return !!this.validationErrors.find(e => e.TabSlug === tab);
  }

  addDetailedError(
    field: string,
    message: string,
    tab: string,
    tabTitle: string
  ) {
    let fieldError = this.validationErrors.find(
      e => e.FieldName == field && e.TabSlug == tab
    );
    if (!fieldError || fieldError.ErrorMessage != message) {
      if (fieldError) this.removeDetailedError(field, tab);
      this.validationErrors.push({
        ID: this.product.ProductId,
        Name: "",
        FieldName: field,
        ErrorMessage: message,
        TabSlug: tab,
        TabTitle: tabTitle,
        goToAction: () => {
          this.tabs.openTab(tab);
        }
      });
      this.alertService.detailedError("productDetails", this.validationErrors);
    }
  }

  removeDetailedError(field: string, tab: string) {
    this.validationErrors.removeWhere(
      e => e.FieldName.toUpperCase() === field.toUpperCase() && e.TabSlug.toUpperCase() === tab.toUpperCase()
    );
    this.alertService.detailedError("productDetails", this.validationErrors);
  }

  onExitAddonsAssociate(status) {
    this.tabState["add-ons"] = "list";
    if (status) {
      this.productAddons.addOnsTable.selected = [];
      this.productAddons.getRows();
    }
  }

  setSeoDefaults(defaults: string) {
    this.product.SeoTitle = defaults;
    this.product.SeoUrl = defaults.toLowerCase().replace(/[^a-zA-Z0-9-]/g, "-");
    // Remove multiple occurence of dash in the Url Handle
    // after conversion.
    do {
      this.product.SeoUrl = this.product.SeoUrl.replace(/-{2}/g, "-");
    } while (this.product.SeoUrl.indexOf("--") > 0);
  }

  setUrlHandle() {
    if (this.product.SeoUrl) {
      this.product.SeoUrl = this.product.SeoUrl.toLowerCase().replace(
        /[^a-zA-Z0-9-]/g,
        "-"
      );
      // Remove multiple occurence of dash in the Url Handle
      // after conversion.
      do {
        this.product.SeoUrl = this.product.SeoUrl.replace(/-{2}/g, "-");
      } while (this.product.SeoUrl.indexOf("--") > 0);
    }
  }

  onProductCustomizationToggle(value: boolean) {
    this.imageCustomArt.setIfProductIsCustomizable(value);
    this.save();
  }

  imageUploadComplete(uploadedImages) {
    this.addProductImages(uploadedImages);
  }

  addProductImages(uploadedImages) {
    let altImages = uploadedImages;
    let featureImage = null;

    if (!this.origProductImages.length) {
      altImages = uploadedImages.slice(1, uploadedImages.length);
      featureImage = uploadedImages[0];
      if (featureImage) {
        this.setFeatureProduct(featureImage);
        this.productImages[0] = new ProductImage({ImageFile: featureImage.ImagePath})
        this.origProductImages = JSON.parse(
          JSON.stringify(this.productImages)
        );
      }
    }

    let query: Observable<ProductImage> = altImages.map(image => {
      let {
        ImagePath,
        ImagePathLarge,
        ImagePathMedium,
        ImagePathSmall,
        ImagePathThumbnail
      } = image;
      let obj: any = {
        ProductID: this.product.ProductId,
        ImageFile: ImagePath,
        ImageLargePath: ImagePathLarge,
        ImageMediumPath: ImagePathMedium,
        ImageSmallPath: ImagePathSmall,
        ImageThumbnailPath: ImagePathThumbnail
      };
      return this.znodeService.createProductImage(obj);
    });

    forkJoin(query).subscribe((result: ProductImage[]) => {
      result.forEach(res => {
        let productImage = this.productImages.find(
          img => img.ProductImageID == res.ProductImageID
        );
        if (productImage) Object.assign(productImage, res);
        // else this.productImages.push(new ProductImage(res));
        let index = this.productImages.findIndex(image => !image.ImageFile && !image.ProductImageID)
        if (index > -1) {
          this.productImages[index] = new ProductImage(res);
        }

      });
      this.origProductImages = JSON.parse(JSON.stringify(this.productImages));
    });
  }

  deleteProductImage(data: IimageGalleryImageItem) {
    let identifier = data.getUniqueIdentifier();
    if (!identifier) {
      let firstAltImage = this.productImages[1];
      if (firstAltImage && firstAltImage.ProductImageID) {
        this.znodeService
          .deleteProductImage(firstAltImage.ProductImageID)
          .subscribe(res => {
            this.productImages.splice(0, 1);
            delete this.productImages[0].ProductImageID;
            this.productImages[0].ImagePath = this.productImages[0].ImageFile;
            this.setFeatureProduct(this.productImages[0]);
            this.origProductImages = JSON.parse(
              JSON.stringify(this.productImages)
            );
          });
      } else {
        this.setFeatureProduct();
        this.productImages.splice(0, 1);
        this.origProductImages = JSON.parse(JSON.stringify(this.productImages));
      }
    } else {
      let id = parseInt(identifier);
      this.znodeService.deleteProductImage(id).subscribe(
        data => {
          var removeIndex = this.productImages
            .map(item => item.ProductImageID)
            .indexOf(id);
          this.productImages.splice(removeIndex, 1);
          this.origProductImages = JSON.parse(
            JSON.stringify(this.productImages)
          );
        },
        err => this.alertService.error(err)
      );
    }
  }

  setFeatureProduct(featureProduct: any = {}) {
    this.product.ImageFile = featureProduct.ImagePath || "";
    this.product.ImageLargePath = featureProduct.ImagePathLarge || "";
    this.product.ImageMediumPath = featureProduct.ImagePathMedium || "";
    this.product.ImageSmallPath = featureProduct.ImageSmallPath || "";
    this.product.ImageSmallThumbnailPath =
      featureProduct.ImagePathThumbnail || "";
    this.save();
  }
}
