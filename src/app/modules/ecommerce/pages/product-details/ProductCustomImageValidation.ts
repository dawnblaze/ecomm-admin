import { Component, ViewChildren } from '@angular/core';
import { TabsComponent } from '../../../corebridge/components/tabs/tabs.component';
import { ModalService } from '../../../../services/modal.service';
import { Router } from '@angular/router';
import { Product } from '../../../ecommerce/models/product';





export class ProductCustomImageValidation {
    

    constructor(private modalService: ModalService) {}


    widthHeightValidate(product){
        //console.log(`Checking image width height.. HEIGHT:${product.Height} WIDTH:${product.Width}`);
        if(product.UseCustomizableArtwork)
        {
            if(product.Width == null || product.Height == null || product.Height == 0 || product.Width == 0) {

                this.modalService.warningNoCancel("Missing Width and Height Values","You've chosen to make this a customizable product. The Width and Height values must be defined prior to saving changes.","Fix Now");

                return false;
                
            } else {
                return true;
            }
        } else {
            return true;
        }

        
    }
}