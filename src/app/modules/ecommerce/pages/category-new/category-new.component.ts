import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import { NgForm } from "@angular/forms";
import { Observable, forkJoin } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { untilDestroyed } from "ngx-take-until-destroy";

import { ZnodeService } from "../../../../services/znode.service";
import { AlertService } from "../../../../services/alert.service";
import { ModalService } from "../../../../services/modal.service";

import { Category } from "../../models/category";
import { Image } from "../../models/image";
import { DetailedError } from "../../../../modules/corebridge/models/alert";
import { AutoSaveBarState } from '../../../corebridge/components/autosave-bar/autosave-bar.component';
import { ThrowStmt } from "@angular/compiler";

@Component({
  selector: "app-category-new",
  templateUrl: "./category-new.component.html",
  styleUrls: ["../category-detail/category-detail.component.less"]
})
export class CategoryNewComponent implements OnInit, OnDestroy {
  public breadcrumbs = [
    {
      title: "Collections",
      path: "/ecommerce/merchandise/collections"
    },
    {
      title: "New"
    }
  ];

  public category: Category = new Category();
  private unchanged: string;
  public loading: boolean = false;
  public saveState: AutoSaveBarState = AutoSaveBarState.none;

  private imageRoot: string;
  private imageVersion: number = 0;

  invalid = {};
  detailedErrors: DetailedError[] = [];

  @ViewChild('detailsForm') ngForm: NgForm;

  constructor(
    private znodeService: ZnodeService,
    private alertService: AlertService,
    private modalService: ModalService,
    private router: Router,
    private elementRef: ElementRef
  ) {}

  ngOnInit() {
    this.imageRoot = this.znodeService.base;
    this.unchanged = JSON.stringify(this.category);

    this.ngForm.form.valueChanges.pipe(debounceTime(500),distinctUntilChanged(),untilDestroyed(this)).subscribe(values => {
      this.setValidationErrors(values);
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.category)) {
      return true;
    }
    return this.modalService.warning(
      "You Have Unsaved Changes",
      "Leaving without saving will discard any changes you have made. Are you sure you want to leave?",
      "Discard and Leave",
      "Stay"
    );
  }

  public truncateShortDescription() {
    this.category.ShortDescription = this.category.ShortDescription.substring(
      0,
      100
    );
  }

  public save() {
    this.saveState = AutoSaveBarState.saving;
    this.loading = true;
    this.znodeService.createCategory(this.category).subscribe(res => {
      this.loading = false;
      this.category = res;
      this.unchanged = JSON.stringify(this.category);
      this.saveState = AutoSaveBarState.saved;
      this.alertService.success("Collection Saved!");
      this.router.navigate(["/ecommerce/merchandise/collections"]);
    },
    err => {
      this.saveState = AutoSaveBarState.error;
      this.alertService.error(err);
    });
  }

  public cancel() {
    this.router.navigate(["/ecommerce/merchandise/collections"]);
  }

  private uploadCategoryImage(image: Image) {
    this.category.ImageFile = image.ImagePath;
    this.category.ImageLargePath = image.ImagePathLarge;
    this.category.ImageMediumPath = image.ImagePathMedium;
    this.category.ImageSmallPath = image.ImagePathSmall;
    this.category.ImageSmallThumbnailPath = image.ImagePathThumbnail;
    this.category.ImageThumbnailPath = image.ImagePathThumbnail;

    this.imageVersion++;
  }

  setValidationErrors(values: any){
    if (this.ngForm.touched) {
      if (!values.title || !values.title.trim().length){
        this.addDetailedError('Title', 'This field is required');
      } else if (/[;'"\\\/]/.test(values.title)) {
        this.addDetailedError('Title', 'Cannot use these special characters: ; \' " \ /');
        this.invalid['Title'] = true;
      } else {
        this.removeDetailedError('Title');
      }
      if(!values.name || !values.name.trim().length){
        this.addDetailedError('Name', 'This field is required');
      } else if (/[;'"\\\/]/.test(values.name)) {
        this.addDetailedError('Name', 'Allowed special characters: & - | .');
        this.invalid['Name'] = true;
      } else {
        this.removeDetailedError('Name');
      }
      if(values.displayOrder == null){
        this.addDetailedError('Display Order', 'This field is required');
      } else {
        this.removeDetailedError('Display Order');
      }
      if(!values.seoUrl || !values.seoUrl.trim().length){
        this.addDetailedError('URL Handle', 'This field is required');
      } else if(this.invalid['SeoUrl']) {
        this.addDetailedError('URL Handle', "Duplicate value exists.");
      } else {
        this.removeDetailedError('URL Handle');
      }
      if(this.saveState == AutoSaveBarState.invalid && !this.ngForm.invalid && !this.detailedErrors.length)
        this.saveState = AutoSaveBarState.none;
    }
  }

  onBlur(event: any, field: string, label: string) {
    event.target.value = event.target.value.trim();
    if (field === "SeoUrl" && event.target.value !== "") {
      this.validateUrlHandle();
    }
    else if (field === "Title") {
      this.category.Name = event.target.value;
      this.category.SeoTitle = event.target.value;
      this.setUrlHandle(event.target.value);
      this.validateUrlHandle();
    }
  }

  setUrlHandle(seoUrl: string, event: any = null) {
    seoUrl = seoUrl.toLowerCase().replace(/[^a-zA-Z0-9-]/g, "-");
    do {
      seoUrl = seoUrl.replace(/-{2}/g, "-");
    } while (seoUrl.indexOf("--") > 0);
    this.category.SeoUrl = seoUrl;
    if(event)
      event.target.value = seoUrl;
  }

  validateUrlHandle() {
    if(this.category.SeoUrl.length > 0) {
      this.znodeService.isSeoUrlExist(0, this.category.SeoUrl).pipe(untilDestroyed(this)).subscribe(
        data => {
          this.invalid['SeoUrl'] = data.IsExist;
          this.setValidationErrors(this.ngForm.form.value);
        }, error => this.alertService.error(error)
      );
    }
  }

  addDetailedError(field: string, message: string) {
    let fieldError = this.detailedErrors.find(e => e.FieldName == field);
    if (!fieldError || fieldError.ErrorMessage != message) {
      if (fieldError)
        this.removeDetailedError(field);
      if (!this.detailedErrors.find(error => error.FieldName === field)) {
        this.detailedErrors.push({
          ID: 0,
          Name: "New Collection",
          FieldName: field,
          ErrorMessage: message
        });

        this.alertService.detailedError("newCollection", this.detailedErrors);
      }
    }
  }

  removeDetailedError(field: string) {
    this.invalid[field] = false;
    this.detailedErrors.removeWhere(e => e.FieldName === field);
    this.alertService.detailedError("newCollection", this.detailedErrors);
  }

  focusInvalid() {
    let invalid = this.elementRef.nativeElement.querySelector('.ng-invalid:not(form), form .error input');
    if(invalid)
      invalid.focus();
    this.saveState = AutoSaveBarState.invalid;
  }

  ngOnDestroy() {
    this.alertService.detailedError("newCollection", []);
  }
}
