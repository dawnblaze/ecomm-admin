import { Component, OnInit, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgForm } from "@angular/forms";
import { untilDestroyed } from "ngx-take-until-destroy";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

import { AlertService } from '../../../../services/alert.service';
import { CodaService } from '../../../../services/coda.service';
import { ModalService } from '../../../../services/modal.service';
import { AutoSaveBarState } from "../../../corebridge/components/autosave-bar/autosave-bar.component";
import { DetailedError } from "../../../../modules/corebridge/models/alert";
import { TemplatePreset } from '../../models/template-preset';
import { TemplatePresetJsonModel, SafetyLine, Margin } from '../../models/template-preset-json';

import * as defaultTemplatePresetJson from './template-preset-default.json';

@Component({
  selector: 'app-surface-preset-details',
  templateUrl: './surface-preset-details.component.html',
  styleUrls: ['./surface-preset-details.component.less']
})
export class SurfacePresetDetailsComponent implements OnInit, OnDestroy {

  public breadcrumbs = [
    {
      title: 'Settings',
      path: 'ecommerce/settings'
    },
    {
      title: 'Online Designer'
    },
    {
      title: 'Surface Presets',
      path: 'ecommerce/settings/surface-presets'
    }
  ];
  public title: string;
  public templatePreset: TemplatePreset;
  public templatePresetJSON: TemplatePresetJsonModel = (defaultTemplatePresetJson as any).default;
  public saveState: AutoSaveBarState = AutoSaveBarState.none;
  public isNew: boolean;
  public isStandard: boolean = false;
  public id: number;
  public hasErrors: boolean;
  public activeIndex: number[];

  public isCollapseAll: boolean = true;
  @ViewChild('cbForm') ngForm: NgForm;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private codaService: CodaService,
    private alertService: AlertService,
    private modalService: ModalService,
    private elementRef: ElementRef,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      this.id = +params['id'];
      if (this.id) {
        this.codaService.getTemplatePresetById(this.id).pipe(untilDestroyed(this)).subscribe(response => {
          this.templatePreset = response;
          this.templatePresetJSON = response.presetJSON ? JSON.parse(response.presetJSON) : this.templatePresetJSON;
          this.title = response.name;
          this.breadcrumbs.push({ title: response.name });
          this.isStandard = this.templatePreset.name.trim().toUpperCase() == 'STANDARD';
          if(!this.isStandard) {
            this.ngForm.form.valueChanges.pipe(debounceTime(2000), untilDestroyed(this)).subscribe(values => {
              this.validate(values);
            });
          }
          this.collapseAll();
        });
        this.isNew = false;
      } else {
        this.templatePreset = this.setBasicDataForNewPreset();
        this.title = 'New Surface Preset';
        this.breadcrumbs.push({ title: 'New' });
        this.isNew = true;
        this.ngForm.form.valueChanges.pipe(debounceTime(2000), untilDestroyed(this)).subscribe(values => {
          this.validate(values);
        });
        this.collapseAll();
      }
    });

  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  setBasicDataForNewPreset() {
    let newTemplatePreset: TemplatePreset = {
      id: 0,
      name: '',
      presetJSON: '',
      isDeleted: false,
      isPrimary: false,
      sortIndex: 99,
      productTemplate: [],
      surfaces: []
    };
    return newTemplatePreset;
  }

  validate(values: any) {
    this.hasErrors = false;
    if (this.ngForm.dirty) {
      let errors: DetailedError[] = [];
      if (values.presetName.trim().toUpperCase() == 'STANDARD') {
        errors.push({ ID: 0, Name: "", FieldName: 'Preset Name', ErrorMessage: 'Standard is a reserved name' });
      } else if (values.presetName.trim().length == 0) {
        errors.push({ ID: 0, Name: "", FieldName: 'Preset Name', ErrorMessage: 'This field is required' });
      }
      if(this.templatePresetJSON.safetyLines.slice(4).find(custom => !custom.name || custom.name.trim().length == 0)) {
        errors.push({ ID: 0, Name: "", FieldName: 'Custom Surface Boundary Name', ErrorMessage: 'All custom surface boundaries must be named' });
      }

      if(errors.length) {
        this.alertService.detailedError('SurfacePreset', errors);
        if (!this.isNew)
          this.saveState = AutoSaveBarState.invalid;
        this.hasErrors = true;
      } else {
        this.alertService.detailedError('SurfacePreset', []);
        if (this.isNew)
          this.saveState = AutoSaveBarState.none;
        else
          this.save();
      }
    }
  }

  save() {
    if(this.isNew) {
      this.ngForm.form.markAsDirty();
      this.validate(this.ngForm.form.value)
    }
    if(this.hasErrors) {
      this.saveState = AutoSaveBarState.invalid;
      this.elementRef.nativeElement.querySelector('input').focus();
      return;
    }

    this.saveState = AutoSaveBarState.saving;

    //serialize the object with the preset settings and save on presetJSON property
    let serializedTemplatePresetJson = JSON.stringify(this.templatePresetJSON);
    this.templatePreset.presetJSON = serializedTemplatePresetJson;

    if (this.isNew) {
      this.codaService.createTemplatePreset(this.templatePreset).pipe(untilDestroyed(this)).subscribe(res => {
        this.handleTemplatePresetAfterSave(res);
      }, err => {
        this.alertService.error(err || 'Surface Preset Creation Failed');
        this.saveState = AutoSaveBarState.none;
      });
    } else {
      this.codaService.updateTemplatePreset(this.id, this.templatePreset).pipe(untilDestroyed(this)).subscribe(res => {
        this.handleTemplatePresetAfterSave(res);
      }, err => {
        this.alertService.error(err || 'Surface Preset Update Failed');
        this.saveState = AutoSaveBarState.error;
      });
    }
  }

  handleTemplatePresetAfterSave(response: any) {
    console.log(response);

    this.saveState = AutoSaveBarState.saved;
    if (this.isNew) {
      this.router.navigate(['/ecommerce/settings/surface-presets']);
    } else {
      this.ngForm.form.markAsPristine();
      if (this.title != this.templatePreset.name) {
        this.title = this.templatePreset.name;
        this.breadcrumbs.pop();
        this.breadcrumbs.push({ title: this.title });
      }
    }
  }

  cancel() {
    this.router.navigate(['/ecommerce/settings/surface-presets']);
  }

  styleChange(event){
    console.log('Event Triggered from inner child: ',event, event.value, event.css);
    // this.getData(event, event.css);
  }

  collapseAll() {
    this.isCollapseAll = !this.isCollapseAll;
    let activeIndexes = []
    if (!this.isCollapseAll) {
      let i = 0;
      while (i < this.templatePresetJSON.safetyLines.length) {
        activeIndexes.push(i);
        i++;
      }
    }
    this.activeIndex = activeIndexes;
  }
  onBlur(event: any) {
    // currently no-op
  }

  addCustom() {
    let surfaceBoundary = new SafetyLine();
    surfaceBoundary.margin = new Margin();
    surfaceBoundary['editing'] = true;
    this.templatePresetJSON.safetyLines.push(surfaceBoundary);
    this.activeIndex.push(this.templatePresetJSON.safetyLines.length - 1);
    this.ngForm.form.markAsDirty();
  }

  edit(event: any, safetyLine: SafetyLine) {
    event.stopPropagation();
    safetyLine['editing'] = true;
  }

  deleteCustom(event: any, index: number) {
    event.stopPropagation();
    if(this.isNew) {
      this.templatePresetJSON.safetyLines.splice(index, 1);
    } else {
      var title = "Delete Custom Surface Boundary";
      var content = "Please confirm you would like to delete the surface boundary. This change cannot be undone.";
      this.modalService.warning(title, content);
      let subscription = this.modalService.response.subscribe(res => {
        if (res) {
          this.templatePresetJSON.safetyLines.splice(index, 1);
        }
        subscription.unsubscribe();
      });
    }
  }

  onBlurCustomName(customBoundary: SafetyLine) {
    if(customBoundary.name && customBoundary.name.trim().length > 0)
      customBoundary['editing'] = false;
  }

  ngOnDestroy() {
    this.alertService.detailedError('SurfacePreset', []);
  }
}
