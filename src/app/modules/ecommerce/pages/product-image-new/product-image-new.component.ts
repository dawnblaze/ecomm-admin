import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { Observable }     from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { AuthService } from '../../../../services/auth.service';

import { Product } from '../../models/product';
import { Image } from '../../models/image';
import { ProductImage } from '../../models/product-image';
import { ProductImageType } from '../../models/product-image-type';

@Component({
  selector: 'cb-product-image-new',
  templateUrl: './product-image-new.component.html'
})
export class ProductImageNewComponent implements OnInit {

  @Input() product: Product;

  @Output() onExit: EventEmitter<any> = new EventEmitter();

  public productImage: ProductImage = new ProductImage();
  public productImageTypes: ProductImageType[];
  public title: string;

  public loading: boolean = false;

  private imageRoot: string;

  constructor(private alertService: AlertService, private znodeService: ZnodeService, private authService: AuthService) {}

  ngOnInit() {
    this.imageRoot = this.znodeService.base;

    this.znodeService.getProductImageTypes().subscribe(
      types => {
        this.productImageTypes = types;
        this.productImage.ProductImageTypeID = this.productImageTypes[0].ProductImageTypeID;

        this.productImage.ProductID = this.product.ProductId;
        this.productImage.ActiveInd = true;
      },
      err => {
        this.alertService.error(err);
      }
    );

    let user: any = this.authService.getUser();
    this.productImage.UserName = user.userName;
  }

  public save() {
    this.loading = true;
    console.log(this.productImage);

    this.znodeService.createProductImage(this.productImage).subscribe(
      image => {
        console.log(image);
        this.alertService.success('Product Image Created');
        this.loading = false;
        this.cancel();
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    );
  }

  public cancel() {
    console.log('leave');
    this.onExit.emit();
  }

  public uploadImage(image: Image) {
    this.productImage.ImageFile = image.ImagePath;
  }

}
