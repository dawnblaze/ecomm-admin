import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, forkJoin } from 'rxjs';

import { FluteDirection } from '../../models/flute-direction';
import { ShippingBox } from '../../models/shipping-box';
import { Profile } from '../../models/profile';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'cb-shipping-box-details',
  templateUrl: './shipping-box-details.component.html'
})
export class ShippingBoxDetailsComponent implements OnInit {

  public shippingPath = '/ecommerce/shipping-boxes/';
  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Shipping Boxes',
      path: this.shippingPath
    }
  ];

  public loading: boolean = true;
  public saving: boolean = false;
  public shippingBox: ShippingBox = new ShippingBox();
  private unchanged: string;
  public profiles: Profile[];
  public title: string;
  public fluteDirections: FluteDirection[];

  constructor(private znodeService: ZnodeService
    , private alertService: AlertService
    , private modalService: ModalService
    , private route: ActivatedRoute
    , private router: Router
    , private location: Location
    , private themeService: ThemeService) { }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      var shippingBoxId = +params['id'];

      forkJoin(
        this.znodeService.getShippingBox(shippingBoxId),
        this.znodeService.getProfiles(),
        this.znodeService.getFluteDirections()
      ).subscribe(
        data => {
          this.shippingBox = data[0];
          this.profiles = data[1];
          this.fluteDirections = data[2];
          this.unchanged = JSON.stringify(this.shippingBox);
          this.title = this.shippingBox.DisplayName;

          this.shippingPath = this.shippingPath+this.shippingBox.ShippingBoxId+'/'
          let crumb = {title: this.shippingBox.DisplayName, path:this.shippingPath};
          this.breadcrumbs.push(crumb);

          this.onLoaded();
        },
        err => this.alertService.error(err)
      );
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.shippingBox) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes'
    , 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  onLoaded(){
    this.loading = false;
  }

  public save() {
    this.saving = true;
    this.loading = true;
    forkJoin(
      this.znodeService.updateShippingBox(this.shippingBox)
    ).subscribe(
      data => {
        this.unchanged = JSON.stringify(this.shippingBox);
        this.alertService.success('Shipping Box Updated Successfully');
        this.saving = false;
        this.loading = false;
      },
      err => this.alertService.error(err)
    );
  }

  public cancel() {
    this.router.navigate(['/ecommerce/shipping-boxes']);
  }
}
