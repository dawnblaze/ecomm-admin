import { Component, OnInit } from '@angular/core';

import { ThemeService } from '../../../../../services/theme.service';

@Component({
  selector: 'cb-color-groups',
  templateUrl: './color-groups.component.html'
})
export class ColorGroupsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Settings',
      path: '/ecommerce/settings'
    },
    {
      title: 'Online Designer',
      path: '/'
    },
    {
      title: 'Color Groups',
      path: '/ecommerce/settings/color-groups'
    }
  ];
  public loading: boolean = true;

  constructor(private themeService: ThemeService) {}

  ngOnInit() {
  }

  onLoaded() {
    this.loading = false;
  }
}
