import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

import { AlertService } from '../../../../../services/alert.service';
import { DaimService } from '../../../../../services/daim.service'
import { ModalService } from '../../../../../services/modal.service';

import { FontGroup } from '../../../models/font-group';

@Component({
  selector: 'cb-font-group-new',
  templateUrl: './font-group-new.component.html'
})
export class FontGroupNewComponent implements OnInit {
  fontGroup: FontGroup;
  loading: boolean = false;
  unchanged: string;

  breadcrumbs = [
    {
      title: 'Settings',
      path: '/ecommerce/settings'
    },
    {
      title: 'Online Designer',
      path: '/'
    },
    {
      title: 'Font Groups',
      path: '/ecommerce/settings/font-groups'
    },
    {
      title: 'New',
      path: '/ecommerce/settings/font-groups/add'
    }
  ];

  constructor(private location: Location, private router: Router, private alertService: AlertService, private daimService: DaimService, private modalService: ModalService) { }

  ngOnInit() {
    this.fontGroup = new FontGroup();
    this.unchanged = JSON.stringify(this.fontGroup);
  }

  cancel() {
    this.router.navigate(['/ecommerce/settings/font-groups']);
  }

  save() {
    this.loading = true;
    this.daimService.addFontGroup(this.fontGroup).subscribe(
      res => {
        this.alertService.success('Font Group Created Successfully');
        this.loading = false;
        this.unchanged = JSON.stringify(this.fontGroup);
        this.router.navigate(['ecommerce/settings/font-groups']);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.fontGroup)) {
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }
}
