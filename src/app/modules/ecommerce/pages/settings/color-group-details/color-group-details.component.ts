import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, forkJoin } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { TabComponent } from '../../../../corebridge/components/tabs/tab/tab.component';
import { TabsComponent } from '../../../../corebridge/components/tabs/tabs.component';
import { ColorGroup } from '../../../models/color-group';

import { AlertService } from '../../../../../services/alert.service';
import { DaimService } from '../../../../../services/daim.service';
import { ModalService } from '../../../../../services/modal.service';
import { ThemeService } from '../../../../../services/theme.service';

@Component({
  selector: 'cb-color-group-details',
  templateUrl: './color-group-details.component.html'
})
export class ColorGroupDetailsComponent implements OnInit {
  @ViewChild(TabsComponent) tabs;

  colorGroup: ColorGroup = new ColorGroup();
  loading: boolean = true;
  saving: boolean = false;
  unchanged: string;

  breadcrumbs = [
    {
      title: 'Settings',
      path: '/ecommerce/settings'
    },
    {
      title: 'Online Designer',
      path: '/'
    },
    {
      title: 'Color Groups',
      path: '/ecommerce/settings/color-groups'
    }
  ];

  constructor(private location: Location, private route: ActivatedRoute, private alertService: AlertService, private daimService: DaimService, private modalService: ModalService, private router: Router, private themeService: ThemeService) { }

  ngOnInit() {
    let currentTab: string;
    this.route.params
      .pipe(switchMap((params: Params) => {
        currentTab = params['tab'];
        return this.daimService.getColorGroup(+params['id'])
      }))
      .subscribe((colorGroup: ColorGroup) => {
        this.colorGroup = colorGroup;
        this.unchanged = JSON.stringify(this.colorGroup);

        this.breadcrumbs.push({
          title: this.colorGroup.name,
          path: `/ecommerce/settings/color-groups/${this.colorGroup.id}`
        });

        this.setTab(this.tabs.openTab(currentTab || 'general'))
        this.loading = false;
      });
  }

  cancel() {
    this.router.navigate(['/ecommerce/settings/color-groups']);
  }

  save() {
    this.saving = true;
    this.daimService.updateColorGroup(this.colorGroup).subscribe(
      res => {
        this.alertService.success('Color Group Updated Successfully');
        this.saving = false;
      },
      error => {
        this.alertService.error(error);
        this.saving = false;
      });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.colorGroup)) {
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  onTabChange(tab: TabComponent) {
    this.tabs.openTab(tab.slug);
    this.setTab(tab);
  }

  setTab(tab: TabComponent) {
    this.location.go('/ecommerce/settings/color-groups/' + this.colorGroup.id + '/' + tab.slug);

    if (this.breadcrumbs.length > 4) {
      this.breadcrumbs.pop();
    }

    this.breadcrumbs.push({ title: tab.title, path: '/ecommerce/settings/color-groups/' + this.colorGroup.id + '/' + tab.slug });
  }
}
