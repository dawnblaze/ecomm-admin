import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, forkJoin } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { TabComponent } from '../../../../corebridge/components/tabs/tab/tab.component';
import { TabsComponent } from '../../../../corebridge/components/tabs/tabs.component';
import { FontGroup } from '../../../models/font-group';

import { AlertService } from '../../../../../services/alert.service';
import { DaimService } from '../../../../../services/daim.service';
import { ModalService } from '../../../../../services/modal.service';
import { ThemeService } from '../../../../../services/theme.service';

@Component({
  selector: 'cb-font-group-details',
  templateUrl: './font-group-details.component.html'
})
export class FontGroupDetailsComponent implements OnInit {
  @ViewChild(TabsComponent) tabs;

  fontGroup: FontGroup = new FontGroup();
  loading: boolean = true;
  saving: boolean = false;
  unchanged: string;

  breadcrumbs = [
    {
      title: 'Settings',
      path: '/ecommerce/settings'
    },
    {
      title: 'Online Designer',
      path: '/'
    },
    {
      title: 'Font Groups',
      path: '/ecommerce/settings/font-groups'
    }
  ];

  constructor(private location: Location, private route: ActivatedRoute, private alertService: AlertService, private daimService: DaimService, private modalService: ModalService, private router: Router, private themeService: ThemeService) { }

  ngOnInit() {
    let currentTab: string;
    this.route.params
      .pipe(switchMap((params: Params) => {
        currentTab = params['tab'];
        return this.daimService.getFontGroup(+params['id'])
      }))
      .subscribe((fontGroup: FontGroup) => {
        this.fontGroup = fontGroup;
        this.unchanged = JSON.stringify(this.fontGroup);

        this.breadcrumbs.push({
          title: this.fontGroup.name,
          path: `/ecommerce/settings/font-groups/${this.fontGroup.id}`
        });

        this.setTab(this.tabs.openTab(currentTab || 'general'))
        this.loading = false;
      });
  }

  cancel() {
    this.router.navigate(['/ecommerce/settings/font-groups']);
  }

  save() {
    this.saving = true;
    this.daimService.updateFontGroup(this.fontGroup).subscribe(
      res => {
        this.unchanged = JSON.stringify(this.fontGroup);
        this.alertService.success('Font Group Updated Successfully');
        this.saving = false;
      },
      error => {
        this.alertService.error(error);
        this.saving = false;
      });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.fontGroup)) {
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  onTabChange(tab: TabComponent) {
    this.tabs.openTab(tab.slug);
    this.setTab(tab);
  }

  setTab(tab: TabComponent) {
    this.location.go('/ecommerce/settings/font-groups/' + this.fontGroup.id + '/' + tab.slug);

    if (this.breadcrumbs.length > 4) {
      this.breadcrumbs.pop();
    }

    this.breadcrumbs.push({ title: tab.title, path: '/ecommerce/settings/font-groups/' + this.fontGroup.id + '/' + tab.slug });
  }
}
