import { Component } from '@angular/core';

import { ThemeService } from '../../../../../services/theme.service';

@Component({
  selector: 'cb-font-groups',
  templateUrl: './font-groups.component.html'
})
export class FontGroupsComponent {

  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Online Designer'
    },
    {
      title: 'Font Groups',
      path: '/ecommerce/settings/font-groups'
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) { }

  onLoaded(){
    this.loading = false;
  }
}
