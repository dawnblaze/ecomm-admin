import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

import { AlertService } from '../../../../../services/alert.service'
import { DaimService } from '../../../../../services/daim.service'
import { ModalService } from '../../../../../services/modal.service';

import { Color } from '../../../models/color';
import { SELECT_VALUE_ACCESSOR } from '@angular/forms/src/directives/select_control_value_accessor';

@Component({
  selector: 'cb-color-new',
  templateUrl: './color-new.component.html',
  styles: [ `
    .cb_fieldset-field_color {
      height:20px;
      width:20px;
      float:left;
      border:solid 1px #b9bfc7;
      margin-right:5px;
    }
    .cb_fieldset-field_input.small {
      width:42px;
      text-align:center;
      margin-left:5px;
    }
    .cb_fieldset-field_label--inline.small {
      width:initial;
    }
    .cb_fieldset-col_container {
      position:relative;
    }
    .cb_fieldset-col_left {
      position:absolute;
      left:0px;
      top:52px;
    }
    .cb_fieldset-col_right {
      margin-left:31px;
    }
    .link-arrows {
      width:20px;
      height:108px;
      border:solid 1px #ccc;
      border-right:none;
      position:absolute;
      top:-44px;
      left:6px;
      z-index:-1;
    }
    .link {
      width:15px;
      height:15px;
      background-color:#fff;
    }
  `
  ]
})
export class ColorNewComponent implements OnInit {
  color: Color;
  color1: string;
  color2: string = '';
  saving: boolean = false;
  link: boolean = true;
  unchanged: string;

  setValue() { this.color1 = "rgba(0,0,0,9)"};

  breadcrumbs = [
    {
      title: 'Settings',
      path: '/ecommerce/settings'
    },
    {
      title: 'Online Designer',
      path: '/'
    },
    {
      title: 'Colors',
      path: '/ecommerce/settings/colors'
    },
    {
      title: 'New',
      path: '/ecommerce/settings/colors/add'
    }
  ];

  constructor(private ref: ChangeDetectorRef, private location: Location, private router: Router, private alertService: AlertService, private daimService: DaimService, private modalService: ModalService) { }

  ngOnInit() {
    
    this.color = new Color();
    this.unchanged = JSON.stringify(this.color);
  }

  cancel() {
    this.router.navigate(['/ecommerce/settings/colors']);
  }

  save() {
    this.saving = true;

    this.daimService.addColor(this.color).subscribe(
      res => {
        this.alertService.success('Color Created Successfully');
        this.unchanged = JSON.stringify(this.color);
        this.saving = false;
        this.router.navigate(['ecommerce/settings/colors', res.id]);
      },
      error => {
        this.alertService.error(error);
        this.saving = false;
      });
  }

  colorChange(color) {
    ;
    let rgb = color.toRgb();

    this.color.r = rgb.r;
    this.color.g = rgb.g;
    this.color.b = rgb.b;

    this.updateCmyk();
  }

  updateRgb(prop, value) {
    this.color[prop] = +value;
    this.checkBounds();

    if (this.link) {
      this.color.r = Math.floor(255 * (1 - (this.color.c / 100)) * (1 - (this.color.k / 100)));
      this.color.g = Math.floor(255 * (1 - (this.color.m / 100)) * (1 - (this.color.k / 100)));
      this.color.b = Math.floor(255 * (1 - (this.color.y / 100)) * (1 - (this.color.k / 100)));
    }
  }

  updateCmyk(prop?, value?) {
    
    if (prop) {
      this.color[prop] = +value;
    }
    this.checkBounds();

    if (this.link) {
      let rx = this.color.r / 255,
        gx = this.color.g / 255,
        bx = this.color.b / 255,
        k = 1 - Math.max(rx, gx, bx);

      this.color.c = k == 1 ? 0 : Math.floor(((1 - rx - k) / (1 - k)) * 100),
      this.color.m = k == 1 ? 0 : Math.floor(((1 - gx - k) / (1 - k)) * 100),
      this.color.y = k == 1 ? 0 : Math.floor(((1 - bx - k) / (1 - k)) * 100);
      this.color.k = Math.floor(k * 100);
    }
  }

  checkBounds() {
    if (this.color.r < 0)
      this.color.r = 0;
    if (this.color.r > 255) {
      this.ref.detectChanges();
      this.color.r = 255;
    }
    if (this.color.g < 0)
      this.color.g = 0;
    if (this.color.g > 255) {
      this.ref.detectChanges();
      this.color.g = 255;
    }
    if (this.color.b < 0)
      this.color.b = 0;
    if (this.color.b > 255) {
      this.ref.detectChanges();
      this.color.b = 255;
    }
    if (this.color.c < 0)
      this.color.c = 0;
    if (this.color.c > 100) {
      this.ref.detectChanges();
      this.color.c = 100;
    }
    if (this.color.m < 0)
      this.color.m = 0;
    if (this.color.m > 100) {
      this.ref.detectChanges();
      this.color.m = 100;
    }
    if (this.color.y < 0)
      this.color.y = 0;
    if (this.color.y > 100) {
      this.ref.detectChanges();
      this.color.y = 100;
    }
    if (this.color.k < 0)
      this.color.k = 0;
    if (this.color.k > 100) {
      this.ref.detectChanges();
      this.color.k = 100;
    }
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.color)) {
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }
}
