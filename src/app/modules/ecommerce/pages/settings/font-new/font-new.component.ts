import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Observable  } from 'rxjs';
import { mergeMap, flatMap } from 'rxjs/operators';

import { AlertService } from '../../../../../services/alert.service'
import { DaimService } from '../../../../../services/daim.service'
import { ModalService } from '../../../../../services/modal.service';
import { ThemeService } from '../../../../../services/theme.service';

import { Font } from '../../../models/font';

@Component({
  selector: 'cb-fonts',
  templateUrl: './font-new.component.html'
})
export class FontNewComponent implements OnInit {
  font: Font;
  file: File;
  saving: boolean = false;
  unchanged: string;
  filename: string;

  breadcrumbs = [
    {
      title: 'Settings',
      path: '/ecommerce/settings'
    },
    {
      title: 'Online Designer',
      path: '/'
    },
    {
      title: 'Fonts',
      path: '/ecommerce/settings/fonts'
    },
    {
      title: 'New',
      path: '/ecommerce/settings/fonts/add'
    }
  ];

  constructor(private location: Location, private router: Router, private alertService: AlertService, private daimService: DaimService, private modalService: ModalService, public themeService: ThemeService) {}

  ngOnInit() {
    this.font = new Font();
    this.unchanged = JSON.stringify(this.font);
  }

  cancel() {
    this.router.navigate(['/ecommerce/settings/fonts']);
  }

  save() {
    this.saving = true
    this.daimService.uploadFont(this.file)
    .pipe(
      flatMap(asset => {
      return this.daimService.getFontFromAsset(asset.id);
    }),
    flatMap(font => {
      this.font.fontFamilyName = font.fontFamilyName;
      return this.daimService.updateFont(this.font);
    })).
    subscribe(
    res => {
      this.alertService.success('Font Created Successfully');
      this.saving = false;
      this.unchanged = JSON.stringify(this.font);

      this.router.navigate(['/ecommerce/settings/fonts']);
    },
    error => {
      this.alertService.error(error);
      this.saving = false;
    });
  }

  onFontChange(file) {
    this.file = file;
    this.filename = this.file.name;
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.font)) {
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }
}
