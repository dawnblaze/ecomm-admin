import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, forkJoin } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { TabComponent } from '../../../../corebridge/components/tabs/tab/tab.component';
import { TabsComponent } from '../../../../corebridge/components/tabs/tabs.component';
import { Font } from '../../../models/font';

import { AlertService } from '../../../../../services/alert.service';
import { DaimService } from '../../../../../services/daim.service';
import { ModalService } from '../../../../../services/modal.service';
import { ThemeService } from '../../../../../services/theme.service';

@Component({
  selector: 'cb-font-details',
  templateUrl: './font-details.component.html',
})
export class FontDetailsComponent implements AfterViewInit {
  @ViewChild(TabsComponent) tabs;

  font: Font = new Font();
  saving: boolean = false;
  unchanged: string;
  title: string;

  breadcrumbs = [
    {
      title: 'Settings',
      path: '/ecommerce/settings'
    },
    {
      title: 'Online Designer',
      path: '/'
    },
    {
      title: 'Fonts',
      path: '/ecommerce/settings/fonts'
    }
  ];

  public loading: boolean = true;

  constructor(private location: Location, private route: ActivatedRoute, private alertService: AlertService, private daimService: DaimService, private modalService: ModalService, private router: Router, public themeService: ThemeService) { }

  ngAfterViewInit() {
    let currentTab: string;
    this.route.params
      .pipe(switchMap((params: Params) => {
        currentTab = params['tab'];

        return this.daimService.getFont(params['fontFamilyName'])
      }))
      .subscribe((font: Font) => {
        this.font = font
        this.title = font.displayName;
        this.unchanged = JSON.stringify(this.font);

        this.breadcrumbs.push({
          title: this.font.displayName,
          path: `/ecommerce/settings/fonts/${this.font.fontFamilyName}`
        });

        this.setTab(this.tabs.openTab(currentTab || 'general'))
        this.loading = false;
      });
  }

  cancel() {
    this.router.navigate(['/ecommerce/settings/fonts']);
  }

  save() {
    this.saving = true;
    this.daimService.updateFont(this.font).subscribe(
      res => {
        this.unchanged = JSON.stringify(this.font);
        this.alertService.success('Font Updated Successfully');
        this.saving = false;
      },
      error => {
        this.alertService.error(error);
        this.saving = false;
      });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.font)) {
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  onTabChange(tab: TabComponent) {
    this.tabs.openTab(tab.slug);
    this.setTab(tab);
  }

  setTab(tab: TabComponent) {
    this.location.go('/ecommerce/settings/fonts/' + this.font.fontFamilyName + '/' + tab.slug);

    if (this.breadcrumbs.length > 4) {
      this.breadcrumbs.pop();
    }

    this.breadcrumbs.push({ title: tab.title, path: '/ecommerce/settings/fonts/' + this.font.fontFamilyName + '/' + tab.slug });
  }
}
