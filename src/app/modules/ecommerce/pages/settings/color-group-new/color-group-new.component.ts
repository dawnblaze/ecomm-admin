import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

import { AlertService } from '../../../../../services/alert.service';
import { DaimService } from '../../../../../services/daim.service'
import { ModalService } from '../../../../../services/modal.service';

import { ColorGroup } from '../../../models/color-group';

@Component({
  selector: 'cb-color-group-new',
  templateUrl: './color-group-new.component.html'
})
export class ColorGroupNewComponent implements OnInit {
  colorGroup: ColorGroup;
  loading: boolean = false;
  unchanged: string;

  breadcrumbs = [
    {
      title: 'Settings',
      path: '/ecommerce/settings'
    },
    {
      title: 'Online Designer',
      path: '/'
    },
    {
      title: 'Color Groups',
      path: '/ecommerce/settings/color-groups'
    },
    {
      title: 'New',
      path: '/ecommerce/settings/color-groups/add'
    }
  ];

  constructor(private location: Location, private router: Router, private alertService: AlertService, private daimService: DaimService, private modalService: ModalService) { }

  ngOnInit() {
    this.colorGroup = new ColorGroup();
    this.unchanged = JSON.stringify(this.colorGroup);
  }

  cancel() {
    this.router.navigate(['/ecommerce/settings/color-groups']);
  }

  save() {
    this.loading = true;
    this.daimService.addColorGroup(this.colorGroup).subscribe(
      res => {
        this.alertService.success('Color Group Created Successfully');
        this.loading = false;
        this.unchanged = JSON.stringify(this.colorGroup);
        this.router.navigate(['ecommerce/settings/color-groups']);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.colorGroup)) {
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }
}
