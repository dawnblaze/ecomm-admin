import { Component, OnInit } from '@angular/core';

import { ThemeService } from '../../../../../services/theme.service';

@Component({
  selector: 'cb-colors',
  templateUrl: './colors.component.html'
})
export class ColorsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Settings',
      path: '/ecommerce/settings'
    },
    {
      title: 'Online Designer',
      path: '/'
    },
    {
      title: 'Colors',
      path: '/ecommerce/settings/colors'
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) {}

  ngOnInit() {
  }

  onLoaded() {
    this.loading = false;
  }
}
