import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Location } from '@angular/common';

import { TabComponent } from '../../../../corebridge/components/tabs/tab/tab.component';
import { TabsComponent } from '../../../../corebridge/components/tabs/tabs.component';

@Component({
  selector: 'cb-fonts',
  templateUrl: './fonts.component.html'
})
export class FontsComponent implements AfterViewInit {
  @ViewChild(TabsComponent) tabs;

  public breadcrumbs = [
    {
      title: 'Settings',
      path: '/ecommerce/settings'
    },
    {
      title: 'Online Designer',
      path: '/'
    },
    {
      title: 'Fonts',
      path: '/ecommerce/settings/fonts'
    }
  ];

  constructor(private location: Location) {}

  ngAfterViewInit() {
    let currentTab: string;
    if (~window.location.pathname.split('/').indexOf('system')) {
      currentTab = 'system';
    }
    else {
      currentTab = 'custom';
    }

    this.setTab(this.tabs.openTab(currentTab));
  }

  onTabChange(tab: TabComponent) {
    this.tabs.openTab(tab.slug);
    this.setTab(tab);
  }

  setTab(tab: TabComponent) {
    this.location.go('/ecommerce/settings/fonts/' + tab.slug);

    if (this.breadcrumbs.length > 3) {
      this.breadcrumbs.pop();
    }

    this.breadcrumbs.push({ title: tab.title, path: '/ecommerce/settings/fonts/' + tab.slug });
  }
}
