import { Component, AfterViewInit, OnInit, ChangeDetectorRef } from "@angular/core";
import { ZnodeService } from '../../../../services/znode.service';
import { DomainService } from "../../../../services/domain.service";

@Component({
    selector: 'cb-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.less']
})
export class SettingsComponent implements OnInit, AfterViewInit {
    public breadcrumbs = [
        {
            title: 'Settings',
            path: '/ecommerce/settings'
        }
    ];
    public settings = [
        {
            title: 'General',
            children: [
                { title: 'Email Templates', description: 'description', path: '/ecommerce/email-templates' },
                { title: 'Product Defaults', description: 'description', path: '/ecommerce/product-defaults' }]

        },
        {
            title: 'Online Designer',
            children: [
                { title: 'Colors', description: 'description', path: '/ecommerce/settings/colors' },
                { title: 'Color Groups', description: 'description', path: '/ecommerce/settings/color-groups' },
                { title: 'Fonts', description: 'description', path: '/ecommerce/settings/fonts' },
                { title: 'Font Groups', description: 'description', path: '/ecommerce/settings/font-groups' },
            ]
        },
        {
            title: 'Shipping',
            children: [
                { title: 'Shipping Method', description: 'description', path: '/ecommerce/shipping-methods' },
                { title: 'Shipping Boxes', description: 'description', path: '/ecommerce/shipping-boxes' }]
        }
    ]

    public activeIndex: number[];
    public isCollapseAll: boolean;
    public searchKey: string;

    constructor(private cdRef: ChangeDetectorRef, private znodeService: ZnodeService, private domainService: DomainService) { }

    ngOnInit() {
        this.activeIndex = [0, 1, 2];
        this.isCollapseAll = this.activeIndex.length ? false : true;

        this.addCustomerCanvasOptionsIfItsEnabled();
    }

    ngAfterViewInit() {
        let foundElement = setInterval(() => {
            let headers = document.querySelectorAll('.ui-accordion-header > a[role="tab"]')
            if (headers.length) {
                let icons = ['fa-tools', 'fa-object-group', 'fa-shipping-fast']
                headers.forEach((el, index) => {
                    let headerEl = el.querySelector('.ui-accordion-header-text')
                    let title = headerEl.innerHTML;
                    headerEl.innerHTML = `<i class="fal ${icons[index]}"></i>${title}`;
                })
                clearInterval(foundElement)
            }
        }, 100)
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }

    collapseAll() {
        this.isCollapseAll = !this.isCollapseAll;
        this.activeIndex = this.isCollapseAll ? [] : [0, 1, 2];
    }

    addCustomerCanvasOptionsIfItsEnabled() {
        this.znodeService.getConfigSettingByBID(this.domainService.bid).subscribe(data => {
            if(data.EnableCustomersCanvas) {
                var onlineDesignerSetting = this.settings.filter(s => s.title == 'Online Designer')[0];

                onlineDesignerSetting.children.push({ title: 'Design Editor Presets', description: 'description', path: '/ecommerce/settings/design-editor-presets' });
                onlineDesignerSetting.children.push({ title: 'Surface Presets', description: 'description', path: '/ecommerce/settings/surface-presets' });
            }
        });
    }
}
