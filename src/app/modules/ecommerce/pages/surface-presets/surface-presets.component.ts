import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-surface-presets',
  templateUrl: './surface-presets.component.html'
})
export class SurfacePresetsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Settings',
      path: 'ecommerce/settings'
    },
    {
      title: 'Online Designer'
    },
    {
      title: 'Surface Presets'
    }
  ];

  public loading: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  onLoaded(){
    this.loading = false;
  }

}
