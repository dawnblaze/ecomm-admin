import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ThemeService } from '../../../../services/theme.service';


@Component({
  selector: 'app-styles',
  templateUrl: './styles.component.html'
})
export class StylesComponent implements OnInit {

	public breadcrumbs = [
    {
      title: 'Styles'
    }
  ];

  public loading: boolean = true;

	constructor(private znodeService: ZnodeService, private alertService: AlertService, private router: Router, private location: Location, private themeService: ThemeService) { }

	ngOnInit() {}

  onLoaded(){
    this.loading = false;
  }
}
