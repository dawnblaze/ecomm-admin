import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

import { EmailTemplate } from '../../models/email-template';

@Component({
  selector: 'app-email-template-details',
  templateUrl: './email-template-details.component.html'
})
export class EmailTemplateDetailsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Email Templates',
      path: '/ecommerce/email-templates'
    },
    {
      title: 'Email Template Details'
    }
  ];

  public template : EmailTemplate;
  private unchanged: string;
  public title: string;
  public config: any;

  public loading: boolean = false;

  constructor(private alertService: AlertService, private route: ActivatedRoute, private znodeService: ZnodeService, private modalService: ModalService, private router: Router, private themeService: ThemeService) {}

  ngOnInit() {
    this.config = {
      allowedContent: true,
      removeFormatAttributes: '',
      fullPage: true
    };
    this.route.params.forEach((params: Params) => {
      console.log(params);
      let name: string = params['name'];
      this.getEmailTemplate(name);
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.template) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  private getEmailTemplate(name: string) {
    let temp = name.split('-');

    this.znodeService.getEmailTemplate(name).subscribe(template => {
      this.template = template;
      this.template.Extension = temp[1];
      this.unchanged = JSON.stringify(this.template);
      this.title = this.template.TemplateName;

      this.breadcrumbs[ this.breadcrumbs.length -1 ].title = this.template.TemplateName;
    });
  }

  public save() {
    this.loading = true;
    this.znodeService.updateEmailTemplate(this.template).subscribe(
      res => {
        this.unchanged = JSON.stringify(this.template);

        this.alertService.success('Email Template Updated Successfully');
        this.loading = false;
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    )
  }

  public cancel() {
    this.router.navigate(['/ecommerce/email-templates']);
  }

  onChange(e) {
    if (e.target && e.target.value) {
      this.template.Html = e.target.value;
    }
  }
}
