import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, forkJoin } from 'rxjs';

import { TabsComponent } from '../../../corebridge/components/tabs/tabs.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

import { Addon } from '../../models/addon';
import { Alert } from '../../../corebridge/models/alert';

@Component({
  selector: 'app-addon-details',
  templateUrl: './addon-details.component.html'
})
export class AddonDetailsComponent implements AfterViewInit {

  @ViewChild(TabsComponent) private tabs: TabsComponent;

  public breadcrumbs = [
    {
      title: 'Add-Ons',
      path: '/ecommerce/merchandise/addons'
    }
  ];

  public addon: Addon = new Addon();
  private unchanged: string;
  public title: string;

  public loading: boolean = true;
  public saving: boolean = false;
  public state: string = 'list';
  public selectedId: number;

  private routerSub;

  constructor(private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService, private route: ActivatedRoute, private router: Router, private location: Location, private themeService: ThemeService) {
    this.routerSub = router.events.subscribe(path => {
      // Since we use location.go() to navigate inside of the component and location.go() doesn't update the router with the new path,
      // we need to capture router path changes and handle them inside of the component.
      if (!this.location.isCurrentPathEqualTo(router.routerState.snapshot.url)) {
        this.setOpenTab();
      }
    });
  }

  ngAfterViewInit() {
    this.route.params.forEach((params: Params) => {
      let addonId = +params['addon'];
      this.selectedId = +params['id'];
      this.getAddon(addonId);
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.addon) ){
      return true;
    }
    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  getAddon(id: number) {
    this.znodeService.getAddon(id).subscribe(addon => {
      this.addon = addon;
      this.unchanged = JSON.stringify(this.addon);
      this.title = this.addon.Name;

      this.breadcrumbs.push({title: this.addon.Title, path: '/ecommerce/merchandise/addons/'+this.addon.AddOnId+'/general'});
      this.loading = false;

      this.setDefaultStockMsg();
      this.setOpenTab();

    });
  }

  setOpenTab() {
    this.route.params.forEach((params: Params) => {
      let tab = params['tab'];
      if(!tab) {
        tab = "general";
      }
      this.tabs.openTab(tab);
      this.changedTab(tab);

      let selected = +params['id'];
      if(selected > 0){
        this.selectedId = selected;
        this.state = 'detail';
      }

      let create = params['new'];
      if(create){
        this.state = 'new';
      }
    });
  }

  save() {
    this.saving = true;

    this.znodeService.updateAddon(this.addon).subscribe(
      response => {
        this.addon = response;
        this.unchanged = JSON.stringify(this.addon);

        this.alertService.success('Add-On Updated Successfully');
        this.saving = false;
      },
      error => {
        this.alertService.error(error);
        this.saving = false;
      }
    )
  }

  cancel() {
    this.router.navigate(['/ecommerce/merchandise/addons']);
  }

  select(addonValueId: number) {
    this.selectedId = addonValueId;
    this.state = 'detail';
    this.location.go('/ecommerce/merchandise/addons/'+this.addon.AddOnId+'/values/'+this.selectedId);
  }

  create(){
    this.breadcrumbs.push({title: 'New', path: ''});
    this.state = 'new';
    this.location.go('/ecommerce/merchandise/addons/'+this.addon.AddOnId+'/values/add');
  }

  listView() {
    this.state = 'list';
    this.breadcrumbs.pop();
  }

  setDefaultStockMsg() {
    if (!this.addon.OutOfStockMessage) {
      this.addon.OutOfStockMessage = "Out of Stock";
    }

    if (this.addon.AllowBackOrder && !this.addon.BackOrderMessage) {
      this.addon.BackOrderMessage = "On Backorder";
    }
  }

  public changedTab(tabSlug: string){
    this.state = 'list';

    this.location.go('/ecommerce/merchandise/addons/'+this.addon.AddOnId+'/'+tabSlug);

    while(this.breadcrumbs.length > 2){
      this.breadcrumbs.pop();
    }

    this.breadcrumbs.push({title: this.capitalizeFirst(tabSlug), path: '/ecommerce/merchandise/addons/'+this.addon.AddOnId+'/'+tabSlug});
  }

  private addBreadcrumb(title: string) {
    this.breadcrumbs.push({title: title, path: ''});
  }

  private capitalizeFirst(str: string): string {
    return str.replace('-',' ').replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  }

  public onBlur(event: any) {
    event.target.value = event.target.value.trim();
  }

  ngOnDestroy() {
    if (this.routerSub != null) {
      this.routerSub.unsubscribe();
    }
  }
}
