import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { Observable, forkJoin } from 'rxjs';

import { AlertService } from '../../../../services/alert.service';
import { ZnodeService } from '../../../../services/znode.service';

import { Domain } from '../../models/domain';
import { Portal } from '../../models/portal';

@Component({
  selector: 'cb-domain-details',
  templateUrl: './domain-details.component.html'
})
export class DomainDetailsComponent implements OnInit {

  @Input() portal: Portal;
  @Input() domainId: number;

  @Output() onExit: EventEmitter<any> = new EventEmitter();

  public domain: Domain;
  public loading: boolean = false;

  constructor(private alertService: AlertService, private znodeService: ZnodeService) {}

  ngOnInit() {
    this.znodeService.getDomain(this.domainId).subscribe(
      domain => this.domain = domain,
      err => this.alertService.error(err)
    )
  }

  save() {
    this.loading = true;
    this.znodeService.updateDomain(this.domain).subscribe(
      res => {
        console.log(res);
        this.loading = false;
        this.alertService.success('Domain Updated');
        this.cancel();
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    );
  }

  cancel() {
    this.onExit.emit();
  }
}
