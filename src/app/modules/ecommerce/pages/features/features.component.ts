import { Component, OnInit } from '@angular/core';
import { ZnodeService } from '../../../../services/znode.service';
import { DomainService } from '../../../../services/domain.service';
import { AlertService } from '../../../../services/alert.service';
import { CodaService } from '../../../../services/coda.service';
import { ConfigSetting } from '../../models/config-setting';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html'
})
export class FeaturesComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Features',
      path: ''
    }
  ];

  public configSetting: ConfigSetting;
  public loading = false;

  constructor(private znodeService: ZnodeService, private domain: DomainService, private alertService: AlertService, private codaService: CodaService) { }

  ngOnInit() {
    this.configSetting = new ConfigSetting();

    // init bid config
    this.initBidConfigSetting();

    this.znodeService.getConfigSettingByBID(this.configSetting.BID).subscribe(
      data => {
        if (data != null) {
          this.configSetting = Object.assign(new ConfigSetting(), data);
          console.log('Get ConfigSetting ', this.configSetting);
          // update sidebar
          this.znodeService.setConfigSetting(this.configSetting);
        }
      },

      err => {
        this.alertService.error(err);
      }
    );
  }

  /**
   * Initalized the current BID ConfigSetting for future reference
   */
  initBidConfigSetting() {
    this.configSetting.BID = this.domain.bid;
  }

  onLoaded() {
    this.loading = false;
  }

  /**
   * Toggle features
   * @param e event
   */
  changeStatus(e) {

    let property = e.target.dataset.field;

    // toggle config setting
    this.configSetting[property] = !this.configSetting[property] ? true : false;

    // save setting
    this.saveConfigSetting(this.configSetting);

    // update sidebar
    this.znodeService.setConfigSetting(this.configSetting);

    if (property == "EnableCustomersCanvas" && this.configSetting[property] == true) {
      this.requestEndorBID();

      this.createStandardEditorPreset();
      this.createStandardTemplatePreset();
    }
  }

  // save config setting for specific portal
  saveConfigSetting(configSetting: ConfigSetting) {
    this.znodeService.saveConfigSetting(configSetting).subscribe(
      data => {
        console.log('Saved ConfigSetting', data);
      },
      err => {
        this.alertService.error(err);
      }
    );
  }

  requestEndorBID() {
    this.znodeService.requestEndorBID(this.configSetting.BID).subscribe(
      data => {
        if (data.HasError == false) {
          console.log("EndorBID requested successfully.");
        } else {
          this.alertService.error(data.ErrorMessage);
        }
      },
      err => {
        this.alertService.error(err);
      }
    );
  }

  createStandardEditorPreset() {
    this.codaService.createStandardEditorPreset().subscribe(
      data => {
        console.log("Standard Editor Preset created successfully.")
      },
      err => {
        this.alertService.error(err);
      }
    );
  }

  createStandardTemplatePreset() {
    this.codaService.createStandardTemplatePreset().subscribe(
      data => {
        console.log("Standard Template Preset created successfully.")
      },
      err => {
        this.alertService.error(err);
      }
    );
  }

}
