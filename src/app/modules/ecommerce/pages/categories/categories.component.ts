import { Component } from '@angular/core';

import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html'
})
export class CategoriesComponent {

  public breadcrumbs = [
    {
      title: 'Collections',
      path: '/ecommerce/merchandise/collections/'
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) { }

  onLoaded(){
    this.loading = false;
  }
}
