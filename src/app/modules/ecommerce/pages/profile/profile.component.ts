import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../../../services/auth.service';
import { AlertService } from '../../../../services/alert.service';

import { User } from '../../models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Profile'
    }
  ];

  public user: any = {};

  constructor(private authService: AuthService, private alertService: AlertService) { }

  ngOnInit() {
    this.user = this.authService.getUser();
    console.log(this.user);
  }
}
