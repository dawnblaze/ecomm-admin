import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.less']
})
export class NotFoundComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Page Not Found'
    }
  ];

  constructor() {}

  ngOnInit() {
  }

}
