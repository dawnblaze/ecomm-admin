import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { ShippingMethod } from '../../models/shipping-method';
import { ShippingRule } from '../../models/shipping-rule';
import { Profile } from '../../models/profile';
import { Country } from '../../models/country';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'cb-shipping-method-new',
  templateUrl: './shipping-method-new.component.html'
})
export class ShippingMethodNewComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Shipping Methods',
      path: '/ecommerce/shipping-methods'
    },
    {
      title: 'New'
    }
  ];

  public loading: boolean = true;
  public shippingOption: ShippingMethod = new ShippingMethod();
  private unchanged: string;
  public profiles: Profile[];
  public countries: Country[];

  // Validations
  public shipCodeUnique: boolean = true;
  public shipCodeInvalid: boolean = false;


  constructor(private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService, private router: Router, private themeService: ThemeService) { }



  ngOnInit() {

    this.setDefaultDestinationCountry();

      forkJoin(
        this.znodeService.getProfiles(),
        this.znodeService.getCountries()
      ).subscribe(
        data => {
          this.profiles = data[0];
          this.countries = data[1];
          this.unchanged = JSON.stringify(this.shippingOption);
          this.onLoaded();
        },
        err => this.alertService.error(err)
      );
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.shippingOption) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  onLoaded(){
    this.loading = false;
  }

  save() {

    if(this.shipCodeInvalid || !this.shipCodeUnique){
      return;
    }

    if(this.shippingOption.CountryCode == 0){
      this.shippingOption.CountryCode = null;
    }

    this.znodeService.createShippingMethod(this.shippingOption).subscribe(
      data => {
        this.shippingOption = data;
        this.unchanged = JSON.stringify(this.shippingOption);

        var batch: any[] = [];
        var shippingRules: ShippingRule[] = [];

        for(var i = 0; i < 4; i++){
          shippingRules[i] = new ShippingRule();
          shippingRules[i].ShippingOptionId = data.ShippingId;
          shippingRules[i].ShippingRuleTypeId = i;

          batch.push(this.znodeService.createShippingRule(shippingRules[i]));
        }

        forkJoin(batch).subscribe(
          rules => {
            console.log(rules);
            this.cancel();
          },
          err => this.alertService.error(err)
        );

      },
      err => this.alertService.error(err)
    );
  }

  cancel() {
    this.router.navigate(['/ecommerce/shipping-methods']);
  }

  isNumber(e){
    return (e != null && !isNaN(e) && e.toString() !== '');
  }

  setDefaultDestinationCountry(){

    this.shippingOption.CountryCode = "US";

  }


   /**
   * @method checkIfValidCharacters
   * @param stringToCheck target.value to test against.
   * @returns true/false
   */
  private checkIfValidCharacters(stringToCheck: string):boolean{
    if(!(/^[\w\d_.-]+$/.test(stringToCheck)) === true) {
      this.shipCodeInvalid = true;
      return true;
    }
    return false;
  }

  /**
   * @method checkIfUniqueValue
   * @param e e.target.value to check
   * 
   */
  public checkIfUniqueValue(e){
    
    // check Product Internal Code against other Values.
    console.log('Target: ', e.target.value)
    this.shipCodeUnique = true;
    this.shipCodeInvalid = false;

    if(!this.checkIfValidCharacters(e.target.value)){
      let shipCodeList = this.znodeService.getShippingMethods().subscribe((data: ShippingMethod[])=>{
        for(let sm of data){
          if(sm.ShippingCode === e.target.value){
            this.shipCodeUnique = false;
            return true;
          }
        }
      })
    }
  }

}
