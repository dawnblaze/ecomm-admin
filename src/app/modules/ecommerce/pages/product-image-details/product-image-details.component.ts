import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Observable, forkJoin }     from 'rxjs';
import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { AuthService } from '../../../../services/auth.service';

import { Product } from '../../models/product';
import { ProductImage } from '../../models/product-image';
import { ProductImageType } from '../../models/product-image-type';

@Component({
  selector: 'cb-product-image-details',
  templateUrl: './product-image-details.component.html'
})
export class ProductImageDetailsComponent implements OnInit {

  @Input() product: Product;
  @Input() productImageId: number;

  @Output() onExit: EventEmitter<any> = new EventEmitter();
  @Output() onLoad: EventEmitter<string> = new EventEmitter();

  public productImage: ProductImage;
  public productImageTypes: ProductImageType[];
  public title: string;

  public loading: boolean = false;

  private imageRoot: string;

  constructor(private alertService: AlertService, private znodeService: ZnodeService, private authService: AuthService) {}

  ngOnInit() {
    this.imageRoot = this.znodeService.base;

    forkJoin(
      this.znodeService.getProductImage(this.productImageId),
      this.znodeService.getProductImageTypes()
    ).subscribe(
      data => {
        this.productImage = data[0];
        this.productImageTypes = data[1];

        this.title = this.productImage.Name;
        this.onLoad.emit(this.title);

        let user: any = this.authService.getUser();
        this.productImage.UserName = user.UserName;
      },
      err => {
        this.alertService.error(err);
      }
    );
  }

  public save() {
    this.loading = true;

    this.znodeService.updateProductImage(this.productImage).subscribe(
      image => {
        this.alertService.success('Product Image Updated');
        this.loading = false;
        this.cancel();
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    );
    
  }

  public cancel() {
    this.onExit.emit();
  }

}
