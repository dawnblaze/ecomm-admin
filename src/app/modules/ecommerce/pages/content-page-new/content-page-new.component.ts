import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Observable, forkJoin } from 'rxjs';

import { AlertService } from '../../../../services/alert.service';
import { ZnodeService } from '../../../../services/znode.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

import { ContentPage } from '../../models/content-page';
import { Portal } from '../../models/portal';
import { Theme } from '../../models/theme';

@Component({
  selector: 'cb-content-page-new',
  templateUrl: './content-page-new.component.html'
})
export class ContentPageNewComponent implements OnInit {

  @Input() portal: Portal;
  @Output() onExit: EventEmitter<any> = new EventEmitter();

  public page: ContentPage = new ContentPage();
  private unchanged: string;
  public themes: Theme[] = [];
  public title: string;
  public loading: boolean = true;
  public seoUrlInvalid:boolean;

  constructor(private alertService: AlertService, private znodeService: ZnodeService, private modalService: ModalService, private themeService: ThemeService) {}

  ngOnInit() {

    this.znodeService.getThemes().subscribe(
      themes => {
        this.themes = themes;
        this.page.ThemeID = 0;
        this.page.PortalID = this.portal.PortalId;
        this.unchanged = JSON.stringify(this.page);
        this.loading = false;
      },
      err => this.alertService.error(err)
    );

  }

  public canDeactivate(): Observable<boolean> | boolean {
    return (this.unchanged == JSON.stringify(this.page));
  }

  save() {
    if(this.loading){
      return;
    }

    this.loading = true;

    this.znodeService.getContentPages(this.portal.PortalId).subscribe(
      pages => {
        let duplicates = pages.filter(page => page.Name.toLowerCase() == this.page.Name.toLowerCase());

        if(duplicates.length > 0){
          this.alertService.error('Store Info named "'+this.page.Name+'" already exists');
          this.loading = false;
        }
        else {
          if(this.page.ThemeID == 0){
            delete this.page.ThemeID;
          }

          console.log(this.page);
          this.znodeService.createContentPage(this.page).subscribe(
            res => {
              console.log(res);
              this.page = res;
              this.unchanged = JSON.stringify(this.page);

              this.loading = false;
              this.alertService.success('Store Info Created');
              this.cancel();
            },
            err => {
              this.alertService.error(err);
              this.loading = false;
            }
          );
        }
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    );
  }

  cancel() {
    if (this.canDeactivate()) {
      this.onExit.emit();
    }
    else {
        this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay')
        .subscribe((res) => {
            if (res) {
                this.onExit.emit();
            }
        });
    }
  }

  public onBlur(event: any, variableName: string = '') {
    event.target.value = event.target.value.trim();
    if (variableName == 'SeoUrl') {
      event.target.value = event.target.value.replace(/\s+/g, '-');
      if(event.target.value !== ''){
        forkJoin(
          this.znodeService.isSeoUrlExist(0, event.target.value)
        ).subscribe(
          data => {
            this['seoUrlInvalid'] = data[0].IsExist;
          },
          error => {
            this.alertService.error(error);
            this.loading = false;
          }
        );
      }
    }
  }
}
