import { Component, OnInit } from '@angular/core';

import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'app-shipping-methods',
  templateUrl: './shipping-methods.component.html'
})
export class ShippingMethodsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Shipping Methods'
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) { }

  ngOnInit() {
  }

  onLoaded(){
    this.loading = false;
  }

}
