import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

import { Review } from '../../models/review';

@Component({
  selector: 'app-review-details',
  templateUrl: './review-details.component.html'
})
export class ReviewDetailsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Customer Reviews',
      path: '/ecommerce/reviews'
    },
    {
      title: 'Edit Review'
    }
  ];

  public review: Review;
  public active: boolean;
  private unchanged: string;
  public title: string;

  public loading: boolean = false;

  constructor(private route: ActivatedRoute, private znodeService: ZnodeService, private modalService: ModalService, private router: Router, private themeService: ThemeService) {}

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let id: number = params['id'];
      this.getReview(id);
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.review) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  private getReview(reviewId: number) {
    this.znodeService.getReview(reviewId).subscribe(review => {
      this.review = review;
      this.active = (review.Status == 'A')? true: false;
      this.unchanged = JSON.stringify(this.review);
    });
  }

  public save() {
    this.loading = true;
    this.review.Status = (this.active)? 'A': 'N';
    this.znodeService.updateReview(this.review).subscribe(
      res => {
        this.review = res;
        this.unchanged = JSON.stringify(this.review);

        this.loading = false;
      },
      err => {
        this.loading = false;
      }
    );
  }

  public cancel() {
    this.router.navigate(['/ecommerce/reviews']);
  }

}
