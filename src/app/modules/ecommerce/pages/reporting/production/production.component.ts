import { Component, OnInit } from '@angular/core';

import { ReportsListComponent } from '../../../components/reports-list/reports-list.component';

@Component({
  selector: 'cb-reporting-production',
  templateUrl: './production.component.html'
})
export class ReportingProductionComponent implements OnInit {
  public reportString: string = "Production";

  public breadcrumbs = [
    {
      title: 'Reports'
    },
    {
      title: 'Production Reports'
    }
  ];

  constructor() {}

  ngOnInit() {
  }

}