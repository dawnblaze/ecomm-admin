import { Component } from '@angular/core';

import { ReportsListComponent } from '../../../components/reports-list/reports-list.component';

@Component({
  selector: 'cb-reporting-sales',
  templateUrl: './sales.component.html'
})
export class ReportingSalesComponent {
  public reportString: string = "Sales";
  public breadcrumbs = [
    {
      title: 'Reports'
    },
    {
      title: 'Sales Reports'
    }
  ];

  constructor() {}

}
