import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cb-reporting',
  templateUrl: './reporting.component.html'
})
export class ReportingComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Reporting'
    }
  ];

  constructor() {}

  ngOnInit() {
  }

}
