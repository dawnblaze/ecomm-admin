import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'cb-reporting-company_financial',
  templateUrl: './company_financial.component.html'
})
export class ReportingCompanyFinancialComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Reports'
    },
    {
      title: 'Company/Financial Reports'
    }
  ];

  constructor() {}

  ngOnInit() {
  }

}