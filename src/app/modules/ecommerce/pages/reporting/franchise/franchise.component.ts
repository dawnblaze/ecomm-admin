import { Component, OnInit } from '@angular/core';

import { ReportsListComponent } from '../../../components/reports-list/reports-list.component';

@Component({
  selector: 'cb-reporting-franchise',
  templateUrl: './franchise.component.html'
})
export class ReportingFranchiseComponent implements OnInit {
  public reportString: string = "Franchise";

  public breadcrumbs = [
    {
      title: 'Reports'
    },
    {
      title: 'Franchise Reports'
    }
  ];

  constructor() {}

  ngOnInit() {
  }

}