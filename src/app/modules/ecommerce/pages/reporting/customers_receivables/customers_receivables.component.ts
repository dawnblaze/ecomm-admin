import { Component } from '@angular/core';

import { ReportsListComponent } from '../../../components/reports-list/reports-list.component';

@Component({
  selector: 'cb-reporting-customers_receivables',
  templateUrl: './customers_receivables.component.html'
})
export class ReportingCustomersReceivablesComponent {
  public reportString: string = "Customers";

  public breadcrumbs = [
    {
      title: 'Reports'
    },
    {
      title: 'Customers Reports'
    }
  ];

  constructor() {}
}