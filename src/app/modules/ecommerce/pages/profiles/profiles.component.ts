import { Component, OnInit } from '@angular/core';

import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html'
})
export class ProfilesComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Profiles'
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) { }

  ngOnInit() {
  }

  onLoaded(){
    this.loading = false;
  }

}
