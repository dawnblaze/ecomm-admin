import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

import { Profile } from '../../models/profile';

@Component({
  selector: 'app-profile-details',
  templateUrl: './profile-details.component.html'
})
export class ProfileDetailsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Profiles',
      path: '/ecommerce/profiles'
    },
    {
      title: ''
    }
  ];

  public profile: Profile;
  private unchanged: string;
  public title: string;

  public loading: boolean = false;

  constructor(private route: ActivatedRoute, private alertService: AlertService, private modalService: ModalService, private znodeService: ZnodeService, private router: Router, private themeService: ThemeService) {}

  ngOnInit() {
    var profileId: number;

    this.route.params.forEach((params: Params) => {
      profileId = +params['profile'];
    });

    this.znodeService.getProfile(profileId).subscribe(
      profile => {
        this.profile = profile;
        this.unchanged = JSON.stringify(this.profile);
        this.title = this.profile.Name;
        this.breadcrumbs[this.breadcrumbs.length-1].title = this.title;
      },
      err => {
        this.alertService.error(err);
      }
    )
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.profile) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  public save() {
    this.loading = true;
    this.znodeService.updateProfile(this.profile).subscribe(
      profile => {
        this.profile = profile;
        this.unchanged = JSON.stringify(this.profile);

        this.alertService.success('Profile Updated Successfully');
        this.loading = false;
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    )
  }

  public cancel() {
    this.router.navigate(['/ecommerce/profiles']);
  }

}
