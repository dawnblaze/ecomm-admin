import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { Observable, forkJoin }     from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Product } from '../../models/product';
import { ProductTier } from '../../models/product-tier';
import { Profile } from '../../models/profile';

@Component({
  selector: 'cb-product-tier-details',
  templateUrl: './product-tier-details.component.html'
})
export class ProductTierDetailsComponent implements OnInit {

  @Input() productTierId: number;
  @Input() product: Product;

  @Output() onExit: EventEmitter<any> = new EventEmitter();
  @Output() onLoad: EventEmitter<string> = new EventEmitter();

  public tier: ProductTier;
  public tiers: ProductTier[];
  public profiles: Profile[];
  public title: string;

  public loading: boolean = false;

  constructor(private alertService: AlertService, private znodeService: ZnodeService) {}

  ngOnInit() {

    forkJoin(
      this.znodeService.getProductTiers(this.product.ProductId),
      this.znodeService.getProfiles()
    ).subscribe(
      data => {
        this.tiers = data[0];
        var tempTier = this.tiers.filter(tier => tier.ProductTierId == this.productTierId);
        this.tier = tempTier[0];

        this.profiles = data[1];
        var selected = this.profiles.filter(profile => profile.ProfileId == this.tier.ProfileId);
        if(selected.length > 0){
          this.title = selected[0].Name;
        }
        else{
          this.title = this.tier.ProfileName || "All Storefronts";
        }
        this.onLoad.emit(this.title);
      },
      err => this.alertService.error(err)
    )

  }

  public save() {
    if(this.loading){
      return;
    }

    this.loading = true;
    if(this.tier.ProfileId > 0){
      let selectedProfile = this.profiles.filter(profile => profile.ProfileId == this.tier.ProfileId)[0];
      this.tier.ProfileName = selectedProfile.Name;
    }
    else {
      this.tier.ProfileId = 0;
      this.tier.ProfileName = "All Storefronts";
    }

    // check validation
    if(isNaN(this.tier.Price)){
      this.alertService.error('Price field requires a number.')
      this.loading = false;
      return false;
    }

    if(isNaN(this.tier.TierStart)){
      this.alertService.error('Tier Start requires a number.')
      this.loading = false;
      return false;
    }

    if(isNaN(this.tier.TierEnd)){
      this.alertService.error('Tier End requires a number.')
      this.loading = false;
      return false;
    }
    
    console.log(this.tier);
    this.znodeService.updateProductTier(this.tier).subscribe(
      data => {
        this.alertService.success('Product Tier Updated Successfully');
        this.loading = false;
        this.cancel();
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    )

  }

  public cancel() {
    this.onExit.emit();
  }

  isNumber(e){
    return (e != null && !isNaN(e) && e.toString() !== '');
  }

}
