import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  ElementRef
} from "@angular/core";

import { Observable } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { untilDestroyed } from "ngx-take-until-destroy";

import { AlertService } from "../../../../services/alert.service";
import { ZnodeService } from "../../../../services/znode.service";
import { ModalService } from "../../../../services/modal.service";
import { AutoSaveBarState } from "../../../corebridge/components/autosave-bar/autosave-bar.component";

import { ContentPage } from "../../models/content-page";
import { Portal } from "../../models/portal";
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl
} from "@angular/forms";

@Component({
  selector: "cb-content-page-details",
  templateUrl: "./content-page-details.component.html",
  styleUrls: ["./content-page-details.component.less"]
})
export class ContentPageDetailsComponent implements OnInit, OnDestroy {
  @Input() portal: Portal;
  @Input() pageId: number;
  @Input() refresh: Observable<void>;

  @Output() onExit: EventEmitter<any> = new EventEmitter();
  @Output() onLoad: EventEmitter<string> = new EventEmitter();

  public page: ContentPage = new ContentPage();
  private unchanged: string;
  public pageTitle: string;
  public loading: boolean = true;
  public seoUrlInvalid: boolean;
  public displayDialog: boolean = false;
  public saveState: AutoSaveBarState = AutoSaveBarState.none;
  public pendingSave: boolean;

  // Tracks the value and validity state of a group of FormControl instances.
  contentPageForm: FormGroup;

  constructor(
    private alertService: AlertService,
    private znodeService: ZnodeService,
    private modalService: ModalService,
    private fb: FormBuilder,
    private elementRef: ElementRef
  ) {}

  ngOnInit() {
    // Create a content page form using FormBuilder
    // Control naming format is followed from the class ContentPage
    // since we will be mapping these with this.page when we will do the saving.
    this.contentPageForm = this.fb.group({
      Title: ["", Validators.required],
      SEOTitle: ["", Validators.required],
      SEOMetaDescription: [""],
      SEOURL: [
        "",
        Validators.compose([Validators.required])
        // Validators.composeAsync([this.seoUrlExists.bind(this)]) -- prevent isSeoUrlExist check for every key input
      ],
      Html: [""]
    });

    this.contentPageForm.valueChanges
      .pipe(debounceTime(500), distinctUntilChanged(), untilDestroyed(this))
      .subscribe(values => {
        let html = this.contentPageForm.get("Html");
        if (html.dirty) {
          let seoDescription = this.contentPageForm.get("SEOMetaDescription");
          if (!this.pageId && !seoDescription.dirty)
            seoDescription.setValue(
              html.value
                .replace(/<[^>]+>/gm, "")
                .replace(/&nbsp;/gi, " ")
                .substring(0, 160)
            );
          else if (!html.value) seoDescription.setValue("");
        }
        if (
          this.saveState == AutoSaveBarState.invalid &&
          this.contentPageForm.valid
        )
          this.saveState = AutoSaveBarState.none;
      });
  }

  ngOnDestroy() {}

  public canDeactivate(): Observable<boolean> | boolean {
    return this.unchanged == JSON.stringify(this.page);
  }

  save() {
    if (this.saveState == AutoSaveBarState.validating) this.pendingSave = true;
    if (this.saveState !== AutoSaveBarState.none) return;
    // Since we are using reactive forms now, then we can check if the content page form
    // is in a valid state
    if (this.contentPageForm.valid) {
      this.saveState = AutoSaveBarState.saving;

      // We will get the values in our content registration form and we will pass it to the
      // user object by using Object.assign which copies the values of all
      // enumerable own properties from one or more source objects to a
      // target object. But, since we don't need to map all properties of the this.page
      // object, then we cannot use Object.assign() and individually assign the properties
      // manually.
      // this.page = Object.assign({}, this.contentPageForm.value);

      this.page.Title = this.contentPageForm.controls.Title.value as string;
      this.page.SEOTitle = this.contentPageForm.controls.SEOTitle
        .value as string;
      this.page.SEOMetaDescription = this.contentPageForm.controls
        .SEOMetaDescription.value as string;
      this.page.SEOURL = this.contentPageForm.controls.SEOURL.value as string;
      this.page.Name = this.page.SEOURL;
      this.page.Html = this.contentPageForm.controls.Html.value as string;

      if (this.page.ThemeID == 0) {
        delete this.page.ThemeID;
      }

      if (this.pageId) {
        this.znodeService
          .updateContentPage(this.page)
          .pipe(untilDestroyed(this))
          .subscribe(
            res => {
              this.page = res;
              this.unchanged = JSON.stringify(this.page);

              this.alertService.success("Store Info Updated");
              this.closeDialog();
            },
            err => {
              this.alertService.error(err);
              this.saveState = AutoSaveBarState.invalid;
            }
          );
      } else {
        this.znodeService
          .createContentPage(this.page)
          .pipe(untilDestroyed(this))
          .subscribe(
            res => {
              this.page = res;
              this.unchanged = JSON.stringify(this.page);

              this.alertService.success("Store Info Created");
              this.closeDialog();
            },
            err => {
              this.alertService.error(err);
              this.saveState = AutoSaveBarState.invalid;
            }
          );
      }
    } else {
      this.saveState = AutoSaveBarState.invalid;
      this.contentPageForm.get("Title").markAsTouched();
      this.contentPageForm.get("SEOTitle").markAsTouched();
      this.contentPageForm.get("SEOURL").markAsTouched();
    }
  }

  cancel() {
    if (this.canDeactivate()) {
      this.closeDialog();
    } else {
      this.modalService
        .warning(
          "You Have Unsaved Changes",
          "Leaving without saving will discard any changes you have made. Are you sure you want to leave?",
          "Discard and Leave",
          "Stay"
        )
        .pipe(untilDestroyed(this))
        .subscribe(res => {
          if (res) {
            this.closeDialog();
          }
        });
    }
  }

  // Check if the seo url already exists.
  seoUrlExists(seoUrl: string) {
    // Avoids initial check against an empty string
    if (seoUrl.length) {
      this.saveState = AutoSaveBarState.validating;
      this.znodeService
        .isSeoUrlExist(this.page.ContentPageID || 0, seoUrl)
        .pipe(untilDestroyed(this))
        .subscribe(
          data => {
            this.saveState = AutoSaveBarState.none;
            if (data.IsExist)
              this.contentPageForm
                .get("SEOURL")
                .setErrors({ seoUrlAlreadyExists: true });
            else this.contentPageForm.get("SEOURL").setErrors(null);
            if (this.pendingSave) {
              this.pendingSave = false;
              if (data.IsExist) this.saveState = AutoSaveBarState.invalid;
              else this.save();
            }
          },
          error => {
            this.alertService.error(error);
            this.saveState = AutoSaveBarState.none;
            this.pendingSave = false;
          }
        );
    }
  }
  setUrlHandle(seoUrl: string) {
    if (seoUrl) {
      seoUrl = seoUrl
        .toLowerCase()
        .replace(/[^a-zA-Z0-9-]/g, "-");

      do {
        seoUrl = seoUrl.replace(/-{2}/g, "-");
      } while (seoUrl.indexOf("--") > 0);

      this.contentPageForm.get("SEOURL").setValue(seoUrl);
    }
  }

  public onBlur(event: any, variableName: string = "") {
    if (variableName == "Title") {
      event.target.value = event.target.value.trim();
      if (!this.pageId) {
        this.contentPageForm.get("SEOTitle").setValue(event.target.value);
        this.setUrlHandle(event.target.value);
        this.contentPageForm.get("SEOURL").markAsTouched();
        this.seoUrlExists(event.target.value);
      }
    } else if (
      variableName == "SEOTitle" &&
      event.target.value.trim().length == 0
    ) {
      this.contentPageForm
        .get("SEOTitle")
        .setValue(this.contentPageForm.get("Title").value);
    } else if (variableName == "SEOURL") {
      if (event.target.value.trim().length == 0)
        this.setUrlHandle(this.contentPageForm.get("Title").value);
      else
        event.target.value = event.target.value
          .replace(/[^a-zA-Z0-9-]/g, "-")
          .toLowerCase();
      this.seoUrlExists(event.target.value);
    }
  }

  showDialog(pageId: number = null) {
    this.loading = true;
    this.pageId = pageId;
    this.contentPageForm.reset();
    this.saveState = AutoSaveBarState.none;
    this.pendingSave = false;
    if (pageId) {
      this.znodeService
        .getContentPage(this.pageId)
        .pipe(untilDestroyed(this))
        .subscribe(
          data => {
            this.page = data;

            // Set the form values
            // .patchValue can also be used here but since we are assign values to
            // ALL controls in the form group, then we will use .setValue
            this.contentPageForm.setValue({
              Title: this.page.Title,
              SEOTitle: this.page.SEOTitle,
              SEOMetaDescription: this.page.SEOMetaDescription,
              SEOURL: this.page.SEOURL,
              Html: this.page.Html
            });

            this.pageTitle = this.page.Title;
            this.onLoad.emit(this.pageTitle);

            if (this.page.ThemeID == null) {
              this.page.ThemeID = 0;
            }
            this.unchanged = JSON.stringify(this.page);
            this.displayDialog = true;
            setTimeout(() => {
              this.loading = false;
            }, 500);
          },
          err => this.alertService.error(err)
        );
    } else {
      this.pageTitle = "New Store Info Page";
      this.page = new ContentPage();
      this.page.ThemeID = 0;
      this.page.PortalID = this.portal.PortalId;
      this.unchanged = JSON.stringify(this.page);
      this.displayDialog = true;
      setTimeout(() => {
        this.loading = false;
      }, 500);
    }
  }

  private closeDialog() {
    this.saveState = AutoSaveBarState.none;
    this.displayDialog = false;
    this.onExit.emit();
  }
}
