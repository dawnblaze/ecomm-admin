import { Component, OnInit } from '@angular/core';

import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html'
})
export class ReviewsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Customer Reviews',
      path: 'ecommerce/reviews/'
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) {}

  ngOnInit() {
  }

  onLoaded(){
    this.loading = false;
  }

}
