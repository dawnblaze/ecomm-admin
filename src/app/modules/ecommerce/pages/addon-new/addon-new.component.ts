import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

import { Addon } from '../../models/addon';
import { Alert } from '../../../corebridge/models/alert';

@Component({
  selector: 'app-addon-new',
  templateUrl: './addon-new.component.html'
})
export class AddonNewComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Add-Ons',
      path: '/ecommerce/merchandise/addons'
    },
    {
      title: 'New'
    }
  ];

  public addon: Addon = new Addon ();
  private unchanged: string;
  public loading: boolean = true;

  constructor(private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService, private router: Router, private location: Location, private themeService: ThemeService) { }

  ngOnInit() {
    this.loading = false;
    this.addon.AllowBackOrder = true;

    this.setDefaultStockMsg();

    this.unchanged = JSON.stringify(this.addon);
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.addon) ){
      return true;
    }
    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  save() {
    this.setDefaultStockMsg();

    this.loading = true;
    this.znodeService.createAddOn(this.addon).subscribe(
      response => {
        this.alertService.success('Add-On Created Successfully');

        this.addon = response;
        this.unchanged = JSON.stringify(this.addon);

        this.loading = false;
        this.router.navigate(['/ecommerce/merchandise/addons/' + response.AddOnId + '/values']);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    )
  }

  cancel() {
    this.router.navigate(['/ecommerce/merchandise/addons']);
  }

  setDefaultStockMsg() {
    if (!this.addon.TrackInventory || !this.addon.OutOfStockMessage) {
      this.addon.OutOfStockMessage = "Out of Stock";
    }

    if (!this.addon.TrackInventory ||
      (this.addon.AllowBackOrder && !this.addon.BackOrderMessage)) {
      this.addon.BackOrderMessage = "On Backorder";
    }
  }

  public onBlur(event: any) {
    event.target.value = event.target.value.trim();
  }
}
