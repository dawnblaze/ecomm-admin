import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { Observable, forkJoin } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { Catalog } from '../../models/catalog';
import { DetailedError } from '../../../../modules/corebridge/models/alert';
import { AutoSaveBarState } from '../../../corebridge/components/autosave-bar/autosave-bar.component';

@Component({
  selector: 'app-catalog-new',
  templateUrl: './catalog-new.component.html',
  styleUrls: ['../catalog-details/catalog-details.component.less']
})
export class CatalogNewComponent implements OnInit, OnDestroy {

  public breadcrumbs = [
    {
      title: 'Catalogs',
      path: '/ecommerce/merchandise/catalogs'
    },
    {
      title: 'New'
    }
  ];

  public catalog: Catalog = new Catalog();
  public title: string;
  public error: string;

  public loading: boolean = false;
  private saved: boolean = false;
  public saveState: AutoSaveBarState = AutoSaveBarState.none;

  detailedErrors: DetailedError[] = [];

  constructor(private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService, private router: Router, private elementRef: ElementRef) { }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.alertService.detailedError('catalogDetails',[]);
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.catalog.Name == null || this.saved ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  public save() {
    if(!this.catalog.Name || !this.catalog.Name.trim().length) {
      this.saveState = AutoSaveBarState.invalid;
      this.elementRef.nativeElement.querySelector('input.ng-invalid').focus();
      return;
    }

    this.loading = true;
    this.saveState = AutoSaveBarState.saving;
    this.catalog.Name = this.catalog.Name.trim();
    this.znodeService.createCatalog(this.catalog).pipe(untilDestroyed(this)).subscribe(res => {
      console.log(res);
      this.saved = true;

      this.loading = false;
      this.saveState = AutoSaveBarState.saved;
      this.router.navigate(['/ecommerce/merchandise/catalogs/']);
    });
  }

  public cancel() {
    this.router.navigate(['/ecommerce/merchandise/catalogs']);
  }

  private onBlur(event: any) {
    this.detailedErrors = [];
    if(!event.target.value.trim().length) {
      this.detailedErrors.push({
        ID: this.catalog.CatalogId,
        Name: this.catalog.Name,
        FieldName: 'Catalog Title',
        ErrorMessage: 'This field is required'
      });
    }
    this.alertService.detailedError('catalogDetails',this.detailedErrors);
    if(this.saveState == AutoSaveBarState.invalid && this.detailedErrors.length == 0)
      this.saveState = AutoSaveBarState.none;
  }

}
