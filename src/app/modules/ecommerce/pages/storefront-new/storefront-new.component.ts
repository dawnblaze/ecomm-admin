import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Observable, forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { untilDestroyed } from 'ngx-take-until-destroy';

import { ZnodeService } from '../../../../services/znode.service';
import { CoreBridgeService } from '../../../../services/corebridge.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { Portal } from '../../models/portal';
import { Catalog } from '../../models/catalog';
import { PortalCatalog } from '../../models/portal-catalog';
import { Image } from '../../models/image';
import { CSS } from '../../models/css';
import { CesaService, MisTypes } from "../../../../services/cesa.service";

import { DetailedError } from '../../../../modules/corebridge/models/alert';
import { AutoSaveBarState } from '../../../corebridge/components/autosave-bar/autosave-bar.component';

@Component({
  selector: 'app-storefront-new',
  templateUrl: './storefront-new.component.html'
})
export class StorefrontNewComponent implements OnInit, AfterViewInit, OnDestroy {

  public breadcrumbs = [
    {
      title: 'My Stores',
      path: '/ecommerce/storefronts'
    },
    {
      title: 'New',
      path: '/ecommerce/storefronts/Add'
    }
  ];

  public portal: Portal;
  private unchanged: string;
  public catalogs: Catalog[];
  public catalog: Catalog;
  public catalogId: number;
  public styles: CSS[];
  public stylesheetId: number;
  public loading: boolean = false;
  public total: number = 0;
  public limit: number = 0;
  private saved: boolean = false;
  private misType: number;
  public saveState: AutoSaveBarState = AutoSaveBarState.none;
  public validationErrors: DetailedError[] = [];

  public fileManagementEnabled:boolean = false;
  public enableFileUploads:boolean = false;

  @ViewChild('cbForm') ngForm: NgForm;

  constructor(
    private znodeService: ZnodeService,
    private corebridgeService: CoreBridgeService,
    private cesaService: CesaService,
    private alertService: AlertService,
    private modalService: ModalService,
    private router: Router,
    private elementRef: ElementRef
  ) { }

  ngOnInit() {
    this.portal = new Portal();
    // Default values required by the API
    this.portal.ImageNotAvailablePath = "~data/default/images/noimage.gif";
    this.portal.MaxCatalogDisplayColumns = 4;
    this.portal.MaxCatalogDisplayItems = 12;
    this.portal.MaxCatalogCategoryDisplayThumbnails = 5;
    this.misType = this.cesaService.getMisType();
    this.znodeService.getCatalogs()
    .subscribe(
      catalogs => {
        this.catalogs = catalogs;
        this.catalog = this.catalogs[0];

        this.unchanged = JSON.stringify(this.portal);
      },
      err => this.alertService.error(err)
    );

    this.znodeService.getPortals().subscribe(data => {
      this.total = data.length;
    });

    if(this.misType === MisTypes.CoreBridge)
    {
      this.corebridgeService.getPortalLimit().subscribe(res =>
      {
        this.limit = res;
      });
    }
    else if(this.misType === MisTypes.Cyrious)
    {
      this.limit = this.cesaService.getEcommStoreLimit();
    }

    this.getIsFileManagementEnabled();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const elements = document.querySelectorAll('p-inputmask');
      elements.forEach(el => {
        let parent = el;
        let label = el.nextElementSibling;
        label.remove();
        parent.appendChild(label);
      })
    }, 100);
    this.ngForm.form.valueChanges.pipe(debounceTime(300),distinctUntilChanged(),untilDestroyed(this)).subscribe(values => {
      if(this.ngForm.valid) {
        this.saveState = AutoSaveBarState.none;
        this.validationErrors = [];
        this.alertService.detailedError('general', this.validationErrors);
      }
    });
  }

  //For: ROG-211 / ROG-222
  getIsFileManagementEnabled(){

    this.corebridgeService.getIsFileManagementEnabled().subscribe(result => {

      this.fileManagementEnabled = result;

    });

  }

  canDeactivate(): Observable<boolean> | boolean {
    var pass: boolean = true;

    if( this.saved ){
      return true;
    }

    if( this.unchanged != JSON.stringify(this.portal) ){
      pass = false;
    }
    else if( this.catalog.CatalogId != this.catalogs[0].CatalogId){
      pass = false;
    }

    if(pass){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  save() {
    if (this.ngForm.invalid) {
      return false;
    }

    if (this.total >= this.limit) {
      this.alertService.error('Unable to create store. Maximum number of stores has been reached. To add more stores contact your sales rep.');
      return false;
    }

    this.loading = true;

    // create portal
    this.znodeService.createPortal(this.portal).subscribe(
      res => {
        console.log(res);

        this.portal = res;
        this.unchanged = JSON.stringify(this.portal);

        // prepare portal catalog data
        var portalCatalog: PortalCatalog = new PortalCatalog();
        portalCatalog.CatalogId = this.catalog.CatalogId;
        portalCatalog.Catalog = this.catalog;
        portalCatalog.PortalId = res.PortalId;
        portalCatalog.Portal = res;
        portalCatalog.CssId = this.stylesheetId;
        portalCatalog.ThemeId = 2; //Use B2B theme when creating new storefronts for now instead of NULL
        forkJoin(
          this.znodeService.createPortalCatalog(portalCatalog)
        ).subscribe(
          data => {
            let node = data[0];
            console.log(node);
            this.loading = false;
            this.saved = true;
            this.router.navigate(['/ecommerce/storefronts/']);
          }
        );
      },
      err => {
        this.loading = false;
        this.alertService.error(err);
      }
    );
  }

  cancel() {
    this.router.navigate(['/ecommerce/storefronts']);
  }

  private uploadStoreLogo(data: Image) {
    var path = data.ImagePath.replace('~/data/default/images/catalog/original/', '');
    this.portal.LogoPath = path;
  }

  private onBlur(event: any, label: string) {
    event.target.value = event.target.value.trim();
    if(this.ngForm.valid) {
      this.saveState = AutoSaveBarState.none;
      this.validationErrors = [];
    } else {
      if(event.target.className.includes('ng-invalid')) {
        if(!this.validationErrors.find(err => err.FieldName == label)) {
          let message = 'This field is required' + (label.includes('Email') ? ' and must be a valid email address.' : '.');
          this.validationErrors.push({ ID: 0, Name: "", FieldName: label, ErrorMessage: message });
        }
      } else {
        this.validationErrors.removeWhere(err => err.FieldName == label);
      }
    }
    this.alertService.detailedError('general', this.validationErrors);
  }

  selectCatalog(catalog: Catalog) {
    this.catalog = catalog;
  }

  focusInvalid() {
    let target = this.elementRef.nativeElement.querySelector('.ng-invalid:not(form)');
    if(this.validationErrors.length == 0)
      this.onBlur({target: target}, target.nextElementSibling.innerText.replace(' *',''));
    target.focus();
    this.saveState = AutoSaveBarState.invalid;
  }

  ngOnDestroy() {
    this.alertService.detailedError('general', []);
  }
}
