import { Component } from '@angular/core';

import { CatalogsTableComponent } from '../../components/catalogs-table/catalogs-table.component';
import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'app-catalogs',
  templateUrl: './catalogs.component.html'
})
export class CatalogsComponent {

  public breadcrumbs = [
    {
      title: 'Catalogs'
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) { }

  onLoaded(){
    this.loading = false;
  }

}
