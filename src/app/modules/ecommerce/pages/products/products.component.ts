import { Component, OnInit } from '@angular/core';

import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html'
})
export class ProductsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Products'
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) { }

  ngOnInit() {
  }

  onLoaded(){
    this.loading = false;
  }

}
