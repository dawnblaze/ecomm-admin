import { Component, OnInit } from "@angular/core";

import { ThemeService } from "../../../../services/theme.service";

@Component({
  selector: "app-storefronts",
  templateUrl: "./storefronts.component.html",
  styleUrls: ["./storefronts.component.less"]
})
export class StorefrontsComponent implements OnInit {
  public breadcrumbs = [
    {
      title: "My Stores",
      path: "/ecommerce/storefronts"
    }
  ];

  public loading: boolean = true;

  constructor(private themeService: ThemeService) {}

  ngOnInit() {}

  onLoaded() {
    this.loading = false;
  }
}
