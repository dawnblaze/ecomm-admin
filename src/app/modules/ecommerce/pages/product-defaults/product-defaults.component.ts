import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import { ThemeService } from '../../../../services/theme.service';
import { ControlProduct } from "../../../cesa/models/control/control-product";
import { CesaProductMapping } from "../../../cesa/models/cesa-product-mapping";
import { CesaShipmentMapping } from "../../../cesa/models/cesa-shipment-mapping";
import { CesaService, MisTypes } from "../../../../services/cesa.service";
import { CoreBridgeService } from '../../../../services/corebridge.service';
import { Observable, forkJoin }     from 'rxjs';
import { Router } from "@angular/router";
import { isNullOrUndefined } from "util";
import { AlertService } from "../../../../services/alert.service";
import { CesaStationMapping } from "../../models/cesa-station-mapping";
import { ControlStation } from "../../models/control-station";
import { CoreBridgeProductSettings } from '../../models/corebridge-product-settings';
import { Product } from '../../models/product';
import { IncomeAccount } from '../../models/income-account';

@Component({
  selector: 'app-product-defaults',
  templateUrl: './product-defaults.component.html'
})
export class ProductDefaultsComponent implements AfterViewInit, OnInit {


  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Product Defaults'
    }
  ];

  misType: MisTypes;
  public loading: boolean = true;
  public templates: any[];
  public controlProducts: ControlProduct[] = [];
  public selectedControlDefaultProduct: ControlProduct;
  public selectedControlDefaultShippingProduct: ControlProduct;
  public cesaDefaultProductMapping: CesaProductMapping = new CesaProductMapping();
  public cesaDefaultShippingProductMapping: CesaShipmentMapping = new CesaShipmentMapping();
  public misText: string;

  public product: Product;
  private unchanged: string;
  public ecommCategories: {CategoryName: string; CategoryId: number;}[];
  public categories: {CategoryName: string; CategoryId: number; IsActive: boolean;}[];
  public quickProducts: any[] = [];
  public incomeAccounts: IncomeAccount[];
  public productSettings: any;

  public productInfo: any = {
    cb_integration_category: 'basic'
  };

  constructor(
    private themeService:  ThemeService, 
    private alertService: AlertService, 
    private cesaService: CesaService, 
    private corebridgeService: CoreBridgeService, 
    private location: Location) {

  }

  ngOnInit(): void {
    this.misType = this.cesaService.getMisType();
    if(this.misType == 1){
      this.misText = "Control";
    }
    else{
      this.misText = "Corebridge";
    }
  }

  ngAfterViewInit() {
    this.loading = true;
    if(this.misType == MisTypes.Cyrious) {
        this.cesaService.getControlProducts().subscribe(res => {
        this.controlProducts = res;

        this.cesaService.getDefaultProductMapping().subscribe(res => {
          this.cesaDefaultProductMapping = res || new CesaProductMapping();
          this.selectedControlDefaultProduct = this.controlProducts.filter(x => x.productId == this.cesaDefaultProductMapping.atomID)[0];
        });

        this.cesaService.getDefaultShippingMapping().subscribe(res => {
          this.cesaDefaultShippingProductMapping = res || new CesaProductMapping();
          this.selectedControlDefaultShippingProduct = this.controlProducts.filter(x => x.productId == this.cesaDefaultShippingProductMapping.atomID)[0];
        });

        }, err => {
          this.alertService.error(err);
          this.loading = false;
        }, () => {
          this.loading = false
        });

      if(isNullOrUndefined(this.cesaDefaultProductMapping))
        this.cesaDefaultProductMapping = new CesaProductMapping();

      if(isNullOrUndefined(this.cesaDefaultShippingProductMapping))
        this.cesaDefaultShippingProductMapping = new CesaProductMapping();
    }
    // else if(this.misType == MisTypes.CoreBridge) {
    else {
      forkJoin(
        this.corebridgeService.getEcommProductCategories(),
        this.corebridgeService.getProductCategories(),
        this.corebridgeService.getIncomeAccounts(),
        this.corebridgeService.getProductSettingsByZnodeProductID(-1)
      ).subscribe(
        data => {
          let [ecommCategories, categories, incomeAccounts, productSettings] = data;
          this.ecommCategories = ecommCategories;
          this.categories = categories;
          this.incomeAccounts = incomeAccounts;
          this.productSettings = productSettings;

          console.log("PRODUCT SETTINGS:");
          console.log(this.productSettings);

          if (this.productSettings.IsSuccess) {
            this.product = new Product;
            
            this.product.CoreBridgeQuickProductCategoryId = this.productSettings.CoreBridgeQuickProductCategoryId || '';
            this.product.CoreBridgeQuickProductId = this.productSettings.CoreBridgeQuickProductId || '';
            this.product.CoreBridgeProductCategoryId = this.productSettings.CoreBridgeProductCategoryId || '';
            this.product.CoreBridgeIncomeAccountId = this.productSettings.CoreBridgeIncomeAccountId || '';

            if (this.productSettings.IntegrationTypeId == 2) {
              this.productInfo.cb_integration_category = 'advanced';
  
              if(this.product.CoreBridgeQuickProductCategoryId != null){
                this.getQuickProducts(this.product.CoreBridgeQuickProductCategoryId);
              }
              
            }
            else if (this.productSettings.IntegrationTypeId == 3) {
              this.productInfo.cb_integration_category = 'reporting';
            }
            else {
              this.productInfo.cb_integration_category = 'basic';
            }

            this.unchanged = JSON.stringify(this.product);
            this.loading = false;
          }
          else {
            this.alertService.error(this.productSettings.Message);
            this.unchanged = JSON.stringify(this.product);
            this.loading = false;
          }

        }
      );
    }
  }

  private getQuickProducts(categoryId:number) {
    this.quickProducts = [];

    if(categoryId > 0){
      this.corebridgeService.getProductsByCategory(categoryId).subscribe(
        data => this.quickProducts = data,
        err => this.alertService.error(err)
      );
    }
  }

  onLoaded(){

  }

  public save() {
    this.loading = true;
    let isSuccess = true;
    if(this.misType == MisTypes.Cyrious) {
      this.cesaDefaultProductMapping.atomID = this.selectedControlDefaultProduct.productId;
      this.cesaDefaultProductMapping.atomName = this.selectedControlDefaultProduct.productName;
      this.cesaDefaultShippingProductMapping.atomID = this.selectedControlDefaultShippingProduct.productId;
      this.cesaDefaultShippingProductMapping.atomName = this.selectedControlDefaultShippingProduct.productName;
      this.cesaService.setDefaultProductMapping(this.cesaDefaultProductMapping).subscribe(res => {},
        err => isSuccess = false);
      this.cesaService.setDefaultShippingMapping(this.cesaDefaultShippingProductMapping).subscribe(res => {},
        err => {isSuccess = false;});

      if(isSuccess)
        this.alertService.success("Settings Saved!");
      else
        this.alertService.error("Error Saving Settings!");
      this.loading = false;
    }
    // else if(this.misType == MisTypes.CoreBridge) {
    else {
      let settings: CoreBridgeProductSettings = new CoreBridgeProductSettings();
      settings.ZnodeProductId = -1;

      if(this.productInfo.cb_integration_category == 'basic'){
        this.product.CoreBridgeQuickProductCategoryId = null;
        this.product.CoreBridgeQuickProductCategoryName = "";
        this.product.CoreBridgeQuickProductId = null;
        this.product.CoreBridgeQuickProductName = "";
        this.product.CoreBridgeProductCategoryId = null;
        this.product.CoreBridgeProductCategoryName = "";
        this.product.CoreBridgeIncomeAccountId = null;
        this.product.CoreBridgeIncomeAccountName = "";

        settings.IntegrationTypeId = 1;
      }

      if(this.productInfo.cb_integration_category == 'advanced'){
        this.product.CoreBridgeProductCategoryId = null;
        this.product.CoreBridgeProductCategoryName = "";
        this.product.CoreBridgeIncomeAccountId = null;
        this.product.CoreBridgeIncomeAccountName = "";

        settings.IntegrationTypeId = 2;

        settings.CoreBridgeQuickProductCategoryId = null;
        settings.CoreBridgeQuickProductId = null;

        if (this.product.CoreBridgeQuickProductCategoryId > 0) {
          settings.CoreBridgeQuickProductCategoryId = this.product.CoreBridgeQuickProductCategoryId;
        }
        if (this.product.CoreBridgeQuickProductId > 0) {
          settings.CoreBridgeQuickProductId = this.product.CoreBridgeQuickProductId;
        }

      }

      if(this.productInfo.cb_integration_category == 'reporting'){
        this.product.CoreBridgeQuickProductCategoryId = null;
        this.product.CoreBridgeQuickProductCategoryName = "";
        this.product.CoreBridgeQuickProductId = null;
        this.product.CoreBridgeQuickProductName = "";

        settings.IntegrationTypeId = 3;

        settings.CoreBridgeProductCategoryId = null;
        settings.CoreBridgeIncomeAccountId = null;

        if (this.product.CoreBridgeProductCategoryId > 0) {
          settings.CoreBridgeProductCategoryId = this.product.CoreBridgeProductCategoryId;
        }
        if (this.product.CoreBridgeIncomeAccountId > 0) {
          settings.CoreBridgeIncomeAccountId = this.product.CoreBridgeIncomeAccountId;
        }
      }

      console.log(settings);

      this.corebridgeService.createOrUpdateProductSettings(settings).subscribe(
        res => {
          if(res.IsSuccess) {
            this.product.ExternalId = res.Id;
            this.alertService.success("Settings Saved!");
            this.loading = false;
          }
          else {
            this.alertService.error(res.Message);
            this.loading = false;
          }
        },
        err => {
          this.alertService.error(err);
          this.loading = false;
        }
      );
    }
  }

  public cancel() {
    this.location.back();
  }
}
