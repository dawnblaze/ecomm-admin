import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core'
import { MenuItem, SortEvent } from 'primeng/api';
import { ZnodeService } from '../../../../services/znode.service';
import  * as  Moment from 'moment';
import { ExcelService } from '../../../../services/excel.service';
import { EventLog } from '../../models/eventLog';


@Component({
    selector: 'app-event-log',
    templateUrl: './event-log.component.html',
    styleUrls:['./event-log.component.sass']
})

export class EventLogComponent implements AfterViewInit{
    
    public breadcrumbs = [
        {
          title: 'Event Log',
          path: ''
        }
    ];

    public menuItems = [
        {label: 'File',
            items: [{
                label: 'Export',
                icon:  'pi pi-fw pi-file',
                items : [
                    {label: 'Excel xlsx', command: ()=>{this.exportToExcel()}}

                ]
            }]
        },
        {label: 'Refresh', icon: 'pi pi-fw pi-refresh', command: () =>{ this.refreshData() }}
        
    ]

  public title = 'eventlogviewer';

  // Data handling
  public data: EventLog[];
  public cols: any[];
  public loading: boolean;
  public selectedEvent: any;


  // Modal properties
  public visible: true;
  public newEvent: boolean;
  public event: any;
  public show: boolean = false;

  // Popup Dialog
  public eventData: any = {LogLevel: "", Message: "", StackTrace: ""};
  public showAllRecordsWarning: boolean = false;
  public allwaysShowWarning: boolean = true; // show pop up warning if selecting all views.
  public msgs: any[];

  // Database static name  
  public currentDB: string = ""

  // status for loader bar
  public status: number;

  // Table Properties
  public rowsToView: number = 50;
  public totalRows: any = 2000;
  public totalRowsViewable: number = 1000;
  public totalRecords: number = 1000;

  public showTotal: any[]; // change to showNumberRows
  public showRowsList: any[] // change to showTotalNumberRows
  public views: MenuItem[];

  public currentTenant: number = 0;
  

  

  public tenantError: boolean = false;
  

  public version: string;

  @ViewChild('tenantValue') tenantValue:ElementRef;
  

    constructor(private znodeService: ZnodeService, private excelService: ExcelService){
        // service
        this.cols = [
            {field:"id",header:"ID"},
            {field:"bid",header:"BID"},
            {field:"IpAddress",header:"Ip"},
            {field:"EntryDT",header:"Date"},
            {field:"LogLevel",header:"Level"},
            {field:"event",header:"Event"},
            {field:"message",header:"Message"}
          ]
    }
    
    ngOnInit() {
        console.log('Event Log Status');
        this.showTotal = [
            {label: "50", value: '50'},
            {label: "75", value: '75'},
            {label: "100",value:'100'}
           ]

           this.showRowsList = [
            {label: "1000", value: '1000'},
            {label: "2000", value: '2000'},
            {label: "3000", value: '3000'},
            {label: "4000", value: '4000'}
          ]
          this.status = 0

        
    }

    ngAfterViewInit():void{
        this.status = 0;
        this.znodeService.getBID().subscribe((b)=> {
            this.currentTenant = b
            console.log('Searching for returnable events..', b)
            this.getLogs(b);
            
            })
          
            this.cols = [
                {field:"id",header:"ID"},
                {field:"BID",header:"BID"},
                {field:"IpAddress",header:"Ip"},
                {field:"EntryDT",header:"Date"},
                {field:"LogLevel",header:"Level"},
                {field:"event",header:"Event"},
                {field:"message",header:"Message"}
              ]
              this.loading  = true;
    
    }

    updateViews(val: number){
        this.rowsToView = val;
      }

    // Close Popup Modal
    close(){
        this.show = false;
    }

    // Large Data Return Warning
    showWarn(){
    this.msgs = [];
    this.msgs.push({severity:'error', summary:'Error: ', detail:`0 rows returned for Tenant <b style="font-size: 20px">${this.currentTenant}</b>. Things To Try: Verify <b>tenant</b> exists, increase <b>Total Views</b> returned.`})
    }

    getTotalViews(e){
    console.log(e)
    if(e.value !== "0"){
        this.updateTotalViews(e.value);
    } else {
        this.showAllRecordsWarning = true;
    }
    // wait for response from user if selected all records.
    
    }

    updateTotalViews(val: any){
    this.totalRows = val;
    this.totalRowsViewable = val;
            this.getLogs(this.currentTenant);
            this.currentDB = `Tenant ${this.currentTenant}`
        
    }

    onRowSelect(e){
        this.newEvent = true;
        this.event = e.data;
        this.eventData = e.data;
        console.log("Selected Event: ",e.data)
        this.show = true;
        }
    
    getEntryDate(dt){
        let ndt = dt.match(/\d+/)[0];
        let eventDate = new Date(parseInt(ndt)).toLocaleString();
        //console.log("DateTime: ",dt, ndt, eventDate);
        return eventDate;
    }

    getLogs(bid){
        this.status = 0;
        this.znodeService.getEventLogs(bid, this.totalRowsViewable).subscribe((data)=>{
            console.log('Data: ',data, bid)
            this.data = data;
            this.status = 100;
            })
            
    }

    getTotalRows(e){
        this.rowsToView = e.value;
        console.log('This Rows to View: ', e)
      }


// Features
    exportToExcel(){
        let ndate = new Date().toLocaleString();
        this.excelService.exportAsExcelFile(this.data, `${ndate}`)
    }

    refreshData(){
        // pulls latest from db
        this.status = 0;
        // simulate time delay for data returned.
        setTimeout(()=>{
            this.getLogs(this.currentTenant);
        },800)
        
    }

    loadAllRecords(){
        // TODO:
    }    
    LoadDataLazy($event){
        // TODO:
    }

    closeWarning(){
        // TODO:
    }

    
}   
