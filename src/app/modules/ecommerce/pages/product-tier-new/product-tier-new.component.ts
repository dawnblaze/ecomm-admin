import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { Observable }     from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Product } from '../../models/product';
import { ProductTier } from '../../models/product-tier';
import { Profile } from '../../models/profile';

@Component({
  selector: 'cb-product-tier-new',
  templateUrl: './product-tier-new.component.html'
})
export class ProductTierNewComponent implements OnInit {

  @Input() product: Product;

  @Output() onExit: EventEmitter<any> = new EventEmitter();

  public tier: ProductTier = new ProductTier();
  public profiles: Profile[];

  public loading: boolean = false;

  constructor(private alertService: AlertService, private znodeService: ZnodeService) {}

  ngOnInit() {

    this.znodeService.getProfiles().subscribe(
      profiles => this.profiles = profiles,
      err => this.alertService.error(err)
    );

  }

  public save() {
    this.loading = true;

    this.tier.ProductId = this.product.ProductId;

    if(this.tier.TierStart == null || this.tier.TierEnd == null){
      this.alertService.error('Required Fields cannot be empty.');
      this.loading = false;
      return false;
    }

    // check validation
    if(isNaN(this.tier.Price)){
      this.alertService.error('Price field requires a number.')
      this.loading = false;
      return false;
    }

    if(isNaN(this.tier.TierStart)){
      this.alertService.error('Tier Start requires a number.')
      this.loading = false;
      return false;
    }

    if(isNaN(this.tier.TierEnd)){
      this.alertService.error('Tier End requires a number.')
      this.loading = false;
      return false;
    }

    if(this.tier.ProfileId > 0){
      let selectedProfile = this.profiles.filter(profile => profile.ProfileId == this.tier.ProfileId)[0];
      this.tier.ProfileName = selectedProfile.Name;
    }
    else {
      this.tier.ProfileId = 0;
      this.tier.ProfileName = "All Storefronts";
    }
    console.log(this.tier);
    this.znodeService.createProductTier(this.tier).subscribe(
      data => {
        console.log(data);
        this.alertService.success('Product Tier Created Successfully');
        this.loading = false;
        this.cancel();
      },
      err => {
        this.loading = false;
        this.alertService.error(err);
      }
    );
  }

  public cancel() {
    this.onExit.emit();
  }

  isNumber(e){
    return (e != null && !isNaN(e) && e.toString() !== '');
  }

}
