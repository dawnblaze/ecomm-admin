import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { Observable, forkJoin } from 'rxjs';

import { AlertService } from '../../../../services/alert.service';
import { ZnodeService } from '../../../../services/znode.service';
import { ModalService } from '../../../../services/modal.service';

import { Portal } from '../../models/portal';
import { Profile } from '../../models/profile';
import { PortalProfile } from '../../models/portal-profile';

@Component({
  selector: 'cb-portal-profile-details',
  templateUrl: './portal-profile-details.component.html'
})
export class PortalProfileDetailsComponent implements OnInit {

  @Input() portal: Portal;
  @Input() portalProfileId: number;
  @Output() onExit: EventEmitter<any> = new EventEmitter();
  @Output() onLoad: EventEmitter<string> = new EventEmitter();

  public title: string;

  public profiles: Profile[] = [];
  public portalProfile: PortalProfile = new PortalProfile();
  private unchanged: string;

  public IsDefaultAnonymous: boolean = false;
  public IsDefaultRegistered: boolean = false;

  public loading: boolean = false;

  constructor(private alertService: AlertService, private znodeService: ZnodeService, private modalService: ModalService) {}

  ngOnInit() {
    forkJoin(
      this.znodeService.getPortalProfile(this.portalProfileId),
      this.znodeService.getProfiles()
    ).subscribe(
      data => {
        this.portalProfile = data[0];
        this.profiles = data[1];

        let selectedProfile: Profile[] = this.profiles.filter(x => x.ProfileId == this.portalProfile.ProfileID);
        if (selectedProfile.length > 0) {
          this.title = selectedProfile[0].Name;
        }
        this.onLoad.emit(this.title);

        this.IsDefaultAnonymous = (this.portal.DefaultAnonymousProfileId == this.portalProfile.ProfileID);
        this.IsDefaultRegistered = (this.portal.DefaultRegisteredProfileId == this.portalProfile.ProfileID);

        this.unchanged = JSON.stringify(this.portalProfile);
      },
      err => this.alertService.error(err)
    )
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.portalProfile) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  save() {
    this.loading = true;
    let profiles = this.profiles.filter(profile => profile.ProfileId==this.portalProfile.ProfileID);
    this.portalProfile.Name = profiles[0].Name;

    // update default profiles on portal
    this.znodeService.updatePortalProfile(this.portalProfile).subscribe(
      res => {
        this.portalProfile = res;
        this.unchanged = JSON.stringify(this.portalProfile);

        let portal = this.portal;
        let update: boolean = false;

        if(portal.DefaultAnonymousProfileId == this.portalProfile.ProfileID && this.IsDefaultAnonymous != true)
        {
          portal.DefaultAnonymousProfileId = 0;
          update = true;
        }
        else if( this.IsDefaultAnonymous == true && portal.DefaultAnonymousProfileId != this.portalProfile.ProfileID)
        {
          portal.DefaultAnonymousProfileId = this.portalProfile.ProfileID;
          update = true;
        }


        if(portal.DefaultRegisteredProfileId == this.portalProfile.ProfileID && this.IsDefaultRegistered != true)
        {
          portal.DefaultRegisteredProfileId = 0;
          update = true;
        }
        else if( this.IsDefaultRegistered == true && portal.DefaultRegisteredProfileId != this.portalProfile.ProfileID)
        {
          portal.DefaultRegisteredProfileId = this.portalProfile.ProfileID;
          update = true;
        }

        if(update)
        {
          this.znodeService.updatePortal(portal).subscribe(
            data => {
              this.portal = data;
              this.loading = false;
              this.cancel();
            },
            err => {
              this.loading = false;
              this.alertService.error(err);
            }
          );
        }
        else {
          this.loading = false;
          this.cancel();
        }
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    );
  }

  cancel() {
    this.onExit.emit();
  }
}
