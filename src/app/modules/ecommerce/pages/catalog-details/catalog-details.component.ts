import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';
import { untilDestroyed } from 'ngx-take-until-destroy';

import { Observable, Subject } from 'rxjs';
import {distinctUntilChanged, debounceTime} from 'rxjs/operators';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';
import { DetailedError } from '../../../../modules/corebridge/models/alert';

import { Catalog } from '../../models/catalog';

import { TabsComponent } from '../../../corebridge/components/tabs/tabs.component';
import { AutoSaveBarState } from '../../../corebridge/components/autosave-bar/autosave-bar.component';
import { CatalogAssociateComponent } from '../catalog-associate';
import { CategoryNodeDetailsComponent } from '../category-node-details';
import { CategoryNodesTableComponent } from '../../components/category-nodes-table/category-nodes-table.component';

@Component({
  selector: 'app-catalog-details',
  templateUrl: './catalog-details.component.html',
  styleUrls: ['./catalog-details.component.less']
})
export class CatalogDetailsComponent implements OnInit, OnDestroy {
  public basePath = '/ecommerce/merchandise/catalogs/'

  public breadcrumbs = [
    {
      title: 'Catalogs',
      path: this.basePath
    }
  ];

  @ViewChild(TabsComponent) private tabs: TabsComponent;
  @ViewChild(CatalogAssociateComponent) private catalogAssociateComponent: CatalogAssociateComponent;
  @ViewChild(CategoryNodeDetailsComponent) private categoryNodeDetailsComponent: CategoryNodeDetailsComponent;
  @ViewChild(CategoryNodesTableComponent) private categoryNodesTable: CategoryNodesTableComponent;
  public tabState = "general"; // general, associated-categories, details, add
  public selectedObjectId: number;

  public catalog: Catalog;
  private unchanged: string;
  public title: string;

  public loading: boolean = false;
  public saveState: AutoSaveBarState = AutoSaveBarState.saved;

  private routerSub;

  detailedErrors: DetailedError[] = [];
  sbjRawInput:Subject<string> = new Subject();

  constructor( private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService, private route: ActivatedRoute, private router: Router, private location: Location, private themeService: ThemeService) {
    this.routerSub = router.events.pipe(untilDestroyed(this)).subscribe(path => {
      // Since we use location.go() to navigate inside of the component and location.go() doesn't update the router with the new path,
      // we need to capture router path changes and handle them inside of the component.
      if (!this.location.isCurrentPathEqualTo(router.routerState.snapshot.url)) {
        this.setOpenTab();
      }

      if (!this.location.isCurrentPathEqualTo(router.routerState.snapshot.url)) {
        var pattern = /catalogs\/[0-9]+/g;
        if( pattern.test(router.routerState.snapshot.url) ){
          // only set tabs if navigating to same page
          this.setOpenTab();
        }
      }
    });
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let id = +params['id'];

      this.znodeService.getCatalog(id).pipe(untilDestroyed(this)).subscribe(catalog => {
        this.catalog = catalog;
        this.unchanged = JSON.stringify(this.catalog);

        this.title = this.catalog.Name;

        let catalogPath = this.basePath + this.catalog.CatalogId
        let crumb = {title: this.catalog.Name, path:catalogPath};
        this.breadcrumbs.push(crumb);

        this.setOpenTab();
      },
      error => {
        console.error(error);
      });
    });
  }

  ngAfterContentInit(){
    this.sbjRawInput.pipe(debounceTime(2000),distinctUntilChanged(),untilDestroyed(this))
    .subscribe(response => this.onInputChange(response))
  }

  canDeactivate(): Observable<boolean> | boolean {

    if( this.unchanged == JSON.stringify(this.catalog) ){
      if(this.tabState == 'details'){
        return this.categoryNodeDetailsComponent.canDeactivate();
      }

      if(this.tabState == 'add'){
        return this.catalogAssociateComponent.canDeactivate();
      }
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  setOpenTab() {
    this.route.params.forEach((params: Params) => {
      let tab = params['tab'];
      let url = this.location.path();
      if(tab){
        this.tabs.openTab(tab);
        this.changedTab(tab);
        this.tabState = tab;

        // let category = +params['category'];
        // if(category > 0){
        //   this.viewCategory(category);
        // } else if (url.endsWith("/add")) {
        //   this.addCategory();
        // }
      } else {
        this.tabs.openTab(this.tabState);
        this.changedTab(this.tabState);
      }
    });
  }

  public changedTab(tabSlug: string){
    this.tabState = tabSlug;

    let tabPath: string = this.basePath+this.catalog.CatalogId+'/'+tabSlug;
    this.location.go(tabPath);
  }

  private capitalizeFirst(str: string): string {
    str = str.replace('-', ' ');
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1);});
  }

  public listCategories() {
    this.tabState='associated-categories';
    // this.location.go(this.basePath+this.catalog.CatalogId+'/associated-categories/general');
  }

  public refreshCategories() {
    this.categoryNodesTable.getRows();
  }

  private addCategory() {
    this.tabState = 'add';
    this.location.go(this.basePath+this.catalog.CatalogId+'/associated-categories/add');
  }

  private viewCategory(id: number) {
    this.selectedObjectId = id;
    this.tabState = 'details';
    // this.location.go(this.basePath+this.catalog.CatalogId+'/associated-categories/'+id);
  }

  private addBreadcrumb(title: string) {
    this.breadcrumbs.push({title: title, path: ''});
  }

  public save(){
    this.loading = true;
    this.saveState = AutoSaveBarState.saving;

    this.znodeService.updateCatalog(this.catalog).pipe(untilDestroyed(this)).subscribe(
      res => {
        this.catalog = res;
        this.unchanged = JSON.stringify(this.catalog);
        this.title = this.catalog.Name;
        this.loading = false;
        this.alertService.success('Catalog Updated');
        this.saveState = AutoSaveBarState.saved;
      },
      err => {
        this.loading = false;
        this.alertService.error(err);
        this.saveState = AutoSaveBarState.error;
      }
    );
  }

  public cancel(){
    this.router.navigate(['/ecommerce/merchandise/catalogs']);
  }

  public onInputChange(value: string) {
    this.detailedErrors.removeWhere(e => e.FieldName === 'Catalog Title');
    if(!value.trim().length){
      this.detailedErrors.push({
        ID: this.catalog.CatalogId,
        Name: this.catalog.Name,
        FieldName: 'Catalog Title',
        ErrorMessage: 'This field is required',
        TabSlug: 'general',
        TabTitle: 'Catalog Details',
        goToAction: () => { this.tabs.openTab('general') }
      });
      this.saveState = AutoSaveBarState.invalid;
    } else if(this.title != this.catalog.Name.trim()) {
      this.save();
    }
    this.alertService.detailedError('catalogDetails',this.detailedErrors);
  }

  public tabHasError(slug: string) : boolean {
    return !!this.detailedErrors.find(e => e.TabSlug === slug);
  }

  ngOnDestroy() {
    if (this.routerSub != null) {
      this.routerSub.unsubscribe();
    }
    this.alertService.detailedError('catalogDetails',[]);
  }
}
