import { Component, OnInit } from '@angular/core';

import { ThemeService } from '../../../../services/theme.service';

@Component({
  selector: 'app-email-templates',
  templateUrl: './email-templates.component.html'
})
export class EmailTemplatesComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Settings'
    },
    {
      title: 'Email Templates'
    }
  ];

  public loading: boolean = true;

  public templates: any[];

  constructor(private themeService: ThemeService) {}

  ngOnInit() {
  }

  onLoaded(){
    this.loading = false;
  }

}
