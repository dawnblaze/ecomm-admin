import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { CSS } from '../../models/css';

@Component({
  selector: 'app-style-new',
  templateUrl: './style-new.component.html'
})
export class StyleNewComponent implements OnInit {

  breadcrumbs = [
    {
      title: 'Styles',
      path: '/ecommerce/styles/'
    },
    {
      title: 'Add Style'
    }
  ];

  css: CSS;
  private unchanged: string;
  private title: string;
  loading: boolean = false;

  constructor(private router: Router, private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService) {}

	ngOnInit() {
    this.css = new CSS();

    this.znodeService.getThemes().subscribe(
      themes => {
        this.css.ThemeID = themes[0].ThemeID;
        this.css.Theme = themes[0];
        this.unchanged = JSON.stringify(this.css);
      },
      err => this.alertService.error(err)
    );
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.css) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  cancel() {
    this.router.navigate(['/ecommerce/styles']);
  }

  save() {
    this.znodeService.createCss(this.css).subscribe(
      stylesheet => {
        console.log(stylesheet);
        this.css = stylesheet;
        this.unchanged = JSON.stringify(this.css);
        this.loading = false;
        this.cancel();
      },
      err => this.alertService.error(err)
    );
  }

  setHexCode(code: string, keys: string[]) {
    for (var key in keys) {
      this.css.StyleDictionary[keys[key]] = code;
    }
  }
}