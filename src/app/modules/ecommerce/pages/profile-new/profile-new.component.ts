import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { Profile } from '../../models/profile';

@Component({
  selector: 'app-profile-new',
  templateUrl: './profile-new.component.html'
})
export class ProfileNewComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Profiles',
      path: '/ecommerce/profiles'
    },
    {
      title: 'New'
    }
  ];

  public profile: Profile = new Profile();
  private unchanged: string;
  public title: string;

  public loading: boolean = false;

  constructor(private route: ActivatedRoute, private alertService: AlertService, private modalService: ModalService, private znodeService: ZnodeService, private router: Router) {}

  ngOnInit() {
    this.unchanged = JSON.stringify(this.profile);
  }

  canDeactivate(): Observable<boolean> | boolean {
    if( this.unchanged == JSON.stringify(this.profile) ){
      return true;
    }

    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  public save() {
    this.loading = true;
    this.znodeService.createProfile(this.profile).subscribe(
      profile => {
        this.profile = profile;
        this.unchanged = JSON.stringify(this.profile);

        this.alertService.success('Profile Created Successfully');
        this.loading = false;
        this.cancel();
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    )
  }

  public cancel() {
    this.router.navigate(['/ecommerce/profiles']);
  }

}
