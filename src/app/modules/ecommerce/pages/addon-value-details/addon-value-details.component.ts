import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ThemeService } from '../../../../services/theme.service';
import { DomainService } from '../../../../services/domain.service';

import { AddonValue } from '../../models/addon-value';
import { TaxClass } from '../../models/tax-class';
import { Supplier } from '../../models/supplier';
import { Addon } from '../../models/addon';

import { ConfigSetting } from '../../models/config-setting';
import { MisTypes, CesaService } from '../../../../services/cesa.service';
import { ATEService } from '../../../../services/ate.service';

@Component({
  selector: 'cb-addon-value-details',
  templateUrl: './addon-value-details.component.html'
})
export class AddonValueDetailsComponent implements OnInit {

  @Input() AddonValueId: number;
  @Input() AddonId: number;
  @Output() onExit: EventEmitter<any> = new EventEmitter();
  @Output() onLoad: EventEmitter<string> = new EventEmitter();

  public addonValue: AddonValue;
  public taxClasses: TaxClass[];
  public suppliers: Supplier[];
  public addon: Addon;
  public defaultAddonValue: AddonValue;
  public configSetting: ConfigSetting;

  public title: string;

  public loading: boolean = true;
  public saving: boolean = false;
  public skuInvalid: boolean = false;

  constructor(private znodeService: ZnodeService, private alertService: AlertService, private route: ActivatedRoute, private location: Location, private themeService: ThemeService, private domainService: DomainService, 
    private cesaService : CesaService,
    private ateService : ATEService) { }

  ngOnInit() {
    forkJoin(
      this.znodeService.getAddonValue(this.AddonValueId),
      this.znodeService.getTaxClasses(),
      this.znodeService.getSuppliers(),
      this.znodeService.getDefaultAddonValue(this.AddonId),
      this.znodeService.getConfigSettingByBID(this.domainService.bid),
      this.znodeService.getAddon(this.AddonId),
    ).subscribe(
      data => {
        this.addonValue = data[0];
        this.title = this.addonValue.Name;
        this.onLoad.emit(this.title);

        this.taxClasses = data[1];
        this.suppliers = data[2];

        this.configSetting = Object.assign(new ConfigSetting(), data[4]);

        if (!this.configSetting.EnableOnlineTaxLookup) {
          this.taxClasses = this.taxClasses.filter(x => x.ExternalId == null).filter(y => y.TaxClassId == 1 || y.TaxClassId == 2);
        }else if(this.cesaService.getMisType() == MisTypes.CoreBridge){
          
          this.taxClasses = this.taxClasses.filter(x => x.ExternalId != null);
          
        }

        this.defaultAddonValue = data[3];
        this.addon = data[5];
        
        this.loading = false;
      }
    );

  }

  save() {
    if (this.skuInvalid) {
      return;
    }
    
    this.saving = true;

    this.znodeService.updateAddonValue(this.addonValue).subscribe(
      response => {
        if (this.addonValue.IsDefault && this.defaultAddonValue && this.defaultAddonValue.AddOnValueId != this.addonValue.AddOnValueId) {
          this.defaultAddonValue.IsDefault = false;

          this.znodeService.updateAddonValue(this.defaultAddonValue).subscribe(
            response => {
              this.alertService.success('Add-On Value Updated Successfully');
              this.saving = false;
            },
            error => {
              this.alertService.error(error);
              this.saving = false;
            });
        }
        else {
          this.alertService.success('Add-On Value Updated Successfully');
          this.saving = false;
        }
      },
      error => {
        this.alertService.error(error);
        this.saving = false;
      }
    )
  }

  cancel() {
    this.onExit.emit();
  }

  isNumber(e){
    return (e != null && !isNaN(e) && e.toString() !== '');
  }

  private onBlur(event: any, isSKU: boolean = false) {
    event.target.value = event.target.value.trim();
    if (isSKU) this.skuInvalid = !(/^[\w\d_.-]+$/.test(event.target.value));
  }

}
