import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Observable, forkJoin }     from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Product } from '../../models/product';
import { Category } from '../../models/category';
import { ProductCategory } from '../../models/product-category';

@Component({
  selector: 'app-product-associate',
  templateUrl: './product-associate.component.html'
})
export class ProductAssociateComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Category',
      path: '/ecommerce/merchandise/collections'
    },
    {
      title: '',
      path: '/ecommerce/merchandise/collections/'
    },
    {
        title: 'Associate Products'
    }
  ];

  public products: Product[];
  public productCategories: ProductCategory[];
  public category: Category;

  constructor(private znodeService: ZnodeService, private alertService: AlertService, private route: ActivatedRoute, private location: Location, private router: Router) { }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            let categoryId = +params['id'];

            forkJoin(
                this.znodeService.getProducts(),
                this.znodeService.getCategoryProducts(categoryId),
                this.znodeService.getCategory(categoryId)
            ).subscribe(
                data => {
                    this.products = data[0];
                    this.productCategories = data[1];
                    this.category = data[2];

                    this.breadcrumbs[1] = {
                        title : this.category.Name,
                        path : '/ecommerce/merchandise/collections/' + this.category.CategoryId
                    };
                },
                err => this.alertService.error(err)
            );
        });
    }

}
