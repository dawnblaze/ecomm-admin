import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Location } from '@angular/common';
import { NgForm } from '@angular/forms';
import { Observable, forkJoin } from 'rxjs';
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { untilDestroyed } from "ngx-take-until-destroy";

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';
import { ThemeService } from '../../../../services/theme.service';

import { Category } from '../../models/category';
import { Image } from '../../models/image';
import { DetailedError } from '../../../../modules/corebridge/models/alert';

import { TabsComponent } from '../../../corebridge/components/tabs/tabs.component';
import { AutoSaveBarState } from '../../../corebridge/components/autosave-bar/autosave-bar.component';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.less']
})
export class CategoryDetailComponent implements OnInit, OnDestroy {

  public categoriesPath = '/ecommerce/merchandise/collections/';
  public breadcrumbs = [
    {
      title: 'Collections',
      path: this.categoriesPath
    }
  ];

  @ViewChild(TabsComponent) private tabs: TabsComponent;
  @ViewChild('detailsForm') ngForm: NgForm;

  public category: Category;
  private unchanged: string;
  public title: string;

  public loading: boolean = false;

  private imageRoot: string;
  private imageVersion: number = 0;

  private routerSub;
  public invalid = {};
  public saveState: AutoSaveBarState = AutoSaveBarState.saved;

  detailedErrors: DetailedError[] = [];

  constructor(private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService, private route: ActivatedRoute, private router: Router, private location: Location, private themeService: ThemeService) {
    this.routerSub = router.events.pipe(untilDestroyed(this)).subscribe(path => {
      // Since we use location.go() to navigate inside of the component and location.go() doesn't update the router with the new path,
      // we need to capture router path changes and handle them inside of the component.
      if (!this.location.isCurrentPathEqualTo(router.routerState.snapshot.url)) {
        this.setOpenTab();
      }
    });
  }

  ngOnInit() {
    this.imageRoot = this.znodeService.base;

    this.route.params.forEach((params: Params) => {
      let id = +params['id'];
      this.znodeService.getCategory(id).pipe(untilDestroyed(this)).subscribe(category => {
        this.category = category;
        this.unchanged = JSON.stringify(this.category);

        this.title = this.category.Name;
        this.categoriesPath = this.categoriesPath + this.category.CategoryId + '/'
        let crumb = { title: this.category.Name, path: this.categoriesPath };
        this.breadcrumbs.push(crumb);

        let tab = params['tab'];
        let url = this.location.path();
        if (tab) {
          this.tabs.openTab(tab);
          this.changedTab(tab);
        } else {
          this.tabs.openTab("general");
          this.changedTab("general");
        }

        this.ngForm.form.valueChanges.pipe(debounceTime(2000), distinctUntilChanged() ,untilDestroyed(this)).subscribe(values => {
          this.setValidationErrors(values);
        });
      });
    });
  }

  setOpenTab() {
    this.route.params.forEach((params: Params) => {
      let tab = params['tab'];
      let url = this.location.path();
      if (tab) {
        this.tabs.openTab(tab);
        this.changedTab(tab);
      } else {
        this.tabs.openTab("general");
        this.changedTab("general");
      }
    });
  }

  canDeactivate(): Observable<boolean> | boolean {
    if (this.unchanged == JSON.stringify(this.category)) {
      return true;
    }
    return this.modalService.warning('You Have Unsaved Changes', 'Leaving without saving will discard any changes you have made. Are you sure you want to leave?', 'Discard and Leave', 'Stay');
  }

  public truncateShortDescription() {
    this.category.ShortDescription = this.category.ShortDescription.substring(0, 100);
  }

  public changedTab(tabSlug: string) {
    let tabPath: string = this.categoriesPath + tabSlug;
    this.location.go(tabPath);
  }

  private capitalizeFirst(str: string): string {
    str = str.replace('-', ' ');
    return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1); });
  }

  save() {
    this.loading = true;
    this.saveState = AutoSaveBarState.saving;
    if (!this.category.SeoTitle) this.category.SeoTitle = this.category.Title;

    this.znodeService.updateCategory(this.category).pipe(untilDestroyed(this)).subscribe(
      res => {
        this.category = res;
        this.unchanged = JSON.stringify(this.category);

        this.title = res.Name;
        this.breadcrumbs[this.breadcrumbs.length - 1].title = res.Name;
        this.alertService.success('Collection Updated Successfully');
        this.ngForm.form.markAsPristine();
        this.loading = false;
        this.saveState = AutoSaveBarState.saved;
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
        this.saveState = AutoSaveBarState.error;
      }
    );

  }

  cancel() {
    this.router.navigate(['/ecommerce/merchandise/collections']);
  }

  private uploadCategoryImage(image: Image) {
    this.category.ImageFile = image.ImagePath;
    this.imageVersion++;
  }

  setValidationErrors(values: any){
    if (this.ngForm.dirty) {
      if (!values.title || !values.title.trim().length){
        this.addDetailedError('Title', 'This field is required', 'general', 'Collection Details');
      } else if (/[;'"\\\/]/.test(values.title)) {
        this.addDetailedError('Title', 'Cannot use these special characters: ; \' " \ /', 'general', 'Collection Details');
        this.invalid['Title'] = true;
      } else {
        this.removeDetailedError('Title', 'general');
        this.invalid['Title'] = false;
      }
      if(!values.name || !values.name.trim().length){
        this.addDetailedError('Name', 'This field is required', 'general', 'Collection Details');
      } else if (/[;'"\\\/]/.test(values.name)) {
        this.addDetailedError('Name', 'Allowed special characters: & - | .', 'general', 'Collection Details');
        this.invalid['Name'] = true;
      } else {
        this.removeDetailedError('Name', 'general');
        this.invalid['Name'] = false;
      }
      if(values.displayOrder == null){
        this.addDetailedError('Display Order', 'This field is required', 'general', 'Collection Details');
      } else {
        this.removeDetailedError('Display Order', 'general');
      }
      if(!values.seoUrl || !values.seoUrl.trim().length){
        this.addDetailedError('URL Handle', 'This field is required', 'general', 'Collection Details');
      } else {
        this.removeDetailedError('URL Handle', 'general');
      }
      if(this.ngForm.invalid || this.detailedErrors.length)
        this.saveState = AutoSaveBarState.invalid;
      else if(this.unchanged == JSON.stringify(this.category))
       this.saveState = AutoSaveBarState.saved;
      else
        this.save();
    }
  }

  onBlur(event: any, field: string, label: string) {
    event.target.value = event.target.value.trim();
    this.setValidationErrors(this.ngForm.form.value);
    if (field === 'SeoUrl' && event.target.value !== '') {
      forkJoin(
        this.znodeService.isSeoUrlExist(this.category.CategoryId, event.target.value),
        this.znodeService.isSeoUrlExist(this.category.CategoryId, event.target.value.replace(/-+/g, ' '))
      ).pipe(untilDestroyed(this))
      .subscribe(
        data => {
          this.invalid[field] = data[0].IsExist || data[1].IsExist;
          if (this.invalid[field])
            this.addDetailedError(label, 'Duplicate value exists.', 'general', 'Collection Details');
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        }
      );
    }
    else if (field === 'Title' && !this.invalid['Title']) {
      this.category.SeoTitle = event.target.value;
    }
  }

  setUrlHandle(event: any) {
    let seoUrl = event.target.value.toLowerCase().replace(/[^a-zA-Z0-9-]/g, "-");
    do {
      seoUrl = seoUrl.replace(/-{2}/g, "-");
    } while (seoUrl.indexOf("--") > 0);
    this.category.SeoUrl = seoUrl;
    event.target.value = seoUrl;
  }

  addDetailedError(field: string, message: string, tab: string, tabTitle: string) {
    let fieldError = this.detailedErrors.find(e => e.FieldName == field && e.TabSlug == tab);
    if(!fieldError || fieldError.ErrorMessage != message){
      if(fieldError)
        this.removeDetailedError(field, tab);
      this.detailedErrors.push({
        ID: this.category.CategoryId,
        Name: this.title,
        FieldName: field,
        ErrorMessage: message,
        TabSlug: tab,
        TabTitle: tabTitle,
        goToAction: () => { this.tabs.openTab(tab) }
      });
      this.alertService.detailedError('catalogDetails', this.detailedErrors);
    }
  }

  removeDetailedError(field: string, tab: string) {
    this.detailedErrors.removeWhere(e => e.FieldName === field && e.TabSlug === tab);
    this.alertService.detailedError('catalogDetails', this.detailedErrors);
  }

  public tabHasError(slug: string): boolean {
    return !!this.detailedErrors.find(e => e.TabSlug === slug);
  }

  ngOnDestroy() {
    this.alertService.detailedError('catalogDetails', []);
  }
}
