import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-editor-presets',
  templateUrl: './editor-presets.component.html'
})
export class EditorPresetsComponent implements OnInit {

  public breadcrumbs = [
    {
      title: 'Settings',
      path: 'ecommerce/settings'
    },
    {
      title: 'Online Designer'
    },
    {
      title: 'Design Editor Presets'
    }
  ];

  public loading: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  onLoaded(){
    this.loading = false;
  }

}
