import { Component } from '@angular/core';

import { ThemeService } from '../../../../../services/theme.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html'
})
export class CompaniesComponent {

  loading: boolean = true;

  public breadcrumbs = [
    {
      title: 'Customers'
    },
    {
      title: 'Companies'
    }
  ];

  constructor(public themeService: ThemeService) {
  }

  onLoaded(){
    this.loading = false;
  }

}
