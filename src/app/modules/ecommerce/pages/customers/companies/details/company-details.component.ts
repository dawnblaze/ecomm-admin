import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { ZnodeService } from '../../../../../../services/znode.service';
import { AlertService } from '../../../../../../services/alert.service';
import { ThemeService } from '../../../../../../services/theme.service';

import { Account } from '../../../../models/account';
import { Alert } from '../../../../../corebridge/models/alert';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html'
})
export class CompanyDetailsComponent implements OnInit {

  public breadcrumbs: any[] = [
    {
      title: 'Customers'
    },
    {
      title: 'Companies',
      path: '/ecommerce/customers/companies'
    }
  ];

  public account:Account;

  constructor(private znodeService:ZnodeService, private route: ActivatedRoute, private alertService: AlertService, private location: Location, private router: Router, public themeService: ThemeService) {}

  ngOnInit() {

    this.route.params.forEach((params: Params) => {
      let id = +params['accountId'];
      this.znodeService.getCompany(id).subscribe(account => {
        this.account = account;
        let crumb = {title: account.CompanyName, path:''};
        this.breadcrumbs.push(crumb);
      });
    });

  }

  cancel() {
    this.router.navigate(['/ecommerce/customers/companies']);
  }

}
