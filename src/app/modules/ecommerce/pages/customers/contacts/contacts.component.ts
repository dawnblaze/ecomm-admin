import { Component } from '@angular/core';

import { ThemeService } from '../../../../../services/theme.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html'
})
export class ContactsComponent{

  public breadcrumbs = [
    {
      title: 'Customers'
    },
    {
      title: 'Contacts'
    }
  ];

  public loading: boolean = true;

  constructor(public themeService: ThemeService) {}

  onLoaded(){
    this.loading = false;
  }

}
