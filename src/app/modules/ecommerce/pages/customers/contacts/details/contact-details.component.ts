import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, forkJoin }     from 'rxjs';

import { ZnodeService } from '../../../../../../services/znode.service';
import { CoreBridgeService } from '../../../../../../services/corebridge.service';
import { AlertService } from '../../../../../../services/alert.service';
import { ThemeService } from '../../../../../../services/theme.service';

import { Contact } from '../../../../models/contact';
import { Profile } from '../../../../models/profile';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html'
})
export class ContactDetailsComponent implements OnInit {
  public loading: boolean = false;
  public profiles: Profile[];

  public breadcrumbs: any[] = [
    {
      title: 'Customers'
    },
    {
      title: 'Contacts',
      path: '/ecommerce/customers/contacts'
    }
  ];

  public contact: Contact;
  public taxInfo: any;
  public profileId: number = 0;
  public myProfiles: Profile[];

  constructor(private znodeService:ZnodeService, private corebridgeService: CoreBridgeService, private route: ActivatedRoute, private alertService: AlertService, private location: Location, private themeService: ThemeService) {}

  ngOnInit() {
    this.loading = true;
    this.route.params.forEach((params: Params) => {
    let accountId: string = params['accountId'];

    forkJoin(
      this.znodeService.getProfiles(),
      this.znodeService.getContactById(parseInt(accountId))
    ).subscribe(data => {
      this.profiles = data[0];
      this.contact = data[1];
      this.myProfiles = this.contact.Profiles;
      this.breadcrumbs.push({ title: this.contact.FirstName + " " + this.contact.LastName });

      this.loading = false;
      });
    });
  }

  resetPassword(){
    if (!this.profileId) {
      alert('Please select a Storefront');
    }
    else {
      this.znodeService.resetEmail(this.contact.Email, this.profileId).subscribe(() => {
        this.alertService.success('Password reset email sent to ' + this.contact.Email);
      }, err => this.alertService.error(err));
    }
  }

  unlockContact() {
    this.znodeService.unlockContact(this.contact).subscribe(() => {
        this.alertService.success('Contact unlock command sent');
    });
  }

  onProfilesChanged() {
    this.znodeService.getAccountProfiles(this.contact.AccountId).subscribe((profiles) => {
      this.myProfiles = profiles;
      this.profileId = 0;
    });
  }
}
