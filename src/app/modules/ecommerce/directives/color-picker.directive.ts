import { Directive, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import Picker from 'vanilla-picker';
import  'spectrum-colorpicker';

declare var jscolor: any;

@Directive({ 
    selector: '[colorpicker]',
    host: {
        '(change)': 'onChange($event)',
        '(blur)': 'onChange($event)'
    }
})
export class ColorPickerDirective {
    @Input()
    set color(color: string) {
        this.updateColor(color);
        if(!this.initialized){
            this.initColor();
        }
    }
    @Output() onUpdate: EventEmitter<string> = new EventEmitter();
    
    private element: any;
    private initialized: boolean = false;

    constructor(el: ElementRef) {
        this.element = el.nativeElement;
    }

    private initColor(){

        if(!this.initialized){
            this.initialized = true;
            //jscolor = new jscolor(this.element, {hash:true, refine:false});
            
        }
    }

    onChange($event) {
        
        var color: string = $event.target.value.trim().replace('#',''); // remove whitespace and hash mark


        if(3 == color.length) {
            // convert rgb to rrggbb
            var rgb = color.split("");
            color = rgb[0] + rgb[0] + rgb[1] + rgb[1] + rgb[2] + rgb[2];
        }

        while(color.length < 6){
            color += "F";
        }
    
        color = color.substring(0, 6).toUpperCase(); // truncate down to 6 capital characters

        color = color.replace(/[^abcdefABCDEF0-9]/g, 'F'); // force non-hex valid chars to F

        color = '#' + color; // prepend # for valid hex code

        this.onUpdate.emit(color);
        this.updateColor(color);
    }

    private updateColor(color: string) {
        this.element.value = color;
        this.element.style.background = color;

        var light = this.isLight(color);
        this.element.style.color = light ? '#000000' : '#FFFFFF';
    }
    
    private isLight(color: string): boolean{
        var rgb = this.hexToRgb(color);

        if(rgb == null){
            return true;
        }

        return (
            0.213 * rgb.r +
            0.715 * rgb.g +
            0.072 * rgb.b >
            255 / 2
        );
    }

    private hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

}