import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Http, HttpModule, JsonpModule } from "@angular/http";
import { RouterModule } from "@angular/router";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { CKEditorModule } from "ng2-ckeditor";
import { EditorModule } from "@tinymce/tinymce-angular";
import { CorebridgeModule } from "../corebridge/corebridge.module";
import { EndNglibraryModule } from "@corebridge/end-nglibrary";

import { PopoverModule } from "ngx-bootstrap";

// components
import { BundleTableComponent } from "./components/bundle-table/bundle-table.component";
import { BundleAssociateTableComponent } from "./components/bundle-associate-table/bundle-associate-table.component";
import { CatalogsTableComponent } from "./components/catalogs-table/catalogs-table.component";
import { CategoriesTableComponent } from "./components/categories-table/categories-table.component";
import { CategoryNodesTableComponent } from "./components/category-nodes-table/category-nodes-table.component";
import { CategoryProfileListComponent } from "./components/category-profile-list/category-profile-list.component";
import { SkuProfileListComponent } from "./components/sku-profile-list/sku-profile-list.component";
import { CategoryProductsTableComponent } from "./components/category-products-table/category-products-table.component";
import { CategoryProductsAssociateTableComponent } from "./components/category-products-associate-table/category-products-associate-table.component";
import { ProductsTableComponent } from "./components/products-table/products-table.component";
import { ReportsListComponent } from "./components/reports-list/reports-list.component";
import { ProductAssociateTableComponent } from "./components/product-associate-table/product-associate-table.component";
import { ProductCategoriesTableComponent } from "./components/product-categories-table/product-categories-table.component";
import { ProductCategoriesAssociateTableComponent } from "./components/product-categories-associate-table/product-categories-associate-table.component";
import { UserTableComponent } from "./components/user-table/user-table.component";
import { AddonsTableComponent } from "./components/addons-table/addons-table.component";
import { AddonValuesTableComponent } from "./components/addon-values-table/addon-values-table.component";
import { StorefrontsTableComponent } from "./components/storefronts-table/storefronts-table.component";
import { OrdersTableComponent } from "./components/orders-table/orders-table.component";
import { ContactsTableComponent } from "./components/contacts-table/contacts-table.component";
import { CompaniesTableComponent } from "./components/companies-table/companies-table.component";
import { EmailTemplatesTableComponent } from "./components/email-templates-table/email-templates-table.component";
import { ContentPagesTableComponent } from "./components/content-pages-table/content-pages-table.component";
import { PaymentSettingsTableComponent } from "./components/payment-settings-table/payment-settings-table.component";
import { PortalProfilesTableComponent } from "./components/portal-profiles-table/portal-profiles-table.component";
import { ProfilesTableComponent } from "./components/profiles-table/profiles-table.component";
import { RedirectsTableComponent } from "./components/redirects-table/redirects-table.component";
import { DomainsTableComponent } from "./components/domains-table/domains-table.component";
import { ProductAssetsTableComponent } from "./components/product-assets-table/product-assets-table.component";
import { ProductImagesTableComponent } from "./components/product-images-table/product-images-table.component";
import { ProductImageCustomComponent } from "./components/product-image-custom/product-image-custom.component";
import { ProductInformationInsertImageComponent} from "./components/product-information-insert-image/product-information-insert-image.component"
import { ProductTabDetailComponent } from "./components/product-tabs/product-tab-detail.component";
import { ProductTiersTableComponent } from "./components/product-tiers-table/product-tiers-table.component";
import { ProductAddonsTableComponent } from "./components/product-addons-table/product-addons-table.component";
import { ProductAddonsAssociateTableComponent } from "./components/product-addons-associate-table/product-addons-associate-table.component";
import { ImageUploadComponent } from "./components/image-upload/image-upload.component";
import { StylesTableComponent } from "./components/styles-table/styles-table.component";
import { StylesPreviewComponent } from "./components/styles-preview/styles-preview.component";
import { PortalStylesTableComponent } from "./components/portal-styles-table/portal-styles-table.component";
import { ShippingMethodsTableComponent } from "./components/shipping-methods-table/shipping-methods-table.component";
import { ShippingBoxesTableComponent } from "./components/shipping-boxes-table/shipping-boxes-table.component";

import { ReviewsTableComponent } from "./components/reviews-table/reviews-table.component";
import { FontsTableComponent } from "./components/fonts-table/fonts-table.component";
import { ContactProfilesTableComponent } from "./components/contact-profiles-table/contact-profiles-table.component";
import { FontLinksTableComponent } from "./components/font-links-table/font-links-table.component";
import { FontLinksAssociateTableComponent } from "./components/font-links-associate-table/font-links-associate-table.component";
import { FontGroupsTableComponent } from "./components/font-groups-table/font-groups-table.component";
import { FontGroupLinksTableComponent } from "./components/font-group-links-table/font-group-links-table.component";
import { FontGroupLinksAssociateTableComponent } from "./components/font-group-links-associate-table/font-group-links-associate-table.component";
import { FontUploadComponent } from "./components/font-upload/font-upload.component";
import { ColorsTableComponent } from "./components/colors-table/colors-table.component";
import { ColorLinksTableComponent } from "./components/color-links-table/color-links-table.component";
import { ColorLinksAssociateTableComponent } from "./components/color-links-associate-table/color-links-associate-table.component";
import { ColorPickerComponent } from "./components/color-picker/color-picker.component";
import { ColorGroupsTableComponent } from "./components/color-groups-table/color-groups-table.component";
import { ColorGroupLinksTableComponent } from "./components/color-group-links-table/color-group-links-table.component";
import { ColorGroupLinksAssociateTableComponent } from "./components/color-group-links-associate-table/color-group-links-associate-table.component";
import { ProductDefaultsComponent } from "./pages/product-defaults/product-defaults.component";

import { StoreThemeModalComponent } from "./components/store-theme-modal/store-theme-modal.component";
import { StoreThemePreviewComponent } from "./components/store-theme-preview/store-theme-preview.component";
import { StoreThemeColorBarComponent } from "./components/store-theme-color-bar/store-theme-color-bar.component";

//directives
import { ColorPickerDirective } from "./directives/color-picker.directive";

// pages
import { ProfileComponent } from "./pages/profile/profile.component";
import { CompaniesComponent } from "./pages/customers/companies/companies.component";
import { ContactsComponent } from "./pages/customers/contacts/contacts.component";
import { ContactDetailsComponent } from "./pages/customers/contacts/details/contact-details.component";
import { CompanyDetailsComponent } from "./pages/customers/companies/details/company-details.component";
import { CatalogsComponent } from "./pages/catalogs/catalogs.component";
import { CatalogDetailsComponent } from "./pages/catalog-details/catalog-details.component";
import { CatalogNewComponent } from "./pages/catalog-new/catalog-new.component";
import { CatalogAssociateComponent } from "./pages/catalog-associate/catalog-associate.component";
import { CategoryNodeDetailsComponent } from "./pages/category-node-details/category-node-details.component";
import { CategoriesComponent } from "./pages/categories/categories.component";
import { CategoryDetailComponent } from "./pages/category-detail/category-detail.component";
import { CategoryNewComponent } from "./pages/category-new/category-new.component";
import { AddonsComponent } from "./pages/addons/addons.component";
import { AddonDetailsComponent } from "./pages/addon-details/addon-details.component";
import { AddonNewComponent } from "./pages/addon-new/addon-new.component";
import { AddonValueNewComponent } from "./pages/addon-value-new/addon-value-new.component";
import { AddonValueDetailsComponent } from "./pages/addon-value-details/addon-value-details.component";
import { ProductsComponent } from "./pages/products/products.component";
import { ProductDetailsComponent } from "./pages/product-details/product-details.component";
import { ProductAssociateComponent } from "./pages/product-associate/product-associate.component";
import { ProductNewComponent } from "./pages/product-new/product-new.component";
import { ProductImageDetailsComponent } from "./pages/product-image-details/product-image-details.component";
import { ProductImageNewComponent } from "./pages/product-image-new/product-image-new.component";
import { ProductTierDetailsComponent } from "./pages/product-tier-details/product-tier-details.component";
import { ProductTierNewComponent } from "./pages/product-tier-new/product-tier-new.component";
import { StorefrontsComponent } from "./pages/storefronts/storefronts.component";
import { StorefrontDetailsComponent } from "./pages/storefront-details/storefront-details.component";
import { StorefrontNewComponent } from "./pages/storefront-new/storefront-new.component";
import { StylesComponent } from "./pages/styles/styles.component";
import { StyleDetailsComponent } from "./pages/style-details/style-details.component";
import { StyleNewComponent } from "./pages/style-new/style-new.component";
import { PortalProfileNewComponent } from "./pages/portal-profile-new/portal-profile-new.component";
import { PortalProfileDetailsComponent } from "./pages/portal-profile-details/portal-profile-details.component";
import { DomainDetailsComponent } from "./pages/domain-details/domain-details.component";
import { DomainNewComponent } from "./pages/domain-new/domain-new.component";
import { OrdersComponent } from "./pages/orders/orders.component";
import { EmailTemplatesComponent } from "./pages/email-templates/email-templates.component";
import { EmailTemplateDetailsComponent } from "./pages/email-template-details/email-template-details.component";
import { EmailTemplateNewComponent } from "./pages/email-template-new/email-template-new.component";
import { ContentPageDetailsComponent } from "./pages/content-page-details/content-page-details.component";
import { ContentPageNewComponent } from "./pages/content-page-new/content-page-new.component";
import { ProfilesComponent } from "./pages/profiles/profiles.component";
import { ProfileDetailsComponent } from "./pages/profile-details/profile-details.component";
import { ProfileNewComponent } from "./pages/profile-new/profile-new.component";
import { ReportingComponent } from "./pages/reporting/reporting.component";
import { ReportingCompanyFinancialComponent } from "./pages/reporting/company_financial/company_financial.component";
import { ReportingCustomersReceivablesComponent } from "./pages/reporting/customers_receivables/customers_receivables.component";
import { ReportingSalesComponent } from "./pages/reporting/sales/sales.component";
import { ReportingProductionComponent } from "./pages/reporting/production/production.component";
import { ReportingFranchiseComponent } from "./pages/reporting/franchise/franchise.component";
import { ComingSoonComponent } from "./pages/coming-soon/coming-soon.component";
import { ShippingMethodsComponent } from "./pages/shipping-methods/shipping-methods.component";
import { ShippingMethodDetailsComponent } from "./pages/shipping-method-details/shipping-method-details.component";
import { ShippingMethodNewComponent } from "./pages/shipping-method-new/shipping-method-new.component";
import { ShippingBoxesComponent } from "./pages/shipping-boxes/shipping-boxes.component";
import { ShippingBoxDetailsComponent } from "./pages/shipping-box-details/shipping-box-details.component";
import { ShippingBoxNewComponent } from "./pages/shipping-box-new/shipping-box-new.component";

import { ReviewsComponent } from "./pages/reviews/reviews.component";
import { ReviewDetailsComponent } from "./pages/review-details/review-details.component";
import { FontsComponent } from "./pages/settings/fonts/fonts.component";
import { FontNewComponent } from "./pages/settings/font-new/font-new.component";
import { FontDetailsComponent } from "./pages/settings/font-details/font-details.component";
import { FontGroupsComponent } from "./pages/settings/font-groups/font-groups.component";
import { FontGroupNewComponent } from "./pages/settings/font-group-new/font-group-new.component";
import { FontGroupDetailsComponent } from "./pages/settings/font-group-details/font-group-details.component";
import { NotFoundComponent } from "./pages/not-found/not-found.component";
import { SettingsComponent } from "./pages/settings/settings.component";
import { ColorsComponent } from "./pages/settings/colors/colors.component";
import { ColorNewComponent } from "./pages/settings/color-new/color-new.component";
import { ColorDetailsComponent } from "./pages/settings/color-details/color-details.component";
import { ColorGroupsComponent } from "./pages/settings/color-groups/color-groups.component";
import { ColorGroupNewComponent } from "./pages/settings/color-group-new/color-group-new.component";
import { ColorGroupDetailsComponent } from "./pages/settings/color-group-details/color-group-details.component";

//services
import { DomainService } from "../../services/domain.service";
import { AlertService } from "../../services/alert.service";
import { ZnodeService } from "../../services/znode.service";
import { ZnodeApiService } from "../../services/znode-api.service";
import { CoreBridgeService } from "../../services/corebridge.service";
import { DaimService } from "../../services/daim.service";

// pipes
import { ImagePathPipe } from "../../pipes/image-path.pipe";
import { ColorPipe } from "../../pipes/color.pipe";
import { ColorPickerModule } from "primeng/colorpicker";
import { Select2Module } from "ng2-select2";
import { PortalCountriesComponent } from "./components/portal-countries-table";
import { OrderApprovalGroupComponent } from "./components/order-approval-group/order-approval-group.component";

// content editor
import { ContentSourceSpinnerComponent } from "./components/content-source-spinner/content-source-spinner.component";

import { OrderApprovalInputFieldComponent } from "./components/order-approval-input-field/order-approval-input-field.component";
import { TypeaheadModule } from "ngx-bootstrap/typeahead";

// Dev Support
import { DevSupportComponent } from "./pages/developer/dev-support.component";
import { FeaturesComponent } from "./pages/features/features.component";
import { EventLogComponent } from "./pages/eventlog/event-log.component";

// PrimeNG
import { TabViewModule } from "primeng/tabview";
import { InputMaskModule } from "primeng/components/inputmask/inputmask";
import { CalendarModule } from "primeng/components/calendar/calendar";
import { AngularFontAwesomeModule } from "angular-font-awesome";
import { PanelModule } from "primeng/panel";
import { ButtonModule } from "primeng/button";
import { SplitButtonModule } from "primeng/splitbutton";
import { InputSwitchModule } from "primeng/inputswitch";
import { TableModule } from "primeng/table";
import { DialogModule } from "primeng/dialog";

import { MenubarModule } from "primeng/menubar";
import { ProgressBarModule } from "primeng/progressbar";
import { InputTextModule } from "primeng/inputtext";
import { DropdownModule } from "primeng/dropdown";
import { CheckboxModule } from "primeng/checkbox";
import { MessagesModule } from "primeng/messages";
import { MessageModule } from "primeng/message";
import { TooltipModule } from "primeng/tooltip";
import { AutoCompleteModule } from "primeng/autocomplete";
import { ListboxModule } from "primeng/listbox";
import { AccordionModule } from 'primeng/accordion';


import {
  HttpClient,
  HttpHandler,
  HttpClientModule
} from "@angular/common/http";

import { ExcelService } from "../../services/excel.service";
import { EditorPresetsComponent } from './pages/editor-presets/editor-presets.component';
import { EditorPresetsTableComponent } from './components/editor-presets-table/editor-presets-table.component';
import { EditorPresetNewComponent } from './pages/editor-preset-new/editor-preset-new.component';
import { SurfacePresetsComponent } from './pages/surface-presets/surface-presets.component';
import { SurfacePresetsTableComponent } from './components/surface-presets-table/surface-presets-table.component';
import { SurfacePresetDetailsComponent } from './pages/surface-preset-details/surface-preset-details.component';
import { AdvancedColorPickerComponent } from './components/advanced-color-picker/advanced-color-picker.component'
import { CustomersCanvasComponent } from './components/customers-canvas/customers-canvas.component';
import { CustomerCanvasIntegrationModalComponent } from './components/customer-canvas-integration-modal/customer-canvas-integration-modal.component';
import { ImagePropertiesDialogComponent } from "./components/image-properties-dialog/image-properties-dialog.component";
import {FileUploadModule} from 'primeng/fileupload';
import { FileUploadModule as FileUploadModule2 } from 'ng2-file-upload';
import { UploadFileToGenerateStateModalComponent } from './components/upload-file-to-generate-state-modal/upload-file-to-generate-state-modal.component';





@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    Select2Module,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    JsonpModule,
    RouterModule,
    CKEditorModule,
    AngularFontAwesomeModule,
    CorebridgeModule,
    EndNglibraryModule,
    PopoverModule,

    // PrimeNG
    InputMaskModule,
    CalendarModule,
    ColorPickerModule,
    AngularFontAwesomeModule,
    EditorModule,
    ButtonModule,
    PanelModule,
    TabViewModule,
    TableModule,
    DialogModule,
    ButtonModule,
    MenubarModule,
    ProgressBarModule,
    InputTextModule,
    DropdownModule,
    SplitButtonModule,
    InputSwitchModule,
    PanelModule,
    CheckboxModule,
    MessagesModule,
    MessageModule,
    TooltipModule,
    AutoCompleteModule,
    ListboxModule,
    AccordionModule,

    TypeaheadModule.forRoot(),
    FileUploadModule,
    FileUploadModule2,

  ],
  declarations: [
    ImagePathPipe,
    ColorPipe,
    BundleTableComponent,
    BundleAssociateTableComponent,
    ImageUploadComponent,
    CategoryProfileListComponent,
    SkuProfileListComponent,
    CategoryProductsTableComponent,
    CategoryProductsAssociateTableComponent,
    CatalogsTableComponent,
    CategoriesTableComponent,
    CategoryNodesTableComponent,
    ProductsTableComponent,
    ReportsListComponent,
    ProductAssociateTableComponent,
    ProductCategoriesTableComponent,
    ProductCategoriesAssociateTableComponent,
    UserTableComponent,
    AddonsTableComponent,
    AddonValuesTableComponent,
    StorefrontsTableComponent,
    OrdersTableComponent,
    CompaniesTableComponent,
    ContactsTableComponent,
    PortalProfilesTableComponent,
    ProfilesTableComponent,
    RedirectsTableComponent,
    DomainsTableComponent,
    ProductAssetsTableComponent,
    ProductImagesTableComponent,
    ProductImageCustomComponent,
    ProductInformationInsertImageComponent,
    ProductTabDetailComponent,
    ProductTiersTableComponent,
    ProductAddonsTableComponent,
    ProductAddonsAssociateTableComponent,
    EmailTemplatesTableComponent,
    ContentPagesTableComponent,
    PaymentSettingsTableComponent,
    PortalCountriesComponent,
    ProfileComponent,
    ContactsComponent,
    ContactDetailsComponent,
    CompanyDetailsComponent,
    CatalogsComponent,
    CatalogDetailsComponent,
    CatalogNewComponent,
    CatalogAssociateComponent,
    CategoryNodeDetailsComponent,
    CategoriesComponent,
    CategoryDetailComponent,
    CategoryNewComponent,
    AddonsComponent,
    AddonDetailsComponent,
    AddonNewComponent,
    AddonValueNewComponent,
    AddonValueDetailsComponent,
    ProductsComponent,
    ProductDetailsComponent,
    ProductAssociateComponent,
    ProductNewComponent,
    ProductImageDetailsComponent,
    ProductImageNewComponent,
    ProductTierDetailsComponent,
    ProductTierNewComponent,
    StorefrontsComponent,
    StorefrontDetailsComponent,
    StorefrontNewComponent,
    StylesComponent,
    StylesTableComponent,
    StylesPreviewComponent,
    PortalStylesTableComponent,
    ShippingBoxesTableComponent,
    ShippingMethodsTableComponent,

    ReviewsTableComponent,
    ColorPickerDirective,
    StyleDetailsComponent,
    StyleNewComponent,
    PortalProfileDetailsComponent,
    PortalProfileNewComponent,
    DomainDetailsComponent,
    OrdersComponent,
    EmailTemplatesComponent,
    EmailTemplateDetailsComponent,
    EmailTemplateNewComponent,
    ProfilesComponent,
    ProfileDetailsComponent,
    ProfileNewComponent,
    ReportingComponent,
    ReportingCompanyFinancialComponent,
    ReportingCustomersReceivablesComponent,
    ReportingSalesComponent,
    ReportingProductionComponent,
    ReportingFranchiseComponent,
    ComingSoonComponent,
    ShippingMethodsComponent,
    ShippingMethodDetailsComponent,
    ShippingMethodNewComponent,
    ShippingBoxesComponent,
    ShippingBoxDetailsComponent,
    ShippingBoxNewComponent,
    ReviewsComponent,
    ReviewDetailsComponent,
    NotFoundComponent,
    CompaniesComponent,
    DomainNewComponent,
    ContentPageDetailsComponent,
    ContentPageNewComponent,
    FontsComponent,
    ContactProfilesTableComponent,
    FontsTableComponent,
    FontLinksTableComponent,
    FontLinksAssociateTableComponent,
    FontNewComponent,
    FontDetailsComponent,
    FontGroupsTableComponent,
    FontGroupLinksTableComponent,
    FontGroupLinksAssociateTableComponent,
    FontGroupsComponent,
    FontGroupNewComponent,
    FontGroupDetailsComponent,
    FontUploadComponent,
    SettingsComponent,
    ColorsComponent,
    ColorsTableComponent,
    ColorNewComponent,
    ColorPickerComponent,
    ColorDetailsComponent,
    ColorGroupsComponent,
    ColorGroupsTableComponent,
    ColorGroupNewComponent,
    ColorGroupDetailsComponent,
    ColorLinksTableComponent,
    ColorLinksAssociateTableComponent,
    ColorGroupLinksTableComponent,
    ColorGroupLinksAssociateTableComponent,
    ProductDefaultsComponent,
    OrderApprovalGroupComponent,
    StoreThemeModalComponent,
    StoreThemePreviewComponent,
    StoreThemeColorBarComponent,

    ContentSourceSpinnerComponent,
    OrderApprovalInputFieldComponent,

    DevSupportComponent,
    FeaturesComponent,
    EventLogComponent,
    EditorPresetsComponent,
    EditorPresetsTableComponent,
    EditorPresetNewComponent,
    SurfacePresetsComponent,
    SurfacePresetsTableComponent,
    SurfacePresetDetailsComponent,
    AdvancedColorPickerComponent,
    CustomersCanvasComponent,
    CustomerCanvasIntegrationModalComponent,
    ImagePropertiesDialogComponent,
    UploadFileToGenerateStateModalComponent
  ],
  providers: [
    HttpClient,
    DomainService,
    AlertService,
    ZnodeService,
    CoreBridgeService,
    DaimService,
    ZnodeApiService,
    ExcelService
  ]
})
export class EcommerceModule {}
