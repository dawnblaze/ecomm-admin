export class ApprovalRule{
    ID: number;
    ApprovalGroupID: number;
    ApprovalRuleTypeID: number;
    ApprovalRuleLimitTypeID: number;
    IsActive: boolean;
    CurrencyLimit: number;
    DaysLimit: number;
    Priority: number;
}
