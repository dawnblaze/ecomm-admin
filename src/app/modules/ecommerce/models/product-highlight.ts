export class ProductHighlight {
    Description: string;
    DisplayOrder: number;
    ImageAltTag: string;
    ImageFile: string;
    ImageLargePath: any;
    ImageMediumPath: any;
    ImageSmallPath: string;
    ImageSmallThumbnailPath: any;
    ImageThumbnailPath: any;
    HighlightId: number;
    HighlightType: any;
    HighlightTypeName: string;
    HighlightTypeId: number;
    Hyperlink: string;
    HyperlinkNewWindow: any;
    IsActive: any;
    LocaleId: number;
    Name: string;
    PortalId: any;
    ProductId: number;
    ShortDescription: any;
    DisplayPopup: boolean;
    ProductHighlightID: number;
    IsAssociatedProduct: boolean;
    UserName: any;
    UserType: any;
}
