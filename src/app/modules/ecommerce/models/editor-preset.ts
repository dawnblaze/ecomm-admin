import { EditorPresetJsonModel } from "./editor-preset-json";

export class EditorPreset {
    id: number;
    isDeleted: boolean;
    isPrimary: boolean;
    name: string;
    presetJSON: string;
    //presetJSON: EditorPresetJsonModel;
    sortIndex: number;
}
