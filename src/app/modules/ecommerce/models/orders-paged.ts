import { Order } from "../../../modules/ecommerce/models/order";

export class OrdersPaged
{
    Orders: Order[];
    PageIndex: number;
    PageSize: number;
    TotalPages: number;
    TotalResults: number;
}