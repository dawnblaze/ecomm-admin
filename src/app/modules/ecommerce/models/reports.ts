export class ReportSection {
    Id: number;
    Name: string;
    Description: string;
    SubSections: ReportSubSection[];
}

export class ReportSubSection {
    Id: number;
    Name: string;
    Description: string;
    SubSections: ReportSubSection;
    Reports: Report[];
}

export class Report {
    Id: number;
    Name: string;
    Description: string;
    Link: string;
}