export class ShippingBox {
    Active: boolean = true;
    BoxWeight: boolean;
    Depth: number;
    DisplayName: string;
    FluteDirection: number = 2;
    Length: number;
    MaxWeightCapacity: number;
    ProfileId: number = null;
    ProfileName: string;
    ShippingBoxId: number;
    Width: number;
}
