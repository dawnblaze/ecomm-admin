export class ConfigSetting {
    ID: number;
    BID: number;
    EnableCarrierRate = false;
    EnableOnlineTaxLookup = false;
    EnableFileUpload = false;
    EnableTermsAndConditions = false;
    EnableVariablePricing = false;
    EnableOrderApprovals = false;
    EnableCustomersCanvas = false;
}
