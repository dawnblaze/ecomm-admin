export class Addon {
    AccountId: any;
    AddOnId: number;
    AllowBackOrder: boolean;
    BackOrderMessage: string = '';
    Description: string = '';
    DisplayOrder: number = 99;
    DisplayType: string = "DropDownList";
    ExternalApiId: any;
    ExternalId: any;
    InStockMessage: string = '';
    IsOptional: boolean;
    LocaleId: number;
    Name: string;
    OutOfStockMessage: string = '';
    PortalId: any;
    ProductId: number;
    PromptMessage: string = '';
    Title: string;
    TrackInventory: boolean = false;
    ProductAddOnId: number;
    AddOnValues: any[];
    IsAssociatedAddOnValue: boolean;
    AddOnValueName: any;
    UserName: any
}
