import { Product } from "../../../modules/ecommerce/models/product";

export class ProductsPaged
{
    Products: Product[];
    PageIndex: number;
    PageSize: number;
    TotalPages: number;
    TotalResults: number;
}