export class Supplier {
    ContactEmail: string;
    ContactFirstName: string;
    ContactLastName: string;
    ContactPhone: string;
    Custom1: string;
    Custom2: string;
    Custom3: string;
    Custom4: string;
    Custom5: string;
    Description: string;
    DisplayOrder: number;
    EmailNotificationTemplate: string;
    EnableEmailNotification: boolean;
    ExternalId: number;
    ExternalSupplierNumber: any;
    IsActive: boolean;
    Name: string;
    NotificationEmail: string;
    PortalId: number;
    SupplierId: number;
    SupplierType: any;
    SupplierTypeId: number;
    Address: any;
    Account: any;
    ClassName: any;
  }
