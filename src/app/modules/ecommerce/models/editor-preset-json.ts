export class EditorPresetJsonModel {
    dpiQualityMeter: boolean;
    spellCheckEnabled: boolean;
    reflightWarnings: boolean;
    dpiWarningLevel: string;
    dpiFailureLevel: string;
    initialMode: string;
    fontListMode: string;
    imageEditorEnabled: boolean;
    arbitraryResizeForImageItemEnabled: boolean;
    canvas: Canvas;
    rendering: Rendering;
    canvasOnlyMode: boolean;
}

export class Canvas {
    safetyLinesVisible: boolean;
    rulers: Rulers;
    snapLines: SnapLines;
}

export class Rulers {
    enabled: boolean;
    unit: string;
    customUnitScale: string;
    origin: Origin;
}

export class Origin {
  X: number;
  Y: number;
}

export class SnapLines {
    enabled: boolean;
    color: string;
    tolerance: number;
}

export class Rendering {
    hiResOutputDpi: number;
    hiResOutputFileFormat: string;
    hiResOutputColorSpace: string;
    hiResOutputToSeparateFiles: boolean;
    hiResOutputCompression: string;
    hiResOutputJpegCompressionQuality: number;
    hiResOutputFlipMode: string;
    proofImageFileFormat: string;
    proofImageSafetyLinesEnabled: boolean;
    proofImageFlipMode: string;
}
