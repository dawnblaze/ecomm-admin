import { Account } from "../../../modules/ecommerce/models/account";

export class AccountsPaged
{
    Accounts: Account[];
    PageIndex: number;
    PageSize: number;
    TotalPages: number;
    TotalResults: number;
}