export class ProductArtwork {
  ID: string;
  ProductID: number;
  Name: string;
  IsActive: boolean = false;
  RequireCustomization: boolean = false;
}
