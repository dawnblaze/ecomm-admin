import { ColorGroup } from './color-group';

export class Color {
  id: number;
  bid: number;
  name: string;
  groups: ColorGroup[];
  c: number = 0;
  m: number = 0;
  y: number = 0;
  k: number = 100;
  r: number = 0;
  g: number = 0;
  b: number = 0;
  CMYK: string;
  RGB: string;
  printAsSpot: boolean = false;
  spotName: string = 'none';
}
