import { Profile } from "./profile";

export class Contact {
    "FirstName": string;
    "LastName": string;
    "Email": string;
    "CompanyName": string;
    "ExternalId": any;
    "PhoneNumber": any;
    "FullName": string;
    "City": string;
    "PostalCode": string;
    "StateCode": string;
    "UserName": string;
    "ParentAccountId": number = 0;
    "AccountId": number;
    "IsConfirmed": boolean;
    "CreateDate": string;
    "UpdateDate": string;
    "EnableCustomerPricing": boolean;
    "UserId": string;
    "ParentCompanyName": string;
    "AccountTypeId": any;
    "Department": any;
    "UserType": any;
    "FullAddress": any;
    "ProfileId": number;
    "IsLockedOut": boolean;
    "Profiles": Profile[];
}
