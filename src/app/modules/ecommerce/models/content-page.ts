export class ContentPage {
    ContentPageID: number;
    Name: string;
    PortalID: number;
    Title: string;
    SEOMetaKeywords: string = "";
    SEOMetaDescription: string = "";
    AllowDelete: boolean = true;
    TemplateName: string = "";
    ActiveInd: boolean = true;
    AnalyticsCode: any = null;
    SEOURL: string = "";
    LocaleId: number = 43;
    MetaTagAdditional: string = null;
    ThemeID: number = null;
    CSSID: number = null;
    MasterPageID: number = null;
    SEOTitle: string = "";
    OldHtml: string = null;
    CustomErrorMessage: string = "";
    StoreName: string = "";
    MasterPageName: string = "";
    Html: string = "";
    UpdatedUser: string = "";
    MappedSeoUrl: string = null;
    IsUrlRedirectEnabled: boolean = false;
    IsNameAvailable: boolean = false;
}
