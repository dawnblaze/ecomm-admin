export class ShippingCarrierServiceType{
    ID: number;
    ShippingCarrierID: number;
    Name: string;
    Active: boolean;
}