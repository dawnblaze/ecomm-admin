export class GenerateStateFileResponse {
    productId: number;
    stateFileName: string;
    generatedSuccessfully: boolean;
    message: string;
    stateFileUrl: string;
    stateFileUrlForDownload: string;
}