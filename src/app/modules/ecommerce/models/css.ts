import { Theme } from './theme';

export class CSS {
    CSSID: number;
    ThemeID: number;
    Name: string;
    FooterColumnLabel: any = {
      "Column1": "",
      "Column2": "",
      "Column3": ""
    };
    StyleDictionary: any = {
        "base-font-family": "\"Times New Roman\", Times, serif;",
        "base-font-size": "14px",
        "header-background": "#FFFFFF",
        "header-shrink-bg-color": "#FFFFFF",
        "heading-text-color": "#343434",
        "button-wishlist-color": "#343434",
        "button-bg-boulder-wishlist": "#343434",
        "checkbox-radio-label": "#343434",
        "blue-color": "#CC0000",
        "input-text-color": "#666666",
        "color-select-shark": "#666666",
        "checkbox-radio-color": "#CC0000",
        "base-color-primary": "#CC0000",
        "base-color-secondary": "#343434",
        "footer-text": "#626262",
        "footer-default-text-color": "#626262",
        "footer-payment-icon": "#626262",
        "white-color":"#fff",
        "success-msg":"#5cb85c",
        "error-msg":"#cc0000",
        "required-star":"#cc0000",
        "button-bg-primary":"#cc0000",
        "button-bg-secondary":"#717171",
        "border-default-color":"#c3c3c3",
        "header-bg-color":"#fff",
        "header-nav-bg-color":"#343434",
        "nav-link-hover-bg":"#cc0000",
        "top-header-nav-text":"#000000",
        "top-header-nav-bg-color":"#343434",
        "top-header-nav-link-hover-bg":"#cc0000",
        "footer-bg-color":"#252525",
        "footer-news-letter-bg":"#2e2e2e"
    };
    Theme: Theme;
}