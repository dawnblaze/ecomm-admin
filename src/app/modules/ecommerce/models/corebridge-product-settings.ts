export class CoreBridgeProductSettings {
    ZnodeProductId: number = null;
    IntegrationTypeId: number = null;
    CoreBridgeQuickProductId: number = null;
    CoreBridgeQuickProductCategoryId: number = null;
    CoreBridgeProductCategoryId: number = null;
    CoreBridgeIncomeAccountId: number = null;
}