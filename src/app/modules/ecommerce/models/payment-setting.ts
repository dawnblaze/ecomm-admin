export class PaymentSetting {
    PaymentSettingID: number;
    PaymentTypeID: number;
    ProfileID: number;
    GatewayTypeID: number;
    GatewayUsername: string;
    GatewayPassword: string;
    EnableVisa: boolean;
    EnableMasterCard: boolean;
    EnableAmex: boolean;
    EnableDiscover: boolean;
    EnableRecurringPayments: boolean;
    EnableVault: boolean;
    TransactionKey: string;
    ActiveInd: boolean;
    DisplayOrder: number;
    TestMode: boolean;
    Partner: string;
    Vendor: string;
    PreAuthorize: boolean;
    IsRMACompatible: boolean;
}
