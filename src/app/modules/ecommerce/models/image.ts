export class Image {
    EntityName: any;
    Id:number;
    ImagePath: string;
    ImagePathLarge: string;
    ImagePathMedium: string;
    ImagePathSmall: string;
    ImagePathThumbnail: string;
}