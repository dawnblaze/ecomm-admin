export class AccountSearchParameters{
  PortalID: number;
  ApprovalGroupID: number;
  SearchString: string;
  Type: string;
}
