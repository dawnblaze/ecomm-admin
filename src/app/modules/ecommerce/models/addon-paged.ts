import { Addon } from "../../../modules/ecommerce/models/addon";

export class AddOnsPaged
{
    AddOns: Addon[];
    PageIndex: number;
    PageSize: number;
    TotalPages: number;
    TotalResults: number;
}