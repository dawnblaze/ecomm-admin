export class ShippingCarrierRateRule{
    ID: number;
    ShippingID: number;
    ShippingRuleTypeID: number;
    ShippingCarrierID: number;
    ShippingCarrierServiceTypeID: number;
    ShippingInsuranceSettingID: number;
}