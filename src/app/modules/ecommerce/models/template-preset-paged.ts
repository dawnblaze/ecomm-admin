import { TemplatePreset } from "./template-preset";

export class TemplatePresetPaged
{
    records: TemplatePreset[];
    pageIndex: number;
    pageSize: number;
    totalPages: number;
    totalResults: number;
}