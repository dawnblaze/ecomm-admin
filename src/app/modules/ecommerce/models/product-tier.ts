export class ProductTier {
    Price: number;
    ProductId: number;
    ProductTierId: number;
    ProfileId: number = 0;
    TierEnd: number;
    TierStart: number;
    ProfileName: string;
    Discount: any;
    IsPercentageAmount: any;
}
