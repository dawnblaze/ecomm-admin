export enum DMBucketType {
    Documents = 0,
    Data = 1,
    Reports = 2
}