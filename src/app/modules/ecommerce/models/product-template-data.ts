import { ProductTemplateSurfaceData } from './product-template-surface-data';

export class ProductTemplateData {
  id: number;
  bid: number;
  templateLinkId: number;
  isActive: boolean;
  isDeleted: boolean;
  modifiedDt: Date;
  name: string;
  stateId: string;
  stateLocation: string;
  templatePresetId: number;
  templatePresetOverrideJson: string;
  surfaces: ProductTemplateSurfaceData[];
}
