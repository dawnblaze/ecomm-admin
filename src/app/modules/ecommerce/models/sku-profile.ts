export class SkuProfile {
    SkuProfileEffectiveID: number;
    SkuId: number;
    ProfileId: number;
    Name: string;
    EffectiveDate: string;
}