export class Catalog {
    CatalogId: number;
    ExternalId: any;
    IsActive: boolean = true;
    Name: string;
    PortalId: any;
    PreserveCategories: boolean;
    ProductIds: string;
    IsDefault: boolean;
    IsFromSitecore: boolean;
    CategoryCount: number;
    ProductCount: number;
}
