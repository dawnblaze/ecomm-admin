export class IncomeAccount {
    Id: number;
    Name: string;
    GLCode: string;
    GLAccountTypeId: number = 5;
}