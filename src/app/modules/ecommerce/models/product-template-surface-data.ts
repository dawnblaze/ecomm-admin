export class ProductTemplateSurfaceData {
  id: number;
  bid: number;
  isDeleted: boolean;
  modifiedDt: Date;
  name: string;
  templatePresetId: number;
  templatePresetOverrideJson: string;
}
