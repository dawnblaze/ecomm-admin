export class ShippingCarrier {
    ID: number;
    Name: string;
    Active: boolean;
}