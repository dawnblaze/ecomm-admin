export interface TemplatePresetJsonModel {
    safetyLines: SafetyLine[];
}

export class SafetyLine {
    name: string;
    color: string;
    altColor: string;
    borderRadius: string;
    margin: Margin;
    stepPx: number;
    widthPx: number;
    pdfBox: string;
}

export class Margin {
    left: number;
    top: number;
    right: number;
    bottom: number;
}