export class ShippingRule {
    BaseCost: number;
    ClassName: any;
    Custom1: any;
    Custom2: any;
    Custom3: any;
    ExternalId: any;
    errorMessage: any;
    LowerLimit: number = 1;
    PerItemCost: number;
    ShippingOption: any;
    ShippingOptionId: number;
    ShippingRuleId: number;
    ShippingRuleType: any;
    ShippingRuleTypeId: number;
    UpperLimit: number = 9999;
}