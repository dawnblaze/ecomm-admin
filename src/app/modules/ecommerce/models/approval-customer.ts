import { ApprovalApprover } from "./approval-approver";

export class ApprovalCustomer extends ApprovalApprover{
    PortalID: number;
    IsExempt: boolean = false;
}