import { Product } from './product';

export class Bundle {
    ParentChildProductID: number;
    ParentProductID: number;
    ChildProductID: number;
    ProductName: string;
    IsActive: boolean;
    product?: Product;
}