export class AccountSearchModel{
  AccountID: number;
  DisplayName: string;
  Email: string;
  ParentAccountID: number;
}
