import { Address } from './address';

export class User {
  AcceptedTermsAndConditions : boolean;
  AcceptedTermsAndConditionsDate : string;
  AcceptedUserEulaDate : string;
  Address : Address;
  AnniversaryDate : string;
  BirthDate : string;
  CanLogin : boolean;
  CommissionPlanId : number;
  CreatedByUserId : number;
  CreatedDateUTC : string;
  DefaultModuleId : number;
  DefaultPhoneId : number;
  EditSequence : number;
  EmailAddress : string;
  FirstName : string;
  firstName : string;
  Gender : string;
  HireDate : string;
  Id : number;
  IsActive : boolean;
  LastLogin : string;
  LastName : string;
  lastName : string;
  LocationId : number;
  ModifiedByUserId : number;
  ModifiedDateUTC : string;
  ReleaseDate : string;
  UserImageLocation : any;
  UserIsOnline : boolean;
  UserName : string;
  UserType: string;
}



