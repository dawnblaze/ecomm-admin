import { Product } from './product';

export class ProductCategory {
    BeginDate: any;
    Category: any;
    CategoryId: number;
    CssId: any;
    DisplayOrder: any;
    EndDate: any;
    IsActive: boolean = true;
    MasterPageId: any;
    Product: Product;
    ProductCategoryId: number;
    ProductId: number;
    ThemeId: number;
}