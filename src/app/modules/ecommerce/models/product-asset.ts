export class ProductAsset {
    ProductAssetId: number;
    ProductId: number;
    ProductFile: string = "";
    FileExtension: string = "";
    FileSize: string = "";
    FileURI: string = "";
    DateUploaded: any;
    IsUploading: boolean = false;
    HasError: boolean = false;
    ErrorMessage: string = "";
    UploadProgress: number = 0;
    UploadMessage: string = "";
    XmlHttpRequest: any = null;
}