export class ControlUser {
  id: number;
  ecommbid: number;
  isactive: boolean;
  username: string;
  usertype: string;
  emailaddress: string;
  firstname: string;
  lastname: string;
}