export class OrderShipment {
    OrderShipmentId: number;
    ShipName: any;
    ShippingMethodId: any;
    ShipToCity: any;
    ShipToCompanyName: any;
    ShipToCountryCode: any;
    ShipToEmail: any;
    ShipToFirstName: any;
    ShipToLastName: any;
    ShipToPhoneNumber: any;
    ShipToPostalCode: any;
    ShipToStateCode: any;
    ShipToStreetAddress1: any;
    ShipToStreetAddress2: any;
    AddressId: number;
    Quantity: number;
    TaxCost: number;
    ShippingCost: number;
    ShippingName: any
}
