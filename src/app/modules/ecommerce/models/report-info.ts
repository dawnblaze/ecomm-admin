export class ReportInfo {
    Id: number;
    Name: string;
    Description: string;
    Path: string;
    IsSSRS :boolean;
    IsEcomm :boolean;
    SubSectionId :number;
    Ordering: number;
    Options: ReportInfoOption[];
    ClientDomainName :string;
    FiltersResourceList: ReportInfoFilter[];
}

export class ReportInfoOption {
    OptionTypeId :number;
    OptionName :string; /// "DateTimeRange"
    Values :any[]; 
    //  {
    //     "Today":"Today",
    //     "This Week":"ThisWeek",
    //     "This Week-to-Date":"ThisWeekToDate",
    //     "This Month":"ThisMonth",
    //     "This Month-to-Date":"ThisMonthToDate",
    //     "This Quarter":"ThisQuarter",
    //     "This Quarter-to-Date":"ThisQuarterToDate",
    //     "This Year":"ThisYear",
    //     "This Year-to-Date":"ThisYearToDate",
    //     "Yesterday":"Yesterday",
    //     "Last Week":"LastWeek",
    //     "Last Month":"LastMonth",
    //     "Last Quarter":"LastQuarter",
    //     "Last Year":"LastYear",
    //     "All Dates":"AllDates",
    //     "Free Date Range":"FreeDateRange"
    //  }
    DefaultValue: string; /// "Today"
    Label: string;        /// "Date Created"
}

export class ReportInfoFilter {
    DataType: string;      /// "string","date","integer","bit","numeric","float","decimal","guid","datetime"
    OperatorType: number;  /// ReportingOperatorType (see enum)
    Values: string[];      /// for datetime first value in array is start date, second value is end date
}

export const enum ReportOperatorType {
    EqualTo = 0,
    LessThan = 1,
    LessThanOrEqualTo = 2,
    GreaterThan = 3,
    GreaterThanOrEqualTo = 4,
    NotEqualTo = 5,
    StartsWith = 6,
    NotStartsWith = 7,
    EndsWith = 8,
    NotEndsWith = 9,
    Contains = 10,
    NotContains = 11,
    Between = 12,
    NotBetween = 13,
    OneOf = 14,
    NotOneOf = 15
}