export class CategoryProfile {
    CategoryProfileID: number;
    CategoryID: number;
    ProfileID: number;
    EffectiveDate: string;
    ProfileName: string;
}
