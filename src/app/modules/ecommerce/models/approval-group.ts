import { ApprovalApprover } from "../../../modules/ecommerce/models/approval-approver";
import { ApprovalCustomer } from "../../../modules/ecommerce/models/approval-customer";
import { ApprovalRule } from "../../../modules/ecommerce/models/approval-rule";

export class ApprovalGroup {
    ID: number;
    PortalID: number;
    IsDefault: boolean;
    UseDefaultGroupApprover: boolean;
    Name: string;
}

export class OrderApproval extends ApprovalGroup {
    IsNew: boolean;
    Approvers: OrderApprovalApprover[];
    Customers: OrderApprovalCustomer[];
    Rules: ApprovalRule[];
    timeFrameLimited?: ApprovalRule;
    orderLimited?: ApprovalRule;
}

export class OrderApprovalApprover extends ApprovalApprover {
    IsNew: boolean;
    DisplayName: string;
    ParentAccountID: number;
}

export class OrderApprovalCustomer extends ApprovalCustomer {
    IsNew: boolean;
    DisplayName: string;
}

export enum OrderApprovalRuleType {
    OrderLimit = 10,
    TimeFrameLimit = 11
}

export enum OrderApprovalRuleLimit {
    RequireApproval = 10,
    NotPermitted = 11
}