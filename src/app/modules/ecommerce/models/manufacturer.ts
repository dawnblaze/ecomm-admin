export class Manufacturer {
    Custom1: string;
    Custom2: string;
    Custom3: string;
    Description: string;
    DisplayOrder: number;
    Email: string;
    EmailNotificationTemplate: string;
    IsActive: boolean;
    IsDropShipper: boolean;
    ManufacturerId: number;
    Name: string;
    PortalId: number;
    Website: string;
}
