import {Catalog} from './catalog';
import {Category} from './category';

export class CategoryNode {
    Catalog: Catalog;
    CatalogId: number;
    Category: Category;
    CategoryId: number;
    CategoryNodeId: number;
    BeginDate: any;
    CssId: number;
    DisplayOrder: number = 99;
    EndDate: any;
    IsActive: boolean = true;
    MasterPageId:  number;
    ParentCategoryNodeId:  number;
    ThemeId:  number;
    ErrorMessage: string;
    Name: string;
    Title: string;
    VisibleInd: any;
    IsCategoryAssociated: boolean;
    Thumbnail?: string;
    SeoUrl?: string;
    ProductCount?: number;
}
