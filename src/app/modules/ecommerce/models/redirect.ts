export class Redirect {
    UrlRedirectId: number;
    IsActive: boolean;
    NewUrl: string;
    OldUrl: string;
}
