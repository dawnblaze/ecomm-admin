export class TaxClass {
    DisplayOrder: number;
    ExternalId: number;
    ActiveInd: boolean;
    Name: string;
    PortalId: number;
    TaxClassId: number;
    StoreName: string;
    CustomErrorMessage: string;
    TaxRuleList: any;
}
