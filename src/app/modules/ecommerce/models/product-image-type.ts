export class ProductImageType {
    ProductImageTypeID: number;
    Name: string;
    Description: string;
}