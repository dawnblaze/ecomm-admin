export class ContentSection {
    ID: number;
    PortalID: number;
    Name: string;
    Description: string;
    HtmlContent: string;
}
