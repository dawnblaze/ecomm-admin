export class ShippingRuleType {
    ClassName: any;
    Description: string;
    IsActive: boolean;
    Name: string;
    ShippingRuleTypeId: number;
}