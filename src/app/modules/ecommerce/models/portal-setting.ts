export class PortalSetting {
    /*
    The properties below are based on Znode.Engine.Api.Models.PortalSettingModel.cs
    */
    PortalSettingId:number;
    PortalId:number;
    EnableFileUploads:boolean;
    MailingAddressBlock: string;
    CheckoutTermsCond: string;
    EnableRegisterRestrictDomain: boolean;
    RegisterAllowedDomains: boolean;
    ShowPrices: boolean;
    EnableOrderApproval: boolean;
    EnableRegisterCompanyID: boolean;
    FaviconPath: string;
    PortalTagline: string;
    CustServiceEmailShowInTopNav: boolean;
    CustServiceEmailLabel: string;
    CustomLinkLabel: string;
    CustomLinkUrl: string;
    ShowVisaIcon: boolean;
    ShowMastercardIcon: boolean;
    ShowAmexIcon: boolean;
    ShowDiscoverIcon: boolean;
}
