export class ShippingMethod {
    CountryCode: any = 0;
    Description: string;
    CustomErrorMessage: any;
    DisplayOrder: number = 99;
    ExternalId: any;
    HandlingCharge: number = 0;
    ActiveInd: boolean = true;
    ProfileId: number = -1;
    ShippingCode: string;
    ShippingId: number;
    PortalId: number;
    ShippingType: any;
    ShippingTypeId: number = 1;
    ShippingRule: any;
    ShippingServiceCodeId: number;
    ShippingServiceCode: any;
    ShippingtypeName: string;
    ProfileName: string;
    ClassName: string = "ZnodeShippingCustom";
    UserType: any;
}
