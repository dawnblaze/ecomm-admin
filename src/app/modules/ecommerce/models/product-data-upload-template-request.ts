export class ProductDataOnUploadTemplateRequest {
    productId: number;
    recordType: number;
    appType: number;
    productName: string;
    stateID: string;
    stateUrl: string;
    templatePresetId: number;
    editorPresetId: number;
}