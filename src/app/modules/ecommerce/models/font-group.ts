import { Font } from './font';

export class FontGroup {
  bid: number;
  id: number;
  name: string;
  isSystem: boolean;
  fonts: Font[];
}
