export class Address {
    id: number;
    addressType: number;
    address1: string;
    address2: string;
    address3: string;
    city: string;
    state: string;
    postalCode: string;
    postalCodeExtended: string;
    country: string;
    createdDateUTC: string;
    createdByUserId: number;
    modifiedDateUTC: string;
    modifiedByUserId: any;
    editSequence: number;
}
