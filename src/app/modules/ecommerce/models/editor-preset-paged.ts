import { EditorPreset } from "./editor-preset";

export class EditorPresetPaged
{
    records: EditorPreset[];
    pageIndex: number;
    pageSize: number;
    totalPages: number;
    totalResults: number;
}