export class ShippingInsuranceType {
    ID: number;
    Name: string;
    Active: boolean;
}