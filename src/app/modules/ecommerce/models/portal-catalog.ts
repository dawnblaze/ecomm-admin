import { Catalog } from './catalog';
import { Portal } from './portal';

export class PortalCatalog {
    Catalog: Catalog;
    CatalogId: number;
    CssId: number;
    LocaleId: number = 43;
    PortalCatalogId: number;
    Portal: Portal;
    PortalId: number;
    ThemeId: number;
    CSSName: string = "Default";
    ThemeName: string = "Default";
    CatalogName: string;
}
