export class DigitalAsset {
    DigitalAssetID: number;
    ProductID: number;
    DigitalAsset: string;
    OrderLineItemID: number;
    UserName: any;
    UserType: any;
}
