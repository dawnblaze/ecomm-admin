export enum RecordType {
    Product = 10,
    CartItem = 20,
    OrderDraftItem = 30,
    OrderItem = 40
}