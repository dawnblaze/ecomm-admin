import { Product } from './product';

export class Review {
    AccountId: number;
    Comments: string;
    Cons: string;
    CreateDate: Date;
    CreateUser: string;
    Custom1: string;
    Custom2: string;
    Custom3: string;
    Product: Product;
    ProductId: number;
    Pros: string;
    Rating: number;
    ReviewId: number;
    Status: string;
    Subject: string;
    UserLocation: string;
    ProductName: string;    
}
