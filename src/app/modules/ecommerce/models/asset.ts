export class Asset {
  aspectRatio: string;
  assetType: any;
  assetTypeID: number;
  assetURL: string;
  bid: number;
  description: string;
  dtUploaded: Date;
  height: string
  highResImageURL: string;
  id: string;
  isTemporary: boolean;
  lowRedImageURL: string;
  mediaType: any;
  mediaTypeID: number;
  metaData: string
  name: string;
  proofURL: string;
  refCount: number;
  size: number;
  thumbnailURL: string;
  width: string;
}
