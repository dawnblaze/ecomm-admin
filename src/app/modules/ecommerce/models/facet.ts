export class Facet {
    FacetGroupID: number;
    FacetGroupLabel: string;
    ControlTypeID: number;
    CatalogID: number;
    DisplayOrder: number;
    FacetGroupCategories: any[];
    FacetGroupFacets: any[];
    FacetName: string;
}
