export class PortalCountry {
    CountryCode: string;
    IsBillingActive: boolean;
    IsShippingActive: boolean;
    PortalCountryId: number;
    PortalId: number;
    CountryName: string;
}