import { ProductTemplateData } from './product-template-data';
import { ProductTemplateSurfaceData } from './product-template-surface-data';

export class TemplatePreset {
    id: number;
    isDeleted: boolean;
    isPrimary: boolean;
    name: string;
    presetJSON: string;
    sortIndex: number;
    productTemplate: ProductTemplateData[];
    surfaces: ProductTemplateSurfaceData[];
}
