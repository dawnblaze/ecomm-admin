import { Color } from './color';

export class ColorGroup {
  id: number;
  bid: number;
  name: string
  isSystem: boolean;
  colors: Color[];
}
