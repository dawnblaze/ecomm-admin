export class ParsePSDResponse {
    numberOfSurfaces: number;
    parsedSuccessfully: boolean;
    message: string;
    tempPSDFileName: string;
}