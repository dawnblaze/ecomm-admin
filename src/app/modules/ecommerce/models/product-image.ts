import { IimageGalleryImageItem } from "../../corebridge/models/image_gallery_image";

export class ProductImage implements IimageGalleryImageItem {
    ProductImageID: number;
    ProductID: number;
    Name: string;
    ImageFile: string = "";
    ImageAltTag: string = "";
    AlternateThumbnailImageFile: string = "";
    ActiveInd: boolean;
    ShowOnCategoryPage: boolean;
    ProductImageTypeID: number;
    DisplayOrder: number = 99;
    ReviewStateID: number;
    ImagePath: string = "";
    ReviewStateString: string = "";
    VendorName: string = "";
    VendorId: number;
    ProductSku: string = "";
    ImageTypeName: string = "";
    UserName: string = "";

    public constructor(init?:Partial<ProductImage>) {
        Object.assign(this, init);
    }
    
    getUniqueIdentifier(): string {
        if (this.ProductImageID)
            return this.ProductImageID.toString();
        return null;
    }
    getImageThumbnail(): string {
        return this.ImageFile;
    }

}