export class SurfacePreset {
    id: number;
    templateId?: number;
    isDeleted?: boolean;
    name: string;
    templatePresetId?: number;
    templatePresetJSON: string;
}
