export class EndorBIDResponse {
    BID: number;
    ErrorCode: number;
    ErrorMessage: string;
    HasError: boolean;
    Response: string;
    StackTrace: string;
    Success: boolean;
  }