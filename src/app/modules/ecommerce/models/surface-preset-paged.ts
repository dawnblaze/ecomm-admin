import { SurfacePreset } from "./surface-preset";

export class SurfacePresetPaged
{
    records: SurfacePreset[];
    pageIndex: number;
    pageSize: number;
    totalPages: number;
    totalResults: number;
}
