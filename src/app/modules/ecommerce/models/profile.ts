export class Profile {
    EmailList: any;
    ExternalId: string;
    Name: string;
    ProfileId: number;
    ShowOnPartnerSignup: boolean;
    ShowPricing: boolean;
    TaxClassId: any;
    TaxExempt: boolean;
    UseWholesalePricing: boolean;
    ShowAdditionalCheckoutInstructions: boolean;
    OrderSyncDisabled: boolean;
    Weighting: number;
    AccountProfileID: number;
    AccountId: number;
    RequirePO: boolean;
    /** Order approval temporary variables - set to optional to avoid breaking existing usage **/
    OrderApprovalsEnabled?: boolean;
    OrderApprovalLimit?: boolean;
    OrderApprovalTimeFrameLimit?: boolean;
    OrderApprovalLimitRequired?: boolean;
    OrderApprovalTimeFrameLimitRequired?: boolean;
    OrderApproverDefaultGroup?: number;
    /** End Order approval **/
}
