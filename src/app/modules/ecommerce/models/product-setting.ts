export class ProductSetting {
  /*
    The properties below are based on Znode.Engine.Api.Models.ProductSettingModel.cs
    */
  ProductSettingId: number;
  ProductId: number;
  ProductNumDifferent: boolean;
  EnableFileUploads: boolean;
  ShipAttributeDifferent: boolean;
  ShipWidth: number;
  ShipHeight: number;
  ShipLength: number;
  ShipWeight: number;
  IsCustom: boolean;
  IsCustomReq: boolean;
  DesignerType: number;
}
