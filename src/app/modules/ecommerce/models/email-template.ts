export class EmailTemplate {
    TemplateName: string;
    DeleteTemplate: any;
    Key: any;
    Html: string;
    CustomErrorMessage: string;
    Extension: string;
    IsNewAdd: boolean = true;
}
