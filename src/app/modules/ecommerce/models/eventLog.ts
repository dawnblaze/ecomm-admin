interface iEventLog{
    id: number;
    bid: number;
    ip: string;
    entryDT: any;
    eventType: string;
    logLevel: string;
    message: string;
    exception: string;
    stackTrace: string;
    capturedObjectState: string;
}



export class EventLog implements iEventLog{
  
    public id: number;
    public bid: number;
    public ip: string;
    public entryDT: any;
    public eventType: string;
    public logLevel: string;
    public message: string;
    public exception: string;
    public stackTrace: string;
    public capturedObjectState: string;
  }