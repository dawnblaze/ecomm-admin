export class TaxabilityCode {
    Id:number;
    Name:string;
    Code:number;
    Description:string;
    Countries:string;
    ATETaxabilityTypeId:number;
    CreatedDate:string;
    ModifiedDate:string;
}