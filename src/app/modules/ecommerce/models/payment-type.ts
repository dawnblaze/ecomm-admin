export class PaymentType {
    PaymentTypeID: number;
    Name: string;
    Description: string;
    ActiveInd: boolean;
}
