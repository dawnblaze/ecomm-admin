export class Country {
    Code: string;
    DisplayOrder: number;
    IsActive: boolean = true;
    Name: string;
    States: any[];
}