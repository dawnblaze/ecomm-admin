import { FontGroup } from './font-group';

export class Font {
  assetID: number;
  bid: number;
  dtModified: Date = new Date();
  displayName: string;
  fontFamilyName: string;
  id: number;
  isActive: boolean = true;
  isEmbeddable: boolean = false;
  isSystem: boolean = false;
  styles: number;
  hasRegular :boolean ;
  hasBold :boolean ;
  hasItalic :boolean ;
  hasItalicBold :boolean ;
  canEmbedRegular :boolean;
  canEmbedBold :boolean;
  canEmbedItalic :boolean;
  canEmbedItalicBold :boolean;
  groups: FontGroup[];
}
