export class PortalProfile {
    PortalProfileID: number;
    PortalID: number;
    ProfileID: number = 0;
    Name: string;
    SerialNumber: number;
}
