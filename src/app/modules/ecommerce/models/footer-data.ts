interface FooterDataItem{
    columnName: string;
    columnHeaderText: string;
}


export class FooterDataModel{
    
    public footerModel: FooterDataItem[];
    public footerData: FooterDataItem;

    constructor(){
        this.footerModel = [
            {columnName: "Column 1 - Catalog", columnHeaderText:"Catalog"}
            ,{columnName: "Column 2 - Store Pages", columnHeaderText:"Customer Support"}
            ,{columnName: "Column 3 - Contact Info", columnHeaderText:"Center Info"}
        ]
    }
}