export class CurrencyType {
  CurrencyTypeID: number;
  Name: string;
  Description: string;
  CurrencySuffix: string;
  Symbol: string;
}
