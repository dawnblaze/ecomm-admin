export class Domain {
    ApiKey: string;
    DomainId: number;
    DomainName: string;
    IsActive: boolean;
    PortalId: number;
    WebAppType: number;
    OldDomainName: any;
}
