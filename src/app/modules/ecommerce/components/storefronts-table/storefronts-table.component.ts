import { Component, Input, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";

import { Observable, forkJoin } from "rxjs";
import { untilDestroyed } from 'ngx-take-until-destroy';

import { TableComponent } from "../../../corebridge/components/table/table.component";
import { RowAction } from "../../../corebridge/components/table2/table.component";

import { ZnodeService } from "../../../../services/znode.service";
import { CoreBridgeService } from "../../../../services/corebridge.service";
import { AlertService } from "../../../../services/alert.service";
import { ModalService } from "../../../../services/modal.service";
import { AuthService } from "../../../../services/auth.service";

import { Portal } from "../../models/portal";
import { Domain } from "../../models/domain";
import { Alert } from "../../../corebridge/models/alert";
import { CesaService, MisTypes } from "../../../../services/cesa.service";

@Component({
  selector: "cb-storefronts-table",
  templateUrl: "./storefronts-table.component.html",
  styleUrls: ["./storefronts-table.component.less"]
})
export class StorefrontsTableComponent extends TableComponent
  implements OnInit, OnDestroy {
  public name: string = "cb-storefronts-table";
  public _rows: Portal[];
  public filtered: Portal[] = [];
  public search: string = "";
  public searchKey: string = "Title";
  public total = 0;
  public isCopying: boolean = false;
  public loading: boolean = false;
  public limit: number;
  private domains: Domain[] = [];
  public profileErrors: string[] = [];
  private loggedIn: boolean = false;

  public cols: Array<Object> = [];
  public rowActions: Array<RowAction> = [RowAction.Preview, RowAction.Clone, RowAction.Delete];

  constructor(
    private znodeService: ZnodeService,
    private corebridgeService: CoreBridgeService,
    private cesaService: CesaService,
    private router: Router,
    private alertService: AlertService,
    private modalService: ModalService,
    private authService: AuthService,
    private cdRef: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();

    this.cols = [
      { field: "StoreName", header: "Name" },
      { field: "CompanyName", header: "Brand" },
      { field: "CatalogName", header: "Catalog" },
      { field: "PortalId", header: "ID" }
    ];
    this.authService.loggedInStatus.pipe(untilDestroyed(this)).subscribe(status => {
      this.loggedIn = status["loggedIn"];
    });
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  load() {
    if (this.loggedIn) {
      this.getRows();
      let misType = this.cesaService.getMisType();
      if (misType == MisTypes.Cyrious) {
        this.limit = this.cesaService.getEcommStoreLimit();
        this.znodeService.getStorefrontDomains().pipe(untilDestroyed(this)).subscribe(
          data => {
            this.domains = data;
          },
          err => this.alertService.error(err)
        );
      } else if (misType == MisTypes.CoreBridge) {
        forkJoin(
          this.corebridgeService.getPortalLimit(),
          this.znodeService.getStorefrontDomains()
        ).pipe(untilDestroyed(this)).subscribe(
          data => {
            this.limit = data[0];
            this.domains = data[1];
          },
          err => this.alertService.error(err)
        );
      }
    }
  }

  getRows() {
    this.loading = true;
    forkJoin(
      this.znodeService.getPortals(),
      this.znodeService.getCatalogs()
    ).pipe(untilDestroyed(this)).subscribe(
      data => {
        this._rows = data[0];
        let count = data[0].length;
        let catalogs = data[1];

        this.profileErrors = [];
        data[0].forEach(portal => {
          if (0 >= portal.DefaultAnonymousProfileId) {
            var error =
              'Storefront "' +
              portal.StoreName +
              '" does not currently have a Default Anonymous Profile set.';
            this.profileErrors.push(error);
          }

          if (0 >= portal.DefaultRegisteredProfileId) {
            var error =
              'Storefront "' +
              portal.StoreName +
              '" does not currently have a Default Registered Profile set.';
            this.profileErrors.push(error);
          }

          this.znodeService.getPortalCatalogs(portal.PortalId).pipe(untilDestroyed(this)).subscribe(
            portalCatalogs => {
              if(portalCatalogs.length) {
                let catalog = catalogs.find(c => c.CatalogId == portalCatalogs[0].CatalogId);
                if(catalog)
                  portal.CatalogName = catalog.Name;
                if(--count == 0)
                  setTimeout(() => {this.loading = false}, 1000);
              }
            },
            error => {
              this.alertService.error(error);
              if(--count == 0)
                setTimeout(() => {this.loading = false}, 1000);
            }
          );
        });

        this.filterRows();
        this.isCopying = false;
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }

  filterRows() {
    if (this.search == "") {
      this.filtered = this._rows;
    } else {
      this.filtered = this._rows.filter(item => {
        if (
          item.StoreName.toLowerCase().indexOf(this.search.toLowerCase()) !== -1
        ) {
          return true;
        }
        if (
          item.CompanyName.toLowerCase().indexOf(this.search.toLowerCase()) !==
          -1
        ) {
          return true;
        }
        return false;
      });
      this.page = 1;
    }
    this.total = this.filtered.length;
    this.loaded();
  }

  gotoDetail(id: number) {
    let link = ["/ecommerce/storefronts", id, "general"];
    this.router.navigate(link);
  }

  delete(id: number) {
    let misName = "";
    let misEmail = "";

    if (this.cesaService.getMisType() == MisTypes.Cyrious) {
      misName = "Cyrious";
      misEmail = "ecommerce@cyrious.com";
    } else {
      misName = "CoreBridge";
      misEmail = "support@corebridge.net";
    }

    let title = `Contact ${misName} Support`;
    let content = `If you'd like to delete this storefront, please contact ${misName} Support at <a href="mailto:${misEmail}?subject=Storefront delete request">${misEmail}</a>. Be sure to indicate the storefront ID and Name that you want to delete.`;

    this.modalService.info(title, content, "OK");
  }

  copy(id: number) {
    console.log('copy')
    if (this.isCopying) {
      return;
    }
    this.isCopying = true;

    this.znodeService.copyPortal(id).pipe(untilDestroyed(this)).subscribe(
      res => {
        this.getRows();

        if (res.HasError) {
          this.alertService.error(res.ErrorMessage);
        } else {
          this.alertService.success("Storefront Copied Successfully");
        }
      },
      error => {
        this.isCopying = false;
        this.alertService.error(error);
      }
    );
  }

  // clearCache(){
  //   this.znodeService.clearCache().subscribe(
  //     res => this.alertService.success('Cache Cleared!'),
  //     err => this.alertService.error(err)
  //   );
  // }

  private getPortalDomains(portalId: number): Domain[] {
    return this.domains.filter(domain => domain.PortalId == portalId);
  }

  public hasDomain(portalId: number): boolean {
    let domains = this.getPortalDomains(portalId);
    return domains.length > 0;
  }

  public previewDomain(portalId: number) {
    let domains = this.getPortalDomains(portalId);
    window.open("http://" + domains[0].DomainName);
  }

  ngOnDestroy() {}
}
