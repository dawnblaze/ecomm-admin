import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { ShippingBox } from '../../models/shipping-box';
import { Profile } from '../../models/profile';

@Component({
  selector: 'cb-shipping-boxes-table',
  templateUrl: './shipping-boxes-table.component.html'
})
export class ShippingBoxesTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-shipping-boxes-table";
  public _rows: ShippingBox[];
  public filtered: ShippingBox[] = [];
  public search: string = "";
  public searchKey: string = "DisplayName";
  public total = 0;
  public profiles: Profile[] = [];
  public searchSuggestions: any[] = [ { id:-1, text: 'Storefronts', children: [ ] } ];
  public searchFilters: any[] = [];
  public dataLoaded = false;
  public profileIds: number[] = [];

  constructor( private znodeService: ZnodeService, private router: Router, private modalService: ModalService, private alertService: AlertService ) {
    super();
  }

  load() {
    this.znodeService.getProfiles()
      .subscribe((profiles) => {
          this.profiles = profiles;

          this.profiles.forEach((profile) => {
              this.searchSuggestions[0].children.push({ id: profile.ProfileId, text: profile.Name });
          });

          this.getRows();
      });
  }

  getRows() {
    let getShippingBoxes: Observable<ShippingBox[]>;
    if(this.profileIds.length > 0) getShippingBoxes = this.znodeService.getShippingBoxesByProfileIds(this.profileIds);
    else getShippingBoxes = this.znodeService.getShippingBoxes();

    getShippingBoxes.subscribe(
      (shippingBoxes) => {
        this._rows = shippingBoxes;
        this.dataLoaded = true;
        this.filterRows();
      },
      err => this.alertService.error(err)
    );
  }

  gotoDetail(id: number) {
    let link = ['/ecommerce/shipping-boxes/', id];
    this.router.navigate(link);
  }

  delete(id: number){
    var title =  "Please confirm you would like to delete the selected shipping box. This change cannot be undone.";
    var content = "Deletion will permanently remove the Shipping Box.";
    this.modalService.warning(title, content);

    let subscription = this.modalService.response.subscribe(
      res => {
        if(res){
          this.znodeService.deleteShippingBox(id).subscribe(
            response => {
              this.alertService.success('Shipping Box Deleted Successfully');
              this.getRows();
            },
            error => this.alertService.error(error)
          );
        }

        subscription.unsubscribe();
      },
      err => this.alertService.error(err)
    )
  }

  copy(row: ShippingBox) {
    this.znodeService.copyShippingBox(row.ShippingBoxId).subscribe(
      res => {
        this.alertService.success('Shipping Box Copied Successfully');
        this.getRows();
      },
      err => {
        this.alertService.error(err);
    });
  }

  onFilterChange(e) {
    this.profileIds = e.map((i) => +i);
    sessionStorage.setItem(this.name + "-searchFilters", JSON.stringify(e));
    this.getRows();
  }

  toggleActivation(row: ShippingBox) {
    row.Active = !row.Active;
    this.znodeService.updateShippingBox(row).subscribe(
      res => {
        this.getRows();
      },
      err => this.alertService.error(err)
    );
  }
}
