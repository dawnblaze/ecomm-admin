import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { ShippingMethod } from '../../models/shipping-method';
import { Profile } from '../../models/profile';

@Component({
  selector: 'cb-shipping-methods-table',
  templateUrl: './shipping-methods-table.component.html'
})
export class ShippingMethodsTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-shipping-methods-table";
  public _rows: ShippingMethod[];
  public filtered: ShippingMethod[] = [];
  public search: string = "";
  public searchKey: string = "Description";
  public total = 0;
  public profiles: Profile[] = [];
  public searchSuggestions: any[] = [ { id:-1, text: 'Storefronts', children: [ ] } ];
  public searchFilters: any[] = [];
  public dataLoaded = false;
  public profileIds: number[] = [];

  constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService ) {
    super();
  }

  load() {
    this.znodeService.getProfiles()
      .subscribe((profiles) => {
          this.profiles = profiles;

          this.profiles.forEach((profile) => {
              this.searchSuggestions[0].children.push({ id: profile.ProfileId, text: profile.Name });
          });

          this.getRows();
      });
  }

  getRows() {
    let getShippingMethods: Observable<ShippingMethod[]>;

    if (this.profileIds.length) {
      getShippingMethods = this.znodeService.getShippingMethodsByProfileIds(this.profileIds);
    }
    else {
      getShippingMethods = this.znodeService.getShippingMethods();
    }

    getShippingMethods.subscribe(
      (data) => {
        this.dataLoaded = true;
        this._rows = data;
        this.filterRows();
      },
      err => this.alertService.error(err)
    );

  }

  gotoDetail(id: number) {
    let link = ['/ecommerce/shipping-methods/', id];
    this.router.navigate(link);
  }

  delete(id: number){
    forkJoin(
      this.znodeService.deleteShippingMethod(id),
    ).subscribe( data => {

      const isShippingDeleted = data[0];

      if (isShippingDeleted === true) {
        this.alertService.success('Shipping Method Deleted Successfully');
      }
      this.getRows();
    }, err => this.alertService.error(err));
  }

  copy(row: ShippingMethod) {
    this.znodeService.copyShippingMethod(row.ShippingId).subscribe(
      res => {
        this.alertService.success('Shipping Method Copied Successfully');
        this.getRows();
      },
      err => {
        this.alertService.error(err);
      });
  }

  onFilterChange(e) {
    this.profileIds = e.map((i) => +i);
    sessionStorage.setItem(this.name + "-searchFilters", JSON.stringify(e));
    this.getRows();
  }

  toggleActivation(row: ShippingMethod) {
    row.ActiveInd = !row.ActiveInd;
      this.znodeService.updateShippingMethod(row).subscribe(
        res => {
          this.getRows();
        },
        err => this.alertService.error(err)
      );
  }
}
