import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { ProductAsset } from '../../models/product-asset';
import { RowAction } from "../../../corebridge/components/table2/table.component";

@Component({
  selector: 'cb-product-assets-table',
  templateUrl: './product-assets-table.component.html'
})
export class ProductAssetsTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-product-assets-table";
  public _rows: ProductAsset[];
  public filtered: ProductAsset[] = [];
  public search: string = "";
  public searchKey: string = "ProductFile";
  public total = 0;

  private isFileUploading: boolean = false;
  private constants: any = {
    maxFileSize: 78643200,
    prohibitedExt: [ "exe", "js", "bin", "msi", "app" ],
    errorFileSize:  "Only files up to 75MB in size are allowed.",
    errorFileExt: "Files with .exe, .js, .bin, .msi and .app extensions are not allowed.",
    cancelUpload: "File Upload cancelled successfully."
  };

  public cols = [
    { field: "ProductFile", header: "Name" },
    { field: "FileExtension", header: "Extension" },
    { field: "FileSize", header: "Size" },
  ];

  public rowActions: Array<RowAction> = [RowAction.Delete];

  @Input() productId: number;
  @Output() onClickUpload: EventEmitter<any> = new EventEmitter<any>();

  constructor( private znodeService: ZnodeService, private alertService: AlertService ) { 
    super();
  }

  load() {
    this.getRows(); 
  }

  getRows() {
    if(this.productId > 0){
      this.znodeService.getProductAssets(this.productId).subscribe(assets => {
        this._rows = assets;
        this.filterRows();
      });
    }
  }

  delete(id: number) {
    console.log('removing product asset');
    this.znodeService.deleteProductAsset(id).subscribe(
      response => {
        this.alertService.success('File Deleted Successfully');
        // Loop through the array and check if there is an isUploading status
        let isUploadingArray = this._rows.map(x => x.IsUploading);
        this.isFileUploading = $.inArray(true, isUploadingArray) !== -1 ? true : false;
        // Remove the object from the array if file is currently uploading
        if (this.isFileUploading) this._rows.splice(this.getRowIndexByID(id), 1);
        else this.getRows();
      },
      error => this.alertService.error(error)
    )
  }

  cancel(xhr: XMLHttpRequest) {
    xhr.abort();
  }

  public onFileChange(event: any) {
    let files = event.target.files;
    
    if (files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file: File = files[i];

        // Add an empty ProductAsset object to array
        let productAsset = new ProductAsset();
        productAsset["ProductFile"] = file.name;
        this._rows.push(productAsset);
        
        // Validate file
        let errorMessage = this.validateFile(file);

        // Get row index
        let rowIndex = this._rows.indexOf(productAsset);

        if (errorMessage !== "") {
          this.removeRow(productAsset, errorMessage);
          continue;
        }
        
        this._rows[rowIndex]["IsUploading"] = true;
        this._rows[rowIndex]["UploadMessage"] = "Uploading";
        this.znodeService.createProductAsset(this.productId, file)
        .subscribe(
          asset => {
            let index = this._rows.indexOf(productAsset);
            if (typeof asset === "number") {
              this.isFileUploading = true;
              this._rows[index]["UploadProgress"] = asset;
              if (asset === 100) this._rows[index]["UploadMessage"] = "Saving";
            } else {
              if ('readyState' in asset) {
                // If returned value is an XMLHttpRequest
                this._rows[index]["XmlHttpRequest"] = asset;
              } else {
                this._rows[index]["IsUploading"] = false;
                this.isFileUploading = false;
                this._rows[index] = asset;
                this.filterRows();
              }
            }
          },
          error => this.removeRow(productAsset, this.constants.cancelUpload)
        );
      }
    }

    // Clear input file so we can upload the same file again.
    event.target.value = "";
  }

  private removeRow(productAsset: ProductAsset, errorMessage) {
    let rowIndex = this._rows.indexOf(productAsset);
    this._rows[rowIndex]["HasError"] = true;
    this._rows[rowIndex]["ErrorMessage"] = errorMessage;
    this._rows[rowIndex]["IsUploading"] = false;
    setTimeout(() => {
      // Index might change after 5 secs, so we need to get index again
      this._rows.splice(this._rows.indexOf(productAsset), 1);
    }, 5000);
  }

  private getRowIndexByID(id: number) {
    let index = this._rows.map(function(x) {return x.ProductAssetId;}).indexOf(id);
    return index;
  }

  private validateFile(file: File) {
    let fileExtSplit = file.name.split('.');
    let fileExt = fileExtSplit[fileExtSplit.length - 1].toLowerCase();
    if (file.size > this.constants.maxFileSize) return this.constants.errorFileSize;
    if ($.inArray(fileExt, this.constants.prohibitedExt) !== -1) return this.constants.errorFileExt;
    return "";
  }

  public openUploadModal() {
    this.onClickUpload.emit();
  }
}
