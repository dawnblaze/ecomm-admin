import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Redirect } from '../../models/redirect';

@Component({
  selector: 'cb-redirects-table',
  templateUrl: './redirects-table.component.html'
})
export class RedirectsTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-redirects-table";
  public _rows: Redirect[];
  public filtered: Redirect[] = [];
  public search: string = "";
  public searchKey: string = "OldUrl";
  public total = 0;

  constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService ) { 
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.znodeService.getRedirects().subscribe(urls => {
      this._rows = urls;
      this.filterRows();
    });
  }
}
