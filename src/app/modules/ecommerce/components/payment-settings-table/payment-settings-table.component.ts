import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";

import { TableComponent } from "../../../corebridge/components/table/table.component";
import { AutoSaveBarState } from "../../../corebridge/components/autosave-bar/autosave-bar.component";

import { ZnodeService } from "../../../../services/znode.service";
import { AlertService } from "../../../../services/alert.service";
import { PaymentSetting } from "../../models/payment-setting";
import { switchMap } from "rxjs/operators";
import { forkJoin } from "rxjs";

import { Portal } from "../../models/portal";

@Component({
  selector: "cb-payment-settings-table",
  templateUrl: "./payment-settings-table.component.html",
  styleUrls: ["./payment-settings-table.component.less"]
})
export class PaymentSettingsTableComponent extends TableComponent
  implements OnInit {
  public name = "cb-payment-settings-table";
  public _rows: any[];
  public filtered: PaymentSetting[] = [];
  public search = "";
  public searchKey = "Name";
  public total = 0;
  public profileId: number;
  public toggledError: boolean;
  public saveState: AutoSaveBarState = AutoSaveBarState.saved;
  public unchanged: string = "";

  @Input() portal: Portal;
  @Output() onValidate: EventEmitter<AutoSaveBarState> = new EventEmitter();
  @Output() onView: EventEmitter<number> = new EventEmitter();
  @Output() checkOnAccountOnly: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private znodeService: ZnodeService,
    private alertService: AlertService
  ) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.znodeService
      .getPortalProfiles(this.portal.PortalId)
      .subscribe(portalProfiles => {
        let paymentSettings: PaymentSetting[] = [];
        let stopLoop = false;
        portalProfiles.forEach(portalProfile => {
          if (!stopLoop) {
            this.znodeService
              .getProfilePaymentSettings(portalProfile.ProfileID)
              .subscribe(
                results => {
                  results.forEach(paymentSetting => {
                    this.znodeService
                      .getPaymentType(paymentSetting.PaymentTypeID)
                      .subscribe(paymentType => {
                        paymentSetting.PaymentTypeName = paymentType.Name;
                      });
                    paymentSettings.push(paymentSetting);
                    if (
                      paymentSetting.PaymentTypeID === 7 ||
                      paymentSetting.PaymentTypeID === 8
                    ) {
                      this.znodeService.isCreditCardActive(
                        paymentSetting.ActiveInd
                      );
                    }
                  });
                  stopLoop = true;
                  this._rows = paymentSettings;
                  this.unchanged = JSON.stringify(paymentSettings);
                  this.checkOnAccountPaymentTypeOnly(paymentSettings);
                },
                err => this.alertService.error(err)
              );
          }
        });
        this.filterRows();
      });
  }

  toggleActivation(paymentSetting: PaymentSetting) {
    const setting: any = paymentSetting;
    if (this.portal.PortalSetting.EnableOrderApproval) {
      if (setting.PaymentTypeName != "On Account") {
        this.saveState = setting.ActiveInd
          ? AutoSaveBarState.none
          : AutoSaveBarState.invalid;
        this.toggledError = true;
        setTimeout(() => {
          this.toggledError = false;
        }, 5000);
      }
    }
    this.onValidate.emit(this.saveState);
  }

  isUpdateCreditCard(before: PaymentSetting, paymentSetting: PaymentSetting) : boolean {
    return paymentSetting['PaymentTypeName'] == 'Credit Card'
      && JSON.stringify(before) != JSON.stringify(paymentSetting);
  }

  checkOnAccountPaymentTypeOnly(paymentSettings: PaymentSetting[]) {
    const onAccountPaymentTypeOnly: boolean =
      paymentSettings.filter(
        p =>
          /*Non On Account Payment Types of Payment Settings Disabled*/
          p.ActiveInd && p.PaymentTypeID != 6 /*On Account*/
      ).length == 0 &&
      /*One On Account Payment Type of Payment Setting Enabled*/
      paymentSettings.filter(
        p => p.ActiveInd && p.PaymentTypeID == 6 /*On Account*/
      ).length == 1;
    this.checkOnAccountOnly.emit(onAccountPaymentTypeOnly);
  }

  save() {
    let before = JSON.parse(this.unchanged);
    let update = [];
    for (let i = 0; i < this._rows.length; i++) {
      let paymentSetting = this._rows[i];
      if (before[i].ActiveInd != paymentSetting.ActiveInd || this.isUpdateCreditCard(before[i], paymentSetting)) {
        update.push(this.znodeService.updatePaymentSetting(paymentSetting));
      }
    }

    if (update.length > 0) {
      forkJoin(update).subscribe(
        res => {
          this.alertService.success("Payment Type Updated");
          this.unchanged = JSON.stringify(this._rows);
          this.checkOnAccountPaymentTypeOnly(this._rows);
        },
        err => this.alertService.success("Payment Type Update Failed")
      );
    }
  }
}
