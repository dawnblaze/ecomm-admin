import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';
import { RowAction } from "../../../corebridge/components/table2/table.component";
import { Column } from "../../../corebridge/components/table2/table.component";

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { Category } from '../../models/category';
import { CategoryNode } from '../../models/category-node';
import { Catalog } from '../../models/catalog';
import { Alert } from '../../../corebridge/models/alert';

@Component({
  selector: 'cb-categories-table',
  templateUrl: './categories-table.component.html',
})
export class CategoriesTableComponent extends TableComponent implements OnInit, OnDestroy {

  public name: string = "cb-categories-table";
  public _rows: Category[] = [];
  public filtered: Category[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  public catalogs: Catalog[] = [];
  public searchSuggestions: any[] = [ { id:-1, text: 'Catalogs', children: [ ] } ];
  public searchFilters: any[] = [];
  public dataLoaded = false;
  public catalogIds: number[] = [];
  public rowActions = [RowAction.Delete];
  public _columns: Array<Column> = [
    { field: 'ImageSmallPath', header: 'Thumbnail'},
    { field: 'Name', header: 'Name' },
    { field: 'SeoUrl', header: 'URL Handle' },
    { field: 'DisplayOrder', header: 'Display Order', class: 'text-center'},
    { field: 'ProductCount', header: '# of Products', class: 'text-center' },
    { field: 'CatalogCount', header: '# of Catalogs', class: 'text-center' }
  ];

  private imageRoot: string;
  public reindexing: boolean = false;

  constructor( private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService, private router: Router ) {
    super();
  }

  load() {
    if (sessionStorage.getItem(this.name + "-searchFilters") != null) {
      this.searchFilters = JSON.parse(sessionStorage.getItem(this.name + "-searchFilters"));
      this.catalogIds = this.searchFilters.map((i) => +i);
    }

    this.znodeService.getCatalogs()
        .pipe(untilDestroyed(this))
        .subscribe((catalogs) => {
            this.catalogs = catalogs;

            this.catalogs.forEach((catalog) => {
                this.searchSuggestions[0].children.push({ id: catalog.CatalogId, text: catalog.Name });
            });

            this.getRows();
        });


    this.imageRoot = this.znodeService.base;
  }

  getRows() {
    let getCategories: Observable<Category[]>;

    if (this.catalogIds.length) {
      getCategories = this.znodeService.getCategoriesByCatalog(this.catalogIds);
    }
    else {
      getCategories = this.znodeService.getCategories('productIds,categoryNodes');
    }

    getCategories.pipe(untilDestroyed(this)).subscribe(
      (data) => {
        data.forEach(category => {
          category['ProductCount'] = category.ProductIds ? category.ProductIds.split(',').length : 0;
          if(category.CategoryNodes && category.CategoryNodes.length){
            category['CatalogCount'] = category.CategoryNodes.filter(n => n.CatalogId).filterDuplicates((x,y) => x.CatalogId == y.CatalogId).length;
          } else {
            category['CatalogCount'] = 0;
          }
        });
        this._rows = data;
        this.filterRows();

        this.dataLoaded = true;
      },
      err => this.alertService.error(err)
    );

  }

  gotoDetail(id: number) {
    let link = ['/ecommerce/merchandise/collections', id];
    this.router.navigate(link);
  }

  delete(id: number){

    var title =  "Please confirm you would like to delete the selected Collection. This change cannot be undone.";
    var content = "Deletion will permanently remove the Collection.";
    this.modalService.warning(title, content);

    let subscription = this.modalService.response.pipe(untilDestroyed(this)).subscribe(
      res => {

        if(res){
          this.znodeService.deleteCategory(id).pipe(untilDestroyed(this)).subscribe(
            response => {
              if(response){
                this.getRows();
                this.filterRows();
                this.alertService.success('Collection Deleted Successfully');
              }
              else{
                this.alertService.error('Could Not Delete Collection');
              }
            },
            error => {
              this.alertService.error(error);
            }
          );
        }
        subscription.unsubscribe();
      },
      err => this.alertService.error(err)
    )

  }

  reindex() {
    this.reindexing = true;
    this.znodeService.reindex().pipe(untilDestroyed(this)).subscribe(
      res => {
        var that = this;

        setTimeout(function(){
          that.reindexing = false;
        }, 1000 * 60 * 3);

        this.alertService.success('Re-Index Successfully Started');
      },
      err => {
        this.reindexing = false;
        this.alertService.error(err);
      }
    );
  }

  filterProcess(item: Category): boolean {
    var keys = ["Name", "CategoryId", "CatalogName"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }

  onFilterChange(e) {
    this.catalogIds = e.map((i) => +i);
    sessionStorage.setItem(this.name + "-searchFilters", JSON.stringify(e));
    this.getRows();
  }

  copy(row: Category) {
    this.znodeService.copyCategory(row.CategoryId).pipe(untilDestroyed(this)).subscribe(
      res => {
        this.alertService.success('Collection Copied Successfully');
        this.getRows();
      },
      err => {
        this.alertService.error(err);
      });
  }

  ngOnDestroy() {}
}
