import { Component, Input, Output, EventEmitter, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { Observable, forkJoin } from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ThemeService } from '../../../../services/theme.service';

import { Product } from '../../models/product';
import { Category } from '../../models/category';
import { ProductCategory } from '../../models/product-category';
import { ZnodeTableComponent } from "../../../corebridge/components/table/znode-table/znode-table.component";
import { ZnodeApiService } from "../../../../services/znode-api.service";
import { Column } from "../../../corebridge/components/table2/table.component";
import { RowAction, Table2Component } from "../../../corebridge/components/table2/table.component";
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'cb-category-products-associate-table',
  templateUrl: './category-products-associate-table.component.html'
})
export class CategoryProductsAssociateTableComponent extends TableComponent implements OnInit, OnDestroy {

  public name: string = "cb-category-products-associate-table";
  public searchKeys = ['ProductId', 'Name', 'RetailPrice', 'SalesPrice', 'WholesalePrice'];
  public filtered: Product[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  public loading: boolean = true;
  public displayDialog = false;
  public dataLoaded = false;
  public _rows = [];
  public toExit: boolean = false;

  private associated: any = {};

  private imageRoot: string;
  private selectedProductIds: number[] = []
  @Input() category;

  @Output() onAdd: EventEmitter<any> = new EventEmitter();
  @Output() onExit: EventEmitter<any> = new EventEmitter();

  @ViewChild('productsAssociateTable') productsAssociateTable: Table2Component;

  public _columns: Array<Column> = [
    { field: 'Thumbnail', header: 'Thumbnail' },
    { field: 'Name', header: 'Name' },
    { field: 'RetailPrice', header: 'Retail', class: 'text-center' },
    { field: 'SalePrice', header: 'Sale', class: 'text-center' },
    { field: 'WholesalePrice', header: 'Wholesale', class: 'text-center' },
    { field: 'QuantityOnHand', header: 'On Hand', class: 'text-center' },
    { field: 'DisplayOrder', header: 'Order', class: 'text-center' },
    { field: 'SkuId', header: 'SKU', class: 'text-center' }

  ];

  constructor(private znodeApiService: ZnodeApiService, private znodeService: ZnodeService, private alertService: AlertService, private themeService: ThemeService) {
    super();
  }

  load() {
    this.imageRoot = this.znodeService.base;
    this.getRows();
  }

  public showDialog() {
    this.getRows();
    this.productsAssociateTable.selected = [];
    this.displayDialog = true;
    // this.getRows();
  }

  // closeDialog() {
  //   this.displayDialog = false;
  // }

  public close() {
    this.displayDialog = false;
    this.onExit.emit();
  }

  public onSave() {
    this.toExit = false;
    this.save();
  }

  public save() {
    // let add: number[] = [];
    // for (let product in this.associated) {
    //   if (this.associated[product]) {
    //     let productId: number = Number(product);
    //     add.push(productId);
    //   }
    // }
    if (this.productsAssociateTable.selected.length > 0) {
      this.saveAssociations(this.productsAssociateTable.selected.map(s => s.ProductId));
    }
  }

  getRows() {
    this.loading = true;
    this.dataLoaded = false;
    this.znodeApiService.getProductsOutsideCategory(this.category).pipe(untilDestroyed(this)).subscribe(
      (res: any) => {
        this._rows = res.data;
        this._rows.forEach(data => { data.Thumbnail = data.ArtworkThumbnailUri || data.ImageSmallPath });

        // for(let product of this.rows){
        //   this.associated[product.ProductId] = false;
        // }
        this.loading = false;
        this.filterRows();
        this.loaded();
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
        this.loaded();
      });
  }

  private saveAssociations(add: number[]) {
    this.znodeService.createCategoryProductsFromIds(this.category, add).pipe(untilDestroyed(this)).subscribe(res => {
      this.alertService.success('Collection Products Updated');
      this.onAdd.emit();
      this.productsAssociateTable.selected = [];
      if (this.toExit) this.close();
      else this.removeFromList(add);
    },
      err => this.alertService.error(err));
  }

  private removeFromList(numbers: any) {
    numbers.forEach(num => {
      let index = this._rows.findIndex((value: any) => num == value.ProductId)
      if (index > -1) {
        this._rows.splice(index, 1)

      }
    })

    this.filterRows();
  }

  refreshActions(items: []) {
    if (items && items.length) {
      items.forEach((item:any) => {
        const index = this.selectedProductIds.indexOf(item.ProductId);
        if (index > -1) this.selectedProductIds.splice(index, 1);
        else this.selectedProductIds.push(item.ProductId);
      })
    } else
      this.selectedProductIds = [];
  }

  onCloseDialog(status: boolean) {
    if (status) {
      this.toExit = true;
      this.save();
    } else {
      this.close();
    }
    // this.displayDialog = false;

  }

  ngOnDestroy() {}
}
