import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Domain } from '../../models/domain';

@Component({
  selector: 'cb-domains-table',
  templateUrl: './domains-table.component.html'
})
export class DomainsTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-domains-table";
  public _rows: Domain[];
  public filtered: Domain[] = [];
  public search: string = "";
  public searchKey: string = "DomainName";
  public total = 0;

  @Input() portal: number;

  @Output() onCreate: EventEmitter<any> = new EventEmitter();
  @Output() onView: EventEmitter<number> = new EventEmitter();

  constructor( private znodeService: ZnodeService, private alertService: AlertService ) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    if(this.portal){
      this.znodeService.getPortalDomains(this.portal).subscribe(domains => {
        this._rows = domains;
        this.filterRows();
      });
    }
    else {
      this.znodeService.getDomains().subscribe(domains => {
        this._rows = domains;
        this.filterRows();
      });
    }
  }

  filterRows() {
    this._rows = this._rows.filter((row) => {
      return !row.DomainName.includes('ecommapi') && !row.DomainName.includes('ecommadmin');
    });
    super.filterRows();
  }

  create() {
    this.onCreate.emit();
  }

  goToDetails(id: number){
    console.log('attempting to edit '+id);
    //this.onView.emit(id);
  }

  delete(id: number){
    console.log('deleting domain #'+id);

    // this.znodeService.deleteDomain(id).subscribe(
    //   res => {
    //     this.getRows();
    //     this.alertService.success('Domain Successfully Removed');
    //   },
    //   err => this.alertService.error(err)
    // );
  }
}
