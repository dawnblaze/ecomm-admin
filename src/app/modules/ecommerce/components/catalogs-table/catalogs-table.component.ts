import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';

import { TableComponent } from '../../../corebridge/components/table/table.component';
import { RowAction } from "../../../corebridge/components/table2/table.component";

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { Catalog } from '../../models/catalog';

@Component({
  selector: 'cb-catalogs-table',
  templateUrl: './catalogs-table.component.html'
})
export class CatalogsTableComponent extends TableComponent implements OnInit, OnDestroy {

  public name: string = "cb-catalogs-table";
  public _rows: Catalog[];
  public filtered: Catalog[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  public RowAction = RowAction;
  public _columns = [
    { field: 'Name', header: 'Name' },
    { field: 'CategoryCount', header: '# of Collections' },
    { field: 'ProductCount', header: '# of Products' }
  ];

  constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService, private modalService: ModalService ) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.znodeService.getCatalogsAndExpandProductIdsCategoryCount().pipe(untilDestroyed(this)).subscribe(catalogs => {
      this._rows = catalogs;
      this.filterRows();
    });
  }

  copy(catalog: Catalog) {
      this.znodeService.copyCatalog(catalog).pipe(untilDestroyed(this)).subscribe(
        response => {
          console.log(response);
          if(response.HasError == false){
            this.getRows();
            this.alertService.success('Catalog Copied Successfully');
          }
          else{
            this.alertService.error(response.ErrorMessage);
          }
        },
        err => this.alertService.error(err)
      );
  }

  delete(catalogID: number) {
    this.znodeService.getPortalCatalogByCatalogID(catalogID).pipe(untilDestroyed(this)).subscribe(portalCatalogs => {
      if (portalCatalogs.length > 0) {
        var title =  "Catalog Delete Error";
        var content = "This catalog is currently associated with one or more storefronts and cannot be deleted.";
        this.modalService.warningNoCancel(title, content, 'OK');
      } else {
        var title =  "Please confirm you would like to delete the selected Catalog. This change cannot be undone.";
        var content = "Deletion will permanently remove the Catalog.";
        this.modalService.warning(title, content);

        let subscription = this.modalService.response.pipe(untilDestroyed(this)).subscribe(res => {
            if(res){
              this.znodeService.deleteCatalog(catalogID).pipe(untilDestroyed(this)).subscribe(response => {
                if(response){
                  this.getRows();
                  this.alertService.success('Catalog Deleted Successfully');
                } else this.alertService.error('Could not delete Catalog');
              });
            }
            subscription.unsubscribe();
          }, err => this.alertService.error(err)
        )
      }
    });

  }

  toggleActivation( catalog: Catalog) {
      catalog.IsActive = !catalog.IsActive;

      this.znodeService.updateCatalog(catalog).pipe(untilDestroyed(this)).subscribe(
        response => {
          this.getRows();
          this.alertService.success('Catalog Updated Successfully');
        },
        error => this.alertService.error(error)
      );
  }

  goToDetail( catalogID: number ) {
    this.router.navigate(['/ecommerce/merchandise/catalogs/', catalogID]);
  }

  ngOnDestroy(){}
}
