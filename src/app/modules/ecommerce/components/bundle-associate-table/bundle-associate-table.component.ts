import { Component, Input, Output, EventEmitter, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { Observable, forkJoin } from 'rxjs';
import { untilDestroyed } from "ngx-take-until-destroy";

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Product } from '../../models/product';
import { Bundle } from '../../models/bundle';
import { BundleTableComponent } from "../bundle-table/bundle-table.component";
import { ZnodeTableComponent } from "../../../corebridge/components/table/znode-table/znode-table.component";
import { ZnodeApiService } from "../../../../services/znode-api.service";

@Component({
  selector: 'cb-bundle-associate-table',
  templateUrl: './bundle-associate-table.component.html'
})
export class BundleAssociateTableComponent extends ZnodeTableComponent implements OnInit, OnDestroy {

  public name: string = "cb-bundle-associate-table";
  public rows: Product[];
  public filtered: Product[] = [];
  public search: string = "";
  public searchKeys = [ 'ProductId', 'Name', 'RetailPrice', 'SalesPrice', 'WholesalePrice' ];
  public searchKey: string = "Name";
  public total = 0;
  public loading: boolean = true;

  private associated: any = {};
  private imageRoot: string;
  private saveAndClose: boolean;

  @Input() product: Product;

  @Output() onExit: EventEmitter<any> = new EventEmitter();

  @ViewChild(BundleTableComponent) associateList: BundleTableComponent;

  constructor(private znodeApiService: ZnodeApiService, private znodeService: ZnodeService, private alertService: AlertService ) {
    super();
  }

  load() {
    this.imageRoot = this.znodeService.base;
    this.saveAndClose = false;
  }

  public cancel(saveAndClose: boolean){
    this.saveAndClose = saveAndClose;
    if(saveAndClose)
      this.save();
    else
      this.onExit.emit();
  }

  public save() {
    let childProductIds = this.associateList.dataTable.selected.map(item => item.ProductId);
    this.associateList.dataTable.setSelectAll(false);
    this.associateList.dataLoaded = childProductIds.length == 0;
    document.getElementsByClassName('ui-dialog-content')[0].scrollTop = 0;
    this.znodeService.createProductBundle(this.product.ProductId, childProductIds).pipe(untilDestroyed(this)).subscribe(
      res => {
        this.alertService.success('Bundled Products Updated');
        if(this.saveAndClose)
          this.onExit.emit();
        else
          this.associateList.getRows();
      },
      err => {
        this.alertService.error(err);
        this.associateList.dataLoaded = true;
      }
    );
  }

  ngOnDestroy() {}
}
