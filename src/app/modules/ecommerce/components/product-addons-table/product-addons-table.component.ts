import { OnDestroy } from "@angular/core";
import { ModalService } from "./../../../../services/modal.service";
import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  ViewChild
} from "@angular/core";
import { untilDestroyed } from "ngx-take-until-destroy";
import { forkJoin } from "rxjs";

import { TableComponent } from "../../../corebridge/components/table/table.component";

import { ZnodeService } from "../../../../services/znode.service";
import { AlertService } from "../../../../services/alert.service";

import { Addon } from "../../models/addon";
import {
  RowAction,
  Table2Component
} from "../../../corebridge/components/table2/table.component";
import { take } from "rxjs/operators";

@Component({
  selector: "cb-product-addons-table",
  templateUrl: "./product-addons-table.component.html"
})
export class ProductAddonsTableComponent extends TableComponent
  implements OnInit, OnDestroy {
  public name: string = "cb-product-addons-table";
  public _rows: Addon[];
  public filtered: Addon[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  public index = 0;
  public RowAction = RowAction;
  public _columns = [
    { field: "Name", header: "Name" },
    { field: "ProductCount", header: "# of Products" },
    { field: "DisplayOrder", header: "Display Order" }
  ];

  public productAddons: Addon[] = [];
  public associated: any = {};
  public selected: any;

  @ViewChild("addOnsTable") addOnsTable: Table2Component;

  @Input() productId: number;
  @Input() isAssociateList: boolean;

  @Output() onAssociate: EventEmitter<any> = new EventEmitter();

  constructor(
    private znodeService: ZnodeService,
    private alertService: AlertService,
    private modalService: ModalService
  ) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    if (!this.isAssociateList) {
      forkJoin(
        this.znodeService.getAddons("productids"),
        this.znodeService.getProductAddons(this.productId)
      )
        .pipe(untilDestroyed(this))
        .subscribe(
          addons => {
            this._rows = addons[1];
            this._rows.forEach(item => {
              item["ProductCount"] = addons[0].find(
                a => a.AddOnId == item.AddOnId
              )["ProductCount"];
            });
            this.filterRows();
            this.loaded();
          },
          err => this.alertService.error(err)
        );
    } else {
      forkJoin(
        this.znodeService.getAddons(),
        this.znodeService.getProductAddons(this.productId)
      )
        .pipe(untilDestroyed(this))
        .subscribe(data => {
          this._rows = data[0];
          this.productAddons = data[1];

          for (var addon of this._rows) {
            this.associated[addon.AddOnId] = false;
          }

          for (var addon of data[1]) {
            this.associated[addon.AddOnId] = true;
          }

          this._rows = this._rows.filter(item => {
            return !this.productAddons.some(addon => {
              return addon.AddOnId == item.AddOnId;
            });
          });

          this.filterRows();
        });
    }
  }

  associate() {
    this.onAssociate.emit();
  }

  delete(id: number) {
    var title =
      "Please confirm you would like to delete the selected Product Add-Ons. This change cannot be undone.";
    var content = "Deletion will permanently remove the Product Add-Ons.";
    this.modalService.warning(title, content);

    this.modalService.response.pipe(take(1)).subscribe(res => {
      if (res) {
        this.znodeService
          .deleteProductAddon(id)
          .pipe(untilDestroyed(this))
          .subscribe(
            response => {
              this.getRows();
              this.alertService.success("Product Addon Removed");
            },
            error => {
              this.alertService.error(error);
            }
          );
      }
    });
  }

  public filterProcess(item: Addon): boolean {
    return;
  }

  onPageChange(page: number) {
    if (page != this.page) {
      this.page = page;
      this.getRows();
    }
  }

  onCountChange(count: number) {
    this.count = count;
    this.page = 1;
    this.getRows();
    sessionStorage.setItem(this.name + "-count", this.count + "");
    sessionStorage.setItem(this.name + "-page", this.page + "");
  }

  ngOnDestroy() {}
}
