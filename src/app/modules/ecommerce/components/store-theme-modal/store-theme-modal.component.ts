import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, AfterViewInit, OnDestroy } from "@angular/core";
import { Observable } from "rxjs";
import { untilDestroyed } from 'ngx-take-until-destroy';
import { Portal } from "../../models/portal";
import { ZnodeService } from "../../../../services/znode.service";
import { AlertService } from "../../../../services/alert.service";
import { CSS } from "../../models/css";
import { ContentSection } from "../../models/content-section";

@Component({
  selector: "store-theme-modal",
  templateUrl: "./store-theme-modal.component.html",
  styleUrls: ["./store-theme-modal.component.less"]
})
export class StoreThemeModalComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  @Input() refresh: Observable<void>;
  @Input() store: Portal;
  @Input() css: CSS;
  @Input() misType: number;

  displayDialog = false;
  public unchangedCss: string;
  public fontFamilyOptions = [];

  public headerBackground: string             = null;
  public headerText: string                   = null;
  public topNavBgColor: string                = null;
  public topNavTextColor: string              = null;
  public topNavLinkHover: string              = null;
  public navigationBackground: string         = null;
  public headerHover: string                  = null;
  public navigationHover: string              = null;
  public inputTextColor: string               = null;
  public buttonPrimary: string                = null;
  public buttonSecondary: string              = null;
  public footerBackground: string             = null;
  public footerText: string                   = null;
  public defaultHover: string                 = null;
  public fontFamily: string                   = null;
  public styleData = {};

  public content: any;
  public contentSections: ContentSection[];
  public featured_Categories: any[];
  public featured_Products: any[];
  public cat_data: any[] = null;
  public product_data: any[] = null;
  public footer_data: any[] = null;

  public contentSection: ContentSection;
  public editContent: boolean = false;
  private unchangedContent: string = "";


  constructor(private znodeService: ZnodeService, private alertService: AlertService) {}

  showDialog() {
    this.displayDialog = true;
  }

  closeDialog() {
    this.displayDialog = false;
  }

  ngOnInit() {

    this.znodeService.getContentSections(this.store.PortalId).pipe(untilDestroyed(this)).subscribe( data => {
      this.contentSections = data;
    });
    this.znodeService.getCategories().pipe(untilDestroyed(this)).subscribe(data => {
      let isFeatured = data.filter(prop => {
        if(prop.Properties.IsFeatured){
          return true;
        }
      })
      this.featured_Categories = isFeatured.slice(0,3);
      console.log('CategoriesData: ', isFeatured, this.featured_Categories);
      
    })

    console.log('Store Data: ', this.store);

    this.fontFamilyOptions = [
      {
        value: "'Times New Roman', Times, serif",
        style: "'Times New Roman'"
      },
      {
        style: "Arial",
        value: "'Arial'"
      },
      {
        value: "'Lucida Console', Monaco, monospace",
        style: "'Lucida Console'"
      }
    ];

    console.log('Store Data: ', this.store);

    this.znodeService.getCss(this.store.PortalId).pipe(untilDestroyed(this)).subscribe( r => {
      console.log('R: ',r);
    
      
    
      if(r !== undefined){
        console.log('CSS style Dictionary: ', this.css)

        /* Use Znode Service to pull in data for setting these defaults */
        // Set defaults for color stylings
        this.headerBackground  = this.css.StyleDictionary["header-bg-color"]               || "#ffffff";
        this.headerText        = this.css.StyleDictionary["heading-text-color"]            || "#000000";
        this.topNavBgColor     = this.css.StyleDictionary["top-header-nav-bg-color"]       || "#ffffff";
        this.topNavTextColor   = this.css.StyleDictionary["top-header-nav-text"]           || "#ffffff";
        this.topNavLinkHover   = this.css.StyleDictionary["top-header-nav-link-hover-bg"]  || "#9aca3f";
        this.headerHover       = this.css.StyleDictionary["header-nav-bg-color"]           || "#9aca3f";
        this.navigationHover   = this.css.StyleDictionary["nav-link-hover-bg"]             || "#9aaa3f";
        this.inputTextColor    = this.css.StyleDictionary["input-text-color"]              || "#000000";
        this.buttonPrimary     = this.css.StyleDictionary["button-bg-primary"];        
        this.buttonSecondary   = this.css.StyleDictionary["button-bg-secondary"];
        this.footerBackground  = this.css.StyleDictionary["footer-bg-color"]               || "#121212";
        this.footerText        = this.css.StyleDictionary["footer-default-text-color"]     || "#ffffff";
        this.defaultHover      = this.css.StyleDictionary["nav-link-hover-bg"]             || "#AA0000";
        this.fontFamily        = this.css.StyleDictionary['base-font-family']
        // this.cat_data = [
        //   {name: this.featured_Categories[0].Title ,image: this.featured_Categories[0].ImageFile}
        //   ,{name:this.featured_Categories[1].Title, image: this.featured_Categories[1].ImageFile}      
        //   ,{name:this.featured_Categories[2].Title, image: this.featured_Categories[2].ImageFile}
        // ];
        if (this.featured_Categories && this.featured_Categories.length) {
          this.cat_data = [];
          this.featured_Categories.forEach(item => {
            this.cat_data.push({
              name: item.Title,
              image: item.ImageFile
            })
          })
        }
        
        // this.product_data = [ 
        //   {name: this.featured_Products[0].Name, image: this.featured_Products[0].ImageFile, price: this.featured_Products[0].RetailPrice } //this.featured_Products[0].ImageFile}
        //   ,{name: this.featured_Products[1].Name, image:this.featured_Products[1].ImageFile, price: this.featured_Products[1].RetailPrice}
        //   ,{name: this.featured_Products[2].Name, image: this.featured_Products[2].ImageFile, price: this.featured_Products[2].RetailPrice}
        // ];
        this.footer_data = [
        {name: this.css.FooterColumnLabel.Column1, data:["cat1","cat2"]}
        ,{name: this.css.FooterColumnLabel.Column2, data:["cat1","cat2"]}
        ,{name: this.css.FooterColumnLabel.Column3, data:["cat1","cat2"]}
        ]
        this.content = {
          logo: this.store.LogoPath,
          // Home_Banner: this.contentSections[0].HtmlContent.split("\"")[1],
          // Login_Banner: this.contentSections[1].HtmlContent.split("\"")[1],
          // Cart_Banner: this.contentSections[2].HtmlContent.split("\"")[1],
          // Home_Lower_Banner: this.contentSections[3].HtmlContent.split("\"")[1],
          // Catalog_Banner: this.contentSections[4].HtmlContent.split("\"")[1],
          // Catalog_Lower_Banner: this.contentSections[5].HtmlContent.split("\"")[1]
        }
        const props = ['Home_Banner', 'Login_Banner', 'Cart_Banner', 'Home_Lower_Banner', 'Catalog_Banner','Catalog_Lower_Banner']
        if (this.contentSections && this.contentSections.length) {
          this.contentSections.forEach((item, i) => {
            if (item.HtmlContent) {
              this.content[props[i]] = item.HtmlContent.split("\"")[1]
            }
          })
        }


        console.log('Style Data updated via observable: ',this.styleData)
        this.updateLive();
    }
  });
  }

  ngAfterViewInit(){
    
  }

  ngOnChanges() {
  }

  updateLive(){
    
    this.styleData = 
    {
      styleData:{
        header_bg_color:    this.css.StyleDictionary['header-bg-color'] || null,
        heading_text_color: this.css.StyleDictionary['heading-text-color']  || null,
        top_nav_bg_color:   this.css.StyleDictionary["top-header-nav-bg-color"] || null,
        top_nav_text_color: this.css.StyleDictionary["top-header-nav-text"] || null,
        top_nav_hover:      this.css.StyleDictionary["top-header-nav-link-hover-bg"] || null,
        nav_bg_color:       this.css.StyleDictionary["header-nav-bg-color"] || null,
        nav_hover:          this.css.StyleDictionary["nav-link-hover-bg"] || null,
        input_text:         this.css.StyleDictionary["input-text-color"] || null,
        footer_bg:          this.css.StyleDictionary["footer-bg-color"] || null,
        footer_text:        this.css.StyleDictionary["footer-default-text-color"] || null,
        primary_button:     this.css.StyleDictionary["button-bg-primary"]|| null,
        secondary_button:   this.css.StyleDictionary["button-bg-secondary"] || null,
        default_hover:      this.css.StyleDictionary["nav-link-hover-bg"] || null,
        footerData: this.footer_data || null,
        content: this.content,
        // product_data: this.product_data || null,
        catData: this.cat_data || null,
        font_family: this.css.StyleDictionary['base-font-family'] || null
      },
        zoom: undefined,
    }
  }

  onBlur(event: any) {
    event.target.value = event.target.value.trim();
  }

  setHexCode(code: string, keys: string[]) {
    for (var key in keys) {
      this.css.StyleDictionary[keys[key]] = code;
    }
  }

  /**
   * @description Sets all colors to current default in admin
   */
  getData(event, element){
    console.log('Parent Style Data: ', this.styleData, event.value, element);
    this.css.StyleDictionary[element] = event.value;
    this.updateLive();
    
  }

  /**
   * @description Resets all values to default or previous saved values.
   * @param event 
   */
  resetData(event){
    // not implemented
  }

  styleChange(event){
    console.log('Event Triggered from inner child: ',event, event.value);
    this.getData(event, event.css);
  }

  setContentStation(section: ContentSection) {
    this.contentSection = section;
    this.unchangedContent = JSON.stringify(section);
  }

  saveContentSection() {
    if(this.unchangedContent != JSON.stringify(this.contentSection)) {
      this.znodeService.updateContentSection(this.contentSection).pipe(untilDestroyed(this))
      .subscribe(data => {
        this.unchangedContent = JSON.stringify(data);
        this.editContent = false;
      },
      err => {
        this.alertService.error(err);
        this.editContent = false;
      });
    } else {
      this.editContent = false;
    }
  }

  selectFontFamily() {
    this.updateLive();
  }

  ngOnDestroy(){}
}
