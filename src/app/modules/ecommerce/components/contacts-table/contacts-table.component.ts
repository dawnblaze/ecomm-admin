import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { ZnodeTableComponent } from '../../../corebridge/components/table/znode-table/znode-table.component';

import { AuthService } from '../../../../services/auth.service';
import { ZnodeService } from '../../../../services/znode.service';
import { ZnodeApiService, ZnodeRequest, ZnodeResponse } from '../../../../services/znode-api.service';
import { AlertService } from '../../../../services/alert.service';

import { Profile } from '../../models/profile';

@Component({
  selector: 'cb-contacts-table',
  templateUrl: './contacts-table.component.html'
})
export class ContactsTableComponent extends ZnodeTableComponent {
  public name: string = "cb-contacts-table";
  public searchSuggestions: any[] = [
    {
      id: 0,
      text: 'Store Access',
      children: []
    }
  ];
  public searchFilters: string[] = [];
  public searchKeys = [ 'FirstName', 'Lastname', 'Email'];
  public userName: string = "";

  constructor( private znodeService: ZnodeService, private router: Router, private authService: AuthService, private alertService: AlertService, private znodeApiService: ZnodeApiService) {
    super();
  }

  load() {
    this.userName = this.authService.getUser().UserName;
    this.znodeService.getProfiles().subscribe(data => {
      this.searchSuggestions[0].children = data.map((profile) => {
        return { id: profile.ProfileId, text: profile.Name };
      });

      this.searchSuggestions[0].children.push({ id: 0, text: 'No Access' });
      this.dataLoaded = true;
      this.getRows();
    });
  }

  getRows() {
    if (this.searchFilters.length) {
      let req = this.request;
      let adjustedIndex = req.index - 1;

      this.znodeService.getContactsByProfileIds(this.userName, this.searchFilters, adjustedIndex, req.size, req.filters, req.sort).subscribe(response => {
        response.Accounts.forEach((contact) => {
          contact.PhoneNumber = contact.PhoneNumber || '';
        });

        this.rows = response.Accounts;
        this.response.count = response.TotalResults;
        this.loaded();
      });
    }
    else {
      this.znodeApiService.getContacts(this.userName, this.request).subscribe(data => {
        this.response = data;
  
        let contacts = this.response.data
        contacts.forEach((contact) => {
          contact.PhoneNumber = contact.PhoneNumber || '';
        });

        this.rows = contacts;
        this.loaded();
      });
    }
  }

  gotoDetail(accountId: string) {
    let link = ['/ecommerce/customers/contacts', accountId];
    this.router.navigate(link);
  }

  onFilterChange(selection: string[]) {
    this.searchFilters = selection;
    this.onPageChange(1);
  }

  onPageChange(page: number) {
    sessionStorage.setItem(this.name + '-page', page + '');
    this.request.index = page;
    this.getRows();
  }
}
