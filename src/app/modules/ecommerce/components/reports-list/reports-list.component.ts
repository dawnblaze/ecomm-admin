import { Component, Input, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Observable, forkJoin } from 'rxjs';

import { environment } from '../../../../../environments/environment'

import { CoreBridgeService } from '../../../../services/corebridge.service';

import { Report, ReportSection, ReportSubSection } from '../../models/reports';
import { ReportInfo, ReportInfoOption, ReportInfoFilter, ReportOperatorType } from '../../models/report-info';
import { ZnodeService } from "../../../../services/znode.service";

@Component({
  selector: 'cb-reports-list',
  templateUrl: './reports-list.component.html'
})
export class ReportsListComponent implements OnInit {
  @Input() reportName: string = "";

  public reports: ReportSection[];
  public filtered: ReportSection[] = [];
  public subSections: ReportSubSection[] = [];
  private unchanged: string;
  private template: string;
  private reportKey: string;

  constructor(private http:Http, private znodeService: ZnodeService, private router: Router ) {  }

  ngOnInit() {
    forkJoin(
      this.znodeService.getReportKey(),
      this.znodeService.getReports()
    ).subscribe(
      data => {
        this.reportKey = data[0].toString();
        this.reports = data[1];
        this.unchanged = JSON.stringify(this.reports);
        this.filtered = this.reports;
        this.filterRows();
        this.subSections = this.filtered[0].SubSections;
        this.assignLinks(this.subSections);
      }
    );
  }

  public filterRows(){
    if(this.reportName == ""){
      this.filtered = this.reports;
    } else {
      this.filtered = this.reports.filter( item => {
        return this.filterProcess(item);
      });
    }
  }

  public filterProcess(item: any): boolean {
    return (String(item["Name"]).toLowerCase().indexOf(this.reportName.toLowerCase()) !== -1) ? true : false;
  }

  private assignLinks(reportSubSections: ReportSubSection[]){
    reportSubSections.forEach(subSection => {
      subSection.Reports.forEach(report => {
        report.Link = `${environment.ssrsUrl}?ReportId=${report.Id}&reportKey=${this.reportKey}`;
      });
    });
  }
}
