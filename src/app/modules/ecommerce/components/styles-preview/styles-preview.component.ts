import { Component, Input } from '@angular/core';

@Component({
  selector: 'cb-styles-preview',
  templateUrl: './styles-preview.component.html',
  styleUrls: ['./styles-preview.component.less']
})
export class StylesPreviewComponent {

  @Input() dictionary: any;

  constructor( ) {
  }

}
