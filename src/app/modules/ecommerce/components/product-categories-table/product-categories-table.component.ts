import { Component, Input, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
import { untilDestroyed } from "ngx-take-until-destroy";

import { TableComponent } from '../../../corebridge/components/table/table.component';
import { Table2Component, RowAction } from "../../../corebridge/components/table2/table.component";

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { Product } from '../../models/product';
import { Category } from '../../models/category';
import { ProductCategory } from '../../models/product-category';
import { RecordAction } from '../../../corebridge/models/action';

@Component({
  selector: 'cb-product-categories-table',
  templateUrl: './product-categories-table.component.html'
})
export class ProductCategoriesTableComponent extends TableComponent implements OnInit, OnDestroy {

  public name: string = "cb-product-categories-table";
  public _rows: Category[];
  public filtered: Category[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  public state: string = 'list';
  public loading: boolean = true;
  public RowAction = RowAction;
  public _columns = [
    { field: 'ImageSmallPath', header: 'Thumbnail'},
    { field: 'Name', header: 'Collection Name' },
    { field: 'SeoUrl', header: 'URL Handle' },
    { field: 'CatalogCount', header: '# of Catalogs' }
  ]
  public recordActions = [
    new RecordAction('Delete', 'fal fa-trash-alt', '', () => this.groupDelete())
  ];

  @Input() product: number;
  @Input() isAssociateList: boolean = false;

  @ViewChild(Table2Component) dataTable: Table2Component;

  constructor( private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService ) {
    super();
  }

  load() {
    this.getRows();
  }

  private associate(){
    this.state='list';
    this.getRows();
  }

  private getRows() {
    this.loading = true;
    if(this.isAssociateList) {
      this.znodeService.getCategories('productIds,categoryNodes').pipe(untilDestroyed(this)).subscribe(
        data => {
          let rows = data.filter(category => category.ProductIds.split(',').indexOf(this.product.toString()) == -1);
          rows.forEach(category => {
            let nodes = category.CategoryNodes || [];
            category['CatalogCount'] = nodes.filterDuplicates((x,y) => x.CatalogId == y.CatalogId).length;
          });
          this._rows = rows;
          this.filterRows();
          this.dataTable.setSelectAll(false);
          this.loading = false;
        },
        err => {
          this.alertService.error(err);
          this.loading = false;
        }
      );
    } else {
      forkJoin(
        this.znodeService.getProductCategories(this.product),
        this.znodeService.getCatalogCategoryNodes()
      ).pipe(untilDestroyed(this)).subscribe(
        data => {
          let rows = data[0];
          let nodes = data[1];
          rows.forEach(item => {
            item.Category['ProductCategoryId'] = item.ProductCategoryId;
            item.Category['CatalogCount'] = nodes.filter(n => n.CategoryId == item.CategoryId)
              .filterDuplicates((x,y) => x.CatalogId == y.CatalogId).length;
          });
          this._rows = rows.map(productCategory => productCategory.Category);
          this.filterRows();
          this.loading = false;
        },
        err => {
          this.alertService.error(err);
          this.loading = false;
        }
      );
    }
  }

  public delete(id: number) {
    var title =  "Please confirm you would like to delete the selected Associated Collection. This change cannot be undone.";
    var content = "Deletion will permanently remove the Associated Collection.";
    this.modalService.warning(title, content);

    this.modalService.response.pipe(take(1)).subscribe(
      confirm => {
        if(confirm) {
          this.loading = true;
          this.znodeService.deleteCategoryProduct(id).pipe(untilDestroyed(this)).subscribe(
            res => {
              this.getRows();
            },
            err => {
              this.alertService.error(err);
              this.loading = false;
            }
          );
        }
      }
    );
  }

  groupDelete() {
    var title =  "Please confirm you would like to delete the selected Associated Collections. This change cannot be undone.";
    var content = "Deletion will permanently remove the Associated Collections.";
    let selected = this.dataTable.selected;
    this.modalService.warning(title, content);

    this.modalService.response.pipe(take(1)).subscribe(
      confirm => {
        if(confirm) {
          this.loading = true;
          let batch: Observable<boolean>[] = [];
          selected.forEach(productCategory => {
            batch.push(this.znodeService.deleteCategoryProduct(productCategory.ProductCategoryId));
          });
          forkJoin(batch).pipe(untilDestroyed(this)).subscribe(
            res => {
              this.getRows();
            },
            err => {
              this.alertService.error(err);
              this.loading = false;
            }
          );
        }
      }
    );
  }

  ngOnDestroy() {}
}
