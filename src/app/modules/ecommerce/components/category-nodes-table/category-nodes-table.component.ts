import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

import { TableComponent } from '../../../corebridge/components/table/table.component';
import { RowAction } from "../../../corebridge/components/table2/table.component";

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ThemeService } from '../../../../services/theme.service';

import { Category } from '../../models/category';
import { Alert } from '../../../corebridge/models/alert';
import { CategoryNode } from '../../models/category-node';
import { ModalService } from '../../../../services/modal.service';


@Component({
  selector: 'cb-category-nodes-table',
  templateUrl: './category-nodes-table.component.html'
})
export class CategoryNodesTableComponent extends TableComponent implements OnInit, OnDestroy {

  public name: string = "cb-category-nodes-table";
  public _rows: CategoryNode[] = [];
  public filtered: Category[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  public loading: boolean = true;

  public reindexing: boolean = false;

  private categories: Category[];

  @Input() catalog: number;

  @Output() onAssociate: EventEmitter<any> = new EventEmitter();
  @Output() onView: EventEmitter<number> = new EventEmitter();
  @Output() onAdd: EventEmitter<any> = new EventEmitter();

  public nodes: CategoryNode[] = [];
  public RowAction = RowAction;
  public _columns = [
    { field: 'Thumbnail', header: 'Thumbnail'},
    { field: 'Name', header: 'Collection Name', sortField: 'SortName' },
    { field: 'SeoUrl', header: 'URL Handle', sortField: 'SortSeoUrl' },
    { field: 'ProductCount', header: '# of Products', sortField: 'SortProductCount' },
    { field: 'DisplayOrder', header: 'Display Order', sortField: 'SortDisplayOrder' }
  ];

  constructor( private znodeService: ZnodeService, private alertService: AlertService, private router: Router, private themeService: ThemeService, private modalService: ModalService) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows(){
    this.loading = true;

    forkJoin(
      this.znodeService.getCatalogCategoryNodes(this.catalog),
      this.znodeService.getCategoriesAndExpandProductIds()
    ).pipe(untilDestroyed(this)).subscribe(
      data => {
        this.nodes = data[0];
        this.categories = data[1];

        this._rows = [];

        this.nodes.forEach(node => {
          var category = this.categories.filter(cat => cat.CategoryId == node.CategoryId)[0];
          category.Name = this.getName(category.CategoryId);
          node.Name = category.Name;
          node.Category = category;
          node.Thumbnail = category.ImageSmallPath;
          node.SeoUrl = category.SeoUrl || '';
          node.ProductCount = category.ProductIds ? category.ProductIds.split(',').length : 0;
          node['class-indent'] = node.ParentCategoryNodeId > 0;
          this._rows.push(node);
        });
        this._rows.forEach(node => { this.setSortFields(node); });

        this.filterRows();
        if(this.filtered.length)
          (document.getElementsByTagName('p-sortIcon').item(1) as HTMLElement).click();
        this.loading = false;
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      });
  }

  setSortFields(node: CategoryNode){
    if(node.ParentCategoryNodeId > 0) {
      let parent = this._rows.find(p => p.CategoryNodeId == node.ParentCategoryNodeId);
      node['SortName'] = parent.Name;
      node['SortSeoUrl'] = parent.SeoUrl + parent.CategoryNodeId.toString();
      node['SortProductCount'] = parent.ProductCount + (parent.CategoryNodeId/100000);
      node['SortDisplayOrder'] = parent.DisplayOrder + (parent.CategoryNodeId/100000);
    } else {
      node['SortName'] = node.Name;
      node['SortSeoUrl'] = node.SeoUrl + node.CategoryNodeId.toString();
      node['SortProductCount'] = node.ProductCount + (node.CategoryNodeId/100000);
      node['SortDisplayOrder'] = node.DisplayOrder + (node.CategoryNodeId/100000);
    }
  }

  public toggleActive(node: CategoryNode) {
    node.IsActive = !node.IsActive;
    this.znodeService.updateCategoryNode(node).pipe(untilDestroyed(this)).subscribe(
      res => this.getRows(),
      err => this.alertService.error(err)
    );
  }

  gotoDetail(id: number) {
    this.onView.emit(id);
  }

  delete(categoryNodeId: number){
    var title =  "Please confirm you would like to delete the selected Associated Collection. This change cannot be undone.";
    var content = "Deletion will permanently remove the Associated Collection.";
    this.modalService.warning(title, content);

    this.modalService.response.pipe(take(1)).subscribe(
      res => {
        if (!res) return;
        var canDelete = true;

        this.nodes.forEach(categoryNode => {
          if(categoryNode.ParentCategoryNodeId == categoryNodeId){
            canDelete = false;
          }
        });

        if(canDelete) {
          this.znodeService.deleteCategoryNode(categoryNodeId).pipe(untilDestroyed(this)).subscribe(
            res => {
              this.alertService.success('Collection Removed from Catalog');
              this.getRows();
            },
            err => this.alertService.error(err)
          );
        }
        else {
          this.alertService.error('Cannot Remove Collection with Child Collections');
        }
      })


  }

  reindex() {
    this.reindexing = true;
    this.znodeService.reindex().pipe(untilDestroyed(this)).subscribe(
      res => {
        var that = this;

        setTimeout(function(){
          that.reindexing = false;
        }, 1000 * 60 * 3);

        this.alertService.success('Re-Index Successfully Started');
      },
      err => {
        this.reindexing = false;
        this.alertService.error(err);
      }
    );
  }

  getName(categoryId: number): string {
    var node = this.getCategoryNodeByCategory(categoryId);
    var child = this.getCategory(node.CategoryId);
    return child.Name;
  }

  getCategory(categoryId: number): Category{
    return this.categories.filter(cat => cat.CategoryId == categoryId)[0];
  }
  getCategoryNodeByCategory(categoryId: number): CategoryNode{
    return this.nodes.filter(node => node.CategoryId == categoryId)[0];
  }
  getCategoryNodeByNode(categoryNodeId: number): CategoryNode{
    return this.nodes.filter(node => node.CategoryNodeId == categoryNodeId)[0];
  }

  associate() {
    this.onAssociate.emit();
  }

  relativeAddPath(): string {
    return location.pathname.endsWith('associated-categories') ? './add' : '../add';
  }

  ngOnDestroy() {}

}
