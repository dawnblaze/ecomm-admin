import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';

import { Font } from '../../models/font';
import { FontGroup } from '../../models/font-group';

@Component({
  selector: 'cb-font-links-table',
  templateUrl: './font-links-table.component.html'
})
export class FontLinksTableComponent extends TableComponent implements OnInit, OnChanges {
  @Input() font: Font = new Font();

  public name: string = "cb-font-links-table";
  public _rows: FontGroup[];
  public filtered: FontGroup[] = [];
  public search: string = '';
  public edit: boolean = false;

  constructor(private router: Router, private daimService: DaimService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['font']) {
      this.getRows();
    }
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.edit = false;

    if (this.font.fontFamilyName) {
      this.daimService.getFont(this.font.fontFamilyName, true)
        .subscribe(font => {
          this.font = font;
          this._rows = this.font.groups;
          if (this._rows) {
            this.filterRows();
          }
        });
    }
  }

  filterProcess(item: FontGroup): boolean {
    var keys = ["displayName", "id"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
