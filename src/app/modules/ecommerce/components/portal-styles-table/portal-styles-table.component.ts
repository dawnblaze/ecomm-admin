import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { CSS } from '../../models/css';
import { Portal } from '../../models/portal';
import { PortalCatalog } from '../../models/portal-catalog';
import { Alert } from '../../../corebridge/models/alert';

@Component({
  selector: 'cb-portal-styles-table',
  templateUrl: './portal-styles-table.component.html'
})
export class PortalStylesTableComponent extends TableComponent implements OnInit {

  @Input() portalId: number;

  public name: string = "cb-portal-styles-table";
	public _rows: CSS[];
  public filtered: CSS[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  private selected: string;
  public portalCatalog: PortalCatalog;

  constructor( private znodeService: ZnodeService,private router: Router, private alertService: AlertService ) { 
    super();
  }

  load() {
    this.getRows();
  }

  private getRows(){

    forkJoin(
      this.znodeService.getPortalCatalogs(this.portalId),
      this.znodeService.getCssList()
    ).subscribe(
      data => {
        this.portalCatalog = data[0][0];

        this._rows = data[1];
        this.filterRows();
      },
      err => this.alertService.error(err)
    );

  }

  private update(css: CSS) {
    this.znodeService.getPortalCatalog(this.portalCatalog.PortalCatalogId).subscribe(
      portalCatalog => {
        portalCatalog.CssId = css.CSSID;
        this.znodeService.updatePortalCatalog(portalCatalog).subscribe(
          res => {
            this.portalCatalog = res;
            this.alertService.success('Stylesheet Changed');
            this.getRows();
          }
        )
      }
    )
  }

}