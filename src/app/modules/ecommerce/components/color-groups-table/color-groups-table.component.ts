import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';
import { AlertService } from '../../../../services/alert.service';

import { ColorGroup } from '../../models/color-group';

@Component({
  selector: 'cb-color-groups-table',
  templateUrl: './color-groups-table.component.html'
})
export class ColorGroupsTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-color-groups-table";
  public _rows: ColorGroup[];
  public filtered: ColorGroup[] = [];
  public search: string = '';

  constructor(private daimService: DaimService, private router: Router, private alertService: AlertService) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.daimService.getColorGroups().subscribe(colorGroups => {
      this._rows = colorGroups;

      this.filterRows();
    });
  }

  gotoDetail(colorGroup: ColorGroup) {
    this.router.navigate(['/ecommerce/settings/color-groups', colorGroup.id]);
  }

  delete(id: number) {
    this.daimService.deleteColorGroup(id).subscribe(res => {
      this.alertService.success('Color Group Deleted Successfully');
      this.getRows();
    });
  }

  filterProcess(item: ColorGroup): boolean {
    var keys = ["id", "name"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
