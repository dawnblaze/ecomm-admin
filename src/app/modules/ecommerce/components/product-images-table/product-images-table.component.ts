import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { ProductImage } from '../../models/product-image';
import { ProductImageType } from '../../models/product-image-type';

@Component({
  selector: 'cb-product-images-table',
  templateUrl: './product-images-table.component.html'
})
export class ProductImagesTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-product-images-table";
  public _rows: any[];
  public filtered: any[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;

  private _product: number;
  @Input()
  set productId(product: number) {
    this._product = product || 0;
    this.getRows(); 
  }
  get productId() { return this._product || 0; }


  @Output() onCreate: EventEmitter<any> = new EventEmitter();
  @Output() onView: EventEmitter<number> = new EventEmitter();

  private productImageTypes: any[] = [];

  private imageRoot: string;

  constructor( private znodeService: ZnodeService, private alertService: AlertService ) { 
    super();
  }

  load() {
    this.imageRoot = this.znodeService.base;

    this.znodeService.getProductImageTypes().subscribe(
      types => {
        this.productImageTypes = types
        this.getRows();
      }, 
      err => this.alertService.error(err)
    );
  }

  public getProductImageTypeName(id: number): string{
    let results = this.productImageTypes.filter(image => image.ProductImageTypeID == id);
    return (results.length > 0) ? results[0].Name : 'Image #'+id;
  }

  public goToDetails(id: number) {
    this.onView.emit(id);
  }

  public create() {
    this.onCreate.emit();
  }
  
  private getRows() {
    if(this.productId > 0){
      this.znodeService.getProductImages(this.productId).subscribe(images => {
        this._rows = images;
        console.log(this._rows);
        this.filterRows();
      });
    }
  }

  public delete(id: number) {
    console.log("Deleting Product Image");
    this.znodeService.deleteProductImage(id).subscribe(
      data => {
        console.log(data);
        this.getRows();
        this.alertService.success('Product Image Removed Successfully');
      },
      err => this.alertService.error(err)
    );
  }

}
