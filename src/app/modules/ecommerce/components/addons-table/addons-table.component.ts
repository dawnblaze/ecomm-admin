import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Addon } from '../../models/addon';

@Component({
  selector: 'cb-addons-table',
  templateUrl: './addons-table.component.html'
})
export class AddonsTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-addons-table";
  public _rows: Addon[];
  public filtered: Addon[] = [];
  public search: string = "";
  public searchKey: string = "Title";
  public total = 0;

  constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService ) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {

    this.znodeService.getAddons().subscribe(
      addons => {
        this._rows = addons;
        this.filterRows();
      },
      err => this.alertService.error(err)
    );

  }

  gotoDetail(id: number) {
    let link = ['/ecommerce/merchandise/addons', id];
    this.router.navigate(link);
  }

  delete(id: number){

    this.znodeService.deleteAddon(id).subscribe(
      response => this.getRows(),
      error => this.alertService.error(error)
    );

  }

  public filterProcess(item: Addon): boolean {
    var keys = ["Name", "AddOnId", "Title"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }

  copy(row: Addon) {
    this.znodeService.copyAddon(row.AddOnId).subscribe(
      res => {
        this.alertService.success('AddOn Copied Successfully');
        this.getRows();
      },
      err => {
        this.alertService.error(err);
      });
  }
}
