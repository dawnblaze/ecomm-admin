import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Portal } from '../../models/portal';
import { PortalProfile } from '../../models/portal-profile';
import { Profile } from '../../models/profile';

@Component({
  selector: 'cb-portal-profiles-table',
  templateUrl: './portal-profiles-table.component.html'
})
export class PortalProfilesTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-portal-profiles-table";
  public _rows: Profile[];
  public filtered: Profile[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;

  private portalProfiles: PortalProfile[] = [];

  @Input() portal: Portal;

  @Output() onCreate: EventEmitter<any> = new EventEmitter();
  @Output() onView: EventEmitter<number> = new EventEmitter();

  constructor( private znodeService: ZnodeService, private alertService: AlertService ) { 
    super();
  }

  load() {
    this.getRows();
  }
  
  private getRows() {
    this.znodeService.getPortalProfiles(this.portal.PortalId).subscribe(portalProfiles => {

      var profiles: any[] = [];
      portalProfiles.forEach(portalProfile => {
        this.znodeService.getProfile(portalProfile.ProfileID).subscribe(
          profile => {
            profiles.push(profile);
            this.portalProfiles.push(portalProfile);
          },
          err => this.alertService.error(err)
        )
      })
      this._rows = profiles;
      this.filterRows();
    });
  }

  public delete(id: number) {
    console.log("Deleting Portal Profile");
    let portalProfiles = this.portalProfiles.filter( portalProfile => portalProfile.ProfileID==id);

    this.znodeService.getPortal(this.portal.PortalId).subscribe(
      portal => {
        if(portal.DefaultAnonymousProfileId == id){
          this.alertService.error('Cannot Delete Default Anonymous Profile.');
        }
        else if(portal.DefaultRegisteredProfileId == id){
          this.alertService.error('Cannot Delete Default Registered Profile.');
        }
        else {
          this.znodeService.deletePortalProfile(portalProfiles[0].PortalProfileID).subscribe(
            res => {
              console.log(res);
              this.alertService.success('Profile Removed');
              this.getRows();
            },
            err => this.alertService.error(err)
          );
        }
      },
      err => {
        console.error(err);
        this.alertService.error(err);
      }
    );
    
  }

  public setDefaultAnonymousProfile(id: number){
    this.znodeService.getPortal(this.portal.PortalId).subscribe(
      portal => {
        this.portal = portal;
        if(this.portal.DefaultAnonymousProfileId == id){
          // already set as default
          this.alertService.info('This Profile is already the Default Anonymous Profile.');
        }
        else { 
          this.portal.DefaultAnonymousProfileId = id;
          this.znodeService.updatePortal(this.portal).subscribe(
            portal => {
              this.getRows();
              this.alertService.success('Default Anonymous Profile Updated');
            },
            err => {
              this.alertService.error(err);
            }
          );
        }
      },
      err => {
        this.alertService.error(err);
      }
    )
  }

  public setDefaultRegisteredProfile(id: number){
    this.znodeService.getPortal(this.portal.PortalId).subscribe(
      portal => {
        this.portal = portal;
        if(this.portal.DefaultRegisteredProfileId == id){
          // already set as default
          this.alertService.info('This Profile is already the Default Registered Profile.');
        }
        else {
          this.portal.DefaultRegisteredProfileId = id;
          this.znodeService.updatePortal(this.portal).subscribe(
            portal => {
              this.getRows();
              this.alertService.success('Default Registered Profile Updated');
            },
            err => {
              this.alertService.error(err);
            }
          );
        }
      },
      err => {
        this.alertService.error(err);
      }
    )
  }

  goToDetails(id: number) {
    var portalProfile: PortalProfile = this.portalProfiles.filter(portalProfile => portalProfile.ProfileID == id)[0];

    this.onView.emit(portalProfile.PortalProfileID);
  }

  create() {
    this.onCreate.emit();
  }
}
