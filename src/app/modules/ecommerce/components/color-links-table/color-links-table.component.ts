import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';

import { Color } from '../../models/color';
import { ColorGroup } from '../../models/color-group';

@Component({
  selector: 'cb-color-links-table',
  templateUrl: './color-links-table.component.html'
})
export class ColorLinksTableComponent extends TableComponent implements OnInit, OnChanges {
  @Input() color: Color;

  public name: string = "cb-color-links-table";
  public _rows: ColorGroup[];
  public filtered: ColorGroup[] = [];
  public search: string = '';
  public edit: boolean = false;

  constructor(private router: Router, private daimService: DaimService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['color']) {
      this.getRows();
    }
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.edit = false;

    if (this.color.id) {
      this.daimService.getColor(this.color.id, true)
        .subscribe(color => {
          this.color = color;
          this._rows = this.color.groups;
          this.filterRows();
        });
    }
  }

  filterProcess(item: ColorGroup): boolean {
    var keys = ["name", "id"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
