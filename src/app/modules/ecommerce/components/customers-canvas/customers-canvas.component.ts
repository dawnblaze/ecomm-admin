import { Component, OnInit } from '@angular/core';
import moduleLoader  from '@aurigma/ui-framework/dist/moduleLoader.js';
import { CodaService } from '../../../../services/coda.service';
import { EditorPreset } from '../../models/editor-preset';
import { TemplatePreset } from '../../models/template-preset';
import { EditorPresetJsonModel } from '../../models/editor-preset-json';
import { TemplatePresetJsonModel } from '../../models/template-preset-json';
import { ZnodeService } from '../../../../services/znode.service';
import { error } from '@angular/compiler/src/util';
import { environment } from '../../../../../environments/environment'
import * as templatePresetDefault from '../../pages/surface-preset-details/template-preset-default.json';

@Component({
  selector: 'cb-customers-canvas',
  templateUrl: './customers-canvas.component.html',
  styleUrls: ['./customers-canvas.component.scss']
})
export class CustomersCanvasComponent implements OnInit {
  productId: Number;
  private editorPreset: EditorPreset;
  private templatePreset: TemplatePreset;
  private editorPresetJson: EditorPresetJsonModel;
  private templatePresetJson: TemplatePresetJsonModel;
  private customersCanvasBaseUrl = environment.customersCanvasBaseUri;
  private uiFrameworkBaseUrl = "/assets/cc/dist";
  constructor(private codaService: CodaService, private znodeService: ZnodeService) {
  }

  public displayDialog: boolean = false;

  ngOnInit(): void {
    this.codaService.getEditorPresets(1, 10).subscribe(resp => {
      let firstPreset: EditorPreset = resp.records[0];
      if(firstPreset) {
        this.editorPreset = firstPreset;
        this.editorPresetJson = <EditorPresetJsonModel>JSON.parse(firstPreset.presetJSON)
      }
    });
    this.templatePresetJson = <TemplatePresetJsonModel>(templatePresetDefault as any).default;
    this.codaService.getTemplatePresets(1,10).subscribe(resp => {
      let firstPreset: TemplatePreset = resp.records[0];
      if(firstPreset) {
        this.templatePreset = firstPreset;
        if (firstPreset.presetJSON) this.templatePresetJson = <TemplatePresetJsonModel>JSON.parse(firstPreset.presetJSON)
      }
    });
  }

  // public close() {
  //   this.displayDialog = false;
  // }

  // public openDialog() {
  //   this.displayDialog = true;
  //   this.initCustomersCanvasEditor();
  // }

  async initCustomersCanvasEditor() {
    let product = {
      id: 0,
      sku: "PRODUCT-001",
      title: "My Product",
      description: "This is a test product.",
      options: [],
      price: 1,
      attributes: []
    };
    let config = {
      "showSteps": true,
      /*Widgets*/
      "widgets": [
        /*Editor*/
        {
          "name": "editor",
          "type": "design-editor",
          "params": {
            "initial": {
              "productDefinition": {
                "defaultSafetyLines": this.templatePresetJson.safetyLines.map(o => {
                  return {
                    margin: o.margin,
                    color: o.color,
                    altColor: o.altColor,
                    stepPx: o.stepPx,
                    widthPx: o.widthPx
                  };
                }),
                "surfaces": [
                  {
                    "printAreas": [
                      {
                        "designFile": "cc-testfile-8bit"
                      }
                    ]
                  }
                ]
              },
              "editorConfig": {
                "design-editor": {
                  "containerColor": "#fff",
                  "color": "#fff",
                  "shadowEnabled": true,
                  "rulers": {
                    "enabled": (this.editorPresetJson && this.editorPresetJson.canvas) ? this.editorPresetJson.canvas.rulers.enabled : false
                  }
                },
                "fontList": {
                  "appFonts": [
                    "*"
                  ]
                },
                "initialMode": "SimpleOnly",
                "restoreProductOnReloadEnabled": false,
                "widgets": {
                  "ObjectInspector": {
                    "isHidden": false,
                    "position": "Right",
                    "bgItemEnabled": false,
                    "variableItemsEnabled": false
                  },
                  "FinishButton": {
                    "mode": "Disabled"
                  }
                }
              }
            }
          }
        },
        /*WelcomeWidget*/
        {
          "name": "WelcomeWidget",
          "type": "static-text",
          "title": "Welcome Widget",
          "params": {
            "text": "Welcome to step 2!"
          }
        },
        /*CheckboxWidget1*/
        {
          "name": "CheckboxWidget1",
          "type": "checkbox",
          "params": {
            "value": false,
            "prompt": "Hello I'm a checkbox."
          }
        },
        /*GalleryWidget1*/
        {
          "type": "gallery",
          "name": "GalleryWidget1",
          "title": "Background",
          "params": {
            "prompt": "Choose a background image",
            "showTitle": true,
            "items": [
              {
                "title": "RubberDuck",
                "name": "rubberduck",
                "previewUrl": "https://endordevnhenry.blob.core.windows.net/bid1/data/default/images/catalog/640/turnkey/1/kittens.jpg"
              },
              {
                "title": "HelloKitty",
                "name": "hellokitty",
                "previewUrl": "https://endordevnhenry.blob.core.windows.net/bid1/data/default/images/catalog/640/turnkey/1/exoticcat.jpg"
              }
            ]
          }
        },
        /*GroupWidget1*/
        {
          "name": "GroupWidget1",
          "type": "group",
          "params": {
            "title": "widget group",
            "type": "collapsible",
            "tabs": [
              {
                "title": "Options Group",
                "widgets": [
                  {
                    "name": "AddMatting",
                    "type": "checkbox",
                    "params": {
                      "value": false,
                      "prompt": "Add Matting"
                    }
                  },
                  {
                    "name": "AddFraming",
                    "type": "checkbox",
                    "params": {
                      "value": false,
                      "prompt": "Add Framing"
                    }
                  },
                  {
                    "type": "option",
                    "title": "Choose canvas size",
                    "name": "size-selector",
                    "params": {
                      "title": "Size",
                      "type": "list",
                      "props": { maxCount: 1 },
                      "values": [{ "title": "8 x 10" }, { "title": "16 x 20" }, { "title": "32 x 40" }]
                    }
                  }
                ]
              },
              {
                "title": "Options Group1",
                "widgets": [
                  {
                    "name": "cats-request",
                    "type": "ajax",
                    "params": {
                      "url": "https://api.thecatapi.com/v1/images/search?limit=9&mime_types=&order=Random&size=small&page=0&category_ids=5&sub_id=demo-a79d6e",
                      "method": "GET"
                    }
                  },
                  {
                    "name": "Dynamic Cats",
                    "type": "gallery",
                    "params": {
                      "prompt": "Pick a cat",
                      "items": {
                        "{{#each $['cats-request'].response as cat}}": {
                          "title": "{{cat.id}}",
                          "name": "{{cat.id}}",
                          "previewUrl": "{{cat.url}}"
                        }
                      }
                    }
                  }
                ]
              }
            ]
          }
        },
        /*FinishButton*/
        {
          "name": "finish-button",
          "type": "finish-button",
          "params": {
            "visible": true,
            "enabled": true
          }
        }
      ],
      /*Steps*/
      "steps": [
        { /*Step 1*/
          "name": "Finishing Options",
          "toolPanel": {
            "name": "GroupWidget1"
          },
          "mainPanel": {
            "name": "editor"
          },
          "bottomPanel": {
            "name": "GalleryWidget1"
          }
        }
        ,
        {
          "name": "Product Review",
          "mainPanel": {
            "name": "WelcomeWidget"
          },
          "bottomPanel": {
            "name": "GalleryWidget1"
          }
        }
      ]
    };
    let order = {
      data: {
        stateId: "MyStateId"
      }
    };

    let driver: any = (await moduleLoader.dynamicImport("ecommerceDriver", `${this.uiFrameworkBaseUrl}/drivers/default-driver.js`)).ecommerceDriver;
    let cEditor: any = (await moduleLoader.dynamicImportDefault("editor", `${this.uiFrameworkBaseUrl}/editor.js`)).editor;

    let userId: string;
    let userIdResponse = await this.znodeService.getCustomersCanvasUserId().toPromise();
    if (!userIdResponse) {
      throw error('Could not load user id for canvas editor');
    } else {
      userId = userIdResponse;
    }
    let ecommerce: any = await driver.init(product, cEditor, config, /* settings */ {customersCanvasBaseUrl: this.customersCanvasBaseUrl}, /* restore data */ order, /*quantity*/ 1, /* user info*/ {id: userId});
    ecommerce.products.current.renderEditor(document.getElementById("editor-container"));

    // On Finish Button Submitting Event Callback
    ecommerce.cart.onSubmitting.subscribe(async function (data) {
      console.log(`On Editor Cart Submitting Data:`);
      console.log(data);
      let lineItems : any[] = data.lineItems;
      let auWizardElement : any = document.querySelector('au-wizard');

      // Get reference to iFrameApi
      let editor = await auWizardElement.scopeInternal.$.editor.customersCanvasPromise;

      // Call Finish Product Design
      // Saves State and generates HiRes PDF and image files
      await editor.finishProductDesign({fileName: "myFileName"}).then( res => {
        // let finishDesignResult : IFinishDesignResult = res;
        // console.log(`Finish Product Design Response:`);
        // console.log(finishDesignResult);
      });
    });

    // On Finish Button Submitted Callback
    ecommerce.cart.onSubmitted.subscribe(function (data) {
      console.log(`On Editor Cart Submitted Data:`);
      console.log(data);
      console.log('Line Items:')
      data.lineItems.forEach(function (li, index) {
        console.log(`Line Item ${index}:`);
        console.log(li);
      })
    });
  }
}
