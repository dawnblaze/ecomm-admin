import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ZnodeTableComponent } from '../../../corebridge/components/table/znode-table/znode-table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { ZnodeApiService, ZnodeRequest, ZnodeResponse } from '../../../../services/znode-api.service';
import { AlertService } from '../../../../services/alert.service';

import { Account } from '../../models/account';

@Component({
  selector: 'cb-companies-table',
  templateUrl: './companies-table.component.html'
})
export class CompaniesTableComponent extends ZnodeTableComponent {

  public name: string = "cb-companies-table";
  public dataLoaded: boolean = false;
  public searchKeys = [ 'CompanyName'];

  constructor( private znodeApiService: ZnodeApiService, private router: Router, private alertService: AlertService ) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.znodeApiService.getCompanies(this.request).subscribe(res => {
      this.response = res;

      this.response.data.forEach((company) => {
        return company.PhoneNumber = company.PhoneNumber || '';
      });

      this.rows = this.response.data;
      this.dataLoaded = true;
      this.loaded();
    });
  }

  gotoDetail(id: number) {
    let link = ['/ecommerce/customers/companies', id];
    this.router.navigate(link);
  }
}
