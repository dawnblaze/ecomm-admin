import {
  Component,
  Input,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter
} from "@angular/core";
import { Router, RouterLink } from "@angular/router";

import { forkJoin } from "rxjs";
import { switchMap } from "rxjs/operators";
import { untilDestroyed } from "ngx-take-until-destroy";

import { TableComponent } from "../../../corebridge/components/table/table.component";
import { RowAction } from "../../../corebridge/components/table2/table.component";

import { ZnodeService } from "../../../../services/znode.service";
import { AlertService } from "../../../../services/alert.service";
import { ModalService } from "../../../../services/modal.service";
import { CesaService, MisTypes } from "../../../../services/cesa.service";

import { Product } from "../../models/product";
import { ProductsPaged } from "../..//models/products-paged";

import { ControlProduct } from "../../../cesa/models/control/control-product";
import { CesaProductMapping } from "../../../cesa/models/cesa-product-mapping";

@Component({
  selector: "cb-products-table",
  templateUrl: "./products-table.component.html"
})
export class ProductsTableComponent extends TableComponent
  implements OnInit, OnDestroy {
  public name: string = "cb-products-table";
  public _rows: Product[];
  public filtered: Product[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  public index = 0;
  public searchSuggestions: any[] = [
    {
      id: -1,
      text: "Collections",
      children: []
    },
    {
      id: -2,
      text: "Catalogs",
      children: []
    }
  ];
  public hideSearch: boolean = true;
  public RowAction = RowAction;
  public _columns = [
    { field: "Thumbnail", header: "Thumbnail" },
    { field: "Name", header: "Name" },
    { field: "RetailPrice", header: "Retail", currency: "USD" },
    { field: "SalePrice", header: "Sale", currency: "USD" },
    { field: "WholesalePrice", header: "Wholesale", currency: "USD" },
    { field: "QuantityOnHand", header: "On Hand", align: "right" },
    { field: "DisplayOrder", header: "Order", align: "center" },
    { field: "CategoryCount", header: "In Collections", align: "center" },
    { field: "SkuName", header: "SKU" }
  ];

  public dataLoaded = false;

  public categoryProductIds = [];
  public searchFilters: string[] = [];

  // Hash of categories and catalogs with lists of included products
  private catProducts: { [id: string]: string[] } = {};

  private imageRoot: string;

  public misType: number;
  public controlProducts: ControlProduct[] = [];
  public selectedControlProduct: ControlProduct;
  public cesaProductMapping: CesaProductMapping = new CesaProductMapping();

  @Output() onLoading: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private znodeService: ZnodeService,
    private router: Router,
    private alertService: AlertService,
    private modalService: ModalService,
    private cesaService: CesaService
  ) {
    super();
  }

  ngOnInit() {
    this.misType = this.cesaService.getMisType();
    if (this.misType == MisTypes.Cyrious) {
      this.cesaService
        .getControlProducts()
        .pipe(untilDestroyed(this))
        .subscribe(controlProducts => {
          this.controlProducts = controlProducts;
        });
    }
  }

  load() {
    if (sessionStorage.getItem(this.name + "-searchFilters") != null) {
      this.searchFilters = JSON.parse(
        sessionStorage.getItem(this.name + "-searchFilters")
      );
    }

    this.imageRoot = this.znodeService.base;

    this.UpdateCategoryAndCatalogAssociations();
  }

  private UpdateCategoryAndCatalogAssociations() {
    forkJoin(
      this.znodeService.getCategoriesAndExpandProductIds(),
      this.znodeService.getCatalogsAndExpandProductIds()
    )
      .pipe(untilDestroyed(this))
      .subscribe(
        data => {
          this.categoryProductIds = data[0].map(category =>
            category.ProductIds.split(",")
          );
          for (var category of data[0]) {
            this.searchSuggestions[0].children.push({
              id: "category" + category.CategoryId,
              text: category.Name
            });
            this.catProducts["category" + category.CategoryId] = [];

            if (category.ProductIds.length > 0) {
              this.searchSuggestions.push(category);
              this.catProducts[
                "category" + category.CategoryId
              ] = category.ProductIds.split(",");
            }
          }

          for (var catalog of data[1]) {
            this.searchSuggestions[1].children.push({
              id: "catalog" + catalog.CatalogId,
              text: catalog.Name
            });
            this.catProducts["catalog" + catalog.CatalogId] = [];

            if (catalog.ProductIds.length > 0) {
              this.searchSuggestions.push(catalog);
              this.catProducts[
                "catalog" + catalog.CatalogId
              ] = catalog.ProductIds.split(",");
            }
          }

          this.onFilterChange(this.searchFilters);
          this.dataLoaded = true;
        },
        err => this.alertService.error(err)
      );
  }

  onResponse = (response: ProductsPaged) => {
    response.Products.forEach(product => {
      product["Thumbnail"] =
        product.ArtworkThumbnailUri || product.ImageSmallPath;
      product["CategoryCount"] = this.categoryProductIds.filter(ids =>
        ids.find(id => id == product.ProductId)
      ).length;
    });
    this._rows = response.Products;
    this.filtered = response.Products;
    this.index = this.page;
    this.total = response.TotalResults;
    this.loaded();
  };

  getRows() {
    if (this.productIdList.length > 0) {
      this.znodeService
        .getProductsByIdsPaged(
          this.productIdList,
          this.page,
          this.count,
          this.search,
          this.sortString,
          this.sortAsc
        )
        .pipe(untilDestroyed(this))
        .subscribe(this.onResponse, err => {
          this.alertService.error(err);
        });
    } else {
      this.znodeService
        .getProductsPaged(
          this.page,
          this.count,
          this.search,
          this.sortString,
          this.sortAsc
        )
        .pipe(untilDestroyed(this))
        .subscribe(this.onResponse, err => {
          this.alertService.error(err);
        });
    }
  }

  gotoDetail(id: number) {
    let link = ["/ecommerce/merchandise/products", id];
    this.router.navigate(link);
  }

  copy(id: number) {
    this.onLoading.emit(true);
    let product = this._rows.find(p => p.ProductId == id);
    this.znodeService
      .copyProduct(product.ProductId)
      .pipe(untilDestroyed(this))
      .subscribe(
        response => {
          if (this.misType == MisTypes.Cyrious) {
            this.cesaService
              .getProductMapping(product.ProductId)
              .pipe(untilDestroyed(this))
              .subscribe(res => {
                this.cesaProductMapping = res || new CesaProductMapping();

                this.selectedControlProduct = this.controlProducts.filter(
                  x => x.productId == this.cesaProductMapping.atomID
                )[0];

                this.cesaProductMapping.eCommID = response.Product.ProductId;
                this.cesaProductMapping.atomID = this.selectedControlProduct.productId;
                this.cesaProductMapping.atomName = this.selectedControlProduct.productName;
                this.cesaService
                  .setProductMapping(this.cesaProductMapping)
                  .pipe(untilDestroyed(this))
                  .subscribe(res => {
                    console.log(res);
                  });
              });
          }
          this.getRows();
          this.alertService.success("Product Copied Successfully");

          this.UpdateCategoryAndCatalogAssociations();
        },
        err => this.alertOnLoaded(err)
      );
  }

  toggleActivation(product: Product) {
    this.onLoading.emit(true);
    this.znodeService
      .getProduct(product.ProductId)
      .pipe(
        switchMap(currentProduct => {
          currentProduct.IsActive = !currentProduct.IsActive;
          return this.znodeService.updateProductSettings(currentProduct);
        })
      )
      .pipe(untilDestroyed(this))
      .subscribe(
        res => {
          console.log("ToggleActivation RESULT: ", res);
          this.alertService.success("Product Updated");
          this.getRows();
        },
        err => this.alertOnLoaded(err)
      );
  }

  delete(id: number) {
    let product = this._rows.find(p => p.ProductId == id);
    var title =
      "Please confirm you would like to delete the selected Product. This change cannot be undone.";
    var content = "Deletion will permanently remove the Product.";
    this.modalService.warning(title, content);

    let subscription = this.modalService.response
      .pipe(untilDestroyed(this))
      .subscribe(
        response => {
          if (response) {
            this.onLoading.emit(true);
            this.znodeService
              .deleteProduct(product.ProductId)
              .pipe(untilDestroyed(this))
              .subscribe(
                res => {
                  console.log(res);
                  this.alertService.success("Product Deleted Successfully");
                  this.getRows();
                },
                error => this.alertOnLoaded(error)
              );
          }
          subscription.unsubscribe();
        },
        err => this.alertService.error(err)
      );
  }

  onSearchChange(search: string) {
    this.search = search;

    // Store search query for this table in the session storage
    sessionStorage.setItem(this.name + "-searchString", JSON.stringify(search));

    this.getRows();
  }

  private productIdList: number[] = [];
  onFilterChange(selection: string[]) {
    this.productIdList = [];
    this.searchFilters = selection;
    sessionStorage.setItem(
      this.name + "-searchFilters",
      JSON.stringify(this.searchFilters)
    );

    if (
      !(
        Object.keys(this.catProducts).length === 0 &&
        this.catProducts.constructor === Object
      )
    ) {
      for (var filter of this.searchFilters) {
        Array.prototype.push.apply(
          this.productIdList,
          this.catProducts[filter]
        );
      }
    }
    this.getRows();
  }

  public filterProcess(item: Product): boolean {
    return;
  }

  onPageChange(page: number) {
    if (page != this.page) {
      this.page = page;
      this.getRows();
    }
  }

  onCountChange(count: number) {
    this.count = count;
    this.page = 1;
    this.getRows();
    sessionStorage.setItem(this.name + "-count", this.count + "");
    sessionStorage.setItem(this.name + "-page", this.page + "");
  }

  sortBy(sort: string) {
    super.sortBy(sort);
    this.getRows();
  }

  alertOnLoaded(message: string) {
    this.alertService.error(message);
    this.onLoaded.emit(true);
  }

  ngOnDestroy() {}
}
