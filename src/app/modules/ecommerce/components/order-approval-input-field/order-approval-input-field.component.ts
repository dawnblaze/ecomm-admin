import { Component, AfterViewInit, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from "@angular/core";
import { mergeMap, map } from 'rxjs/operators';
import { Observable, Observer } from 'rxjs';
import { ZnodeService } from '../../../../services/znode.service';
import { AccountSearchParameters } from "../../models/approval-account-search-parameters";
import { AccountSearchModel } from "../../models/approval-account-search";
import { OrderApproval } from "../../models/approval-group";
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'cb-order-approval-input-field',
  templateUrl: './order-approval-input-field.component.html',
  styleUrls: ['./order-approval-input-field.component.less']
})

export class OrderApprovalInputFieldComponent implements OnInit {

  @Output() onSelectionChange = new EventEmitter<AccountSearchModel[]>();

  @Input() selectedOption: AccountSearchModel[];
  @Input() selectedItems: SelectItem[];
  @Input() searchDelayMs : number;
  @Input() searchTermMinLength : number = 3;
  @Input() approvalGroup: OrderApproval;
  @Input() searchType: string;
  @Input() searchText: string;
  @Input() noItemsLabel: string;
  @Input() placeholder: string;
  @Input() portalID: number;

  @ViewChild('inputField') inputField: ElementRef;

  public hasSelection: boolean = false;
  public showDropdown: boolean = false;
  public selectedValue:string;
  public searchAccounts: AccountSearchModel[];

  public isEdit:boolean = false;

  // backspaceCount = 2 activates editMode
  public backspaceCount: number = 1;
  public isEditMode: boolean = false;
  public searchEditObj: AccountSearchModel;

  private accounts: number[];

  constructor(private znodeService: ZnodeService, private elRef: ElementRef) {}

  ngOnInit() {
    this.accounts = [];
    this.selectedItems = [];
    console.log('LOADED');
    console.log(this.selectedOption);
    this.selectedOption.map(item => {
      this.selectedItems.push({ label: item.DisplayName, value: item } );
      this.accounts.push(item.AccountID);
    });
  }

  onSearch(event) {
      let query = event.query;

      let searchParam = new AccountSearchParameters();
      searchParam.ApprovalGroupID = (this.searchType == 'Exempt') ? 1 : this.approvalGroup.ID;
      searchParam.PortalID = this.portalID;
      searchParam.SearchString = query;
      searchParam.Type = this.searchType;

      // search from the backend
      this.znodeService.orderApprovalSearchAccounts(searchParam).pipe(
        map(accounts => accounts.filter(x => this.accounts.indexOf(x.AccountID) == -1))
      ).subscribe(accounts => {
        this.searchAccounts = accounts.sort((a,b)=>a.DisplayName.localeCompare(b.DisplayName));
        console.log('SEARCH ACCOUNTS');
        console.log(this.searchAccounts);
      });
  }

  // searchQuery(value) {
  //   this.showDropdown = false;
  //   if (value && value.length >= this.searchTermMinLength) {
  //     this.showDropdown
  //     setTimeout(() => {
  //       this.hasSelection = false;
  //       let searchParam = new AccountSearchParameters();
  //       searchParam.ApprovalGroupID = (this.searchType == 'Exempt') ? 1 : this.approvalGroup.ID;
  //       searchParam.PortalID = this.portalID;
  //       searchParam.SearchString = value;
  //       searchParam.Type = this.searchType;

  //       this.znodeService.orderApprovalSearchAccounts(searchParam).pipe(
  //         map(accounts => accounts.filter(x => this.accounts.indexOf(x.AccountID) == -1 && x.AccountID > -1))
  //       ).subscribe(accounts => {
  //         // if (this.isEditMode) accounts.unshift(this.searchEditObj);
  //         this.searchAccounts = this.sortAccounts(accounts, value);
  //         if (this.searchAccounts.length > 0) this.showDropdown = true;
  //       });
  //     }, this.searchDelayMs);
  //   }
  // }

  // sortAccounts(accounts: AccountSearchModel[], searchString: string) : AccountSearchModel[]{

  //   if(accounts.length == 0) return accounts;

  //   searchString = searchString.toLowerCase();

  //   // Companies starting with the string
  //   let sortedAccounts = accounts.filter(a => a.ParentAccountID == 0 && a.DisplayName.toLowerCase().startsWith(searchString)).sort((a, b) => this.sortByName(a, b));
  //   accounts = accounts.filter(f => !sortedAccounts.find(a => a.AccountID == f.AccountID));
  //   if(accounts.length == 0) return sortedAccounts;
  //   // Contacts whose first name starts with the string
  //   sortedAccounts = sortedAccounts.concat(accounts.filter(a => a.ParentAccountID != 0 && a.DisplayName.split(' ')[0] && a.DisplayName.split(' ')[0].toLowerCase().startsWith(searchString)).sort((a, b) => this.sortByName(a, b)));
  //   accounts = accounts.filter(f => !sortedAccounts.find(a => a.AccountID == f.AccountID));
  //   if(accounts.length == 0) return sortedAccounts;
  //   // Contacts whose last name starts with the string
  //   sortedAccounts = sortedAccounts.concat(accounts.filter(a => a.ParentAccountID != 0 && a.DisplayName.split(' ')[1] && a.DisplayName.split(' ')[1].toLowerCase().startsWith(searchString)).sort((a, b) => this.sortByName(a, b)));
  //   accounts = accounts.filter(f => !sortedAccounts.find(a => a.AccountID == f.AccountID));
  //   if(accounts.length == 0) return sortedAccounts;
  //   // Contacts whose email starts with the string
  //   sortedAccounts = sortedAccounts.concat(accounts.filter(a => a.ParentAccountID != 0 && a.Email.toLowerCase().startsWith(searchString)).sort((a, b) => this.sortByName(a, b)));
  //   accounts = accounts.filter(f => !sortedAccounts.find(a => a.AccountID == f.AccountID));
  //   if(accounts.length == 0) return sortedAccounts;
  //   // Companies whose name contains the string
  //   sortedAccounts = sortedAccounts.concat(accounts.filter(a => a.ParentAccountID == 0 && a.DisplayName.toLowerCase().indexOf(searchString) > 0).sort((a, b) => this.sortByName(a, b)));
  //   accounts = accounts.filter(f => !sortedAccounts.find(a => a.AccountID == f.AccountID));
  //   if(accounts.length == 0) return sortedAccounts;
  //   // Contacts whose first name contains the string
  //   sortedAccounts = sortedAccounts.concat(accounts.filter(a => a.ParentAccountID != 0 && a.DisplayName.split(' ')[0] && a.DisplayName.split(' ')[0].toLowerCase().indexOf(searchString) > 0).sort((a, b) => this.sortByName(a, b)));
  //   accounts = accounts.filter(f => !sortedAccounts.find(a => a.AccountID == f.AccountID));
  //   if(accounts.length == 0) return sortedAccounts;
  //   // Contacts whose last name contains the string
  //   sortedAccounts = sortedAccounts.concat(accounts.filter(a => a.ParentAccountID != 0 && a.DisplayName.split(' ')[1] && a.DisplayName.split(' ')[1].toLowerCase().indexOf(searchString) > 0).sort((a, b) => this.sortByName(a, b)));
  //   accounts = accounts.filter(f => !sortedAccounts.find(a => a.AccountID == f.AccountID));
  //   if(accounts.length == 0) return sortedAccounts;
  //   // Contacts whose email contains the string
  //   sortedAccounts = sortedAccounts.concat(accounts.filter(a => a.ParentAccountID != 0 && a.Email.toLowerCase().indexOf(searchString) > 0).sort((a, b) => this.sortByName(a, b)));
  //   return sortedAccounts;
  // }

  // sortByName(a: AccountSearchModel, b: AccountSearchModel) : number {
	// 	if(a.DisplayName.toLowerCase() < b.DisplayName.toLowerCase()) return -1;
	// 	if(a.DisplayName.toLowerCase() > b.DisplayName.toLowerCase()) return 1;
	// 	return 0;
  // }

  onSelect(account: any) {
    account.PortalID = this.portalID;
    if (!account.ID)
      account.IsNew = true;
    if (this.approvalGroup) {
      account.ApprovalGroupID = this.approvalGroup.ID;
    }
    if (this.searchType === 'Exempt')
      account.IsExempt = true;
    console.log('SELECTED');
    console.log(account);
    this.selectedItems.push({ label: account.DisplayName, value: account });
    this.selectedOption.push(account);
    this.accounts.push(account.AccountID);
    // this.removeById(this.searchAccounts, account);
    this.searchText = "";
    this.onSelectionChange.emit(this.selectedOption);
  }

  removeItem(event: any, item: any) {
    console.log('REMOVED');
    this.removeById(this.selectedItems, item.value);
    let foundIndex = -1;
    for(var i = 0, len = this.selectedOption.length; i < len; i++) {
      if (this.selectedOption[i].AccountID === item.value.AccountID) {
        foundIndex = i;
        break;
      }
    }
    if (foundIndex !== -1)
      this.selectedOption.splice(foundIndex, 1);
    this.accounts.splice(this.accounts.indexOf(item.value.AccountID), 1);
    console.log(item);
    this.onSelectionChange.emit(this.selectedOption);
  }

  removeById(array: any[], item: any) {
    let foundIndex = -1;
    for(var i = 0, len = array.length; i < len; i++) {
      if (array[i].value.ID === item.ID) {
        foundIndex = i;
        break;
      }
    }
    if (foundIndex !== -1)
    array.splice(foundIndex, 1);
  }

  // onBlur(value: string) {
  //   this.showDropdown = false;
  //   this.selectedValue = '';
  // }

  // onFocus() {
  //   this.hasSelection = false;
  // }

  // removeItem(index: number, accountId: number) {
  //   this.selectedOption.splice(index, 1)
  //   let accountIndex = this.accounts.indexOf(accountId);
  //   if (accountIndex != -1) this.accounts.splice(accountIndex, 1);
  // }

  // isIdExist(id: number) {
  //   return this.selectedOption.some(item => {
  //     return item.AccountID == id;
  //   });
  // }

  // onClickField() {
  //   this.inputField.nativeElement.focus();
  // }

  // activateEditMode(status: boolean) {
  //   this.isEditMode = status;
  //   this.backspaceCount = 0;
  //   if (this.isEditMode) {
  //     this.searchEditObj = this.selectedOption[this.selectedOption.length - 1];
  //     this.selectedValue = this.searchEditObj.DisplayName;
  //   } else {
  //     this.searchEditObj = null;
  //     this.selectedValue = '';
  //   }
  // }
}
