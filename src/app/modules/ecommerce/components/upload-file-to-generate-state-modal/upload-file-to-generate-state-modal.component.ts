import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import { CodaService } from "../../../../services/coda.service";
import { AlertService } from "../../../../services/alert.service";

import { ProductTemplateData } from "../../models/product-template-data";
import { TemplatePreset } from '../../models/template-preset';
import { ParsePSDResponse } from "../../models/parse-psd-response";
import { GenerateStateFileResponse } from "../../models/generate-state-file-response";
import { GenerateStateFileRequest } from "../../models/generate-state-file-request";

@Component({
  selector: "upload-file-to-generate-state-modal",
  templateUrl: "./upload-file-to-generate-state-modal.component.html",
  styleUrls: ["./upload-file-to-generate-state-modal.component.less"]
})
export class UploadFileToGenerateStateModalComponent implements OnInit {
  public displayDialog: boolean = false;
  public saving: boolean = false;
  public loading: boolean = true;

  @Input() productId: number;
  @Output() onSave: EventEmitter<any> = new EventEmitter();

  isFileUploading: boolean = false;
  isProcessingTemplate: boolean = false;
  uploadedFile: File;
  fileSize: string = '';
  uploadProgress: number = 0;
  uploadedSize: string = '0 KB';

  parsePSDResponse: ParsePSDResponse;
  generateStateFileResponse: GenerateStateFileResponse;
  hasErrors: boolean = false;
  errorMessage: string = '';


  constructor(private codaService: CodaService, private alertService: AlertService) { }

  ngOnInit() { }

  showDialog() {
    this.displayDialog = true;
    let chooseLabel = `<span style="color:#939393"><b>Drag Template file to Upload or <u style="color:#1b9ad7"> choose a file</u></b>
                  <br> (IDML, PSD)</span>`
    let changeLabel = setInterval(() => {
      let label = document.querySelector('.upload-state .ui-button-text.ui-clickable')
      label.innerHTML = chooseLabel;
      if (changeLabel) {
        clearInterval(changeLabel);
      }
    }, 100)
    this.reset();
  }

  async closeDialog(saveAndClose: boolean) {
    if (saveAndClose) {
      this.import();
      return;
    }
    this.displayDialog = false;
  }

  public onUpload(event) {
  }

  public onProgress(event) {
  }

  public onSelect(event: any) {
    this.isFileUploading = true;

    let files = event.files;
    if (files.length > 0) {
      this.uploadedFile = files[0];
      this.fileSize = (this.uploadedFile.size / 1024).toFixed(2) + ' KB';

      this.codaService.parsePSD(this.uploadedFile).subscribe(
        result => {
          if (typeof result === "number") {
            this.uploadProgress = result;
            if (result === 100) {
              this.isFileUploading = false;
              this.isProcessingTemplate = true;
            }
            else {
              this.uploadedSize = ((this.uploadedFile.size / 1024) * result / 100).toFixed(2) + ' KB';
            }
          } else {
            if ('readyState' in result == false) {
              this.parsePSDResponse = result;
              this.isProcessingTemplate = false;

              if (this.parsePSDResponse.parsedSuccessfully) {
                this.hasErrors = false;
              } else {
                this.errorMessage = 'No valid surface(s) found in template.';
              }
            }
          }
        }, err => {
          console.error(err);

          this.isProcessingTemplate = false;
          this.isFileUploading = false;
          this.hasErrors = true;
          this.errorMessage = err;
        }
      );
      
      // .subscribe(
      //   parsePSDResponse => {
      //     console.log(parsePSDResponse);
  
      //     this.parsePSDResponse = parsePSDResponse;
  
      //     if (parsePSDResponse.parsedSuccessfully) {
      //       alert('File converted to a product successfully. response: ' + JSON.stringify(parsePSDResponse));
      //     } else {
      //       console.error(parsePSDResponse.message);
      //       this.alertService.error(parsePSDResponse.message);
      //     }

      //     this.isFileUploading = false;
      //   },
      //   err => {
      //     console.error(err);
      //     this.alertService.error(err);
      //     this.isFileUploading = false;
      //   }
      // );
    }
  }

  public onError(event) {

  }

  private import() {
    let generateStateFileRequest: GenerateStateFileRequest = {
      productId: this.productId,
      tempPSDFileName: this.parsePSDResponse.tempPSDFileName
    };
    this.saving = true;
    this.codaService
      .generateStateFile(generateStateFileRequest)
      .subscribe(generateStateFileResponse => {
        if (generateStateFileResponse.generatedSuccessfully) {
          this.generateStateFileResponse = generateStateFileResponse;
          this.onSave.emit();
          this.displayDialog = false;
        } else {
          this.hasErrors = true;
          this.errorMessage = generateStateFileResponse.message;
          this.saving = false;
        }
      }, error => {
        this.hasErrors = true;
        this.errorMessage = error;
        this.saving = false;
      }
    );
  }

  private reset() {
    this.isFileUploading = false;
    this.isProcessingTemplate = false;
    this.saving = false;
    this.hasErrors = false;
    this.errorMessage = '';
    this.uploadedFile = null;
  }
}
