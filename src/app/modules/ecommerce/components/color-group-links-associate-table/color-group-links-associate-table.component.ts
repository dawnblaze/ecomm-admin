import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';
import { ThemeService } from '../../../../services/theme.service';

import { Color } from '../../models/color';
import { ColorGroup } from '../../models/color-group';

@Component({
  selector: 'cb-color-group-links-associate-table',
  templateUrl: './color-group-links-associate-table.component.html'
})
export class ColorGroupLinksAssociateTableComponent extends TableComponent implements OnInit, OnChanges {
  @Input() colorGroup: ColorGroup;
  @Output() onExit: EventEmitter<any> = new EventEmitter();

  name: string = "cb-color-group-links-associate-table";
  _rows: Color[];
  filtered: Color[] = [];
  search: string = '';
  edit: boolean = false;
  associatedColors: any = {};
  loading: boolean = true;
  saving: boolean = false;

  constructor(private daimService: DaimService, private router: Router, private themeService: ThemeService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['colorGroup']) {
      this.getAssociatedColors();
    }
  }

  load() {
    this.getAssociatedColors();

    this.daimService.getColors()
      .subscribe(colors => {
        this._rows = colors;
        this.filterRows();

        this.loading = false;
      });
  }

  getAssociatedColors() {
    if (this.colorGroup.id) {
      this.associatedColors = this.colorGroup.colors.reduce((acc, color) => {
        acc[color.id] = true;
        return acc;
      }, {});
    }
  }

  cancel() {
    this.onExit.emit();
  }

  save() {
    this.saving = true;

    this.daimService.addColorLink(this.colorGroup.id, this.associatedColors)
      .subscribe(colors => {
        this.saving = false;
        this.cancel();
      },
      error => {
        this.saving = false;
        this.cancel();
      });
  }

  filterProcess(item: Color): boolean {
    var keys = ["name", "id"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
