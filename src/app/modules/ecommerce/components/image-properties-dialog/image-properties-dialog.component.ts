import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router, Params } from "@angular/router";
import { AlertService } from '../../../../services/alert.service';
import { AuthService } from '../../../../services/auth.service';
import { ZnodeService } from '../../../../services/znode.service';
import { Observable, forkJoin } from 'rxjs';
import { ProductImage } from '../../models/product-image'
import { ProductImageType } from '../../models/product-image-type';
import { debug } from 'util';

@Component({
  selector: 'cb-image-properties-dialog',
  templateUrl: './image-properties-dialog.component.html',
  styleUrls: ['./image-properties-dialog.component.less']
})
export class ImagePropertiesDialogComponent implements OnInit {

  saveText: string = "Save";
  displayDialog: boolean = false;
  loading: boolean = false;

  public productImage: ProductImage;

  // selected product image id
  @Input() productImageId: number;

  constructor(private alertService: AlertService, private znodeService: ZnodeService, private authService: AuthService, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  public load() {
    this.znodeService.getProductImage(this.productImageId).subscribe(
      data => {
        this.productImage = data[0];

      },
      err => {
        this.alertService.error(err);
      }
    );
  }

  public save() {
    this.loading = true;

    this.znodeService.updateProductImage(this.productImage).subscribe(
      image => {
        this.alertService.success('Product Image Updated');
        this.loading = false;
        this.closeDialog()
      },
      err => {
        this.alertService.error(err);
        this.loading = false;
      }
    );

  }

  showDialog(productImage: ProductImage) {
    let user: any = this.authService.getUser();
    this.productImage = productImage;
    this.productImage.UserName = user.UserName;
    this.displayDialog = true;
  }

  closeDialog(isSave = false) {
    if (isSave) {
      this.save();
      return
    }
    this.displayDialog = false;
  }

}
