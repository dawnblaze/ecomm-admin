import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagePropertiesDialogComponent } from './image-properties-dialog.component';

describe('ImagePropertiesDialogComponent', () => {
  let component: ImagePropertiesDialogComponent;
  let fixture: ComponentFixture<ImagePropertiesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagePropertiesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagePropertiesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
