import { Component, OnInit, Input, OnChanges, ViewChild, ElementRef, Output, EventEmitter } from "@angular/core";
import { Observable } from "rxjs";
import { Portal } from "../../models/portal";
import { ZnodeService } from "../../../../services/znode.service";
import { CSS } from "../../models/css";

@Component({
    selector: "store-theme-color-bar",
    templateUrl: "./store-theme-color-bar.component.html",
    styleUrls: ["./store-theme-color-bar.component.less"]
})
export class StoreThemeColorBarComponent implements OnInit, OnChanges {

    public _cssStyle: string = null;
    public _cssElement: string = null;
    public _name: string = "Bob";
    public _styleBarData: any = {name: "", cssStyle: "", cssElementName: ""};
    public css: CSS;


    @Input('name') styleElementName: string;
    @Input('cssStyle') cssStyle: string;
    @Input('cssElement') cssElement: string; 
    @Input('cssClassName') cssClassName: string;
    @Input('readOnly') readOnly: boolean;

    //@ViewChild(this.cssElement) cElement: ElementRef;
    
    @Output('update') updateStyleChange: EventEmitter<any> =  new EventEmitter<any>();
    

    updateStyle(){
        this.updateStyleChange.emit()
    }

    getData(event, element){
        console.log('Triggering inner child: ', event, element)
        //this.cElement.nativeElement.value = event.value;
        if(event.value === undefined){
            this.updateStyleChange.emit({value: event.target.value, element: element, css: this.cssClassName});
        }else{
            this.updateStyleChange.emit({value: event.value, element: element, css: this.cssClassName});
        }
    }

    constructor(private znodeService: ZnodeService){

    }
    
    ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
        //console.log('Changes in color-bar: ',changes);
    }
    
    
    ngOnInit(): void {
        

        //console.log('Name, Element, Style: ',this.styleElementName, this.cssElement,this.cssStyle);
    }


    
}