import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { CSS } from '../../models/css';
import { Alert } from '../../../corebridge/models/alert';

@Component({
  selector: 'cb-styles-table',
  templateUrl: './styles-table.component.html'
})
export class StylesTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-styles-table";
	public _rows: CSS[];
  public filtered: CSS[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;

  constructor( private znodeService: ZnodeService,private router: Router, private alertService: AlertService ) { 
    super();
  }

	load() {
    this.getRows();
  }

  private getRows(){
    this.znodeService.getCssList().subscribe(
      list => {
        this._rows = list;
        this.filterRows();
      },
      err => this.alertService.error(err)
    );
  }

  gotoDetail(id: number) {
    let link = ['/ecommerce/styles', id];
    this.router.navigate(link);
  }

  delete(css: CSS) {
    this.znodeService.deleteCss(css.CSSID).subscribe(
      data => {
        this.alertService.success('CSS Deleted');
        this.getRows();
      },
      err => this.alertService.error(err)
    );
  }

}