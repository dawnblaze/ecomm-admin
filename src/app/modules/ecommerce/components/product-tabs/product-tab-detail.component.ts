import {
  Component,
  Input,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter,
  ViewChild,
  AfterViewInit,
  ElementRef,
  ChangeDetectorRef
} from "@angular/core";
import { NgForm } from "@angular/forms";
import { forkJoin } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";

import { Product } from "../../models/product";
import { ZnodeService } from "../../../../services/znode.service";
import { ShippingRuleType } from "../../../ecommerce/models/shipping-rule-type";
import { TaxClass } from "../../../ecommerce/models/tax-class";

import { AlertService } from "../../../../services/alert.service";
import { DetailedError } from "../../../../modules/corebridge/models/alert";
import { Sku } from "../../models/sku";
import { CoreBridgeProductSettings } from "../../models/corebridge-product-settings";
import { ControlProductCategory } from "../../../cesa/models/control/control-product-category";
import { ControlProduct } from "../../../cesa/models/control/control-product";
import { CesaProductMapping } from "../../../cesa/models/cesa-product-mapping";

import { MisTypes, CesaService } from "../../../../services/cesa.service";
import { CoreBridgeService } from "../../../../services/corebridge.service";
import { untilDestroyed } from "ngx-take-until-destroy";
import { AutoSaveBarState } from "../../../corebridge/components/autosave-bar/autosave-bar.component";

@Component({
  selector: "cb-product-tab-detail",
  templateUrl: "./product-tab-detail.component.html"
})
export class ProductTabDetailComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @Input() title: string;
  @Input() product: Product;
  @Input() sku: Sku;
  @Input() productInfo: any;
  @Input() shippingRuleTypes: ShippingRuleType[];
  @Input() taxClasses: TaxClass[];
  @Input() settings: CoreBridgeProductSettings;
  @Input() isActiveTab: boolean;

  @Output() onSave = new EventEmitter<AutoSaveBarState>();
  @Output() onValidate = new EventEmitter<any>();
  @Output() onViewError = new EventEmitter<void>();
  @Output() onTitleChange = new EventEmitter<string>();

  @ViewChild("detailsForm") ngForm: NgForm;

  public skuInvalid: boolean = false;
  public ipc_checkbox: Boolean = false; // boolean for state of "different From Sku"
  public isAvailabilityChanged: boolean = false;
  public unchanged = {
    product: "",
    sku: "",
    productInfo: "",
    controlProduct: ""
  };

  public MisTypes = MisTypes;
  public misType: MisTypes;
  public detailedErrors: DetailedError[] = [];
  private focusElement: HTMLElement;

  // Corebridge Integration
  public integrationTypes = [
    { IntegrationTypeId: 1, Name: "Basic", Value: "basic" },
    { IntegrationTypeId: 2, Name: "Advanced", Value: "advanced" },
    { IntegrationTypeId: 3, Name: "Reporting", Value: "reporting" }
  ];

  // Cyrious Integration
  public controlCategories: ControlProductCategory[] = [];
  public controlProducts: ControlProduct[] = [];
  public selectedControlProduct: ControlProduct;
  public cesaProductMapping: CesaProductMapping = new CesaProductMapping();
  public controlVariablesToSet: string;

  constructor(
    private znodeService: ZnodeService,
    private alertService: AlertService,
    private cesaService: CesaService,
    private corebridgeService: CoreBridgeService,
    private elementRef: ElementRef,
    private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.unchanged.sku = JSON.stringify(this.sku);
    this.unchanged.product = JSON.stringify(this.product);
    this.unchanged.productInfo = JSON.stringify(this.productInfo);
    this.setMisType();
    this.ngForm.form.markAsTouched();
    this.ngForm.form.valueChanges
      .pipe(debounceTime(2000), distinctUntilChanged(), untilDestroyed(this))
      .subscribe(values => {
        if (this.isActiveTab && this.ngForm.dirty)
          this.onProductChange(values);
      });
  }
  validateAllFormFields() {
    for (let controlsKey in this.ngForm.form.controls) {
      this.ngForm.form.controls[controlsKey].markAsTouched();
    }
  }
  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnChanges() {
    if (this.product && this.product.ProductSetting) {
      this.ipc_checkbox = this.product.ProductSetting.ProductNumDifferent;
    }
  }

  async onProductChange(values: any) {
    // this.validateAllFormFields();
    let changed = this.ngForm.touched || this.ngForm.dirty ? "dirty" : "touched";
    if (this.ngForm.touched || this.ngForm.dirty) {
      let errors: DetailedError[] = [];
      let controls = this.ngForm.controls;
      if (!this.product.Name || !this.product.Name.trim().length) {
        this.addDetailedError(
          errors,
          "Product Title",
          "This field is required."
        );
      } else if (controls.productName[changed]) {
        this.onTitleChange.emit(this.product.Name);
      }
      if (!controls.skuName.valid) {
        if (!values.skuName || !this.sku.Sku.length) {
          this.skuInvalid = true;
          this.addDetailedError(
            errors,
            "SKU or Part #",
            "This field is required."
          );
        }
        else if (!/^[\w\d_.-]+$/.test(this.sku.Sku)) {
          this.skuInvalid = true;
          this.addDetailedError(
            errors,
            "SKU or Part #",
            "Allowed special characters: - . _"
          );
        }
        else {
          this.skuInvalid = false;
        }
      } else {
        this.skuInvalid = false;
      }
      if (values.ipc) {
        if (!values.productNumber || !this.product.ProductNumber.length)
          this.addDetailedError(
            errors,
            "Internal Product Code",
            "This field is required."
          );
        else if (!/^[\w\d_.-]+$/.test(this.product.ProductNumber))
          this.addDetailedError(
            errors,
            "Internal Product Code",
            "Allowed special characters: - . _"
          );
        else if (controls.productNumber[changed]) {
          this.znodeService
            .getProducts()
            .pipe(untilDestroyed(this))
            .subscribe(products => {
              if (products.find(p => p.ProductNumber == values.productNumber))
                this.addDetailedError(
                  errors,
                  "Internal Product Code",
                  "Duplicate value exists."
                );
            });
          controls.productNumber.markAsPristine();
        }
      }
      if (!controls.minSelectableQuantity.valid || !this.product.MinSelectableQuantity) {
        this.addDetailedError(
          errors,
          "Min. Selectable Quantity",
          "This field is required. Minimum allowed value is 1."
        );
      }
      if (!controls.maxSelectableQuantity.valid || !this.product.MaxSelectableQuantity) {
        this.addDetailedError(
          errors,
          "Max Selectable Quantity",
          "This field is required. Minimum allowed value is 1."
        );
      }
      if (!controls.quantityIncrement.valid || !this.product.QuantityIncrement) {
        this.addDetailedError(
          errors,
          "Quantity Increment",
          "This field is required. Minimum allowed value is 1."
        );
      }
      if (values.trackInventory) {
        if (!controls.quantityOnHand.valid || !this.isNumber(this.product.QuantityOnHand)) {
          this.addDetailedError(
            errors,
            "Quantity On Hand",
            "This field is required."
          );
        }

        if (!controls.outOfStockMessage.valid ||
          (!this.product.OutOfStockMessage || !this.product.OutOfStockMessage.trim().length)
        ) {
          this.addDetailedError(
            errors,
            "Out of Stock Message",
            "This field is required."
          );
        }

        if (this.product.AllowBackOrder &&
          (!this.product.BackOrderMessage ||
            !this.product.BackOrderMessage.trim().length)
        ) {
          this.addDetailedError(
            errors,
            "Back Order Message",
            "This field is required."
          );
        }
      }
      if (!this.isNumber(this.product.RetailPrice)) {
        this.addDetailedError(
          errors,
          "Retail Price",
          "This field is required."
        );
      }

      if (this.product.SeoUrl == null || this.product.SeoUrl == "") {
        this.addDetailedError(
          errors,
          "URL Handle",
          "This field is required.",
          "information",
          "Information"
        );
      } else {
        let seoExistsResponse = await this.znodeService.isSeoUrlExist(this.product.ProductId, this.product.SeoUrl).toPromise();
        if (seoExistsResponse.IsExist) {
          this.addDetailedError(
            errors,
            "URL Handle",
            "URL Handle is in use by another Product, Category, or Content Page. Please enter another value.",
            "information",
            "Information"
          );
        }
      }

      let skuFound = await this.znodeService.findSkuProfileBySku(values.skuName).toPromise()
      if (skuFound && skuFound.SkuAttriutes.length && skuFound.SkuAttriutes[0].SkuId != this.product.SkuId) {
        this.skuInvalid = true;
        this.addDetailedError(
          errors,
          "SKU or Part #",
          values.skuName + " Already exists"
        );
      }
      else {
        this.skuInvalid = false;
      }

      this.onValidate.emit({ tab: "general", errors: errors });
      if (errors.length == 0) this.update();
    }
  }

  update() {
    if (this.product.ProductId && this.ngForm.form.valid) {
      this.focusElement = this.elementRef.nativeElement.ownerDocument.activeElement;
      if (
        this.misType == MisTypes.CoreBridge &&
        JSON.stringify(this.productInfo) != this.unchanged.productInfo
      ) {
        this.saveCoreBridgeSettings();
      } else {
        this.saveChanges();
      }
    }
  }

  saveCoreBridgeSettings() {
    this.isSaving(true);
    this.settings.ZnodeProductId = this.product.ProductId;
    this.corebridgeService
      .createOrUpdateProductSettings(this.settings)
      .pipe(untilDestroyed(this))
      .subscribe(
        res => {
          this.unchanged.productInfo = JSON.stringify(this.productInfo);
          this.product.ExternalId = res.Id;
          this.saveChanges();
        },
        err => {
          this.alertService.error(err);
          this.saveChanges();
        }
      );
  }

  saveChanges() {
    let update = [];
    if (JSON.stringify(this.sku) != this.unchanged.sku) {
      this.product.SkuName = this.sku.Sku;
      if (this.sku.SkuId) {
        update.push(this.znodeService.updateSku(this.sku));
      }
      else {
        this.sku.ProductId = this.product.ProductId;
        if (this.product.QuantityOnHand) {
          if (!this.sku.Inventory)
            this.sku.Inventory = {};
          this.sku.Inventory.QuantityOnHand = this.product.QuantityOnHand;
        }
        update.push(this.znodeService.createSku(this.sku));
      }
    }
    if (JSON.stringify(this.product) != this.unchanged.product) {
      update.push(this.znodeService.updateProduct(this.product));
    }
    if (this.misType == MisTypes.Cyrious) {
      let selected = JSON.stringify(this.selectedControlProduct);
      if (selected != this.unchanged.controlProduct) {
        if (selected == JSON.stringify(this.controlProducts[0])) {
          update.push(
            this.cesaService.deleteProductMapping(this.product.ProductId)
          );
        } else {
          this.cesaProductMapping.eCommID = this.product.ProductId;
          this.cesaProductMapping.atomID = this.selectedControlProduct.productId;
          this.cesaProductMapping.atomName = this.selectedControlProduct.productName;
          update.push(
            this.cesaService.setProductMapping(this.cesaProductMapping)
          );
        }
      }
    }
    if (update.length > 0) {
      this.isSaving(true);
      forkJoin(update)
        .pipe(untilDestroyed(this))
        .subscribe(
          res => {
            this.unchanged.sku = JSON.stringify(this.sku);
            this.unchanged.product = JSON.stringify(this.product);
            if (this.misType == MisTypes.Cyrious)
              this.unchanged.controlProduct = JSON.stringify(
                this.selectedControlProduct
              );
            this.ngForm.form.markAsPristine();
            this.isSaving(false);
          },
          err => this.isSaving(false, err)
        );
    } else {
      this.isSaving(false);
    }
  }

  isSaving(flag: boolean, error?: string) {
    if (flag) {
      this.ngForm.form.disable();
      this.onSave.emit(AutoSaveBarState.saving);
    } else {
      this.ngForm.form.enable();
      this.focusElement.focus();
      this.onSave.emit(error ? AutoSaveBarState.invalid : AutoSaveBarState.saved);
    }
    if (error) this.alertService.error(error);
  }

  setMisType() {
    this.misType = this.cesaService.getMisType() || MisTypes.CoreBridge;

    if (this.misType == MisTypes.Cyrious) {
      this.controlProducts = [
        { productName: "Default", productId: 0, isActive: false, categoryId: 0 }
      ];
      forkJoin(
        this.cesaService.getControlProductCategories(),
        this.cesaService.getControlProducts(),
        this.cesaService.getProductMapping(this.product.ProductId)
      )
        .pipe(untilDestroyed(this))
        .subscribe(
          data => {
            let [categories, products, map] = data;
            this.controlCategories = categories;
            this.controlProducts = this.controlProducts.concat(products);
            this.cesaProductMapping = map || new CesaProductMapping();
            if (map) {
              this.selectedControlProduct = products.find(
                x => x.productId == this.cesaProductMapping.atomID
              );
              if (!this.selectedControlProduct)
                this.selectedControlProduct = this.controlProducts[0];
              this.unchanged.controlProduct = JSON.stringify(
                this.selectControlProduct
              );
            }
          },
          err => {
            this.alertService.error(err);
          }
        );
    }
  }

  addDetailedError(errors: DetailedError[], field: string, message: string, tabSlug?: string, tabTitle?: string) {
    let detailedError: DetailedError = {
      ID: this.product.ProductId,
      Name: "",
      FieldName: field,
      ErrorMessage: message
    };
    if (detailedError.ID) {
      detailedError.TabSlug = tabSlug ? tabSlug : "general";
      detailedError.TabTitle = tabTitle ? tabTitle : "Product Details";
      detailedError.goToAction = () => {
        this.onViewError.emit();
      };
    }
    errors.push(detailedError);
  }

  onBlur(event: any) {
    if (!this.product.ProductId) this.onProductChange(this.ngForm.form.value);
  }

  skuChanged() {
    this.product.SkuName = this.sku.Sku;
  }

  isNumber(e) {
    return !(e == null || isNaN(e) || e.toString() == "");
  }

  /**
   * @method toggleUseInternalCode
   * @param e target.value to check against
   *
   */

  public toggleUseInternalCode(checked: boolean) {
    this.ipc_checkbox = checked;
    this.product.ProductSetting.ProductNumDifferent = checked;
    if (!checked) {
      // force values so no error will be detected.
      this.product.ProductNumber = this.product.SkuName;
    }
  }

  selectIntegrationType(type) {
    this.ngForm.form.markAsDirty();
    this.productInfo.cb_integration_category = type.Value;
    switch (this.productInfo.cb_integration_category) {
      case "basic":
        this.product.CoreBridgeQuickProductCategoryId = null;
        this.product.CoreBridgeQuickProductCategoryName = "";
        this.product.CoreBridgeQuickProductId = null;
        this.product.CoreBridgeQuickProductName = "";
        this.product.CoreBridgeProductCategoryId = null;
        this.product.CoreBridgeProductCategoryName = "";
        this.product.CoreBridgeIncomeAccountId = null;
        this.product.CoreBridgeIncomeAccountName = "";
        this.settings.IntegrationTypeId = 1;
        break;
      case "advanced":
        this.product.CoreBridgeProductCategoryId = null;
        this.product.CoreBridgeProductCategoryName = "";
        this.product.CoreBridgeIncomeAccountId = null;
        this.product.CoreBridgeIncomeAccountName = "";
        this.settings.IntegrationTypeId = 2;
        this.settings.CoreBridgeQuickProductCategoryId = null;
        this.settings.CoreBridgeQuickProductId = null;
        if (this.product.CoreBridgeQuickProductCategoryId > 0)
          this.settings.CoreBridgeQuickProductCategoryId = this.product.CoreBridgeQuickProductCategoryId;
        if (this.product.CoreBridgeQuickProductId > 0)
          this.settings.CoreBridgeQuickProductId = this.product.CoreBridgeQuickProductId;
        break;
      case "reporting":
        this.product.CoreBridgeQuickProductCategoryId = null;
        this.product.CoreBridgeQuickProductCategoryName = "";
        this.product.CoreBridgeQuickProductId = null;
        this.product.CoreBridgeQuickProductName = "";
        this.settings.IntegrationTypeId = 3;
        this.settings.CoreBridgeProductCategoryId = null;
        this.settings.CoreBridgeIncomeAccountId = null;
        if (this.product.CoreBridgeProductCategoryId > 0)
          this.settings.CoreBridgeProductCategoryId = this.product.CoreBridgeProductCategoryId;
        if (this.product.CoreBridgeIncomeAccountId > 0)
          this.settings.CoreBridgeIncomeAccountId = this.product.CoreBridgeIncomeAccountId;
        break;
    }
  }

  selectControlProduct(controlProduct: ControlProduct) {
    this.ngForm.form.markAsDirty();
    this.selectedControlProduct = controlProduct;
  }

  selectTaxClass(tax) {
    this.ngForm.form.markAsDirty();
    this.product.TaxClassId = tax.TaxClassId;
  }

  selectShippingRule(type) {
    this.ngForm.form.markAsDirty();
    this.product.ShippingRuleTypeId = type.ShippingRuleTypeId;
    this.update();
  }

  selectOutOfStockOption(allowBackOrder: boolean) {
    this.product.AllowBackOrder = allowBackOrder;
    this.product.BackOrderMessage = allowBackOrder ? "On Backorder" : "";
    this.update();
  }

  ngOnDestroy() {
    this.alertService.detailedError("general", []);
  }
}
