import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';

import { TableComponent } from "../../../corebridge/components/table/table.component";
import { RowAction } from "../../../corebridge/components/table2/table.component";
import { EditorPreset } from '../../models/editor-preset';

import { CodaService } from '../../../../services/coda.service';
import { AlertService } from "../../../../services/alert.service";
import { ModalService } from '../../../../services/modal.service';

@Component({
  selector: 'cb-editor-presets-table',
  templateUrl: './editor-presets-table.component.html'
})
export class EditorPresetsTableComponent extends TableComponent
  implements OnInit, OnDestroy {

  public _rows: EditorPreset[];
  public filtered: EditorPreset[] = [];
  public search: string = "";
  public searchKey: string = "name";
  public total = 0;
  public RowAction = RowAction;
  public _columns = [
    { field: "name", header: "Name" }
  ];

  constructor(private codaService: CodaService, private router: Router, private alertService: AlertService, private modalService: ModalService) {
    super();
  }

  ngOnInit() {
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.codaService.getEditorPresets(1, 20).pipe(untilDestroyed(this)).subscribe(editorPresetPaged => {
      this._rows = editorPresetPaged.records;
      this.total = editorPresetPaged.totalResults;
      this.filterRows();
    }, err => {
      this.loaded();
      this.alertService.error(err);
    });
  }

  onCountChange(count: number) {
    this.count = count;
    this.page = 1;
    this.getRows();
    sessionStorage.setItem(this.name + "-count", this.count + "");
    sessionStorage.setItem(this.name + "-page", this.page + "");
  }

  goToDetail(editorPresetId: number) {
    this.router.navigate(['/ecommerce/settings/design-editor-presets/', editorPresetId]);
  }

  delete(editorPresetId: number) {
    if(this._rows.find(preset => preset.id == editorPresetId).name.toUpperCase() == 'STANDARD') {
      this.modalService.warningNoCancel('Cannot Delete Editor Preset','Deletion of Standard Editor Preset is not allowed.','OK')
      return;
    }
    var title = "Please confirm you would like to delete the selected Editor Preset. This change cannot be undone.";
    var content = "Deletion will permanently remove the Editor Preset.";
    this.modalService.warning(title, content);

    let subscription = this.modalService.response.pipe(untilDestroyed(this)).subscribe(res => {
      if (res) {
        this.codaService.deleteEditorPreset(editorPresetId).pipe(untilDestroyed(this)).subscribe(response => {
          if (response) {
            this.getRows();
            this.alertService.success('Editor Preset Deleted Successfully');
          } else this.alertService.error('Could not delete Editor Preset');
        });
      }
      subscription.unsubscribe();
    }, err => this.alertService.error(err)
    )
  }

  clone(editorPresetId: number) {
    let preset = this._rows.find(e => e.id == editorPresetId);
    if(preset) {
      let copy = new EditorPreset();
      Object.assign(copy, preset);
      copy.name = "Copy of " + copy.name;
      copy.isDeleted = false;
      copy.id = 0;
      this.codaService.getEditorPresets(1, this.total).pipe(untilDestroyed(this)).subscribe(response => {
        let copies = response.records.filter(item =>
          item.name.startsWith(copy.name) && item.name.substr(copy.name.length - 1).match(/\s\d+$/));
        if(copies.length > 0) {
          let n = copies.map(item => parseInt(item.name.split(' ').pop())).sort();
          copy.name += ' ' + (++n[n.length-1]);
          this.create(copy);
        } else {
          if(response.records.find(item => item.name == copy.name))
            copy.name += ' 1';
          this.create(copy);
        }
      }, err => this.create(copy));

    }
  }
  create(newEditorPreset: EditorPreset) {
    this.codaService.createEditorPreset(newEditorPreset).pipe(untilDestroyed(this)).subscribe(response => {
      this.getRows();
      this.alertService.success('Editor Preset Cloned Successfully');
    }, err => this.alertService.error(err));
  }

  ngOnDestroy() {

  }
}
