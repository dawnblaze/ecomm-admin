import { Component, OnInit, EventEmitter, ElementRef, Input, Output } from '@angular/core';
import { Http } from '@angular/http';

import { DaimService } from '../../../../services/daim.service';
import { AlertService } from '../../../../services/alert.service';

import { Asset } from '../../models/asset';

@Component({
  selector: 'cb-font-upload',
  templateUrl: './font-upload.component.html'
})
export class FontUploadComponent implements OnInit {
  loading: boolean = false;
  @Output() fontChange = new EventEmitter<File>();

  constructor(private daimService: DaimService, private alertService: AlertService) {}

  ngOnInit () {
  }

  onFileChange(input) {
    let file = input.target.files[0];
    this.fontChange.emit(file);
  }
}
