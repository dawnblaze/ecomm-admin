import { Component, Input, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Review } from '../../models/review';

@Component({
  selector: 'cb-reviews-table',
  templateUrl: './reviews-table.component.html'
})
export class ReviewsTableComponent extends TableComponent implements OnInit {

  public _rows: Review[];
  public reviews: Review[];
  public filtered: Review[] = [];
  public search: string = "";
  public searchKey: string = "ProductName";
  private searchFilters: any[] = [];
  public total = 0;

  constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService ) { 
    super();
  }

  load() {
    this.getRows();
  }

  private getRows() {
    this.znodeService.getReviews().subscribe(reviews => {
      this._rows = reviews;
      this.reviews = this._rows;
      this.filterRows();
      this.onFilterChange(this.searchFilters);
    });
  }

  public gotoDetail(id: number) {
    let link = ['/ecommerce/reviews/', id];
    this.router.navigate(link);
  }

  toggleActivation(review: Review) {
    review.Status = (review.Status == 'A')? 'N': 'A';
    review.ProductName = null;
    review.AccountId = null;

    this.znodeService.updateReview(review).subscribe(
      res => {
        this.alertService.success('Product Updated');
        this.getRows();
      },
      err => this.alertService.error(err)
    );
  }

  public delete(reviewId: number) {
    this.znodeService.deleteReview(reviewId).subscribe(
      res => {
        this.getRows();
        this.alertService.success('Review Deleted');
      },
      err => {
        this.alertService.error(err);
      }
    )
  }

  onFilterChange(selection: any[]) {
    this.searchFilters = selection;
    sessionStorage.setItem(this.name + "-searchFilters", JSON.stringify(this.searchFilters));

    this._rows = this.reviews.filter( review => {

      for(var obj of this.searchFilters){
      
        var found: boolean = false;
      
        for(var reviewId of obj.ReviewIds){
          if(reviewId == review.ReviewId)
            found = true;
        }
      
        if(!found)
          return false;
      
      }
    
      return true;
    });

    this.filterRows();
  }

  public filterProcess(item: Review): boolean {
    var keys = ["ReviewId", "ProductName", "Comments", "CreateUser", "UserLocation", "Rating", "CreateDate", "Status"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
