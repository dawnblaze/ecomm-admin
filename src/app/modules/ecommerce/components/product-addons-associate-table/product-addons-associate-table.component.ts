import { Component, Input, Output, EventEmitter, OnInit, ViewChild, OnDestroy } from '@angular/core';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Addon } from '../../models/addon';

import { ProductAddonsTableComponent } from '../product-addons-table/product-addons-table.component'

import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'cb-product-addons-associate-table',
  templateUrl: './product-addons-associate-table.component.html'
})
export class ProductAddonsAssociateTableComponent extends TableComponent implements OnInit, OnDestroy {

  public name: string = "cb-product-addons-associate-table";
  public _rows: Addon[];
  public filtered: Addon[] = [];
  public productAddons: Addon[] = [];
  public associated: any = {};

  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  public displayDialog = false;
  public toExit: boolean;
  public toUpdateAddonsList: boolean;

  @Input() productId: number;
  @Input() isAssociateList: boolean;

  @Output() onExit: EventEmitter<any> = new EventEmitter();
  @Output() onAdd: EventEmitter<any> = new EventEmitter();

  @ViewChild('productAddonsTable') productAddonsTable: ProductAddonsTableComponent;

  constructor( private znodeService: ZnodeService, private alertService: AlertService ) {
    super();
  }

  show() {
    this.displayDialog = true;
  }

  cancel(){
    this.onExit.emit();
  }

  onSave() {
    this.toUpdateAddonsList = true;
    this.save();
  }

  onCloseDialog(status: boolean) {
    if (status) {
      this.toExit = true;
      this.save();
    }
    else if (this.toUpdateAddonsList) {
      this.onExit.emit(true)
      this.toUpdateAddonsList = false;
    }
    else {
      this.productAddonsTable.addOnsTable.selected = []
      this.productAddonsTable.getRows();
    }
    this.displayDialog = false;
  }


  public save(){
    this.znodeService.getProductAddons(this.productId).pipe(untilDestroyed(this)).subscribe(
      addons => {

        var selected: number[] = [];
        var add: number[] = [];
        var remove: number[] = [];
        // for(var addonId in this.associated){
        //   if(this.associated[addonId]){
        //     selected.push(Number(addonId));

        //     var AddOnId: number = Number(addonId);

        //     var result = addons.find(addon => AddOnId == addon.AddOnId);
        //     if(undefined == result){
        //       // selected, but not in existing addon list
        //       // needs to be added
        //       add.push(AddOnId);
        //     }

        //   }
        // }
        selected = this.productAddonsTable.addOnsTable.selected.map(item => item.AddOnId);

        add = selected;




        // for(var addon of addons) {
        //   console.log('addons', addon)
        //   var AddOnId = addon.AddOnId;
        //   var id = selected.find(addon => addon == AddOnId);

        //   if(undefined == id){
        //     // existing product was not selected
        //     // need to remove
        //     remove.push(addon.AddOnId);
        //   }


        // }

        if(add.length > 0 || remove.length > 0){
          this.saveAssociations(add, remove);
        }
        else {
          this.alertService.success('Product AddOns Updated');
          this.cancel();
        }

      },
      err => {
        this.alertService.error(err)
      }
    );

  }

  private saveAssociations(add: number[], remove: number[]) {
    var batch: Observable<any>[] = [];

    var productAddOns: Addon[] = [];

    for (var id of add){
      var res = this.productAddonsTable.addOnsTable.selected.find( addon => addon.AddOnId == id);
      if(res){
        var addon = res;
        addon.ProductId = this.productId;

        productAddOns.push(addon);
      }
    }

    if(productAddOns.length > 0){
      batch.push(this.znodeService.createProductAddons(productAddOns));
    }

    // for(var id of remove){
    //   var res = this.productAddons.filter(productAddOn => productAddOn.AddOnId == id);
    //   if(res.length > 0){
    //     batch.push(this.znodeService.deleteProductAddon(res[0].ProductAddOnId));
    //   }
    // }

    forkJoin(batch).pipe(untilDestroyed(this)).subscribe(
      data => {
        this.alertService.success('Product Addons Updated');
        this.cancel();
        this.productAddonsTable.addOnsTable.selected = []
        if (this.toExit) {
          this.productAddonsTable.getRows();
          this.displayDialog = false;
          this.toExit = false;
          this.onExit.emit(true);
        } else if (this.toUpdateAddonsList) {
          this.productAddonsTable.getRows();
        }
      },
      err => this.alertService.error(err)
    );

  }

  ngOnDestroy() {}

}
