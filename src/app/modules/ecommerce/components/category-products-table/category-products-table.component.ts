import { Component, Input, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Router, RouterLink } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { Column } from "../../../corebridge/components/table2/table.component";
import { TableComponent } from '../../../corebridge/components/table/table.component';
import { RowAction, Table2Component } from "../../../corebridge/components/table2/table.component";
import { CategoryProductsAssociateTableComponent } from "../category-products-associate-table";

import { RecordAction } from '../../../corebridge/models/action';
import { ZnodeTableComponent } from '../../../corebridge/components/table/znode-table/znode-table.component';
import { ZnodeApiService, ZnodeRequest, ZnodeResponse } from '../../../../services/znode-api.service';
import { AlertService } from '../../../../services/alert.service';
import { ThemeService } from '../../../../services/theme.service';
import { ModalService } from '../../../../services/modal.service';

import { Product } from '../../models/product';
import { ProductCategory } from '../../models/product-category';
import { ZnodeService } from "../../../../services/znode.service";

@Component({
  selector: 'cb-category-products-table',
  templateUrl: './category-products-table.component.html'
})
export class CategoryProductsTableComponent extends TableComponent implements OnDestroy {

  public name: string = "cb-category-products-table";
  public state: string = 'list';
  public inactiveMessage: string = 'All associated products in this category are inactive.';

  public rowActions = [RowAction.Activate, RowAction.Delete];
  public _rows: Product[] = [];
  public dataLoaded = false;
  public filtered: Product[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  private productCategories: ProductCategory[];

  public _columns: Array<Column> = [
    { field: 'Thumbnail', header: 'Thumbnail' },
    { field: 'Name', header: 'Name' },
    { field: 'RetailPrice', header: 'Retail', class: 'text-center' },
    { field: 'SalePrice', header: 'Sale', class: 'text-center' },
    { field: 'WholesalePrice', header: 'Wholesale', class: 'text-center' },
    { field: 'QuantityOnHand', header: 'On Hand', class: 'text-center' },
    { field: 'DisplayOrder', header: 'Order', class: 'text-center' },
    { field: 'SkuId', header: 'SKU', class: 'text-center' }

  ];
  public recordActions = [
    new RecordAction('Set Active', 'fal fa-toggle-on', '', () => this.groupActivation(true), 0, false, true, this.canActivateGroup),
    new RecordAction('Set Inactive', 'fal fa-toggle-off', '', () => this.groupActivation(false), 0, false, true, this.canDeactivateGroup),
    new RecordAction('Delete', 'fal fa-trash-alt', '', () => this.groupDelete())
  ];

  @ViewChild('productsTable') productsTable: Table2Component;
  @ViewChild('categoryProductAssociateModal') categoryProductAssociateModal: CategoryProductsAssociateTableComponent;

  @Input() category: number = 0;

  constructor(private znodeApiService: ZnodeApiService, private znodeService: ZnodeService, private router: Router, private alertService: AlertService, private themeService: ThemeService, private modalService: ModalService) {
    super();
  }

  load() {
    this.getRows(true);
  }

  getRows(refresh: boolean = false) {
    let request = new ZnodeRequest();
    request.cache = refresh;
    this.dataLoaded = false;
    forkJoin(
      this.znodeService.getProductCategories(0, this.category),
      this.znodeApiService.getProductsByCategory(this.category, request)
    ).pipe(untilDestroyed(this)).subscribe(res => {
      this.productCategories = res[0];
      res[0].forEach(data => {
        let product = res[1].data.find(p => p.ProductId == data.ProductId);
        data.Product['Thumbnail'] = (product ? product.ArtworkThumbnailUri : data.Product.ArtworkThumbnailUri) || data.Product.ImageSmallPath;
        data.Product.IsActive = data.IsActive;
      });
      this._rows = res[0].map(data => data.Product);

      this.dataLoaded = true;
      this.filterRows();
      this.loaded();
    });
  }

  public delete(id: number) {
    var title = "Please confirm you would like to delete the selected Associated Product. This change cannot be undone.";
    var content = "Deletion will permanently remove the Associated Product.";
    this.modalService.warning(title, content);

    this.modalService.response.pipe(take(1)).subscribe(res => {
      if (!res) return;
      let productCategory = this.productCategories.find(p => p.ProductId == id);
      if(productCategory) {
        this.znodeService.deleteCategoryProduct(productCategory.ProductCategoryId).pipe(untilDestroyed(this)).subscribe(
          res => {
            this.getRows(true);
          },
          err => this.alertService.error(err)
        );
      }
    })
  }

  private associate() {
    this.state = 'list';
    this.getRows();
  }

  toggleActivation(product: Product) {
    let productCategory = this.productCategories.find(p => p.ProductId == product.ProductId);
    if(productCategory) {
      productCategory.IsActive = !productCategory.IsActive;
      this.znodeService.updateCategoryProduct(productCategory).pipe(untilDestroyed(this)).subscribe(
        res => {
          this.getRows(true);
        },
        err => this.alertService.error(err)
      );
    }
  }

  refreshActions() {
    if (this.productsTable.selected.length) {
      this.recordActions.forEach(a => { a.Refresh(this.productsTable.selected) });
    }
  }
  canActivateGroup(selected: any[]): boolean {
    return selected.filter(p => !p.IsActive).length > 0;
  }
  canDeactivateGroup(selected: any[]): boolean {
    return selected.filter(p => p.IsActive).length > 0;
  }

  groupActivation(isActive: boolean) {
    let error = null;
    let selected = this.productsTable.selected;
    if (selected.length > 0) {
      let count = selected.length;
      this.dataLoaded = false;
      selected.forEach(row => {
        let productCategory = this.productCategories.find(p => p.ProductId == row.ProductId);
        if(productCategory) {
          productCategory.IsActive = isActive;
          this.znodeService.updateCategoryProduct(productCategory).pipe(untilDestroyed(this)).subscribe(
            res => {
              if (--count == 0) {
                this.getRows(true);
                if (error)
                  this.alertService.error(error);
              }
            },
            err => {
              error = err;
              if (--count == 0) {
                this.getRows(true);
                this.alertService.error(error);
              }
            }
          );
        }
      });
    }
  }

  groupDelete() {
    let error = null;
    let selected = this.productsTable.selected;
    if (selected.length > 0) {
      let count = selected.length;
      var title = "Please confirm you would like to delete the selected Associated Products. This change cannot be undone.";
      var content = "Deletion will permanently remove the Associated Products.";
      this.modalService.warning(title, content);

      this.modalService.response.pipe(take(1)).subscribe(res => {
        if (!res) return;
        selected.forEach(row => {
          let productCategory = this.productCategories.find(p => p.ProductId == row.ProductId);
          if(productCategory)  {
            this.znodeService.deleteCategoryProduct(productCategory.ProductCategoryId).pipe(untilDestroyed(this)).subscribe(
              res => {
                if (--count == 0) {
                  this.getRows(true);
                  if (error)
                    this.alertService.error(error);
                }
              },
              err => {
                error = err;
                if (--count == 0) {
                  this.getRows(true);
                  this.alertService.error(error);
                }
              }
            );
          }
        });
      });
    }
  }


  ngOnDestroy() { }
}
