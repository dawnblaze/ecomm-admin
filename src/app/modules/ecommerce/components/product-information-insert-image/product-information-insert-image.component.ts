import { Component, Input, OnInit, AfterViewInit, Output, EventEmitter, OnDestroy } from "@angular/core";
import { untilDestroyed } from "ngx-take-until-destroy";

import { ProductAsset } from '../../models/product-asset';
import { ZnodeService } from "../../../../services/znode.service";

@Component({
  selector: 'cb-product-information-insert-image-modal',
  templateUrl: './product-information-insert-image.component.html',
  styleUrls: ['./product-information-insert-image.component.less']
})

export class ProductInformationInsertImageComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() productId: number;
  @Output() onUploadDone: EventEmitter<any> = new EventEmitter<any>();

  public displayDialog: boolean = false;
  public uploadedFiles: ProductAsset[] = [];
  public contentStyle = {
    "margin-left": "0px",
    "padding-right": "0px"
  }

  public uploadingFiles: ProductAsset[] = [];
  public isFileUploading: boolean = false;
  public cancel: boolean = false;
  public isSubmitValid:boolean = false;
  // public znodeService: ZnodeService;
  constructor(public znodeService: ZnodeService){
    this.znodeService = znodeService;
  }

  ngOnInit() {
  }

  ngAfterViewInit() {

  }

  ngOnDestroy () {

  }

  close(saveAndClose: boolean) {
    if(saveAndClose) {
      this.onUploadDone.emit(this.uploadedFiles);
    } else if (this.uploadedFiles.length) {
      this.cancel = true;
      this.uploadedFiles.forEach(file => {
        this.znodeService.deleteProductAsset(file.ProductAssetId).pipe(untilDestroyed(this)).subscribe();
      });
    }
    this.displayDialog = false;
  }

  public open() {
    this.displayDialog = true;
    setTimeout(() => {
      let a = document.querySelector('.ui-button-text.ui-clickable')
      a.innerHTML = "<b>Drag Files to Upload or <span class='choose'>choose files</span></b>"
    }, 100)

    this.isFileUploading = false;
    this.cancel = false;
    this.uploadedFiles = [];
    this.uploadingFiles = [];
  }

  // Once file is completed uploading
  public onUpload(event){
    // for(let file of event.files) {
    //   this.uploadedFiles.push(file);
    // }

    console.log('Files Uploaded: ', this.uploadedFiles);

  }

  // during progress of uploaded file
  public onProgress(event){

  }

  public onSelect(event: any){
    let files = event.files;

    if (files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file: File = files[i];

        // Add an empty ProductAsset object to array
        let productAsset = new ProductAsset();
        productAsset["File"] = file;
        productAsset.ProductFile = file.name;
        productAsset.IsUploading = true;
        productAsset.UploadProgress = 0;
        productAsset.FileSize = (file.size / 1024).toFixed(2) + ' KB';
        productAsset["UploadedSize"] = '0 KB';
        this.uploadingFiles.push(productAsset);
      }

      let uploadFiles = this.uploadingFiles.filter(file => file.UploadProgress == 0);
      for (let i = 0; i < uploadFiles.length && !this.cancel; i++) {
        let productAsset = uploadFiles[i];
        this.znodeService.createProductAsset(this.productId, productAsset["File"])
        .subscribe(
          asset => {
            let index = uploadFiles.indexOf(productAsset);
            if (typeof asset === "number") {
              uploadFiles[index]["UploadProgress"] = asset;
              if (asset === 100) uploadFiles[index]["UploadMessage"] = "Saving";
              else {
                let file = uploadFiles[index]["File"];
                uploadFiles[index]["UploadedSize"] = ((file.size / 1024) * asset / 100).toFixed(2) + ' KB';
              }
            } else {
              if ('readyState' in asset) {
                // If returned value is an XMLHttpRequest
                uploadFiles[index]["XmlHttpRequest"] = asset;
              } else {
                uploadFiles[index]["IsUploading"] = false;
                uploadFiles[index]["UploadProgress"] = 100;
                if(this.cancel) {
                  this.znodeService.deleteProductAsset(asset.ProductAssetId).pipe(untilDestroyed(this)).subscribe();
                } else {
                  asset.UploadProgress = 100;
                  this.uploadedFiles.push(asset);
                  this.uploadingFiles.splice(this.uploadingFiles.indexOf(productAsset), 1);
                }
              }
            }
            this.isFileUploading = this.uploadingFiles.length > this.uploadedFiles.length;
          }
        );
      }
    }
  }

  public onError(event){

  }

  public onRemove(event){

  }

  public upLoadHandler(event){

  }

}
