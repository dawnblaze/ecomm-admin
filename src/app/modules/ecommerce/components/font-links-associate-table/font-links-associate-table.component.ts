import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';
import { ThemeService } from '../../../../services/theme.service';

import { Font } from '../../models/font';
import { FontGroup } from '../../models/font-group';

@Component({
  selector: 'cb-font-links-associate-table',
  templateUrl: './font-links-associate-table.component.html'
})
export class FontLinksAssociateTableComponent extends TableComponent implements OnInit, OnChanges {
  @Input() font: Font;
  @Output() onExit: EventEmitter<any> = new EventEmitter();

  name: string = "cb-font-links-associate-table";
  _rows: FontGroup[];
  filtered: FontGroup[] = [];
  search: string = '';
  edit: boolean = false;
  associatedGroups: any = {};
  loading: boolean = true;
  saving: boolean = false;

  constructor(private daimService: DaimService, private router: Router, private themeService: ThemeService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['font']) {
      this.getAssociatedGroups();
    }
  }

  load() {
    this.getAssociatedGroups();

    this.daimService.getFontGroups()
      .subscribe(fontGroups => {
        this._rows = fontGroups;
        if (this._rows)
          this.filterRows();

        this.loading = false;
      });
  }

  getAssociatedGroups() {
    if (this.font.id) {
        this.associatedGroups = this.font.groups.reduce((acc, group) => {
          acc[group.id] = true;
          return acc;
        }, {});
    }
  }

  cancel() {
    this.onExit.emit();
  }

  save() {
    this.saving = true;
    let obs: Observable<any>[] = [];

    for(let groupID in this.associatedGroups) {
      if (this.associatedGroups[groupID]) {
        obs.push(this.daimService.addFontLink(this.font.fontFamilyName, +groupID));
      }
      else {
        obs.push(this.daimService.deleteFontLink(this.font.fontFamilyName, +groupID));
      }
    }

    forkJoin(...obs)
      .subscribe(res => {
        this.saving = false;
        this.cancel();
      },
      error => {
        this.saving = false;
        this.cancel();
      });
  }

  filterProcess(item: FontGroup): boolean {
    var keys = ["displayName", "id"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
