import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';

import { FontGroup } from '../../models/font-group';


@Component({
  selector: 'cb-font-groups-table',
  templateUrl: './font-groups-table.component.html'
})
export class FontGroupsTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-font-groups-table";
  public _rows: FontGroup[];
  public filtered: FontGroup[] = [];
  public search: string = '';

  constructor(private daimService: DaimService, private router: Router) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.daimService.getFontGroups().subscribe(fontGroups => {
      this._rows = fontGroups;
      this.filterRows();
    });
  }

  gotoDetail(fontGroup: FontGroup) {
    this.router.navigate(['ecommerce/settings/font-groups', fontGroup.id]);
  }

  delete(fontGroup: FontGroup) {
    this.daimService.deleteFontGroup(fontGroup.id).subscribe(res => {
      this.getRows();
    });
  }

  filterProcess(item: FontGroup): boolean {
    var keys = ["id", "name"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
