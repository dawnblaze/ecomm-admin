import { ModalService } from './../../../../services/modal.service';
import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { Router } from "@angular/router";

import { forkJoin } from "rxjs";

import { ZnodeService } from "../../../../services/znode.service";
import { AlertService } from "../../../../services/alert.service";

import { CategoryProfile } from "../../models/category-profile";
import { Profile } from "../../models/profile";
import { Column } from "../../../corebridge/components/table2/table.component";
import { isUndefined } from "ngx-bootstrap/chronos/utils/type-checks";
import { Portal } from '../../models/portal';
import { PortalProfile } from '../../models/portal-profile';
@Component({
  selector: "cb-category-profile-list",
  templateUrl: "./category-profile-list.component.html",
  styleUrls: ["../portal-countries-table/portal-countries-table.component.less"]
})
export class CategoryProfileListComponent implements OnInit {
  @Input() category: number;

  public categoryProfiles: CategoryProfile[] = [];
  public categoryProfileIDs: Number[] = [];
  public profiles: Profile[] = [];
  public filteredProfiles: Profile[] = [];
  public newProfile: Profile;
  public newEffectiveDate: string;
  public loading: boolean = true;
  public isAddMode: boolean = false;
  public unchanged: string;
  public portalProfiles: PortalProfile[] = [];
  public filteredPortalProfiles: PortalProfile[] = [];

  @Output() isChanged = new EventEmitter<boolean>();

  constructor(
    private znodeService: ZnodeService,
    private router: Router,
    private alertService: AlertService,
    private modalService: ModalService
  ) { }

  ngOnInit() {
  }

  public initCategory(catId: number) {
    this.category = catId;
    this.getCategoryProfiles();
  }

  private getCategoryProfiles() {
    let portals: Portal[];
    this.portalProfiles = [];
    this.filteredPortalProfiles = [];
    this.categoryProfiles = [{
      CategoryProfileID: 0,
      CategoryID: 0,
      ProfileID: 0,
      EffectiveDate: '',
      ProfileName: ''
    }];
    if (this.category) {
      this.loading = true;
      forkJoin(
        this.znodeService.getCategoryProfiles(this.category),
        this.znodeService.getPortals(),
        this.znodeService.getPortalProfiles()
      ).subscribe(results => {
        this.categoryProfiles = this.categoryProfiles.concat(results[0]);
        this.fixDates();

        portals = results[1];
        let portalProfiles = results[2];
        portalProfiles.forEach(item => {
          let found = portals.find(portal => portal.PortalId == item.PortalID);
          if (found) item.Name = found.StoreName
          this.portalProfiles.push(item)
        })

        this.filterPortalProfile()
        this.unchanged = JSON.stringify(this.categoryProfiles)
        this.loading = false;
      })
    }
  }

  public add() {
    var node = new CategoryProfile();
    node.CategoryID = this.category;
    node.ProfileID = (this.newProfile as any).ProfileID;
    node.ProfileName = this.newProfile.Name;
    node.EffectiveDate = this.newEffectiveDate;
    this.categoryProfiles.push(node);

    this.filterPortalProfile();
    this.isAddMode = false;
  }

  public remove(categoryProfile: CategoryProfile, index: number) {
    if (categoryProfile.CategoryProfileID > 0) {
      var title = "Please confirm you would like to delete the selected Collection Available Date. This change cannot be undone.";
      var content = "Deletion will permanently remove the Collection Available Date.";
      this.modalService.warning(title, content).subscribe(res => {
        if (res) {
          this.znodeService
            .deleteCategoryProfile(categoryProfile.CategoryProfileID)
            .subscribe(
              res => this.getCategoryProfiles(),
              err => this.alertService.error(err)
            );
        }
      });
    } else {
      this.categoryProfiles.splice(index, 1);
      this.filterPortalProfile();
      this.changeList();
    }
  }

  public save(): boolean {
    var success: boolean = true;

    this.categoryProfiles.slice(1).forEach(categoryProfile => {
      var date: Date = new Date(categoryProfile.EffectiveDate);
      categoryProfile.EffectiveDate = date.toISOString();

      this.znodeService.updateCategoryProfile(categoryProfile).subscribe(
        res => console.log(res),
        err => {
          success = false;
          this.alertService.error(err);
        }
      );
    });

    return success;
  }

  public fixDates() {
    this.categoryProfiles.slice(1).forEach(categoryProfile => {
      var date: Date = new Date(categoryProfile.EffectiveDate);
      categoryProfile.EffectiveDate = this.getDateString(date);
    });
  }

  getDateString(date: Date): string {
    var month: string = String(date.getMonth() + 1);
    if (month.length < 2) month = "0" + month;

    var day: string = String(date.getDate());
    if (day.length < 2) day = "0" + day;

    var year: string = String(date.getFullYear());

    return month + "/" + day + "/" + year;
  }

  onClickAdd() {
    this.isAddMode = true;
    this.newProfile = null;
    this.newEffectiveDate = null;
  }
  onInputAdd() {
    let div = document.getElementsByClassName('ui-dialog-content')[0];
    div.scrollTop = div.scrollHeight;
  }
  onSelectProfile(profile: Profile) {
    this.newProfile = profile;
    this.isChanged.emit(true);
    if (this.newEffectiveDate) {
      this.add();
    }
  }

  onSelectDate(date: any) {
    this.newEffectiveDate = this.getDateString(new Date(date));
    this.isChanged.emit(true);
    if (this.newProfile) {
      this.add();
    }
  }

  changeList() {
    let status = (this.unchanged != JSON.stringify(this.categoryProfiles)) ? true : false;
    this.isChanged.emit(status)
  }

  filterPortalProfile() {
    this.filteredPortalProfiles = this.portalProfiles.filter(portalProfile => {
      return !this.categoryProfiles.find(categoryProfile => categoryProfile.ProfileID == portalProfile.ProfileID)
    })
  }
}
