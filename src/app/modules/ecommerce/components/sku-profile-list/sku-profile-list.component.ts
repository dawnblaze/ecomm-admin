import { Component, Input, OnInit, Output, EventEmitter, OnDestroy } from "@angular/core";

import { Observable, forkJoin } from "rxjs";
import { take } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

import { ZnodeService } from "../../../../services/znode.service";
import { AlertService } from "../../../../services/alert.service";
import { ModalService } from "../../../../services/modal.service";

import { Profile } from "../../models/profile";
import { SkuProfile } from "../../models/sku-profile";
import { PortalProfile } from "../../models/portal-profile";
import { Portal } from "../../models/portal";

// TODO-JUDYLL: Might need to refer to this implementation
// for EC-1462.
@Component({
  selector: "cb-sku-profile-list",
  templateUrl: "./sku-profile-list.component.html",
  styleUrls: ["./sku-profile-list.component.less"]
})
export class SkuProfileListComponent implements OnInit, OnDestroy {
  @Input()
  public set skuId(value: number) {
    this._skuId = value;
    this.load();
  }
  public get skuId() : number {
    return this._skuId;
  }
  private _skuId: number;

  @Output() isChanged = new EventEmitter<boolean>();
  @Output() isSaving = new EventEmitter<boolean>();

  public skuProfiles: SkuProfile[] = [];
  public newProfile: PortalProfile;
  public newEffectiveDate: string;
  public portalProfiles: PortalProfile[] = [];
  public filteredPortalProfiles: PortalProfile[] = [];
  public emptyProfile: SkuProfile = {SkuProfileEffectiveID: 0, SkuId: 0, ProfileId: 0, Name: '', EffectiveDate: ''};
  public isAddMode: boolean = false;
  public unchanged: string;

  public loading: boolean = true;

  constructor(
    private znodeService: ZnodeService,
    private alertService: AlertService,
    private modalService: ModalService
  ) {}

  ngOnInit() {
  }

  private load() {
    this.loading = true;
    let portals: Portal[];
    this.portalProfiles = [];
    this.filteredPortalProfiles = [];
    forkJoin(
      this.znodeService.getSkuProfiles(this.skuId),
      this.znodeService.getPortals(),
      this.znodeService.getPortalProfiles()
    ).pipe(untilDestroyed(this)).subscribe(
      data => {
        this.skuProfiles = data[0];
        this.skuProfiles.forEach(skuProfile => {
          skuProfile.EffectiveDate = this.fixDateString(skuProfile.EffectiveDate);
        });
        this.unchanged = JSON.stringify(this.skuProfiles);

        portals = data[1];
        let pProfile = data[2];
        pProfile.forEach(item => {
          let found = portals.find(portal => portal.PortalId == item.PortalID);
          if (found) item.Name = found.StoreName;
          this.portalProfiles.push(item);
          let skuProfile = this.skuProfiles.find(p => p.ProfileId == item.ProfileID);
          if(skuProfile)
            skuProfile['PortalProfile'] = item;
        });

        this.filterPortalProfile();
        this.loading = false;
      },
      err => {
        console.error(err);
        this.alertService.error(err);
      }
    );
  }

  public add() {
    if (this.skuProfiles.findIndex(p => p.ProfileId == this.newProfile.ProfileID) !== -1) {
      return;
    }

    if (this.skuProfiles.length < this.portalProfiles.length) {
      var node = new SkuProfile();
      node.SkuId = this.skuId;
      node.ProfileId = this.newProfile.ProfileID;
      node.Name = this.newProfile.Name;
      node.EffectiveDate = this.newEffectiveDate;
      this.skuProfiles.push(node);
      this.save(node);
      this.isAddMode = false;
    }
  }

  public remove(index: number) {
    let skuProfile: SkuProfile = this.skuProfiles[index];

    if (skuProfile.SkuProfileEffectiveID > 0) {
      var title = "Please confirm you would like to delete the selected Availability Restriction. This change cannot be undone.";
      var content = "Deletion will permanently remove the Availability Restriction.";
      this.modalService.warning(title, content).pipe(take(1)).subscribe(
        response => {
          if (response) {
            this.znodeService
              .deleteSkuProfile(skuProfile.SkuProfileEffectiveID)
              .pipe(untilDestroyed(this)).subscribe(
                res => this.load(),
                err => this.alertService.error(err)
              );
          }
        },
        err => this.alertService.error(err)
      );
    } else {
      this.skuProfiles.splice(index, 1);
    }
  }

  public save(data: SkuProfile) {
    this.loading = true;
    this.isSaving.emit(true);
    this.znodeService.updateSkuProfile(data).pipe(untilDestroyed(this)).subscribe(
      res => {
        this.isChanged.emit(false);
        this.isSaving.emit(false);
        this.load();
      },
      err => {
        this.loading = false;
        this.isSaving.emit(false);
        this.alertService.error(err);
      }
    );
  }

  fixDateString(value: any): string {
    var date: Date = new Date(value);
    var month: string = String(date.getMonth() + 1);
    if (month.length < 2) month = "0" + month;

    var day: string = String(date.getDate());
    if (day.length < 2) day = "0" + day;

    var year: string = String(date.getFullYear());
    return month + "/" + day + "/" + year;
  }

  filterPortalProfile() {
    this.filteredPortalProfiles = this.portalProfiles.filter(portalProfile => {
      return !this.skuProfiles.find(
        skuProfile => skuProfile.ProfileId == portalProfile.ProfileID
      );
    }).sort((a,b) => a.Name.localeCompare(b.Name));
  }

  onClickAdd() {
    this.isAddMode = true;
    this.newProfile = null;
    this.newEffectiveDate = null;
  }
  onSelectProfile(portalProfile: PortalProfile, skuProfile: SkuProfile) {
    if(!skuProfile) {
      this.newProfile = portalProfile;
      this.isChanged.emit(true);
      if (this.newEffectiveDate) {
        this.add();
      }
    }
    else if(skuProfile.ProfileId != portalProfile.ProfileID) {
      skuProfile.ProfileId = portalProfile.ProfileID;
      this.save(skuProfile);
    }
  }
  onSelectDate(date: any, skuProfile: SkuProfile) {
    let effectiveDate = this.fixDateString(date);
    if(!skuProfile) {
      this.newEffectiveDate = effectiveDate;
      this.isChanged.emit(true);
      if (this.newProfile) {
        this.add();
      }
    }
    else if(skuProfile.EffectiveDate != effectiveDate) {
      skuProfile.EffectiveDate = effectiveDate;
      this.save(skuProfile);
    }
  }

  ngOnDestroy() {}
}
