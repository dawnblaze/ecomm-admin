import { Component, Input, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, RouterLink } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';
import { untilDestroyed } from "ngx-take-until-destroy";

import { TableComponent } from '../../../corebridge/components/table/table.component';
import { RowAction, Table2Component } from "../../../corebridge/components/table2/table.component";

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { Bundle } from '../../models/bundle';
import { Product } from '../../models/product';
import { RecordAction } from '../../../corebridge/models/action';
import { ZnodeApiService } from "../../../../services/znode-api.service";
import { ZnodeTableComponent } from "../../../corebridge/components/table/znode-table/znode-table.component";
import { ThemeService } from "../../../../services/theme.service";

@Component({
  selector: 'cb-bundle-table',
  templateUrl: './bundle-table.component.html'
})
export class BundleTableComponent extends ZnodeTableComponent implements OnInit, OnDestroy {

  @Input() product: Product;
  @Input() isAssociateList: boolean;

  @ViewChild(Table2Component) dataTable: Table2Component;

  public name: string = "cb-bundle-table";
  public rows: Product[];
  public filtered: Product[] = [];
  public search: string = "";
  public searchKeys = [ 'ProductId', 'Name', 'RetailPrice', 'SalesPrice', 'WholesalePrice' ];
  public searchKey: string = "ProductName";
  public total = 0;
  public RowAction = RowAction;
  public _columns = [
    { field: 'Thumbnail', header: 'Thumbnail'},
    { field: "Name", header: "Name" },
    { field: "RetailPrice", header: "Retail", currency: "USD" },
    { field: "SalePrice", header: "Sale", currency: "USD" },
    { field: "WholesalePrice", header: "Wholesale", currency: "USD" },
    { field: "QuantityOnHand", header: "On Hand", align: "right" },
    { field: "SkuName", header: "SKU", align: "center" }
  ];
  public recordActions = [
    new RecordAction('Delete', 'fal fa-trash-alt', '', () => this.groupDelete())
  ];

  private imageRoot: string;
  public state: string = 'list';

  constructor( private znodeApiService: ZnodeApiService, private znodeService: ZnodeService, private alertService: AlertService, private modalService: ModalService  ) {
    super();
  }

  load() {
    console.log(this.product);
    console.log(this.product.ProductId);
    this.imageRoot = this.znodeApiService.base;
    this.getRows();
  }

  getRows() {
    let getData: Observable<any>;
    this.dataLoaded = false;
    if(!this.isAssociateList) {
      this.request.cache = true;
      getData = this.znodeApiService.getProductsBundled(this.product.ProductId, this.request);
    } else {
      getData = this.znodeApiService.getProductsNotBundled(this.product.ProductId, this.request);
    }
    getData.pipe(untilDestroyed(this)).subscribe(resp => {
      this.rows = resp.data;
      this.rows.forEach(data => { data['Thumbnail'] = data.ArtworkThumbnailUri || data.ImageSmallPath });
      this.response = resp;
      this.dataLoaded = true;
      this.loaded();
    }, err => this.alertService.error(err));
  }

  delete(childProductId: number){
    var title =  "Please confirm you would like to delete the selected Product Bundle. This change cannot be undone.";
    var content = "Deletion will permanently remove the Product Bundle.";

    this.modalService.warning(title, content);

    let subscription = this.modalService.response.subscribe(
      confirm => {
        if(confirm) {
          this.znodeService.deleteProductBundle(this.product.ProductId, childProductId).pipe(untilDestroyed(this)).subscribe(
            data => this.getRows(),
            err => this.alertService.error(err)
          );
        }
        subscription.unsubscribe();
      },
      err => this.alertService.error(err)
    );
  }

  groupDelete(){
    var title =  "Please confirm you would like to delete the selected Product Bundles. This change cannot be undone.";
    var content = "Deletion will permanently remove the Product Bundles.";
    let selected = this.dataTable.selected;
    this.modalService.warning(title, content);

    let subscription = this.modalService.response.subscribe(
      confirm => {
        if(confirm) {
          let batch: Observable<boolean>[] = [];
          selected.forEach(childProduct => {
            batch.push(this.znodeService.deleteProductBundle(this.product.ProductId, childProduct.ProductId));
          });
          forkJoin(batch).pipe(untilDestroyed(this)).subscribe(
            data => this.getRows(),
            err => this.alertService.error(err)
          );
        }
        subscription.unsubscribe();
      },
      err => this.alertService.error(err)
    );
  }

  associated() {
    this.getRows();
    this.state = 'list';
  }

  ngOnDestroy() {}
}
