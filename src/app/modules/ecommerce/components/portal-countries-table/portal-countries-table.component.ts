import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TableComponent } from '../../../corebridge/components/table/table.component';
// import { RowAction } from '../../../corebridge/components/table2/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { PortalCountry } from '../../models/portal-country';
import { switchMap } from 'rxjs/operators';

import { ContentPage } from '../../models/content-page';
import { Portal } from '../../models/portal';

export enum RowAction { MakeDefault, Remove }

@Component({
    selector: 'cb-portal-country-table',
    templateUrl: './portal-countries-table.component.html',
    styleUrls: ['./portal-countries-table.component.less']
})
export class PortalCountriesComponent extends TableComponent implements OnInit {
    public name = 'cb-portal-country-table';
    public cols: Array<Object> = [{ field: "CountryName", header: "Country" }, { field: "IsShippingActive", header: "Shipping" }, { field: "IsBillingActive", header: "Billing" }];
    public rowActions = [RowAction.MakeDefault, RowAction.Remove];

    public _rows: any[];
    public filtered: PortalCountry[] = [];
    public search: string = '';
    public searchKey: string = 'CountryName';
    public total = 0;
    public isAddMode = false;
    public rowAction = RowAction;
    public filteredCountries = []

    @Input() portal: Portal;
    @Output() onCreate: EventEmitter<any> = new EventEmitter();
    @Output() onView: EventEmitter<number> = new EventEmitter();

    constructor( private znodeService: ZnodeService, private alertService: AlertService ) {
        super();
    }

    load() {
        this.getRows();
    }

    getRows(){
        this.znodeService.getPortalCountriesByPortalId(this.portal.PortalId).subscribe(
            portalCountries => {
                this._rows = portalCountries;
            }
        );
    }

    toggleBillingAddress(portalCountry: PortalCountry){
        this.znodeService.getPortalCountry(portalCountry.PortalCountryId).pipe(
            switchMap(
                currentPortalCountry => {
                    currentPortalCountry.IsBillingActive = !currentPortalCountry.IsBillingActive;
                    return this.znodeService.updatePortalCountry(currentPortalCountry);
                }
            )
        ).subscribe(
            result => {
                this.alertService.success('Country Updated');
                this.getRows();
            }
        );
    }

    toggleShippingAddress(portalCountry: PortalCountry){
        this.znodeService.getPortalCountry(portalCountry.PortalCountryId).pipe(
            switchMap(
                currentPortalCountry => {
                    currentPortalCountry.IsShippingActive = !currentPortalCountry.IsShippingActive;
                    return this.znodeService.updatePortalCountry(currentPortalCountry);
                }
            )
        ).subscribe(
            result => {
                this.alertService.success('Country Updated');
                this.getRows();
            }
        );
    }

    makeDefaultCountry(rowData) {
        let index = this._rows.indexOf(rowData)
        if (index > -1) {
            this._rows.forEach(item => item.isDefault = false);
            this._rows[index].isDefault = true
        }
    }

    removeCountry(rowData) {
        let index = this._rows.indexOf(rowData)
        if (index > -1) {
            this._rows.splice(index, 1);
            this.filteredDropdownList()
        }
    }

    onEnableAdd() {
        this.isAddMode = true;
        this.filteredDropdownList();
    }

    filteredDropdownList() {
        let countries = [
            'United States of America',
            'Canada',
            'United Kingdom',
            'Australia'
        ]
        this.filteredCountries = countries.filter((country) => {
            return !this._rows.find((item: any) => {
                if (typeof item == 'object') {
                    return country.toLowerCase().includes(item.CountryName.toLowerCase());
                }
            })
        })
    }

    addCountry(country) {
        this._rows.push({CountryName: country});
        this.filteredDropdownList();
        this.isAddMode = false
    }
}
