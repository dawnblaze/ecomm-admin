import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';
import { AlertService } from '../../../../services/alert.service';
import { ThemeService } from '../../../../services/theme.service';

import { Font } from '../../models/font';

@Component({
  selector: 'cb-fonts-table',
  templateUrl: './fonts-table.component.html'
})
export class FontsTableComponent extends TableComponent implements OnInit {
  @Input() isSystem: boolean;
  @ViewChild('fileUpload') fileUpload: ElementRef;

  name: string = "cb-fonts-table";
  _rows: Font[];
  filtered: Font[] = [];
  search: string = '';
  loading: boolean = true;

  constructor(private daimService: DaimService, private router: Router, private alertService: AlertService, public themeService: ThemeService) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.daimService.getFonts().subscribe(fonts => {
      if (this.isSystem) {
        this._rows = fonts.filter((font) => {
          return font.isSystem;
        });
      }
      else {
        this._rows = fonts.filter((font) => {
          return !font.isSystem;
        });
      }

      this.filterRows();

      this.loading = false;
    });
  }

  gotoDetail(font: Font) {
    this.router.navigate(['/ecommerce/settings/fonts', font.fontFamilyName]);
  }

  delete(fontFamilyName: string) {
    this.daimService.deleteFont(fontFamilyName).subscribe(res => {
      this.getRows();
    });
  }


  filterProcess(item: Font): boolean {
    var keys = ["displayName", "id"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }

  onUploadClick() {
    let click = new MouseEvent('click');
    this.fileUpload.nativeElement.dispatchEvent(click);
  }

  onFileChange(input) {
    let file = input.target.files[0];
    this.daimService.uploadFont(file)
    .subscribe(res => {
      this.getRows();
      this.alertService.success('Font Uploaded');
    },
    error => {
      this.alertService.error(error);
    });
  }
}
