import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  Input,
  AfterViewInit,
  Output,
  EventEmitter
} from "@angular/core";

import { NgForm } from "@angular/forms";
import { DomSanitizer } from "@angular/platform-browser";
import { AlertService } from "../../../../services/alert.service";
import { CesaService, MisTypes } from "../../../../services/cesa.service";
import { CoreBridgeService } from "../../../../services/corebridge.service";
import { ZnodeService } from "../../../../services/znode.service";
import { DomainService } from "../../../../services/domain.service";
import { ModalService } from "../../../../services/modal.service";

import { Observable, forkJoin } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { untilDestroyed } from "ngx-take-until-destroy";
import { DesignerType } from "../../models/designer-type";

import { ArtworkModule } from "../../artwork.module";
import { ProductArtwork } from "../../models/product-artwork";
import { Product } from "../../models/product";
import { ProductImage } from "../../models/product-image";

import { CustomerCanvasIntegrationModalComponent } from "../../components/customer-canvas-integration-modal/customer-canvas-integration-modal.component";
import { ProductSetting } from "../../models/product-setting";
import { ConfigSetting } from "../../models/config-setting";

@Component({
  selector: "cb-product-image-custom",
  templateUrl: "./product-image-custom.component.html",
  styleUrls: ["./product-image-custom.component.less"]
})
export class ProductImageCustomComponent
  implements AfterViewInit, OnInit, OnDestroy {
  product: Product;

  @Output() onSave: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onError: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild("customArtForm") ngForm: NgForm;
  @ViewChild(CustomerCanvasIntegrationModalComponent)
  ccIntegrationModal: CustomerCanvasIntegrationModalComponent;

  OnlyECODLEnabled: boolean = false;
  OnlyCODAEnabled: boolean = false;
  ECODLandCODAEnabled: boolean = false;

  activeDesignerTypeOfTheProduct: DesignerType;

  designerTabs: any[] = [
    {
      text: "Custom Art (Legacy)",
      value: DesignerType.ECODL,
      isActiveDesignerOfProduct: false
    },
    {
      text: "Custom Art",
      value: DesignerType.CUSTOMERCANVAS,
      isActiveDesignerOfProduct: false
    }
  ];
  activeDesignerTab: DesignerType;
  artwork: ArtworkModule;
  image: ProductImage

  constructor(
    private coreBridgeService: CoreBridgeService,
    private znodeService: ZnodeService,
    private domainService: DomainService,
    private cesaService: CesaService,
    private alertService: AlertService,
    private modalService: ModalService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() { }

  onProductLoad(product: Product) {
    this.product = product;
    this.setCustomizationMode();
  }

  ngAfterViewInit() {
    this.artwork = new ArtworkModule(
      this.sanitizer,
      this.alertService,
      this.domainService,
      this.coreBridgeService,
      this.modalService,
      this.znodeService,
      this.cesaService
    );
    this.ngForm.form.valueChanges
      .pipe(debounceTime(2000), untilDestroyed(this))
      .subscribe(values => {
        if (!this.ngForm.pristine)
          this.save();
      });
  }

  /**
   * Sets the customization mode, veryfing if only is CODA enabled, only ECODL is enabled or both are enabled
   */
  setCustomizationMode() {
    let callsToSetCustomization: Observable<any>[] = [];
    callsToSetCustomization.push(this.znodeService.getConfigSettingByBID(this.domainService.bid));
    callsToSetCustomization.push(this.znodeService.getProductArtwork(this.product.ProductId));

    let mysType = this.cesaService.getMisType();
    if (mysType == MisTypes.CoreBridge) {
      callsToSetCustomization.push(this.coreBridgeService.getZnodeOnlineDesignerEnabled());
    }

    forkJoin(callsToSetCustomization)
      .pipe(untilDestroyed(this))
      .subscribe(
        data => {
          let configSettings: ConfigSetting = data[0];
          let productArtworksResult: ProductArtwork[] = data[1];

          let codaIsEnabled = configSettings.EnableCustomersCanvas;
          let ecodlIsEnabled = false;

          if (mysType == MisTypes.Cyrious) {
            ecodlIsEnabled = this.cesaService.getECodEnabled();
          } else if (mysType == MisTypes.CoreBridge) {
            ecodlIsEnabled = data[2];
          }

          if (ecodlIsEnabled && codaIsEnabled) {
            this.ECODLandCODAEnabled = true;
          } else if (ecodlIsEnabled) {
            this.OnlyECODLEnabled = true;
          } else if (codaIsEnabled) {
            this.OnlyCODAEnabled = true;
          }

          this.setActiveDesignerTypeOnLoadProduct(this.product.ProductSetting);

          let productArtwork: ProductArtwork = productArtworksResult[0];
          this.artwork.OnProductLoad(this.product, productArtwork);

          this.onSave.emit(false);
        },
        err => this.onError.emit(false)
      );
  }

  /**
   * Set the property activeDesignerTypeOfTheProduct with the productSetting from ZNodeAPI
   * @param productSetting 
   */
  setActiveDesignerTypeOnLoadProduct(productSetting: ProductSetting) {
    if (productSetting.DesignerType == undefined || productSetting.DesignerType == 0) {
      this.activeDesignerTypeOfTheProduct = DesignerType.ECODL;
    } else if (productSetting.DesignerType == 1) {
      this.activeDesignerTypeOfTheProduct = DesignerType.CUSTOMERCANVAS;
    }

    this.setActiveDesignerTab(this.activeDesignerTypeOfTheProduct);
  }

  /**
   * Set the Active Designer Type of the product on the database
   * @param designerType 
   */
  setActiveDesignerType(designerType: DesignerType) {
    this.activeDesignerTypeOfTheProduct = designerType;
    this.product.ProductSetting.DesignerType = designerType;
    this.save();

    this.setActiveDesignerTab(designerType);
  }

  /**
   * Set the active tab based on the active designer type of the product, this will change the text informing which one is inactive
   * @param designerType active designer of the product 
   */
  setActiveDesignerTab(designerType: DesignerType) {
    this.activeDesignerTab = designerType;

    this.designerTabs.forEach(tab => {
      if (tab.value == designerType) {
        tab.isActiveDesignerOfProduct = true;
      } else {
        tab.isActiveDesignerOfProduct = false;
      }
    });
  }

  setImage(productImage: ProductImage) {
    this.image = productImage;
  }

  setIfProductIsCustomizable(value: boolean | null) {
    this.product.ProductSetting.IsCustom = value;
    this.ngForm.form.markAsDirty();
  }

  save() {
    this.znodeService
      .updateProductSettings(this.product)
      .pipe(untilDestroyed(this))
      .subscribe(
        res => {
          console.log(res);
          this.artwork.OnProductSave(
            this.product,
            this.product.ProductSetting.IsCustom
          );

          this.onSave.emit(false);
          this.ngForm.form.markAsPristine();
        },
        err => {
          err => this.onError.emit(err)
        }
      );
  }

  ngOnDestroy() {
    if (this.artwork != null) this.artwork.Dispose();
  }
}
