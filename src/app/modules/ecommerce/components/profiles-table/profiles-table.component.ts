import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { Portal } from '../../models/portal';
import { Profile } from '../../models/profile';

@Component({
  selector: 'cb-profiles-table',
  templateUrl: './profiles-table.component.html'
})
export class ProfilesTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-profiles-table";
  public _rows: Profile[];
  public filtered: Profile[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;

  constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService, private modalService: ModalService ) {
    super();
  }

  load() {
    this.getRows();
  }

  public goToDetails(id: number) {
    this.router.navigate(['ecommerce/profiles', id]);
  }

  private getRows() {
    this.znodeService.getProfiles().subscribe(profiles => {
      this._rows = profiles;
      this.filterRows();
    });
  }


  public delete(id: number){

    this.znodeService.getPortals().subscribe(
      stores => {
        var matches = stores.filter(store => store.PortalId == id);

        if(matches.length > 0) {
          this.alertService.error('Cannot Delete Profile. Currently Attached to ' + matches[0].StoreName + '.');
        }
        else {
          var title =  "Please confirm you would like to delete the selected profile. This change cannot be undone.";
          var content = "Deletion will remove the profile from any associated storefronts.";

          this.modalService.warning(title, content);

          let subscription = this.modalService.response.subscribe(
            res => {

              if(res){

                this.znodeService.getPortalProfilesByProfile(id).subscribe(
                  res => {
                    console.log(res);
                    for (var portalProfile of res) {
                      this.znodeService.deletePortalProfile(portalProfile.PortalProfileID).subscribe(
                        res => console.log(res),
                        err => this.alertService.error(err)
                      );
                    }

                    this.znodeService.deleteProfile(id).subscribe(
                      res => {
                        console.log(res);
                        this.getRows();
                        this.alertService.success('Profile Removed');
                      },
                      err => this.alertService.error(err)
                    );

                  },
                  err => this.alertService.error(err)
                );
              }
              subscription.unsubscribe();
            },
            err => this.alertService.error(err)
          );

        }

      },
      err => {
        console.error(err);
        this.alertService.error(err);
      }
    );

  }


  public updatePricing(row: Profile){
    row.ShowPricing = !row.ShowPricing;
    this.znodeService.updateProfile(row).subscribe(
      res => {
        this.alertService.success('Profile Updated');
        this.getRows();
      },
      err => this.alertService.error(err)
    );
  }

  public updateWholesalePricing(row: Profile){
    row.UseWholesalePricing = !row.UseWholesalePricing;
    this.znodeService.updateProfile(row).subscribe(
      res => {
        this.alertService.success('Profile Updated');
        this.getRows();
      },
      err => this.alertService.error(err)
    );
  }

  public updateTaxExempt(row: Profile){
    row.TaxExempt = !row.TaxExempt;
    this.znodeService.updateProfile(row).subscribe(
      res => {
        this.alertService.success('Profile Updated');
        this.getRows();
      },
      err => this.alertService.error(err)
    );
  }

  public updatePartnerSignup(row: Profile){
    row.ShowOnPartnerSignup = !row.ShowOnPartnerSignup;
    this.znodeService.updateProfile(row).subscribe(
      res => {
        this.alertService.success('Profile Updated');
        this.getRows();
      },
      err => this.alertService.error(err)
    );
  }

  public copy(row: Profile) {
    this.znodeService.getProfile(row.ProfileId)
    .subscribe(profile => {
      delete profile.ProfileId;
      profile.Name = 'Copy of ' + profile.Name;

      this.znodeService.createProfile(profile).subscribe(
        res => {
          this.alertService.success('Profile Copied Successfully');
          this.getRows();
        },
        err => {
          this.alertService.error(err);
        });
    });
  }
}
