import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';

import { Font } from '../../models/font';
import { FontGroup } from '../../models/font-group';

@Component({
  selector: 'cb-font-group-links-table',
  templateUrl: './font-group-links-table.component.html'
})
export class FontGroupLinksTableComponent extends TableComponent implements OnInit, OnChanges {
  @Input() fontGroup: FontGroup;

  public name: string = "cb-font-group-links-table";
  public _rows: Font[];
  public filtered: Font[] = [];
  public search: string = '';
  public edit: boolean = false;

  constructor(private daimService: DaimService, private router: Router) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['fontGroup']) {
      this.getRows();
    }
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.edit = false;

    if (this.fontGroup.id) {
      this.daimService.getFontGroup(this.fontGroup.id, true)
        .subscribe(fontGroup => {
          this.fontGroup = fontGroup;
          this._rows = this.fontGroup.fonts;
          this.filterRows();
        });
    }
  }

  gotoDetail(font: Font) {
    this.router.navigate(['/ecommerce/settings/fonts', font.fontFamilyName]);
  }

  filterProcess(item: Font): boolean {
    var keys = ["displayName", "id"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
