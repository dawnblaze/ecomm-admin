import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Order } from '../../models/order';
import { OrderLineItem } from '../../models/order-line-item';
import { Portal } from '../../models/portal';
import { OrdersPaged } from '../../models/orders-paged';

@Component({
  selector: 'cb-orders-table',
  templateUrl: './orders-table.component.html'
})
export class OrdersTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-orders-table";
  public _rows: Order[];
  public filtered: Order[] = [];
  public search: string = "";
  public searchKey: string = "OrderId";
  public total = 0;
  public index = 0;
  public submit: string;
  public searchSuggestions: any[] = [ { id:-1, text: 'Storefronts', children: [ ] } ];
  public searchFilters: any[] = [];
  public dataLoaded = false;
  public portalIds: number[] = [];

  public orderStates: { [id: number]: string; } = { };
  public paymentTypes: { [id: number]: string; } = { };
  public storefronts: { [id: number]: string; } = {};

  public downloadOrderId: number;

  public  PaymentStatus  = {
    0	 :	'Pre-Authorized Credit Card',
    1	 :	'Authorized Credit Card',
    2	 :	'Credit Card was Declined',
    3	 :	'Refund Applied to Credit Card',
    4	 :	'Credit Card Payment was Voided',
    5	 : 'Credit Card Payment was Pending',
    10 : 'Purchase Order Payment Pending',
    11 : 'Purchase Order Payment Received',
    20 : 'COD Payment Pending',
    21 : 'COD Payment Received'
  };

  constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService ) {
    super();
  }

  load() {
    this.buildDictionaries();

  }

  buildDictionaries() {

    this.znodeService.getOrderStates().subscribe(
      res => {
        for(let state of res){
          this.orderStates[state.OrderStateID] = state.Description;
        }
      }
    );

    this.znodeService.getPaymentTypes().subscribe(
      res => {
        for(let type of res){
          this.paymentTypes[type.PaymentTypeId] = type.Name;
        }
      }
    );

    this.znodeService.getPortals().subscribe(
      res => {
        for(let store of res){
          this.storefronts[store.PortalId] = store.StoreName;
          this.searchSuggestions[0].children.push({ id: store.PortalId, text: store.StoreName });
        }
        this.dataLoaded = true;
        this.getRows();
      }
    );


  }

  getRows() {
    let getOrders: Observable<OrdersPaged>;
    var adjustedPageIndex = this.page - 1;

    if (this.portalIds.length) {
      getOrders = this.znodeService.getOrdersFromSearch(adjustedPageIndex, this.count, this.search, this.sortString, this.sortAsc, this.portalIds);
    }
    else {
      getOrders = this.znodeService.getOrdersFromSearch(adjustedPageIndex, this.count, this.search, this.sortString, this.sortAsc);
    }

    getOrders.subscribe(orders => {
      this._rows = orders.Orders;
      this.filtered = orders.Orders;
      this.index = this.page;
      this.total = orders.TotalResults;
      this.loaded();

      if (!this.sortString) {
        this.sortAsc = false;
        this.sortBy('OrderId');
      }
    });
  }

  test(id: string){
    console.log('sorting by '+id);
    this.sortBy(id);
  }

  public downloadOrderData(id: number){
    this.znodeService.getOrderData(id).subscribe(
      res => {
        let table: any[] = res.Table;
        this.createCSV('Order_'+id, table);
      }
    );
  }

  public downloadLineItemData(id: number){
    this.znodeService.getOrderLineItemData(id).subscribe(
      res => {
        let table: any[] = res.Table;
        this.createCSV('LineItems', table);
      }
    );
  }

  public createCSV(filename: string, table: any[]){
    var contents: string = "";
    var headers: string[] = [];

    for(var key in table[0]){
      headers.push(key);
    }

    contents += headers.join(',');
    contents += "\r\n";

    for(var row of table){
      var line: string[] = [];
      for(var column of headers){
        var data = row[column];

        if (data != null) {
          // Sanitize data for csv
          data = data.toString().replace(/"/g, '""');
          if (data.search(/("|,|\n)/g) >= 0)
            data = '"' + data + '"';
        }

        line.push(data);
      }
      contents += line.join(',');
      contents += "\r\n";
    }

    var uri = 'data:text/csv;charset=utf-8,' + encodeURI(contents);

    var link: HTMLAnchorElement = document.createElement("a");
    link.href = uri;

    link.style.visibility = "hidden";
    link.setAttribute('download', filename + ".csv");

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  public download() {
    if (this.submit == 'csv') {
      this.downloadOrderData(this.downloadOrderId);
    }
    else {
      this.downloadLineItemData(this.downloadOrderId);
    }
  }

  matchesSearch(value: string): boolean {
    if (value) {
      return value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1;
    }
    else {
      return false;
    }
  }

  viewReceipt(order: Order){
    let url = document.getElementById('receiptUrl');
    url.setAttribute('href', order.ReceiptUrl);
    url.click();
  }

  viewOrderItem(item: OrderLineItem) {
    this.znodeService.getOrderDownloadLink(item.OrderId)
      .subscribe((data) => {
        let orderLineItem = data.OrdersLineItems.find((i) => {
          return item.OrderLineItemId == i.OrderLineItemId;
        });

        window.open(orderLineItem.DownloadLink);
      });
  }

  public filterProcess(item: Order): boolean {
    return;
  }

  onSearchChange(search: string){
    this.search = search;

    // Store search query for this table in the session storage
    sessionStorage.setItem(this.name + "-searchString", JSON.stringify(search));

    this.getRows();
  }

  onPageChange(page: number){
    if (page != this.page)
    {
        this.page = page;
        this.getRows();
    }
  }

  onCountChange(count: number) {
    this.count = count;
    this.page = 1;
    this.getRows();
    sessionStorage.setItem(this.name + '-count', this.count + '');
    sessionStorage.setItem(this.name + '-page', this.page + '');
  }

  sortBy(sort: string) {
    super.sortBy(sort);
    this.getRows();
  }

  onFilterChange(e) {
    this.portalIds = e.map((i) => +i);
    sessionStorage.setItem(this.name + "-searchFilters", JSON.stringify(e));
    this.getRows();
  }
}
