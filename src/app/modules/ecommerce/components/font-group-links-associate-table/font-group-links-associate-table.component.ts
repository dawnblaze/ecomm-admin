import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';
import { ThemeService } from '../../../../services/theme.service';

import { Font } from '../../models/font';
import { FontGroup } from '../../models/font-group';

@Component({
  selector: 'cb-font-group-links-associate-table',
  templateUrl: './font-group-links-associate-table.component.html'
})
export class FontGroupLinksAssociateTableComponent extends TableComponent implements OnInit, OnChanges {
  @Input() fontGroup: FontGroup;
  @Output() onExit: EventEmitter<any> = new EventEmitter();

  name: string = "cb-font-group-links-associate-table";
  _rows: Font[];
  filtered: Font[] = [];
  search: string = '';
  edit: boolean = false;
  associatedFonts: any = {};
  loading: boolean = true;
  saving: boolean = false;

  constructor(private daimService: DaimService, private router: Router, private themeService: ThemeService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['fontGroup']) {
      this.getAssociatedFonts();
    }
  }

  load() {
    this.getAssociatedFonts();

    this.daimService.getFonts()
      .subscribe(fonts => {
        this._rows = fonts;
        this.filterRows();

        this.loading = false;
      });
  }

  getAssociatedFonts() {
    if (this.fontGroup.id) {
      this.associatedFonts = this.fontGroup.fonts.reduce((acc, font) => {
        acc[font.fontFamilyName] = true;
        return acc;
      }, {});
    }
  }

  cancel() {
    this.onExit.emit();
  }

  save() {
    this.saving = true;
    let obs: Observable<any>[] = [];

    for(let font in this.associatedFonts) {
      if (this.associatedFonts[font]) {
        obs.push(this.daimService.addFontLink(font, this.fontGroup.id));
      }
      else {
        obs.push(this.daimService.deleteFontLink(font, this.fontGroup.id));
      }
    }

    forkJoin(...obs)
      .subscribe(res => {
        this.saving = false;
        this.cancel();
      },
      error => {
        this.saving = false;
        this.cancel();
      });
  }

  filterProcess(item: Font): boolean {
    var keys = ["displayName", "id"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
