import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser'
import { Router, RouterLink } from '@angular/router';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { EmailTemplate } from '../../models/email-template';

@Component({
  selector: 'cb-email-templates-table',
  templateUrl: './email-templates-table.component.html'
})
export class EmailTemplatesTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-email-templates-table";
  public _rows: EmailTemplate[];
  public filtered: EmailTemplate[] = [];
  public search: string = "";
  public searchKey: string = "TemplateName";
  public total = 0;
  private templateBlackList = ['znode.cer']

  constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService, private modalService: ModalService, private sanitizer: DomSanitizer, ) {
    super();
  }

  load() {
    this.getRows();
  }

  private getRows() {
    this.znodeService.getEmailTemplates().subscribe(templates => {
      this._rows = templates;
      this.filterRows();
    });
  }

  public gotoDetail(name: string) {
    name = name.replace('.', '-');
    let link = ['/ecommerce/email-templates', name];
    this.router.navigate(link);
  }

  public preview(template: EmailTemplate) {
    let name = template.TemplateName.replace('.', '-');

    this.znodeService.getEmailTemplate(name).subscribe(res => {
      this.modalService.template(res.TemplateName, this.sanitizer.bypassSecurityTrustResourceUrl("data:text/html," + encodeURIComponent(res.Html)));
    });
  }

  public delete( template: EmailTemplate ) {
    this.znodeService.deleteEmailTemplate(template).subscribe(
      res => {
        console.log(res);
        this.getRows();
        this.alertService.success(template.TemplateName + ' Deleted');
      },
      err => {
        this.alertService.error(err);
        console.log(err);
      }
    )
  }

  public copy(row: EmailTemplate) {
    this.znodeService.getEmailTemplate(row.TemplateName.replace('.', '-'))
      .subscribe(template => {
        let newTemplate = new EmailTemplate();
        newTemplate.TemplateName = 'copy_of_' + row.TemplateName;
        newTemplate.Html = template.Html;

        this.znodeService.createEmailTemplate(newTemplate).subscribe(
          res => {
            this.alertService.success('Email Template Copied Successfully');
            this.getRows();
          },
          err => {
            this.alertService.error(err);
          });
      });
  }
}
