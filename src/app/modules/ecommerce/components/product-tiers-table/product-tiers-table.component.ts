import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Product } from '../../models/product';
import { Profile } from '../../models/profile';
import { ProductTier } from '../../models/product-tier';

@Component({
  selector: 'cb-product-tiers-table',
  templateUrl: './product-tiers-table.component.html'
})
export class ProductTiersTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-product-tiers-table";
  public _rows: ProductTier[];
  public filtered: ProductTier[] = [];
  public search: string = "";
  public searchKey: string = "ProductTierId";
  public total = 0;

  private _product: number;
  @Input()
  set product(product: number) {
    this._product = product || 0;
    this.getRows(); 
  }
  get product() { return this._product || 0; }


  @Output() onCreate: EventEmitter<any> = new EventEmitter();
  @Output() onView: EventEmitter<number> = new EventEmitter();

  constructor( private znodeService: ZnodeService, private alertService: AlertService ) { 
    super();
  }

  load() {
    this.getRows();
  }

  private goToDetails(id: number) {
    console.log(id);
    this.onView.emit(id);
  }

  public create(){
    this.onCreate.emit();
  }
  
  private getRows() {
    if(this.product > 0){
      this.znodeService.getProductTiers(this.product).subscribe(tiers => {
        this._rows = tiers.filter(tier => tier.ProductId == this.product);
        this.filterRows();
      });
    }
  }

  public delete(id: number) {
    this.znodeService.deleteProductTier(id).subscribe(
      data => {
        this.getRows();
        this.alertService.success('Product Tier Deleted Successfully');
      },
      err => this.alertService.error(err)
    )
  }
  
}
