import { Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit, Component, OnChanges, SimpleChanges } from '@angular/core';

import * as $ from 'jquery';
import  'spectrum-colorpicker';
import { getRenderedText } from '@angular/core/src/render3';



@Component({
  selector: 'cb-color-picker',
  templateUrl: './color-picker.component.html'
})
export class ColorPickerComponent implements AfterViewInit, OnChanges {

  color1: string;
  color2: string = "#22317B";

  @Input() color = 'Purple';
  @Output() colorChange = new EventEmitter()

  @ViewChild('input') input: ElementRef;

  ngAfterViewInit() {
    this.initColorPicker();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initColorPicker();
  }

  initColorPicker() {
    

    ($(this.input.nativeElement) as any).spectrum({
      flat: true,
      color: this.color,
      showButtons: false,
      move: color => {
        this.colorChange.next(color);
      }
    });
   }

}
