import { Component, Input, Output, EventEmitter, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { TableComponent } from '../../../corebridge/components/table/table.component';
import { ProductCategoriesTableComponent } from '../product-categories-table/product-categories-table.component';

import { Observable, forkJoin } from 'rxjs';
import { untilDestroyed } from "ngx-take-until-destroy";

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Product } from '../../models/product';
import { Category } from '../../models/category';
import { ProductCategory } from '../../models/product-category';

@Component({
  selector: 'cb-product-categories-associate-table',
  templateUrl: './product-categories-associate-table.component.html'
})
export class ProductCategoriesAssociateTableComponent extends TableComponent implements OnInit, OnDestroy {

  public name: string = "cb-product-categories-associate-table";
  public _rows: Category[];
  public filtered: Category[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  public loading: boolean = true;

  private saveAndClose: boolean;

  @Input() product: number;

  @Output() onExit: EventEmitter<any> = new EventEmitter();

  @ViewChild(ProductCategoriesTableComponent) categoryList: ProductCategoriesTableComponent;

  constructor( private znodeService: ZnodeService, private alertService: AlertService ) {
    super();
  }

  load() {
    this.saveAndClose = false;
  }

  public cancel(saveAndClose: boolean){
    this.saveAndClose = saveAndClose;
    if(saveAndClose)
      this.save();
    else
      this.onExit.emit();
  }

  public save(){
    this.loading = false;
    let add = this.categoryList.dataTable.selected;
    this.categoryList.dataTable.setSelectAll(false);
    this.znodeService.getProductCategories(this.product).pipe(untilDestroyed(this)).subscribe(
      categories => {
        let batch: Observable<ProductCategory>[] = [];
        add = add.filter(category => !categories.find(c => c.CategoryId == category.CategoryId));
        add.forEach(category => {
          var categoryProduct: ProductCategory = new ProductCategory();
          categoryProduct.ProductId = this.product;
          categoryProduct.CategoryId = category.CategoryId;
          batch.push(this.znodeService.createCategoryProduct(categoryProduct));
        });
        if(batch.length) {
          forkJoin(batch).pipe(untilDestroyed(this)).subscribe(
            data => {
              this.alertService.success('Product Collections Updated');
              if(this.saveAndClose)
                this.onExit.emit();
              else
                this.categoryList.load();
            },
            err => {
              this.alertService.error(err);
              if(this.saveAndClose)
                this.onExit.emit();
            }
          );
        }
      },
      err => {
        this.alertService.error(err);
        if(this.saveAndClose)
          this.onExit.emit();
      }
    );
  }

  ngOnDestroy() {}
}
