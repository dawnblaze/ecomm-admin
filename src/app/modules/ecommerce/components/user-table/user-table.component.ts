import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { UserService } from '../../../../services/user.service';
import { AuthService } from '../../../../services/auth.service';
import { AlertService } from '../../../../services/alert.service';

import { User } from '../../models/user';

@Component({
  selector: 'cb-user-table',
  templateUrl: './user-table.component.html'
})
export class UserTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-user-table";
  public _rows: User[];
  public filtered: User[] = [];
  public search: string = "";
  public searchKey: string = "firstName";
  public total = 0;

  constructor( private userService: UserService, private authService: AuthService, private router: Router, private alertService: AlertService ) { 
    super();
  }

  load() {
   //this.getRows();
   let user: User = this.authService.getUser();
   this.filtered = [user];
  }

  getRows() {
    this.userService.getUsers().subscribe(users => {
      console.log(users);
      this._rows = users;
      this.filterRows();
    });
  }

}
