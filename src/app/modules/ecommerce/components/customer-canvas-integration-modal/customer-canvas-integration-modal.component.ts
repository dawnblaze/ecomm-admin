import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";

import { CustomersCanvasComponent } from "../../components/customers-canvas/customers-canvas.component";
import { CodaService } from "../../../../services/coda.service";
import { EditorPreset } from "../../models/editor-preset";
import { TemplatePreset } from "../../models/template-preset";
import { Product } from "../../models/product";

import { GuidGeneratorServiceService } from "../../../../services/guid-generator-service.service";
import { ParsePSDResponse } from "../../models/parse-psd-response";
import { ModalService } from "../../../../services/modal.service";
import { AlertService } from "../../../../services/alert.service";
import { GenerateStateFileRequest } from "../../models/generate-state-file-request";
import { ProductDataOnUploadTemplateRequest } from "../../models/product-data-upload-template-request";
import { GenerateStateFileResponse } from "../../models/generate-state-file-response";
import { AppType } from "../../models/app-type";
import { RecordType } from "../../models/record-type";
import { UploadFileToGenerateStateModalComponent } from "../../components/upload-file-to-generate-state-modal/upload-file-to-generate-state-modal.component";
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ProductTemplateData } from '../../models/product-template-data';
import { ProductTemplateSurfaceData } from '../../models/product-template-surface-data';
import { TemplatePresetJsonModel } from '../../models/template-preset-json';

@Component({
  selector: "customer-canvas-integration-modal",
  templateUrl: "./customer-canvas-integration-modal.component.html",
  styleUrls: ["./customer-canvas-integration-modal.component.less"]
})
export class CustomerCanvasIntegrationModalComponent implements OnInit {
  @Input() product: Product;

  displayDialog = false;
  public loading: boolean = false;
  public uploading: boolean = false;
  stateFileGeneratedSuccessfully: boolean = false;
  public allInformationAreFilled: boolean = false;
  public selectedTemplatePresetJsonModel: TemplatePresetJsonModel;
  public templatePresetJsonModels: TemplatePresetJsonModel[];
  public templatePresets: TemplatePreset[];
  public selectedTemplatePreset: TemplatePreset;
  public selectedProductTemplate: ProductTemplateData;
  public selectedSurface: ProductTemplateSurfaceData;
  @ViewChild(CustomersCanvasComponent) customerCanvas: CustomersCanvasComponent;
  @ViewChild(UploadFileToGenerateStateModalComponent) fileUploadToStateModal: UploadFileToGenerateStateModalComponent;
  @ViewChild("inputuploadpsd") inputUploadPsd: ElementRef;

  editorPresets: EditorPreset[];

  editorPresetId: number;
  templatePresetId: number;
  stateFileName: string;

  parsePSDResponse: ParsePSDResponse;
  generateStateFileResponse: GenerateStateFileResponse;

  _templateName: string; // temporary binding for template name

  importCompleted: boolean = false;

  constructor(
    private codaService: CodaService,
    private el: ElementRef,
    private guidGenerator: GuidGeneratorServiceService,
    private modalService: ModalService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.codaService.getEditorPresets(1, 999).subscribe(editorPresetPaged => {
      this.editorPresets = editorPresetPaged.records;
    });

    this.codaService
      .getTemplatePresets(1, 999)
      .subscribe(templatePresetPaged => {
        this.templatePresets = templatePresetPaged.records;
      });
  }

  showDialog() {
    this.displayDialog = true;
    this.customerCanvas.initCustomersCanvasEditor();
  }

  closeDialog() {
    this.displayDialog = false;
  }

  setEditorPreset(selected: EditorPreset) {
    this.editorPresetId = selected.id;

    this.validateInformation();
  }

  setTemplatePreset(selected: TemplatePreset) {
    this.templatePresetId = selected.id;

    this.validateInformation();
  }

  validateInformation(): boolean {
    if (this.stateFileName && this.editorPresetId && this.templatePresetId) {
      this.allInformationAreFilled = true;
      return true;
    }

    this.allInformationAreFilled = false;
    return false;
  }

  upload() {
    this.uploading = true;

    let inputEl = this.inputUploadPsd.nativeElement;
    if (inputEl.files.length == 0) return;
    let file: File = inputEl.files[0];

    this.codaService.parsePSD(file).subscribe(
      parsePSDResponse => {
        console.log(parsePSDResponse);

        this.parsePSDResponse = parsePSDResponse;

        if (parsePSDResponse.parsedSuccessfully) {
          return this.modalService
            .warning(
              "Please Confirm the Number of Surfaces",
              "The number of surfaces found in the uploaded PSD is " + parsePSDResponse.numberOfSurfaces + ". Do you confirm that number?",
              "Yes",
              "No"
            )
            .subscribe(confirmed => {
              if (confirmed) {
                let generateStateFileRequest: GenerateStateFileRequest = {
                  productId: this.product.ProductId,
                  tempPSDFileName: parsePSDResponse.tempPSDFileName
                };

                this.codaService
                  .generateStateFile(generateStateFileRequest)
                  .subscribe(generateStateFileResponse => {
                    this.uploading = false;

                    if (generateStateFileResponse.generatedSuccessfully) {
                      this.generateStateFileResponse = generateStateFileResponse;
                      this.stateFileName =
                        generateStateFileResponse.stateFileName;
                      this.stateFileGeneratedSuccessfully = true;

                      this.validateInformation();

                      console.log(
                        "Url to download state file from DM: " +
                        generateStateFileResponse.stateFileUrlForDownload
                      );
                    } else {
                      this.alertService.error(
                        generateStateFileResponse.message
                      );
                    }
                  });
              } else {
                this.uploading = false;
              }
            });
        } else {
          console.error(parsePSDResponse.message);
          this.alertService.error(parsePSDResponse.message);
        }
      },
      err => {
        console.error(err);
        this.alertService.error(err);
        this.uploading = false;
      }
    );
  }

  save() {
    if (this.allInformationAreFilled) {
      this.loading = true;

      let productDataOnUploadTemplateRequest: ProductDataOnUploadTemplateRequest = {
        productId: this.product.ProductId,
        productName: this.product.Name,
        stateID: this.stateFileName,
        stateUrl: this.generateStateFileResponse.stateFileUrl,
        appType: AppType.Ecommerce,
        recordType: RecordType.Product,
        editorPresetId: this.editorPresetId,
        templatePresetId: this.templatePresetId
      };

      this.codaService
        .generateProductDataAfterUploadTemplate(productDataOnUploadTemplateRequest)
        .subscribe(
          () => {
            this.loading = false;

            this.alertService.success(
              "The records needed for the template were created successfully.",
              5000
            );
            this.closeDialog();
          },
          err => {
            console.error(err);
            this.alertService.error(err);
            this.loading = false;
          }
        );
    } else {
      this.alertService.error(
        "Make sure you filled out all the required fields"
      );
    }
  }

  cancel() {
    this.closeDialog();
  }

  openModalToUploadFile() {
    this.fileUploadToStateModal.showDialog();
  }

  handleImportCompleted(event) {
    this.importCompleted = true;
    this.loadEditorPreset();
  }

  public loadEditorPreset() {
    this.selectedProductTemplate = new ProductTemplateData();
  }

  public onTemplatePresetSelected(surfaceData: any) {
    let surface: TemplatePreset = <TemplatePreset>surfaceData;
    if(surface) {
      // this.selectedTemplatePreset = surface;
      this.selectedTemplatePresetJsonModel = <TemplatePresetJsonModel>JSON.parse(surface.presetJSON)
    }
  }
}
