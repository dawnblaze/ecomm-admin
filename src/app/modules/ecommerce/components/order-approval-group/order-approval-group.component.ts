import { Component, Input, OnInit, ViewChild, OnDestroy, Output, EventEmitter, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from "@angular/forms";
import { forkJoin } from 'rxjs';
import { untilDestroyed } from "ngx-take-until-destroy";
import { debounceTime, map } from "rxjs/operators";

import { Profile } from '../../models/profile';
import { Portal } from '../../models/portal';
import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';
import { ModalService } from '../../../../services/modal.service';

import { AccountSearchModel } from "../../models/approval-account-search";
import { AccountSearchParameters } from "../../models/approval-account-search-parameters";
import { ApprovalRule } from '../../models/approval-rule';
import { OrderApproval, OrderApprovalApprover, OrderApprovalCustomer, OrderApprovalRuleType, OrderApprovalRuleLimit } from '../../models/approval-group';
import { AutoSaveBarState } from '../../../corebridge/components/autosave-bar/autosave-bar.component';
import { DetailedError } from "../../../../modules/corebridge/models/alert";

@Component({
  selector: 'app-order-approval-group',
  templateUrl: './order-approval-group.component.html',
  styleUrls: ['./order-approval-group.component.less']
})
export class OrderApprovalGroupComponent implements OnInit, OnDestroy {

  @Input() profile: Profile;
  @Input() portal: Portal;
  @Output() onValidate = new EventEmitter<DetailedError[]>();
  @Output() onSave = new EventEmitter<AutoSaveBarState>();
  @ViewChild('cbForm') ngForm: NgForm;

  public orderApprovals: OrderApproval[] = [];
  public exemptedCustomers: OrderApprovalCustomer[] = [];
  public existingAccounts: number[] = [];
  public orderRules: ApprovalRule[] = [];
  public loading: boolean = false;
  public exemptedCustomersLoaded: boolean = false;

  public orderApprovalsCopy: any = {};
  public exemptedCustomersCopy: any = {};

  public origCopyApprovals: OrderApproval[];
  public origCopyExemptedCustomers: OrderApprovalCustomer[];

  public ruleLimitTypes = [{id:10, name:'Require Approval'}, {id:11, name:'Not Permitted'}];
  public hasError: boolean = false;
  public activeIndex: number[];
  public approverOptions: OrderApprovalApprover[];

  constructor(
    private znodeService: ZnodeService,
    private alertService: AlertService,
    private modalService: ModalService,
    private router: Router,
    private elementRef: ElementRef,
    private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getGroups();
    this.getExemptCustomers();
    this.getApproverOptions();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  /* Approval Group Methods */
  getGroups() {
    let openTabs: number[] = [];
    this.orderApprovalsCopy = {};

    this.znodeService.getApprovalGroupsByPortalId(this.portal.PortalId).subscribe(orderApprovals => {
      orderApprovals.forEach((approval) => {
        approval = Object.assign(approval, new OrderApproval);

        approval.orderLimited = this.newRuleLimit(approval.ID, OrderApprovalRuleType.OrderLimit);
        approval.timeFrameLimited = this.newRuleLimit(approval.ID, OrderApprovalRuleType.TimeFrameLimit);

        approval.Rules.forEach((rule) => {
          rule = Object.assign(rule, new ApprovalRule);
          if (rule.ApprovalRuleTypeID == OrderApprovalRuleType.OrderLimit) {
            approval.orderLimited = rule;
          } else if (rule.ApprovalRuleTypeID == OrderApprovalRuleType.TimeFrameLimit) {
            approval.timeFrameLimited = rule;
          }
        });

        approval.Approvers.forEach((approver) => {
          approver = Object.assign(approver, new OrderApprovalApprover);
          this.existingAccounts.push(approver.AccountID);
        });

        approval.Customers.forEach((customer) => {
          customer = Object.assign(customer, new OrderApprovalCustomer);
          this.existingAccounts.push(customer.AccountID);
        });

        // if (approval.Approvers.length < 1) this.addApproverInput(approval, false);
        // if (approval.Customers.length < 1 && !approval.IsDefault) this.addCustomerInput(approval, false);

        this.orderApprovalsCopy[approval.ID] = JSON.stringify(approval);
        openTabs.push(openTabs.length);
      });

      this.orderApprovals = orderApprovals;
      this.origCopyApprovals = JSON.parse(JSON.stringify(this.orderApprovals));
      if(!this.activeIndex)
        this.activeIndex = openTabs;

      if(this.orderApprovals.length == 0) {
        this.addGroupSection();
      }

      this.ngForm.form.valueChanges.pipe(debounceTime(2000), untilDestroyed(this)).subscribe(values => {
        if(this.exemptedCustomersLoaded && this.ngForm.dirty)
          this.validate();
      });
    });
  }
  addGroupSection() {
    let group: OrderApproval = {
      ID: -1, Approvers: [], Customers: [], Rules: [],
      IsNew: true, IsDefault: false, UseDefaultGroupApprover: false,
      PortalID: this.portal.PortalId, Name: `Group ${this.orderApprovals.length}`
    };

    // this.addApproverInput(group, false);
    // this.addCustomerInput(group, false);
    group.orderLimited = this.newRuleLimit(group.ID, OrderApprovalRuleType.OrderLimit);
    group.timeFrameLimited = this.newRuleLimit(group.ID, OrderApprovalRuleType.TimeFrameLimit);
    if(!this.orderApprovals.length) {
      group.Name = 'Default Group';
      group.IsDefault = true;
    }

    this.orderApprovals.push(group);
    this.activeIndex.push(this.activeIndex.length);
    this.onValidate.emit([{ ID: 0, Name: group.Name, FieldName: "Rules", ErrorMessage: 'Group should have at least 1 active rule.' }]);
    this.onSave.emit(AutoSaveBarState.invalid);

    if(this.orderApprovals.length > 1) {
      this.rename(null, this.orderApprovals.length - 1, group);
    }
  }
  removeGroup(event: any, groupIndex: number, group: OrderApproval) {
    event.stopPropagation();
    if (group.IsNew) {
      this.orderApprovals.splice(groupIndex, 1);
      this.validate();
    } else {
      this.znodeService.deleteApprovalGroup(group.ID).subscribe(deleted => {
        this.orderApprovals.splice(groupIndex, 1);
        delete this.orderApprovalsCopy[group.ID];
        this.alertService.success('Approval Group Deleted Successfully');
        this.validate();
      });
    }
  }

  /* Approval Approver Methods */
  addApproverInput(group: OrderApproval, newInput: boolean) {
    let orderApprover: OrderApprovalApprover = {
      IsNew: true,
      AccountID: -1,
      ApprovalGroupID: group.ID,
      ID: group.Approvers.length + 1,
      DisplayName: '',
      ParentAccountID: null
    };

    group.Approvers.push(orderApprover);
  }

  getApproverOptions() {
    let searchParam = new AccountSearchParameters();
    searchParam.ApprovalGroupID = -1;
    searchParam.PortalID = this.portal.PortalId;
    searchParam.SearchString = "";
    searchParam.Type = "Approver";

    this.znodeService.orderApprovalSearchAccounts(searchParam)
    .subscribe(accounts => {
      this.approverOptions = accounts.sort((a,b)=>a.DisplayName.localeCompare(b.DisplayName));
    });
  }
  handleApproversChange(approvers, index: number) {
    let group = this.orderApprovals[index];
    let accounts = this.approverOptions
    .filter(a => approvers.indexOf(a.AccountID) > -1 && !group.Approvers.find(b => a.AccountID == b.AccountID));
    accounts.map(approver => {
      approver.IsNew = true;
      if(!group.IsNew) approver.ApprovalGroupID = group.ID;
    });
    group.Approvers = group.Approvers.concat(accounts).sort((a,b)=>a.DisplayName.localeCompare(b.DisplayName));
  }
  mapToID = (item:OrderApprovalApprover) => item.AccountID;

  /* Approval Customer Methods */
  addCustomerInput(group: OrderApproval, newInput: boolean) {
    let orderCustomer: OrderApprovalCustomer = {
      AccountID: -1,
      ApprovalGroupID: group.ID,
      ID: group.Customers.length + 1,
      IsNew: true,
      IsExempt: false,
      DisplayName: '',
      PortalID: this.portal.PortalId
    };

    group.Customers.push(orderCustomer);
  }

  /* Approval Rules Methods */
  newRuleLimit(groupID: number, ruleTypeID: number) {
    let orderRuleLimit: ApprovalRule = {
      ApprovalGroupID: groupID,
      ApprovalRuleTypeID: ruleTypeID,
      ApprovalRuleLimitTypeID: OrderApprovalRuleLimit.RequireApproval,
      IsActive: false,
      CurrencyLimit: 0,
      DaysLimit: 0,
      Priority: (ruleTypeID == OrderApprovalRuleType.TimeFrameLimit) ? 1 : 2,
      ID: -1
    }
    return orderRuleLimit;
  }
  updateRules(group: OrderApproval, approvalRule: ApprovalRule, $event = null) {
    if (group.IsNew) return;

    // if (!this.validateRules(group)) {
    //   approvalRule.IsActive = true;
    //   if($event) $event.target.checked = true;
    // }
  }
  validateRules(group: OrderApproval) {
    if (!group.orderLimited.IsActive && !group.timeFrameLimited.IsActive) {
      this.alertService.error('Approval Group should have at least 1 active rule.');
      return false;
    }

    return true;
  }
  validateApprovalLimit(group: OrderApproval) {
    if ((group.orderLimited.IsActive && group.timeFrameLimited.IsActive) &&
      (group.timeFrameLimited.CurrencyLimit < group.orderLimited.CurrencyLimit)) return true;

    return false;
  }
  setApprovalRuleLimit(group: OrderApproval, approvalRule: ApprovalRule, $event) {
    approvalRule.ApprovalRuleLimitTypeID = $event;
    // this.updateRules(group, approvalRule);
  }

  /* Exemption Methods */
  getExemptCustomers() {
    this.exemptedCustomersLoaded = false;
    this.znodeService.getApprovalCustomerExempt(this.portal.PortalId).subscribe(customers => {
      this.exemptedCustomers = customers;
      this.exemptedCustomers.forEach(exempt => {
        this.exemptedCustomersCopy[exempt.ID] = JSON.stringify(exempt);
        this.existingAccounts.push(exempt.AccountID);
      });
      this.origCopyExemptedCustomers = JSON.parse(JSON.stringify(this.exemptedCustomers));
      this.exemptedCustomersLoaded = true;
    });
  }
  addExemptedCustomerSection(newInput: boolean) {
    let exemptedCustomer: OrderApprovalCustomer = {
      IsNew: true,
      AccountID: -1,
      IsExempt: true,
      ApprovalGroupID: null,
      PortalID: this.portal.PortalId,
      ID: this.exemptedCustomers.length + 1,
      DisplayName: ''
    };

    this.exemptedCustomers.push(exemptedCustomer);
  }
  updateExemptedCustomer(account: AccountSearchModel, customer: OrderApprovalCustomer) {
    let prevAccountID = customer.AccountID;
    let prevAccountIndex = this.existingAccounts.indexOf(prevAccountID);
    if (prevAccountIndex != -1) this.existingAccounts.splice(prevAccountIndex, 1);

    customer.AccountID = account.AccountID;
    customer.DisplayName = account.DisplayName;
    if (this.existingAccounts.indexOf(customer.AccountID) == -1) this.existingAccounts.push(customer.AccountID);
  }

  rename(event: any, groupIndex: number, group: OrderApproval) {
    if(event) event.stopPropagation();
    group['editing'] = true;
    setTimeout(() => {
      let index = groupIndex + 1;
      this.elementRef.nativeElement.querySelector('p-accordiontab:nth-child(' + index + ') input').focus();
    }, 500);
  }

  onBlurName(group: OrderApproval) {
    if(group.Name && group.Name.trim().length > 0)
      group['editing'] = false;
  }

  validate() {
    var errors: DetailedError[] = [];

    this.exemptedCustomers.forEach(exempt => {
      if (exempt.IsNew && exempt.AccountID == -1) {
        errors.push({ ID: 0, Name: "", FieldName: 'Exempt Customers', ErrorMessage: 'Customer Exemption has an empty customer field.' });
        return;
      }
    });

    this.orderApprovals.forEach(group => {
      if (!group.orderLimited.IsActive && !group.timeFrameLimited.IsActive) {
        errors.push({ ID: 0, Name: group.Name, FieldName: "Rules", ErrorMessage: 'Group should have at least 1 active rule.' });
      }
      else if (this.validateApprovalLimit(group)) {
        errors.push({ ID: 0, Name: group.Name, FieldName: "Rules", ErrorMessage: 'Time Frame Limit must be greater than or equal to Order Limit.' });
      }

      if (group.IsDefault) {
        if(!group.Approvers.length)
          errors.push({ ID: 0, Name: group.Name, FieldName: "Approvers", ErrorMessage: 'Group should have at least 1 approver.' });
      } else {
        if (!group.UseDefaultGroupApprover && !group.Approvers.length)
          errors.push({ ID: 0, Name: group.Name, FieldName: "Approvers", ErrorMessage: 'Group should have at least 1 approver or use default.' });
        if (!group.Customers.length)
          errors.push({ ID: 0, Name: group.Name, FieldName: "Customers", ErrorMessage: 'Group should have at least 1 customer.' });
      }

      group.Approvers.forEach(approver => {
        if (approver.IsNew && approver.AccountID == -1 && !group.UseDefaultGroupApprover) {
          errors.push({ ID: 0, Name: group.Name, FieldName: "Approvers", ErrorMessage: 'Group has an empty approver field.'});
          return;
        }
      });

      group.Customers.forEach(customer => {
        if (customer.IsNew && customer.AccountID == -1) {
          errors.push({ ID: 0, Name: group.Name, FieldName: "Customers", ErrorMessage: 'Group has an empty customer field.'});
          return;
        }
      });
    });

    this.onValidate.emit(errors);
    this.hasError = errors.length > 0;
    if(!this.hasError) {
      this.alertService.detailedError('approvals', []);
      this.save();
    }
  }

  save() {
    if(!this.hasError && !this.ngForm.form.disabled) {
      let observables = [],
      hasChanges = false;
      this.ngForm.form.disable();

      this.orderApprovals.forEach((approval) => {
        approval.Rules = [];
        approval.Rules.push(approval.orderLimited);
        approval.Rules.push(approval.timeFrameLimited);

        if (approval.IsNew) {
          hasChanges = true;
          observables.push(this.znodeService.createOrderApproval(approval));
        } else if (this.orderApprovalsCopy[approval.ID] != JSON.stringify(approval)) {
          hasChanges = true;
          observables.push(this.znodeService.updateOrderApproval(approval));
        }

        let tmpCopy = this.origCopyApprovals.find(item => item.ID == approval.ID)
        if (tmpCopy) {
          tmpCopy.Approvers.forEach(origApprover => {
            let found = approval.Approvers.find(approver => approver.AccountID == origApprover.AccountID);
            if (!found) observables.push(this.znodeService.deleteApprovalApprover(origApprover.ID));
          });
          tmpCopy.Customers.forEach(origCustomer => {
            let found = approval.Customers.find(customer => customer.AccountID == origCustomer.AccountID);
            if (!found) observables.push(this.znodeService.deleteApprovalCustomer(origCustomer.ID))
          })
        }
      });

      let hasExemptCustomer: boolean = false;
      this.exemptedCustomers.forEach(exempt => {
        if (exempt.IsNew) {
          hasChanges = true;
          hasExemptCustomer = true;
          observables.push(this.znodeService.createApprovalCustomer(exempt));
         } else if (this.exemptedCustomersCopy[exempt.ID] != JSON.stringify(exempt)) {
          hasChanges = true;
          hasExemptCustomer = true;
          observables.push(this.znodeService.updateApprovalCustomer(exempt));
        }
      });

      this.origCopyExemptedCustomers.forEach(origExempt => {
        let found = this.exemptedCustomers.find(exempt => exempt.AccountID == origExempt.AccountID);
        if (!found) {
          hasChanges = true;
          hasExemptCustomer = true;
          observables.push(this.znodeService.deleteApprovalCustomer(origExempt.ID))
        };
      })



      if (hasChanges) {
        this.loading = true;
        this.onSave.emit(AutoSaveBarState.saving);
        forkJoin(observables).subscribe(res => {
          this.getGroups();
          this.getExemptCustomers();
          this.loading = false;
          this.onSave.emit(AutoSaveBarState.saved);
          this.ngForm.form.markAsPristine();
          this.ngForm.form.enable();
        },
        err => {
          this.onSave.emit(AutoSaveBarState.error);
          this.ngForm.form.enable();
        });
      } else {
        this.ngForm.form.enable();
        this.onSave.emit(AutoSaveBarState.none);
      }
    }
  }

  cancel() {
    this.router.navigate(["/ecommerce/storefronts"]);
  }

  ngOnDestroy() {
    this.alertService.detailedError('approvals', []);
  }
}
