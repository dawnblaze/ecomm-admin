import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';

import { TableComponent } from "../../../corebridge/components/table/table.component";
import { RowAction } from "../../../corebridge/components/table2/table.component";

import { CodaService } from '../../../../services/coda.service';
import { AlertService } from "../../../../services/alert.service";
import { ModalService } from '../../../../services/modal.service';
import { TemplatePreset } from '../../models/template-preset';

@Component({
  selector: 'cb-surface-presets-table',
  templateUrl: './surface-presets-table.component.html'
})
export class SurfacePresetsTableComponent extends TableComponent
  implements OnInit, OnDestroy {

  public _rows: TemplatePreset[];
  public filtered: TemplatePreset[] = [];
  public search: string = "";
  public searchKey: string = "name";
  public total = 0;
  public RowAction = RowAction;
  public _columns = [
    { field: "name", header: "Name" }
  ];

  constructor(private codaService: CodaService, private router: Router, private alertService: AlertService, private modalService: ModalService) {
    super();
  }

  ngOnInit() {
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.codaService.getTemplatePresets(1, 20).pipe(untilDestroyed(this)).subscribe(surfacePresetPaged => {
      this._rows = surfacePresetPaged.records;
      this.total = surfacePresetPaged.totalResults;
      this.filterRows();
    }, err => {
      this.loaded();
      this.alertService.error(err);
    });
  }

  goToDetail(surfacePresetId: number) {
    this.router.navigate(['/ecommerce/settings/surface-presets/', surfacePresetId]);
  }

  delete(surfacePresetId: number) {
    if(this._rows.find(preset => preset.id == surfacePresetId).name.toUpperCase() == 'STANDARD') {
      this.modalService.warningNoCancel('Cannot Delete Surface Preset','Deletion of Standard Surface Preset is not allowed.','OK')
      return;
    }
    var title = "Please confirm you would like to delete the selected Surface Preset. This change cannot be undone.";
    var content = "Deletion will permanently remove the Surface Preset.";
    this.modalService.warning(title, content);

    let subscription = this.modalService.response.pipe(untilDestroyed(this)).subscribe(res => {
      if (res) {
        this.codaService.deleteTemplatePreset(surfacePresetId).pipe(untilDestroyed(this)).subscribe(response => {
          if (response) {
            this.getRows();
            this.alertService.success('Surface Preset Deleted Successfully');
          } else this.alertService.error('Could not delete Surface Preset');
        });
      }
      subscription.unsubscribe();
    }, err => this.alertService.error(err)
    )
  }

  clone(surfacePresetId: number) {
    let preset = this._rows.find(e => e.id == surfacePresetId);
    if(preset) {
      let copy = new TemplatePreset();
      Object.assign(copy, preset);
      copy.name = "Copy of " + copy.name;
      copy.isDeleted = false;
      copy.id = 0;
      this.codaService.getTemplatePresets(1, this.total).pipe(untilDestroyed(this)).subscribe(response => {
        let copies = response.records.filter(item =>
          item.name.startsWith(copy.name) && item.name.substr(copy.name.length - 1).match(/\s\d+$/));
        if(copies.length > 0) {
          let n = copies.map(item => parseInt(item.name.split(' ').pop())).sort();
          copy.name += ' ' + (++n[n.length-1]);
          this.create(copy);
        } else {
          if(response.records.find(item => item.name == copy.name))
            copy.name += ' 1';
          this.create(copy);
        }
      }, err => this.create(copy));

    }
  }
  create(newSurfacePreset: TemplatePreset) {
    this.codaService.createTemplatePreset(newSurfacePreset).pipe(untilDestroyed(this)).subscribe(response => {
      this.getRows();
      this.alertService.success('Surface Preset Cloned Successfully');
    }, err => this.alertService.error(err));
  }

  ngOnDestroy() {

  }
}
