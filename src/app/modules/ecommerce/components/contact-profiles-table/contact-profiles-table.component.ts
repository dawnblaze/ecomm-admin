import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Profile } from '../../models/profile';
import { Contact } from '../../models/contact';

@Component({
  selector: 'cb-contact-profiles-table',
  templateUrl: './contact-profiles-table.component.html'
})
export class ContactProfilesTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-contact-profiles-table";
  public _rows: Profile[];
  public filtered: Profile[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;
  public associatedProfiles: Profile[];

  @Input() contact: Contact;

  @Output() profilesChanged: EventEmitter<any> = new EventEmitter<any>();

  constructor( private znodeService: ZnodeService, private alertService: AlertService ) {
    super();
  }

  load() {
    this.getRows();
  }

  activateProfile(profile: Profile) {
    let observable;
    let ids = this.associatedProfiles.map((p) => p.ProfileId);

    if (ids.indexOf(profile.ProfileId) != -1) {
      ids.splice(ids.indexOf(profile.ProfileId), 1);
    }
    else {
      ids.push(profile.ProfileId);
    }

    observable = this.znodeService.updateAccountProfiles(this.contact.AccountId, ids);

    observable.subscribe(res => {
      this.znodeService.getAccountProfiles(this.contact.AccountId)
        .subscribe((profiles) => {
          this.associatedProfiles = profiles;
          this.profilesChanged.emit();
        });
    });
  }

  private getRows() {
    forkJoin(
      this.znodeService.getProfiles(),
      this.znodeService.getAccountProfiles(this.contact.AccountId)
    ).subscribe(data => {
      this.associatedProfiles = data[1];
      this._rows = data[0];
      this.filterRows();
    });
  }

  isAssociated(profile: Profile) {
    let res = this.associatedProfiles.filter((p) => {
      return p.ProfileId == profile.ProfileId;
    });

    return res.length > 0;
  }
}
