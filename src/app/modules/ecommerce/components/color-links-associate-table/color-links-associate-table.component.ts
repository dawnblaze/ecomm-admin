import { Component, Input, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';
import { ThemeService } from '../../../../services/theme.service';

import { Color } from '../../models/color';
import { ColorGroup } from '../../models/color-group';

@Component({
  selector: 'cb-color-links-associate-table',
  templateUrl: './color-links-associate-table.component.html'
})
export class ColorLinksAssociateTableComponent extends TableComponent implements OnInit, OnChanges {
  @Input() color: Color;
  @Output() onExit: EventEmitter<any> = new EventEmitter();

  name: string = "cb-color-links-associate-table";
  _rows: ColorGroup[];
  filtered: ColorGroup[] = [];
  search: string = '';
  edit: boolean = false;
  associatedGroups: any = {};
  loading: boolean = true;
  saving: boolean = false;

  constructor(private daimService: DaimService, private router: Router, private themeService: ThemeService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['color']) {
      this.getAssociatedGroups();
    }
  }

  load() {
    this.getAssociatedGroups();

    this.daimService.getColorGroups()
      .subscribe(colorGroups => {
        this._rows = colorGroups;
        this.filterRows();

        this.loading = false;
      });
  }

  getAssociatedGroups() {
    if (this.color.id) {
        this.associatedGroups = this.color.groups.reduce((acc, group) => {
          acc[group.id] = true;
          return acc;
        }, {});
    }
  }

  cancel() {
    this.onExit.emit();
  }

  save() {
    this.saving = true;
    let obs: Observable<any>[] = [];

    for(let groupID in this.associatedGroups) {
      if (this.associatedGroups[groupID]) {
        obs.push(this.daimService.addColorLink(this.color.id, +groupID));
      }
      else {
        obs.push(this.daimService.deleteColorLink(this.color.id, +groupID));
      }
    }

    forkJoin(...obs)
      .subscribe(res => {
        this.saving = false;
        this.cancel();
      },
      error => {
        this.saving = false;
        this.cancel();
      });
  }

  filterProcess(item: ColorGroup): boolean {
    var keys = ["name", "id"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
