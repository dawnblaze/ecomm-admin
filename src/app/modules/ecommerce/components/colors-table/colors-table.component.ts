import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';

import { Color } from '../../models/color';

@Component({
  selector: 'cb-colors-table',
  templateUrl: './colors-table.component.html'
})
export class ColorsTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-colors-table";
  public _rows: Color[];
  public filtered: Color[] = [];
  public search: string = '';

  constructor(private daimService: DaimService, private router: Router) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.daimService.getColors().subscribe(colors => {
      this._rows = colors;

      this.filterRows();
    });
  }

  gotoDetail(color: Color) {
    this.router.navigate(['/ecommerce/settings/colors', color.id]);
  }

  delete(id: number) {
    this.daimService.deleteColor(id).subscribe(res => {
      this.getRows();
    });
  }


  filterProcess(item: Color): boolean {
    var keys = ["name", "id"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
