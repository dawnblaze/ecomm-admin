import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TableComponent } from '../../../corebridge/components/table/table.component';
import { RowAction } from '../../../corebridge/components/table2/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { ContentPage } from '../../models/content-page';
import { Portal } from '../../models/portal';

@Component({
  selector: 'cb-content-pages-table',
  templateUrl: './content-pages-table.component.html'
})
export class ContentPagesTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-content-pages-table";
  public cols: Array<Object> = [{ field: "Title", header: "Name" }];
  public rowActions = [RowAction.Delete];
  public _rows: ContentPage[];
  public filtered: ContentPage[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;

  @Input() portal: Portal;
  @Input() relativeAddPath: string ="./new";
  @Output() onCreate: EventEmitter<any> = new EventEmitter();
  @Output() onView: EventEmitter<number> = new EventEmitter();

  constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService ) {
    super();
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.znodeService.getContentPages(this.portal.PortalId).subscribe(pages => {
      this._rows = pages;
      this.filterRows();
    });
  }

  gotoDetail(id: number) {
    this.onView.emit(id);
  }

  create() {
    this.onCreate.emit();
  }

  delete(id: number) {
    this.znodeService.deleteContentPage(id).subscribe(
      data => {
        this.alertService.success('Store Info Deleted Successfully');
        this.getRows();
      },
      err => {
        this.alertService.error(err);
      }
    )
  }

}
