import { Component, OnInit, ViewChild, ElementRef, Input} from '@angular/core';

@Component({
  selector: 'content-source-spinner',
  templateUrl: './content-source-spinner.component.html',
  styleUrls: ['./content-source-spinner.component.scss']
})
export class ContentSourceSpinnerComponent implements OnInit {

  @ViewChild('spinner') spinner: ElementRef;
  @Input() visibleStatus: string;

  constructor() { }

  ngOnInit() {
    
  }

  

}
