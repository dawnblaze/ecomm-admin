import { Component, OnInit, EventEmitter, ElementRef, Input, Output } from '@angular/core';
import { Http } from '@angular/http';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Image } from '../../models/image';

@Component({

    selector: 'cb-image-upload',
    templateUrl: './image-upload.component.html'
})
export class ImageUploadComponent implements OnInit {
    constructor(private http: Http, private el: ElementRef, private znodeService: ZnodeService, private alertService: AlertService) {}

    @Input() portalId: number = 1;

    @Output() onUpload = new EventEmitter<Image>();

    public loading: boolean = false;

    ngOnInit () {
        if(this.portalId == null || this.portalId < 1){
            this.setPortalId();
        }
    }

    upload() {
        this.loading = true;

        let inputEl = this.el.nativeElement.getElementsByTagName('input')[0];
        if (inputEl.files.length == 0) return;
        let file: File = inputEl.files[0];

        this.znodeService.uploadImage(file, this.portalId).subscribe(
            image => {
                console.log(image);
                this.onUpload.emit(image);
                this.loading = false;
            },
            err => {
                console.error(err);
                this.alertService.error(err);
                this.loading = false;
            }
        );
    }

    setPortalId() {
        this.znodeService.getPortals().subscribe(
            portals => {
                this.portalId = portals[0].PortalId;
            },
            err => this.alertService.error(err)
        );
    }
}
