import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";

@Component({
  selector: 'cb-advanced-color-picker[colorValue][name]',
  templateUrl: './advanced-color-picker.component.html',
  styleUrls: ['./advanced-color-picker.component.less']
})

export class AdvancedColorPickerComponent implements OnInit{

  @Input() colorValue: string;
  @Output() colorValueChange = new EventEmitter();
  change(newValue) {
    console.log('colorValue Changed:', newValue);
    this.colorValue = newValue;
    this.colorValueChange.emit(newValue);
  }
  @Input() name: string;
  @Input() tooltipLocationId: string;

  public dashedName: string = '';
  ngOnInit() {
    this.dashedName = this.name.replace(" ", "-").toLowerCase()
  }

  clickColorPicker() {
    let el = <HTMLElement>document.querySelector('.ui-colorpicker-panel')
    el.classList.add('advanced-colorpicker-panel')

    let parentEl = `.color-picker-container.${this.dashedName}`

    let inputWidth = <HTMLElement>document.querySelector(`${parentEl} input.cb_fieldset-field_input[ng-reflect-name=${this.dashedName}]`)

    if (inputWidth) {
      let width = inputWidth.clientWidth;
      el.style.width = (width + 2) + 'px';
      el.style.left = -(width + 5) + 'px';
    }
  }



}
