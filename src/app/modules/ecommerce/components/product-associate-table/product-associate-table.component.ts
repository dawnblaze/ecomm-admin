import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { Observable, forkJoin }     from 'rxjs';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { Product } from '../../models/product';
import { Category } from '../../models/category';
import { ProductCategory } from '../../models/product-category';

import { TableComponent } from '../../../corebridge/components/table/table.component';

@Component({
  selector: 'cb-product-associate-table',
  templateUrl: './product-associate-table.component.html'
})
export class ProductAssociateTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-product-associate-table";
  public _rows: Product[];
  public filtered: Product[] = [];
  public search: string = "";
  public searchKey: string = "Name";
  public total = 0;

  @Input() category: number = 0;

  private categoryProducts: ProductCategory[];
  public products: Product[];
  public selectedProducts: any = {};

  private imageRoot: string;

  constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService, private location: Location ) {
    super();
  }

  load() {
    this.imageRoot = this.znodeService.base;
    this.getRows();
  }

  getRows() {
    forkJoin(
        this.znodeService.getProducts(),
        this.znodeService.getCategoryProducts(this.category)
    ).subscribe(
        data => {
            this.products = data[0];
            this.categoryProducts = data[1];

            this.products.forEach(product => {
              let selected = this.categoryProducts.filter(catProd => catProd.ProductId==product.ProductId);
              this.selectedProducts[product.ProductId] = selected.length > 0;
            });

            this._rows = this.products;
            this.filterRows();
        },
        err => this.alertService.error(err)
    );

  }

  isSelected(id: number): boolean {
    let products = this.categoryProducts.filter( catProduct => catProduct.ProductId==id);
    return products.length > 0;
  }

  public cancel() {
    this.location.back();
  }

  save() {
    for(let id in this.selectedProducts){
      if( this.isSelected(Number(id)) != this.selectedProducts[id] ){
        if(this.isSelected(Number(id))){
          // product de-selected
          let categoryProduct = this.categoryProducts.filter( catProd => catProd.ProductId == Number(id) );
          this.znodeService.deleteCategoryProduct(categoryProduct[0].ProductCategoryId).subscribe();
        }
        else {
          // product selected
          let categoryProduct = new ProductCategory();
          categoryProduct.ProductId = Number(id);
          categoryProduct.CategoryId = this.category;

          this.znodeService.createCategoryProduct(categoryProduct).subscribe(
            res => {},
            err => this.alertService.error(err)
          )
        }

      }
    }

    this.alertService.success('Collection Products Updated')
  }

}
