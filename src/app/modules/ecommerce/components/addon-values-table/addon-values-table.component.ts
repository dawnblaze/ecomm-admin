import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { ZnodeService } from '../../../../services/znode.service';
import { AlertService } from '../../../../services/alert.service';

import { AddonValue } from '../../models/addon-value';

@Component({
  selector: 'cb-addon-values-table',
  templateUrl: './addon-values-table.component.html'
})
export class AddonValuesTableComponent extends TableComponent implements OnInit {

  public name: string = "cb-addon-values-table";
  public _rows: AddonValue[];
  public filtered: AddonValue[] = [];
  public search: string = "";
  public searchKey: string = "Title";
  public sort: string = "AddOnValueId";
  public total = 0;

  @Input() public AddonId: number = 0;
  @Output() onCreate: EventEmitter<any> = new EventEmitter();
  @Output() onSelect: EventEmitter<number> = new EventEmitter();

  public constructor( private znodeService: ZnodeService, private router: Router, private alertService: AlertService ) {
    super();
  }

  public load() {
    this.getRows();
  }

  public getRows() {
    this.znodeService.getAddonValues(this.AddonId).subscribe(values => {
      this._rows = values;

      this.filterRows();
    });
  }

  public goToDetails(id: number){
    this.onSelect.emit(id);
  }

  public delete(id: number){
    console.log('Delete Addon Value #'+id);
    this.znodeService.deleteAddonValue(id).subscribe(
      response => {
        console.log(response);
        this.getRows();
      },
      error => {
        console.error(error);
        this.alertService.error(error);
      }
    )
  }

  public copy(addonValueId: number) {
    this.znodeService.getAddonValue(addonValueId)
      .subscribe(addonValue => {
        delete addonValue.AddOnValueId;
        addonValue.Name = 'Copy of ' + addonValue.Name;

        this.znodeService.copyAddonValue(addonValue).subscribe(
          res => {
            this.alertService.success('Add-On Value Copied Successfully');
            this.getRows();
          },
          err => {
            this.alertService.error(err);
          });
      });
  }

  public create() {
    this.onCreate.emit();
  }

  isNumber(e){
    return (e != null && !isNaN(e) && e.toString() !== '');
  }
}
