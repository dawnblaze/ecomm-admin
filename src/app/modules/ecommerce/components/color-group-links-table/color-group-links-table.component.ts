import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';

import { Observable, forkJoin } from 'rxjs';

import { TableComponent } from '../../../corebridge/components/table/table.component';

import { DaimService } from '../../../../services/daim.service';
import { AlertService } from '../../../../services/alert.service';

import { Color } from '../../models/color';
import { ColorGroup } from '../../models/color-group';

@Component({
  selector: 'cb-color-group-links-table',
  templateUrl: './color-group-links-table.component.html'
})
export class ColorGroupLinksTableComponent extends TableComponent implements OnInit, OnChanges {
  @Input() colorGroup: ColorGroup;

  public name: string = "cb-color-group-links-table";
  public _rows: Color[];
  public filtered: Color[] = [];
  public search: string = '';
  public edit: boolean = false;

  constructor(private router: Router, private daimService: DaimService, private alertService: AlertService) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['colorGroup']) {
      this.getRows();
    }
  }

  load() {
    this.getRows();
  }

  getRows() {
    this.edit = false;

    if (this.colorGroup.id) {
      this.daimService.getColorGroup(this.colorGroup.id, true)
        .subscribe(colorGroup => {
          this.colorGroup = colorGroup;
          this._rows = this.colorGroup.colors;
          this.filterRows();
        });
    }
  }

  delete(colorId: number) {
    this.daimService.deleteColorLink(colorId, this.colorGroup.id)
      .subscribe(
        response => this.getRows(),
        error => this.alertService.error(error)
      );
  }

  filterProcess(item: Color): boolean {
    var keys = ["name", "id"];

    for(var key of keys){
      if(item[key] == null){
        continue;
      }
      var value = String(item[key]);

      if( value.toLowerCase().indexOf(this.search.toLowerCase()) !== -1){
        return true;
      }
    }

    return false;
  }
}
