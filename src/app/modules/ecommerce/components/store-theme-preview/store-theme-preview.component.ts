import { Component, OnInit, Input, AfterViewInit, OnChanges, SimpleChanges, SimpleChange, ViewChild, ElementRef } from '@angular/core';
import { ButtonModule, Button } from 'primeng/button';
import { viewClassName } from '@angular/compiler';
import { ZnodeService } from "../../../../services/znode.service";

@Component({
  selector: 'app-store-theme-preview',
  templateUrl: './store-theme-preview.component.html',
  styleUrls: ['./store-theme-preview.component.less']
})
export class StoreThemePreviewComponent implements OnInit, AfterViewInit, OnChanges {

  constructor(private znodeService: ZnodeService) { }
  public loading = true;

  public productData: any[] = [{ Name: "", ImageFile: null, RetailPrice: "" }, { Name: "", ImageFile: null, RetailPrice: "" }, { Name: "", ImageFile: null, RetailPrice: "" }];
  public catData: any[] = [{ Title: "", ImageFile: null }, { Title: "", ImageFile: null }, { Title: "", ImageFile: null }];
  public backgroundImage: any = { top: "none", lower: "none", logo: "none" };
  // see => preview-style-bar.component.ts
  public _styleData = {
    header_bg_color: null,
    heading_text_color: null,
    top_nav_bg_color: null,
    top_nav_text_color: null,
    top_nav_hover: null,
    nav_bg_color: null,
    nav_hover: null,
    input_text: null,
    footer_bg: null,
    footer_text: null,
    primary_button: null,
    secondary_button: null,
    default_hover: null,

    footerData: null,
    banner1: null,
    banner2: null,
    banner3: null,
    logo: null,
    productData: null,
    catData: null,
    font_family: null
  };


  public defaultImage = "url('../../../../../assets/img/noimage.gif')";
  public defaultColor = ""; // if needed
  public fontFamilyClass = "";

  @ViewChild("hoverText", null) hoverText: ElementRef;


  @Input()
  set styleData(data: any) {
    this._styleData = data;
  }

  get styleData() {
    return this._styleData;
  }


  ngOnInit() {

  }

  ngAfterViewInit() {

    //console.log('Child Style Data: ', this.styleData);
  }

  ngOnChanges(changes: SimpleChanges) {
    for (let property in changes) {
      if (property === 'styleData') {
        let cv = changes[property].currentValue;
        //console.log('Current Value: ',cv)

        if (cv.styleData !== undefined) {
          this.styleData = cv.styleData;
          console.log('Style Data Verfied: ', this.styleData)

          this.backgroundImage.top = this.styleData.content && this.styleData.content.Home_Banner ? `url(${this.styleData.content.Home_Banner})` : 'none';
          this.backgroundImage.lower = this.styleData.content && this.styleData.content.Home_Lower_Banner ? `url(${this.styleData.content.Home_Lower_Banner})` : 'none';
          this.backgroundImage.logo = this.styleData.content && this.styleData.content.logo ? `url(${this.styleData.content.logo})` : 'none';


          this.znodeService.getCategories().subscribe(data => {
            let categoriesFeatured = data.filter(prop => {
              return prop.Properties.IsFeatured;

            })

            console.log('Categories Featured: ', categoriesFeatured);
            let catData = categoriesFeatured.slice(0,3);
            for (let i = catData.length - 1; i >= 0; i--) {
              this.catData[i] = catData[i];
            }
          })



          // console.log('StyleData.ProductData: ',this.styleData.productData);
          this.znodeService.getProducts().subscribe(data => {

            let productsFeatured = data.filter(prop => {
              return prop.IsHomepageSpecial;
            })

            console.log('StyData.ProductData Updated: ', productsFeatured);

            let productData = productsFeatured.slice(0,3);
            for (let i = productData.length - 1; i >= 0; i--) {
              this.productData[i] = productData[i];
            }

          }).add(() => { this.loading = false });
          this.setFontFamily()
        }
      }
    }
  }

  updateColor(event) {
    console.log('Trying to update color on hover..', this.styleData, event);
    this.defaultColor = this.hoverText.nativeElement.style.color;
    if(this.styleData && this.styleData.top_nav_hover)
      this.hoverText.nativeElement.style.color = this.styleData.top_nav_hover;
  }

  defaultColorOfText() {
    if (this.defaultColor !== "") {
      console.log('Changing to default color: ', this.defaultColor);
      if(this.styleData && this.styleData.top_nav_text_color)
        this.hoverText.nativeElement.style.color = this.styleData.top_nav_text_color;
    }
  }

  //Emulate hover for footer Text
  updateFooterTextColor(ev) {
    if(this.styleData && this.styleData.default_hover)
      ev.target.style.color = this.styleData.default_hover;
  }

  defaultFooterTextColor(ev) {
    if(this.styleData && this.styleData.footer_text)
      ev.target.style.color = this.styleData.footer_text;
  }

  // Emulate hover for Nav Text
  updateNavTextColor(ev) {
    if(this.styleData && this.styleData.nav_hover)
      ev.target.style.color = this.styleData.nav_hover;
  }

  defaultNavTextColor(ev) {
    ev.target.style.color = "#FFFFFF";
  }

  // Emulate hover for Nav Home Button
  updateNavHomeColor(ev) {
    if(this.styleData && this.styleData.nav_hover)
      ev.target.style.backgroundColor = this.styleData.nav_hover;
  }

  defaultHomeColor(ev) {
    ev.target.style.backgroundColor = "black";
  }

  setFontFamily() {
    if(this.styleData && this.styleData.font_family) {
      let fontFamily = this.styleData.font_family.replace(/\s+/g, '-').toLowerCase();
      fontFamily = fontFamily.replace(/['"]+/g, '')
      this.fontFamilyClass = fontFamily.split(',')[0];
    }
  }
}
