export interface IQuark{
    ID: number;
}
export interface IProton extends IQuark{
    ClassTypeID: number;
    ModifiedDT: Date;
}
export interface IAtom extends IProton{
    BID: number;
}
export interface IActivatableAtom extends IAtom{
    IsActive: boolean;
}
export interface IActivatableDefaultable extends IActivatableAtom{
    IsDefault: boolean;
}
export interface INamed{
    Name: string;
}
export interface ITemporaryQuark{
    TempID?: string;
}
export interface INamedQuark extends IQuark, INamed {}