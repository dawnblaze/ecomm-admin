import { ITemporaryQuark, INamed, IQuark } from "./iatom";

export class Alert {
    type: string = '';
    message: string = '';
    timeout: number = 3000;
    pinned: boolean = false;
    timeoutID?: number;
    goToAction?: () => void
}

export interface DetailedError extends IQuark, ITemporaryQuark, INamed {
    FieldName: string,
    ChildName?: string,
    ErrorMessage: string,
    TabSlug?: string,
    TabTitle?: string,
    goToAction?: () => void
}
