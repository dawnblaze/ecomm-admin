import { SafeResourceUrl } from '@angular/platform-browser'

export class Modal {
    type: string = '';
    message: string = '';
    title: string = '';
    cancelText: string = 'Cancel';
    confirmText: string = 'Confirm';
    iframeUrl?: SafeResourceUrl;
}
