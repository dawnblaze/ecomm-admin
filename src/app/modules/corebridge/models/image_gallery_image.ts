
export interface IimageGalleryImageItem {
    getUniqueIdentifier(): string;
    getImageThumbnail(): string;
}
