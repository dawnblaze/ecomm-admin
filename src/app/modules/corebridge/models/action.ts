import {Subject, Observable} from 'rxjs';

import {IAtom, IQuark} from './iatom';

export interface IExecuteResponse{
    success:boolean;
    id?: number;
    message?: string;
}

export abstract class AbstractAction{
    generateErrorMsg:(entity:IQuark, response:any)=>string = null;
    generateSuccessMsg:(entity:IQuark, response:any)=>string = null;

    public text: string;
    public iconName: string;
    public hint: string;
    public requiredRight: number | null = null;
    public showIfDisabled: boolean = false;
    public WithToolTip: boolean = false;
    public ToolTipText: string;
    public doBeforeExecute:(enity: any, action:AbstractAction) => Observable<boolean> = null;
    public doAfterExecute:(entity: any, action:AbstractAction) => any = null;
    public readonly _onExecute: Subject<IExecuteResponse> = new Subject<IExecuteResponse>();
    public get onExecute() {
        return this._onExecute.asObservable();
    }

    protected executeFx: (AbstractAction) => void;
    public DoExecute(
                    doBefore:(enity: any, action:AbstractAction) => Observable<boolean> = null,
                    doAfter:(entity: any, action:AbstractAction) => any = null
                    ){

        if(doBefore){
            this.doBeforeExecute = doBefore;
        }

        if(doAfter){
            this.doAfterExecute = doAfter;
        }

        if (this.executeFx != null)
            this.executeFx(this);
    }

    protected visible: boolean | null = true;
    public abstract get IsVisible();

    protected enabled: boolean | null = true;
    public abstract get IsEnabled();

    constructor(executeFx: (action: AbstractAction) => void){
        this.executeFx = executeFx;
    }
}

export class PageAction extends AbstractAction{
    public IsInDropdown:boolean = false;
    public DoBeforeAction:() => Observable<boolean> = null;
    public enabledFx: () => boolean;
    public get IsEnabled(){
        if (this.enabledFx != null)
            return this.enabledFx();
        else
            return this.enabled;
    }
    public visibleFx: () => boolean;
    public get IsVisible(){
        if (this.visibleFx != null)
            return this.visibleFx();
        else
            return this.visible;
    }

    constructor(
        text: string, iconName: string, hint: string,
        executeFx: (action: AbstractAction) => void,
        requiredRight: number | null = null,
        showIfDisabled = false,
        visible: boolean | null = true,
        visibleFx: () => boolean = null,
        enabled: boolean | null = true,
        enabledFx: () => boolean = null
        ){
        super(executeFx);
        this.text = text;
        this.iconName = iconName;
        this.hint = hint || text;
        this.requiredRight = requiredRight;
        this.showIfDisabled = showIfDisabled;
        this.visible = visible;
        this.visibleFx = visibleFx;
        this.enabled = enabled;
        this.enabledFx = enabledFx;
    }
    public OnDropdownShown() {}
}

export class RecordAction extends AbstractAction{
    public enabledFx: (records: IAtom[]) => boolean;
    public get IsEnabled(){
        return this.enabled;
    }
    public visibleFx: (records: any[]) => boolean;
    public get IsVisible(){
        return this.visible;
    }

    constructor(
        text: string, iconName: string, hint: string,
        executeFx: (action: AbstractAction) => void,
        requiredRight: number | null = null,
        showIfDisabled = false,
        visible: boolean | null = true,
        visibleFx: (records: any[]) => boolean = null,
        enabled: boolean | null = true,
        enabledFx: (records: IAtom[]) => boolean = null,
        withTooltip: boolean = false
        ){
        super(executeFx);
        this.text = text;
        this.iconName = iconName;
        this.hint = hint || text;
        this.requiredRight = requiredRight;
        this.showIfDisabled = showIfDisabled;
        this.visible = visible;
        this.visibleFx = visibleFx;
        this.enabled = enabled;
        this.enabledFx = enabledFx;
        this.WithToolTip = withTooltip;
    }

    public Refresh(records: any[]){
        if (this.visibleFx != null)
            this.visible = this.visibleFx(records);
        if (this.enabledFx != null)
            this.enabled = this.enabledFx(records);
    }
}
