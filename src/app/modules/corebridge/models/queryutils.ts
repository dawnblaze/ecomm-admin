export class QueryUtils{

    public static ReplaceCurrentHistoryQueryJSONParameter(queryKey: string, queryValue: object){
        window.history.replaceState('', '', QueryUtils.setJSONQueryParam(window.location.href, queryKey, queryValue));
    }
    public static ReplaceCurrentHistoryQueryStringParameter(queryKey: string, queryValue: string){
        window.history.replaceState('', '', this._updateURLParameter(window.location.href, queryKey, queryValue));
    }

    /**
     * Returns a URL where the query parameters string contains a key of param, with a unicode-compliant base64 string value of JSON.stringify(paramVal)
     * @param  {string} url
     * @param  {string} param
     * @param  {object} paramVal
     * @returns string
     */
    public static setJSONQueryParam(url: string, param: string, paramVal: object): string{
        return QueryUtils._updateURLParameter(url, param, QueryUtils.encodeURLSafeUnicodeSafeBase64(JSON.stringify(paramVal)));
    }
    /**
     * Returns an object read from the current URL's query params, JSON.parsed from a unicode-compliant base64 string
     * @param  {string} key
     * @returns object
     */
    public static getJSONQueryParam(key: string): object{
        return QueryUtils._getQueryParam(key, QueryUtils.getJSONFromBase64);
    }
    /**
     * set the query param section of URL to include param=paramVal
     * @param  {} url
     * @param  {} param
     * @param  {} paramVal
     * @returns string
     */
    private static _updateURLParameter(url: string, param: string, paramVal: string|undefined): string{
        var newAdditionalURL = "";
        var tempArray = url.split("?");
        var baseURL = tempArray[0];
        var additionalURL = tempArray[1];
        var temp = "";
        if (additionalURL) {
            tempArray = additionalURL.split("&");
            for (var i=0; i<tempArray.length; i++){
                if(tempArray[i].split('=')[0] != param){
                    newAdditionalURL += temp + tempArray[i];
                    temp = "&";
                }
            }
        }
    
        var rows_txt = '';
        if (paramVal !== undefined){
            rows_txt += temp + param + "=" + paramVal;
        }
        return baseURL + "?" + newAdditionalURL + rows_txt;
    }
    /**
     * Read the current query string and return an object with values decoded with decoder
     * @param  {(str:string)=>string} decoder
     * @returns object
     */
    private static _getQueryParams(decoder: (str: string) => string): object{
        var kvps = window.location.search.substr(1).split('&');
        if (kvps == null || kvps.length < 1) return {};
        var params = {};
        for (var i = 0; i < kvps.length; ++i)
        {
            var kvp=kvps[i].split('=', 2);
            if (kvp.length == 1)
                params[kvp[0]] = "";
            else {
                params[kvp[0]] = decoder(kvp[1].replace(/\+/g, " "));
            }
        }
        return params;
    }
    /**
     * Read the current query string, find `key`, and return `key`'s value as an object decoded with decoder
     * @param  {string} key
     * @param  {(str:string)=>string} decoder
     * @returns object
     */
    private static _getQueryParam(key: string, decoder: (str: string) => string): object{
        var kvps = window.location.search.substr(1).split('&');
        if (kvps == null || kvps.length < 1) return null;
        var result = null;
        for (var i = 0; i < kvps.length; ++i)
        {
            var kvp=kvps[i].split('=', 2);
            if (kvp.length == 2 && kvp[0] == key){
                result = decoder(kvp[1].replace(/\+/g, " "));
                break;
            }
        }
        return result;
    }
    /**
     * Read the current query string, and return whether or not there is a value of `key` in it
     * @param  {string} key
     * @returns boolean
     */
    public static HasQueryParam(key: string): boolean{
        var kvps = window.location.search.substr(1).split('&');
        if (kvps == null || kvps.length < 1) return false;
        
        for (var i = 0; i < kvps.length; ++i)
        {
            var kvp=kvps[i].split('=', 2);
            if (kvp.length == 2 && kvp[0] == key){
                return true;
            }
        }
        return false;
    }
    //https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
    /**
     * Given a string, return a base64 string that has been made URL safe
     * @param  {string} str
     * @returns string
     */
    private static encodeURLSafeUnicodeSafeBase64(str: string): string {
        // first we use encodeURIComponent to get percent-encoded UTF-8,
        // then we convert the percent encodings into raw bytes which
        // can be fed into btoa.
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode(parseInt('0x' + p1));
        })).replace(/\+/g, '-').replace(/\//g, '_').replace(/\=+$/, ''); //https://github.com/RGBboy/urlsafe-base64/blob/master/lib/urlsafe-base64.js
    }
    /**
     * Given a string from the URL, return a decoded string that has been made URL safe
     * @param  {string} str
     * @returns string
     */
    private static decodeURLSafeUnicodeSafeBase64(str: string): string {
        //https://github.com/RGBboy/urlsafe-base64/blob/master/lib/urlsafe-base64.js
        str += Array((5 - str.length % 4)%5).join('=');
        str = str.replace(/-/g, '+').replace(/_/g, '/');        
        // Going backwards: from bytestream, to percent-encoding, to original string.
        return decodeURIComponent(atob(str).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }
    /**
     * Given a string from the URL, return a JSON object
     * @param  {string} str
     * @returns string
     */
    private static getJSONFromBase64(str: string): string {
        let rawJSON = QueryUtils.decodeURLSafeUnicodeSafeBase64(str);
        if (rawJSON === 'null' || rawJSON === 'undefined'){
            return null;
        }
        else {
            return JSON.parse(rawJSON);
        }
    }
    /**
     * Return all query params as any object decoded using the URL safe base64 decoder
     * @returns object
     */
    public static getBase64EncodedQueryParams(): object{
        return this._getQueryParams(this.decodeURLSafeUnicodeSafeBase64);
    }
    /**
     * Return all query params as any object decoded using the basic decodeURIcomponent decoder
     * @returns object
     */
    public static getUriEncodedQueryParams(): any{
        return this._getQueryParams(decodeURIComponent);
    }
}