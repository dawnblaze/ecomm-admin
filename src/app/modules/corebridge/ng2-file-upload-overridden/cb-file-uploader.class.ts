import { FileUploader, FileUploaderOptions, FileItem, ParsedResponseHeaders } from "../../../../../node_modules/ng2-file-upload";

/**
 * this is derived from the source code of ng2file upload under commit fb48f4411d163a4219ffae12bfa24ce7f803e837
 * 
 * https://github.com/valor-software/ng2-file-upload
 */
export class cbFileUploader extends FileUploader{
  constructor(options: FileUploaderOptions){
    super(options);
    this.uploadAll = this.cb_uploadAll.bind(this);
    this.uploadItem = this.cb_uploadItem.bind(this);
    this._onCompleteItem = this.cb__onCompleteItem.bind(this);
  }

  private cb_uploadAll(){
    let items = this.getNotUploadedItems().filter((item: FileItem) => !item.isUploading);
    if (!items.length) {
      return;
    }
    items.map((item: FileItem) => item._prepareToUploading());
    (items as FileItem[]).forEach(item=>item.upload());
  }

  private cb_uploadItem(value: FileItem): void {
    let index = this.getIndexOfItem(value);
    let item = this.queue[ index ];
    let transport = this.options.isHTML5 ? '_xhrTransport' : '_iframeTransport';
    item._prepareToUploading();
    this.isUploading = this.queue.filter(item=>item.isUploading || item.isReady).length > 0;
    (this as any)[ transport ](item);
  }

  public cb__onCompleteItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): void {
    item._onComplete(response, status, headers);
    this.onCompleteItem(item, response, status, headers);
    let nextItems:FileItem[] = this.getReadyItems();
    let previsUploading = this.isUploading;
    this.isUploading = this.queue.filter(item=>item.isUploading || item.isReady).length > 0;
    if((nextItems||[]).length){
      nextItems.forEach(nextItem=>nextItem.upload());
      return;
    }
    if(previsUploading && !this.isUploading){
      this.onCompleteAll();
    }
    this.progress = this._getTotalProgress();
    this._render();
  }  
}