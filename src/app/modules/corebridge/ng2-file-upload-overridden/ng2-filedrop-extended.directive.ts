import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { FileDropDirective } from 'ng2-file-upload';

export type fileDropValidator = (files:File[])=>boolean;
/**
 * https://github.com/valor-software/ng2-file-upload/blob/development/src/file-upload/file-drop.directive.ts
 */
@Directive({
  selector: '[ng2FileDropX]'
})
export class Ng2FiledropExtendedDirective extends FileDropDirective {

  constructor(el:ElementRef) { 
    super(el)
  }
  @Input() isDefaultImage: boolean = false;
  @Input() fileDropValidator:fileDropValidator = null;

  @HostListener('drop', [ '$event' ])
  public onDrop(event: any): void {
    let transfer = this._getTransfer(event);
    if (!transfer) {
      return;
    }

    if(this.isDefaultImage){
      if(transfer.files && transfer.files.length > 1)
      {
        this.fileOver.emit(false);
        this._preventAndStop(event);
        return;
      }
    }
      
    if(!this.isValidDrop(transfer.files)){
      this.fileOver.emit(false);
      this._preventAndStop(event);
      return;
    }

    let options = this.getOptions();
    let filters = this.getFilters();
    this._preventAndStop(event);
    this.uploader.addToQueue(transfer.files, options, filters);
    this.fileOver.emit(false);
    this.onFileDrop.emit(transfer.files);
  }

  private isValidDrop(fileList:FileList) {
    if(!this.fileDropValidator){
      return true;
    }
    const files = Array.from(fileList);
    return this.fileDropValidator(files);
  }

}
