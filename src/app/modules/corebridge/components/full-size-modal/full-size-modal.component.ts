import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'cb-full-size-modal',
  templateUrl: './full-size-modal.component.html',
  styleUrls: ['./full-size-modal.component.less']
})
export class FullSizeModalComponent implements OnInit {

  @Input()
  public displayDialog: boolean = false;

  @Input() isLoading:boolean = false;

  @Output()
  public onCloseDialog = new EventEmitter<boolean>();

  @Input()
  public backButtonText: string = '';

  constructor() { }

  ngOnInit() {
  }

}
