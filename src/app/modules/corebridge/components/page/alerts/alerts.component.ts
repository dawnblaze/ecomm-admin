import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';
import {filter} from 'rxjs/operators';

import { Alert, DetailedError } from '../../../models/alert';
import { AlertService } from '../../../../../services/alert.service';

@Component({
  selector: 'cb-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.less']
})
export class AlertsComponent implements OnInit, OnDestroy {

  public _toggleOn: boolean = false;
  public set toggleOn(toggleOn) {
    this.alertService.toggleErrorDetails(toggleOn);
  }
  public get toggleOn() {
    return this._toggleOn;
  }
  public _toggleConsole: boolean = true;

  public alerts:Alert[];
  public detailedAlerts:DetailedError[] = [];
  public detailedErrorsSummary: any[] = [];
  public hiddenDetailedErrorsSummary: any[] = []

  public iconType:any = {
    error: 'fa-exclamation-circle',
    success: 'fa-check-circle',
    addition: 'fa-plus',
    warning: 'fa-exclamation-triangle',
    info: 'fa-info-circle'
  }

  constructor(public alertService: AlertService, private router: Router) {
    router.events.pipe(filter((event) => event instanceof NavigationEnd))
    .pipe(untilDestroyed(this)).subscribe((event:NavigationEnd) => {
      this.alerts.forEach(a => {
        let shortTimeout = 3000;
        if (a.timeoutID != null){
          window.clearTimeout(a.timeoutID);
          a.timeout = shortTimeout;
          this.startTimeout(a);
        }
      });
    });
  }

  ngOnInit() {
    this.alerts = new Array<Alert>();
    this.alertService.noteAdded.pipe(untilDestroyed(this)).subscribe(note => {
        this.alerts.push(note);
        if(note.timeout > 0){
          setTimeout(() => {
            if (!note.pinned) {
              this.removeAlert(note);
            }
          }, note.timeout);
        }
    });

    this.alertService.detailedNoteAdded.pipe(untilDestroyed(this)).subscribe(note => {
      this.detailedErrorsSummary = [];
      let tempDetailedErrorsSummaryLookup = {};
      (note||[]).forEach(error => {
        if (error.TabSlug && !tempDetailedErrorsSummaryLookup[error.TabSlug]){
          tempDetailedErrorsSummaryLookup[error.TabSlug] = true;
          this.detailedErrorsSummary.push({
            key: error.TabSlug,
            gotoActions: error.goToAction ? [{ type: 'function', value: error.goToAction}] : [{ type: 'tabs', value: error.TabSlug }],
            summaryText: 'Issues need attention on ' + error.TabTitle + '. ',
            gotoText: 'View',
          });
        }
        let path:string[] = [];
        if(error.TabTitle)
          path.push(error.TabTitle)
        if (error.Name)
          path.push(error.Name)
        if (error.FieldName)
          path.push(error.FieldName)
        error['path'] = path;
      });
      this.detailedAlerts = note;
    });

    this.alertService.detailedAlertHideSummary.pipe(untilDestroyed(this)).subscribe(summaryKeyList => {
      this.hiddenDetailedErrorsSummary = summaryKeyList;

    });

    this.alertService.showErrorDetails.pipe(untilDestroyed(this)).subscribe(showErrorDetails => {
      this._toggleOn = showErrorDetails;
    });

    this.alertService.detailedAlertNavigationAdded.pipe(untilDestroyed(this)).subscribe((gotoActions) => {
      gotoActions.forEach(navAction => {
        if(navAction.type === 'function'){
          navAction.value();
        }
      });
    });
  }

  public isDetailedErrorSummaryHidden(summaryKey) : boolean {
    return this.hiddenDetailedErrorsSummary.indexOf(summaryKey) > -1;
  }

  private startTimeout(alert: Alert){
    alert.timeoutID = window.setTimeout(() => {
      this.removeAlert(alert);
    }, alert.timeout);
  }

  ngOnDestroy(){}

  removeAlert( alert: Alert ){
    var index = this.alerts.indexOf(alert);
    if(index >= 0) {
      this.alerts.splice(index, 1);
    }
  }

  navigateToError(gotoActions: any[]) {
    this.alertService.navigate(gotoActions);
  }
}
