import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../../../services/auth.service';
import { ThemeService } from '../../../../../services/theme.service';
import { CesaService, MisTypes } from '../../../../../services/cesa.service';

import { User } from '../../../../ecommerce/models/user';

@Component({
  selector: 'cb-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

  @Input() overlay:boolean = false;
  @Output() sidebarToggle: EventEmitter<boolean> = new EventEmitter<boolean>();
  private user: User;

  public modules: string[] = ['E-Commerce'];
  public currentModule = 'E-Commerce';
  public helpSupportOptions: any[];
  private mistype: number;

  public alerts: any = [
    {
      title: 'INV-SLC-6968',
      path: '/dashboard',
      icon: 'paper',
      read: false,
      date: '4m'
    },
    {
      title: 'INV-SLC-6967',
      path: '/dashboard',
      icon: 'paper',
      read: true,
      date: '15m'
    },
    {
      title: 'INV-SLC-6952',
      path: '/dashboard',
      icon: 'paper',
      read: true,
      date: '6h'
    },
    {
      title: 'INV-SLC-6948',
      path: '/dashboard',
      icon: 'paper',
      read: true,
      date: '8h'
    }
  ];

  public messages: any = [
    {
      sender: "Jason Heppler",
      content: "Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.",
      date: "4m",
      img: "https://placekitten.com/300/300",
      read: false,
      path: '/dashboard'
    },
    {
      sender: "Derek Kopp",
      content: "Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.",
      date: "5m",
      img: "https://placekitten.com/301/301",
      read: false,
      path: '/dashboard'
    },
    {
      sender: "Jason Heppler",
      content: "Capitalise on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.",
      date: "8m",
      img: "https://placekitten.com/300/300",
      read: false,
      path: '/dashboard'
    }
  ];

  constructor(private router:Router, private authService:AuthService, public themeService: ThemeService, private cesaService: CesaService) {
  }

  ngOnInit() {
    this.user = this.authService.getUser();
    this.mistype = this.cesaService.getMisType();

    // initialize the help options for specific mistype
    this.helpSupportOptions = this.generateHelpOptions();
  }

  getCorebridgeHelpOptions() {
    return [
      {
        name: 'Knowledgebase ',
        link: 'https://support.corebridge.net/solution/categories/19000100321'
      }
    ];
  }

  getCyriousHelpOptions() {
    return [
      {
        name: 'Knowledgebase',
        link: 'http://www.cyrious.com/customer/training/setup/ecommerce-training/'
      }, {
        name: 'Support Request',
        link: 'https://www.wrike.com/frontend/requestforms/index.html?token=eyJhY2NvdW50SWQiOjE4MDQ2MzQsInRhc2tGb3JtSWQiOjE0NDE4NX0JNDY4NTcwNDk3NzU3MgkzYWQwY2I5YzY2M2Y3ZGMzMzhiYzMzNjdkMTMwNWY0NjMzYTE5ZDUwODMyMTA1ZTgwMjhkNTY4YmYwZmY0NDNk'
      }
    ];
  }

  generateHelpOptions() {

    let helpOptions = [];

    if (this.mistype === MisTypes.CoreBridge) {
      helpOptions = this.getCorebridgeHelpOptions();
    } else if (this.mistype === MisTypes.Cyrious) {
      helpOptions = this.getCyriousHelpOptions();
    }

    return helpOptions;
  }

  showSidebar() {
    this.overlay = !this.overlay;
    this.sidebarToggle.emit(this.overlay);
  }
}
