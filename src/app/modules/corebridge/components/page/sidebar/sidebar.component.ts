import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { HostBinding, HostListener } from "@angular/core";

import { CoreBridgeService } from '../../../../../services/corebridge.service';
import { CesaService, MisTypes } from "../../../../../services/cesa.service";
import { AuthService } from '../../../../../services/auth.service';
import { ZnodeService } from '../../../../../services/znode.service';
import { DomainService } from '../../../../../services/domain.service';

@Component({
  selector: 'cb-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent implements OnInit {
  @Input() overlay: boolean;
  @Output() sidebarToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  @HostBinding('class.closed') sidebarCollapsed:boolean = false;
  private misType: number;
  private misText: string;

  menu:any = [
    // this.menu[0]
    {
      title: 'My Stores',
      icon: 'fal fa-shopping-cart',
      path: '/ecommerce/storefronts'
    },
    // this.menu[1]
    {
      title: 'Merchandise',
      icon: 'fal fa-boxes-alt',
      path: '/ecommerce/merchandise/catalogs',
      children: [
        {
          title: 'Catalogs',
          icon: 'catalog',
          path: '/ecommerce/merchandise/catalogs'
        },
        {
          title: 'Collections',
          icon: 'collections',
          path: '/ecommerce/merchandise/collections'
        },
        {
          title: 'Products',
          icon: 'inventory',
          path: '/ecommerce/merchandise/products'
        },
        {
          title: 'Add-Ons',
          icon: 'plugin',
          path: '/ecommerce/merchandise/addons'
        }
      ]
    },
    // this.menu[2]
    {
      title: 'Orders',
      icon: 'fal fa-clipboard-list',
      path: '/ecommerce/orders'
    },
    // this.menu[3]
    {
      title: 'Customers',
      icon: 'fal fa-address-book',
      path: '/ecommerce/customers/companies',
      children: [
        {
          title: 'Companies',
          icon: 'companies',
          path: '/ecommerce/customers/companies'
        },
        {
          title: 'Contacts',
          icon: 'customers',
          path: '/ecommerce/customers/contacts'
        }
      ]
    },
    // this.menu[4]
    {
      title: 'Reports',
      icon: 'fal fa-chart-bar',
      path: '/ecommerce/reports/customers_receivables',
      children: [
        {
          title: 'Customers',
          icon: 'parts',
          path: '/ecommerce/reports/customers_receivables'
        },
        {
          title: 'Sales',
          icon: 'parts',
          path: '/ecommerce/reports/sales'
        }
      ]
    },
    // // this.menu[5]
    // {
    //   title: 'Settings',
    //   icon: 'fal fa-sliders-h',
    //   path: '/ecommerce/settings/colors',
    //   children: [
    //     {
    //       title: 'Shipping Methods',
    //       icon: 'modifiers',
    //       path: '/ecommerce/shipping-methods'
    //     },
    //     {
    //       title: 'Shipping Boxes',
    //       icon: 'modifiers',
    //       path: '/ecommerce/shipping-boxes',
    //       // misType: standard property that checks the visibility of menu item to sepicific mis system
    //       // if not define, menu item is visible to any mis type
    //       misType: MisTypes.CoreBridge
    //     },
    //     {
    //       title: 'Email Templates',
    //       icon: 'envelope',
    //       path: '/ecommerce/email-templates'
    //     },
    //     // Added for ROG-99
    //     {
    //       title: `Product Defaults`,
    //       icon: 'envelope',
    //       path: '/ecommerce/product-defaults'
    //     }
    //   ]
    // },


    // this.menu[6]
    
    {
      title: 'Settings',
      icon: 'fal fa-sliders-h',
      path: '/ecommerce/settings'
    },
    {
      title: 'Dev Support',
      custom_icon: 'devSupport',
      path: '/developer',
    }

  ];

  activeItem:string = this.menu[0].title;
  openItem:string = "";
  private shippingBoxMenu: any;
  public eCodEnabled: boolean = false;

  constructor(private znodeService: ZnodeService, private domain: DomainService, private corebridgeService: CoreBridgeService, private cesaService: CesaService, private authService: AuthService) {
  }

  ngOnInit() {
    let eCodEnabled: boolean = false;
    this.misType = this.cesaService.getMisType();
    this.menu = this.filterMenuItemByMisType(this.menu);

    // this.znodeService.getConfigSettingByBID(this.domain.bid).subscribe(data => {
    //   this.toggleShippingBoxMenu(data.EnableCarrierRate);
    // });

    // Wait for any config settings changes from dev support page
    // this.znodeService.passConfigSetting.subscribe(
    //   config => this.toggleShippingBoxMenu(config.EnableCarrierRate));

    if(this.misType == MisTypes.Cyrious)
    {
      // eCodEnabled = this.cesaService.getECodEnabled();
      // if(eCodEnabled) {
      //   this.createEcodMenus();
      // }
      this.misText = "Control";

      // this.menu[8].children.push(
      //   {
      //     title: `Product Defaults`,
      //     icon: 'envelope',
      //     path: '/ecommerce/product-defaults'
      //   }
      // );
    }
    else {
      // this.corebridgeService.getZnodeOnlineDesignerEnabled().subscribe(enabled => {
      //   eCodEnabled = enabled;
      //   if (eCodEnabled) {
      //     this.createEcodMenus();
      //   }
      // });
      this.misText = "Corebridge";
    }
  }

  toggleShippingBoxMenu(isCarrierRateEnabled: boolean) {
    for (let x = 0; x < this.menu.length; x++) {
      if (this.menu[x].title === 'Settings') {
        let settingsMenu = this.menu[x].children;
        let isShippingBoxExist: boolean = true;

        for (let y = 0; y < settingsMenu.length; y++) {
          if (settingsMenu[y].title === 'Shipping Boxes'){
            isShippingBoxExist = true;
            this.shippingBoxMenu = settingsMenu[y];

            if (!isCarrierRateEnabled) {
              let index = settingsMenu.indexOf(settingsMenu[y]);
              settingsMenu.splice(index, 1);
            }
            break;
          }

          isShippingBoxExist = false;
        }
        this.menu[x].children = settingsMenu;

        // If shipping box does not exist in the settings menu, add it again
        if (!isShippingBoxExist && isCarrierRateEnabled) this.menu[x].children.splice(1, 0, this.shippingBoxMenu);

        break;
      }
    }
  }

  filterMenuItemByMisType(children: any) {
    let that = this;
    let userName = this.authService.getUser().UserName;

    return children.filter( function(menuItem) {
      if (menuItem.title === 'Dev Support') {
        if (that.misType === MisTypes.Cyrious
          && (userName === 'administrator')) {
            return true;
        }
        else if (that.misType === MisTypes.CoreBridge
        && (userName === 'cbms.support'
        || userName === 'cbms.admin')) {
          return true;
        }
        else {
          return false;
        }
      }

      if (menuItem.misType === undefined) {
        return true;
      }

      else menuItem.misType === that.misType
    });

  }

  createEcodMenus() {
    let ecodMenu = {
      title: 'Online Designer',
      icon: 'modifiers',
      path: '/ecommerce/settings/colors',
      children: [
        {
          title: 'Colors',
          icon: 'modifiers',
          path: '/ecommerce/settings/colors'
        },
        {
          title: 'Color Groups',
          icon: 'modifiers',
          path: '/ecommerce/settings/color-groups'
        },
        {
          title: 'Fonts',
          icon: 'modifiers',
          path: '/ecommerce/settings/fonts'
        },
        {
          title: 'Font Groups',
          icon: 'modifiers',
          path: '/ecommerce/settings/font-groups'
        },
        {
          title: 'Design Editor Presets',
          icon: 'modifiers',
          path: '/ecommerce/settings/design-editor-presets'
        }
      ]
    };
    this.menu[5].children.push(ecodMenu);
  }
  triggerMenuItem( item: any, subItem: any ) {
    this.activeItem = item.title;

    if(subItem){
      if(!this.sidebarCollapsed && item.children.length > 0 && this.openItem != item.title + '.' + subItem.title) {
        this.openItem = item.title + '.' + subItem.title;
      } else {
        this.openItem = item.title;
      }
    }
    else {
      if(!this.sidebarCollapsed && item.children.length > 0 && !this.openItem.startsWith(item.title)) {
        this.openItem = item.title;
      } else {
        this.openItem = "";
      }
    }
  }

  toggleSidebar(){
    this.sidebarCollapsed = !this.sidebarCollapsed;
    document.querySelector('.cb_root').classList.toggle('closed');
  }

  menuItemClicked(hasChild: boolean) {
    if (!hasChild) {
      this.overlay = false;
      this.sidebarToggle.emit(this.overlay);
    }
  }
}
