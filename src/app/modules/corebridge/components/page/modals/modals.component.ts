import { Component, OnInit } from '@angular/core';

import { Modal } from '../../../models/modal';
import { ModalService } from '../../../../../services/modal.service';

@Component({
  selector: 'cb-modals',
  templateUrl: './modals.component.html',
  styleUrls: ['./modals.component.less']
})
export class ModalsComponent implements OnInit {

  public modal: Modal = new Modal();
  public message: string = '';
  public open: boolean = false;

  constructor(private modalService: ModalService) {}

  ngOnInit() {
    this.modalService.modalAdded.subscribe(modal => {
        this.openModal(modal);
    });
    this.modalService.modelOpen.subscribe(open => {
        if (!open) {
          this.modalService.respond(true);
          this.closeModal();
        }
    });
  }

  openModal(modal: Modal){
    this.modal = modal;
    this.open = true;
  }

  closeModal(){
    this.open = false;
    this.modal = new Modal();
  }

  confirm(){
    this.modalService.respond(true);
    this.closeModal();
  }

  cancel(){
    this.modalService.respond(false);
    this.closeModal();
  }

}
