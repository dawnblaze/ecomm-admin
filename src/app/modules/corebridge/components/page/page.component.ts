import { Component, Input, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AlertsComponent } from './alerts/alerts.component';
import { AuthService } from '../../../../services/auth.service';
import { CoreBridgeService } from '../../../../services/corebridge.service';
import { ThemeService, Theme, CyriousTheme, CorebridgeTheme } from '../../../../services/theme.service';
import { CesaService, MisTypes } from "../../../../services/cesa.service";

@Component({
  selector: 'cb-page',
  templateUrl: './page.component.html',
  providers: [AuthService]
})
export class PageComponent implements OnInit {
  public loading: boolean = true;
  public loggedIn: any;
  public sidebarToggle: boolean = false;
  @Input() breadcrumbs: any;

  constructor(private router:Router, private authService:AuthService, private corebridgeService: CoreBridgeService, private themeService: ThemeService, private cesaService: CesaService) {}

  ngOnInit() {
    if (!this.authService.isLoggedIn()) {
      this.loggedIn = false;
      this.router.navigate(['/logout']);
    } else {
      this.loggedIn = true;
    }

    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) return;

      window.scrollTo(0, 0);
    });


    this.cesaService.getTenantInfo().subscribe(response => {
      let theme: Theme;

      if(response.misType == MisTypes.Cyrious //Cyrious Control
        || window.location.host.indexOf('cyrious.com') !== -1){ //Temporary theme override for cyrious tenants not yet using CESA
        theme = new CyriousTheme();
      }
      else { //CoreBridge MS
        theme = new CorebridgeTheme();
      }
      sessionStorage.setItem(CesaService.BidKey, response.bid.toString());
      sessionStorage.setItem(CesaService.MisTypeKey, response.misType.toString());
      sessionStorage.setItem(CesaService.MisDomainKey, response.misDomain);

      this.themeService.applyTheme(theme);
      this.loading = false;
    }, (error) => {
      this.themeService.applyTheme(new CorebridgeTheme());
      this.loading = false;
    });
    this.authService.loggedInStatus.subscribe( status => this.loggedIn = status['loggedIn'] );
/*
    this.corebridgeService.getTenantCssFile().subscribe((res) => {
      let theme: Theme;

      switch(res) {
          case 'cyrious':
            theme = new CyriousTheme();
            break;
          case 'corebridge':
          default:
            theme = new CorebridgeTheme();
            break;
      }

      this.themeService.applyTheme(theme).subscribe(() => {
        this.loading = false;
      });
    }, (err) => {
      let theme = new CorebridgeTheme();

      this.themeService.applyTheme(theme).subscribe(() => {
        this.loading = false;
      });
    });
*/
  }

  onSidebarToggle(toggle) {
    this.sidebarToggle = toggle;
  }
}
