import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  AfterViewInit,
  OnChanges
} from "@angular/core";
import { Domain } from "../../../ecommerce/models/domain";

export enum RowAction {
  Preview,
  Clone,
  Activate,
  Delete,
  Remove
}

export class Column {
  field: string;
  header: string;
  class?: "text-center";
  sortField?: string;
  sortArrows?: string;
}

@Component({
  selector: "cb2-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.less"]
})
export class Table2Component implements OnInit, AfterViewInit, OnChanges {
  @Input() public cols: Array<Object>;
  @Input() public isTableCheckbox: boolean;
  @Input() public idField: string = "ID";
  @Input() public rowActions = [];
  @Input() public canActivate: boolean = false;
  @Input() public appendInactiveField: string = "Name";
  @Input() public compact: boolean = false;
  @Input() public width: string = "auto";
  @Input() public showFooter: boolean = true;
  public rowAction = RowAction;
  public isOpenAction: number;
  private domains: Domain[] = [];

  @Input()
  set value(rows: any[]) {
    this._rows = rows;

    if (typeof rows !== "undefined" && this.total == 0) {
      this.total = rows.length;
    }
    if (this.selected.length) {
      let checkRows = this._rows.filter(row =>
        this.selected.find(x => x[this.idField] == row[this.idField])
      );
      checkRows.forEach(row => {
        row["isChecked"] = true;
      });
      this.selected = checkRows.slice(0);
      this.selectedChange.emit();
    }
  }

  @Output() onLoaded: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() previewEvent = new EventEmitter<number>();
  @Output() copyEvent = new EventEmitter<number>();
  @Output() gotoDetail = new EventEmitter<number>();
  @Output() deleteEvent = new EventEmitter<number>();
  @Output() toggleActiveEvent = new EventEmitter<any>();
  @Output() selectedChange = new EventEmitter<any[]>();

  public name: string = "cb-table";
  public _rows: any[];
  public selected: any[] = [];
  public search: string;
  public searchKey: string = "Name";
  public sortAsc: boolean = true;
  public sortString: string = "";
  public sort: string = "";
  public page: number = 1;
  public count: number = 50;
  public total: number = 0;
  public rememberPage: boolean = true;

  public checkedItem: Object;

  constructor() {}

  ngOnInit() {}

  ngOnChanges() {}

  ngAfterViewInit() {}

  onClickItem(item: Object, state: boolean) {
    if (!state) {
      if (this.selected.removeWhere(x => x[this.idField] == item[this.idField]))
        this.selectedChange.emit([item]);
    } else if (!this.selected.find(s => s == item)) {
      this.selected.push(item);
      this.selectedChange.emit([item]);
    }
  }

  setSelectAll(on: boolean) {
    if (on) {
      (this._rows || []).forEach(x => {
        x["isChecked"] = true;
      });
      this.selected = (this._rows || []).slice(0);
    } else {
      this.selected = [];
      (this._rows || []).forEach(x => {
        x["isChecked"] = false;
      });
    }
    this.selectedChange.emit(this.selected);
  }

  previewEventEmit(rowData: any) {
    this.previewEvent.emit(rowData[this.idField]);
  }
  copyEventEmit(rowData: any) {
    this.copyEvent.emit(rowData[this.idField]);
  }
  toggleActive(rowData: any) {
    if (this.canActivate) this.toggleActiveEvent.emit(rowData);
  }
  deleteEventEmit(rowData: any) {
    this.deleteEvent.emit(rowData[this.idField]);
  }

  public onRowClickHandler(event: Event, rowData: any) {
    const target = event.target as HTMLElement;
    if (
      target.parentElement.nodeName === "TR" ||
      target.parentElement.nodeName === "TD"
    )
      this.gotoDetail.emit(rowData[this.idField]);
  }
}
