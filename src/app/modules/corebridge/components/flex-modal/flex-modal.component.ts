import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { RowAction } from "../../../corebridge/components/table2/table.component";

export class ButtonTexts {
  cancelText?: string;
  saveText?: string;
  saveAndClose?: string;
}
@Component({
  selector: 'cb-flex-modal',
  templateUrl: './flex-modal.component.html',
  styleUrls: ['./flex-modal.component.less']
})
export class FlexModalComponent implements OnInit {

  public rowAction = RowAction;

  @Input()
  public displayDialog: boolean = false;

  @Input() isLoading: boolean = false;

  @Input() isSubmitValid: boolean = true;

  @Input() isCancelDisabled: boolean = false;

  @Input() rowActions: RowAction[] = [];

  @Input() isActive: boolean = true;

  @Output()
  public onCloseDialog = new EventEmitter<boolean>();

  @Output()
  public onSave = new EventEmitter<any>()

  @Output()
  public onRemove = new EventEmitter<any>();

  @Output()
  public onSetActive = new EventEmitter<any>();

  @Input() modalTitle: string;
  @Input() styleClass: string;

  @Input()
  public buttonTexts: ButtonTexts = {};

  @Input() width: string = '900px';
  @Input() contentStyle: object;

  constructor() { }

  ngOnInit() {
  }

  close() {
    this.onCloseDialog.emit(false)
  }

  saveAndClose() {
    this.onCloseDialog.emit(true)
  }

  save() {
    this.onSave.emit(true)
  }

  remove() {
    this.onRemove.emit();
  }

  setActive() {
    this.onSetActive.emit()
  }




}
