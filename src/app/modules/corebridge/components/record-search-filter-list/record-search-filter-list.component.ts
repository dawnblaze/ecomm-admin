import { untilDestroyed } from 'ngx-take-until-destroy';
import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'cb-record-search-filter-list',
  templateUrl: './record-search-filter-list.component.html',
  styleUrls: [
    './record-search-filter-list.component.less'
  ]
})
export class RecordSearchFilterListComponent implements OnInit {


    @Input() customResultsCount:number = null;

    @Input() public results: Array<any> = null;

    ngOnInit() {

    }
}
