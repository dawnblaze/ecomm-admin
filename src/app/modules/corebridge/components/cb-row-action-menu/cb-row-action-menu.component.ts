import { untilDestroyed } from 'ngx-take-until-destroy';
import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ContentChildren, TemplateRef, QueryList} from '@angular/core';
import {RecordAction, AbstractAction} from "../../models/action";
import {PopoverDirective} from "ngx-bootstrap";
import {EventService} from "../../../../services/event.service";
import { Observable } from 'rxjs';
import { CbTemplateDirective } from '../../directives/cb-template.directive';
import { IDictionary } from '../../models/dictionary';

@Component({
    selector: 'cb-row-action-menu',
    templateUrl: './cb-row-action-menu.component.html',
    styleUrls: ['./cb-row-action-menu.component.less']
})
export class CbRowActionMenuComponent implements OnInit {
    isOpen: boolean;
    @Input()
    IdActiveAction: number;

    @Output()
    previewDomain: EventEmitter<any> = new EventEmitter<any>()

    public dropdownActionsValue: RecordAction[] = [];

    public clickedCorrectSource: boolean;

    @ContentChildren(CbTemplateDirective) templates:QueryList<CbTemplateDirective>;
    public templateDictionary:IDictionary<TemplateRef<any>> = {};

    @Output()
    ngClick: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

    @ViewChild('rowPopover')
    public popover: PopoverDirective;

    @ViewChild('rowActionMenuBtn')
    public popoverTriggerBtn: Element;

    @Output()
    dropdownActionsChange: EventEmitter<RecordAction[]> = new EventEmitter<RecordAction[]>();

    @Input()
    public set dropdownActions(value: RecordAction[]) {
        this.dropdownActionsValue = value;
        this.dropdownActionsChange.emit(value);
    };

    @Output() clickAction = new EventEmitter<Event>();

    public get dropdownActions() {
        return this.dropdownActionsValue;
    }

    @Input()
    public doBeforeExecute:(enity: any, action:AbstractAction) => Observable<boolean> = null;

    @Input()
    public doAfterExecute:(entity: any, action:AbstractAction) => any = null;

    @Input()
    public customClass: string = 'row-action-menu';

    ngOnDestroy(){}

constructor(public _event: EventService) {}

    ngOnInit() {

    }

    ngAfterContentInit(){
        if(this.templates){
            this.templates.forEach(tpl => this.templateDictionary[tpl.name]=tpl.template);
        }
    }

    toggle() {
        if (!this.popover) {
            return;
        }
        /** Not angular digested because of the issue with "open" method **/
        if (this.popover.isOpen) {
            this.hide();
        } else {
            this.open();
        }
    }

    hide() {
        setTimeout(() => {
            this.popover.isOpen = false;
        }, 200);
    }

    /**
     * Ngx-Popover has an issue with "blur" event. Used jQuery to simulate the feature and avoid conflicting feature
     * @param timeout
     */
    open(timeout = 200) {

        const popover = $(document).data('cb-row-action-popover');

        if (popover) {
            popover.hide();
        }

        setTimeout(() => {
            $(document).data('cb-row-action-popover', this.popover);
            this.popover.isOpen = true;
        }, timeout);


    }

    onSelect($event: MouseEvent, action: RecordAction){
        /** Usually turbo tables has ngClick event **/
        if(this.ngClick.observers.length){
            this.clickAction.emit($event);
            return;
        }
        /** For other component usage - where it's not insider turbo table **/
        action.onExecute.pipe(untilDestroyed(this)).subscribe();
    }

    onClickPreviewDomain() {
        console.log('onclick preview')
        this.previewDomain.emit()
    }
}
