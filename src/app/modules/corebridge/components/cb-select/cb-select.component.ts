import { Component, Provider, forwardRef, Input, Output, OnInit, AfterContentInit, EventEmitter, HostListener, ViewChild, ElementRef, OnDestroy } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR, Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { Subject, ReplaySubject } from "rxjs";
import { BsDropdownDirective } from "ngx-bootstrap";
import { EventService } from "../../../../services/event.service";
import { untilDestroyed } from "ngx-take-until-destroy";

@Component({
  selector: 'cb-select',
  templateUrl: './cb-select.component.html',
  styleUrls: ['./cb-select.component.less'],
  host: {
    '[class.cb-widget]': 'true',
    '[class.cb-widget-empty]': '(ngModel===null || ngModel===undefined) && !allowNullInput',
    '[class.cb-widget-focused]': 'focused',
    '[class.cb-widget-dropdown-open]': 'dropdownIsOpen'
  },
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CbSelectComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CbSelectComponent),
      multi: true
    }
  ]
})
export class CbSelectComponent implements OnInit, OnDestroy, ControlValueAccessor, Validator, IDropdownKeyboardNavigatorHost {

  private sbjWriteOrChangeValue = new ReplaySubject<any>(1);
  public readonly keyboard: DropdownKeyboardNavigator;
  constructor(public event: EventService) {
    this.keyboard = new DropdownKeyboardNavigator(this);

    this.typingDebouncer
    .pipe(untilDestroyed(this), debounceTime(750))
    .subscribe(() => this.onTypingReset());
  }

  @Input() previewDisplayText:string = null;//https://corebridge.atlassian.net/browse/END-5381
  /** filter predicate to get list of hiddenOptions */
  @Input() fxHideOption = (cbSelect:CbSelectComponent,options:any) => false;
  @Input() public Label: string = '';
  @Input() tabindex: number;

  public hiddenOptions: any[] = [];
  private _options: any[] = [];
  public get options(): any[] {
    return this._options;
  }
  @Input() public set options(setVal: any[]) {
    this._options = setVal;
    this.refreshInitiallySelectedIndex();
    this.setDisplayText();
    this._updateOptions();
  }

  @Input() hasBottomLink: boolean = false;
  @Input() bottomLinkText: string;
  @Input() restrainedWidth: boolean = false;

  @Input()
  public appendDropdownTo: string = '';

  @Input() public set enumOptions(enumType:any){
    //where enumType is CompanyStatus, OrderStatus etc....
    this._optionText = "key";
    this._optionValue = "value";
    let enumArr:any[] = [];
    enumArr.loadFromEnum(enumType);
    //if set _options instead of options, call this._updateOptions() after
    this.options = enumArr;
  }

  private _required: boolean = false;
  @Input() public set required(value: boolean|string) {
    this._required = Boolean(value);
    this._updateOptions();
  }
  public get required(): boolean|string {
    return Boolean(this._required);
  }

  private _ngModel: any = null;
  get ngModel() {
    return this._ngModel;
  }
  set ngModel(value: any) {
    this._ngModel = value;

    if (this.enableCustomOptionInput && this.customOptionInputValue) {
      this._ngModel = this.customOptionInputValue;
    }

    this.refreshInitiallySelectedIndex();
    this.setDisplayText();
  }


  private _showNoneOption: boolean = false;
  @Input() public set showNoneOption(value:boolean) {
    this._showNoneOption = value;
    this._updateOptions();
  }
  public get showNoneOption() : boolean {
    return this._showNoneOption;
  }

  private _optionText: string = null;
  @Input() public set optionText(value: string) {
    this._optionText = value;
    this._updateOptions();
  }
  public get optionText(): string {
    return this._optionText;
  }

  private _optionValue: string = null;
  @Input() public set optionValue(value: string) {
    this._optionValue = value;
    this._updateOptions();
  }
  public get optionValue(): string {
    return this._optionValue;
  }

  @Input() public allowNullInput: boolean = false;
  @Input() public cbSelectClass: string = 'cb_select';
  @Input() public notFoundText: string = "NOT FOUND";
  @Input() altOptionValue: string = null;
  @Input() useAltOnValueZero: boolean = false;
  @Input() Nested: boolean = false;
  @Input() public DisplayText: string = '';
  @Input() public isIconButton: boolean = false;
  @Input() public onlyDropdownIcon: boolean = false;
  @Input() public buttonIcon: string = '';
  @Input() public enableAutoFocus: boolean = false;
  @Input() public alignDropDownOnRight: boolean = false;
  @Input() public enableCustomOptionInput: boolean = false;
  @Input() public customOptionInputValue: string;
  @Input() public floatLabelOnNull: boolean = false;

  /** emits event if we either receive or change the dropdown value */
  @Output() writeOrChangeValue = new EventEmitter<any>();
  @Output() open: EventEmitter<any> = new EventEmitter<any>();
  @Output() focus: EventEmitter<any> = new EventEmitter<any>();
  @Output() close: EventEmitter<any> = new EventEmitter<any>();
  @Output() change: EventEmitter<any> = new EventEmitter<any>();
  @Output() public customOptionInputValueChange = new EventEmitter<string>();

  @ViewChild('cbList') list: ElementRef;
  @ViewChild('cbDropdown') cbDropdown: BsDropdownDirective;
  @ViewChild('cbButton') cbButton: ElementRef;
  @ViewChild('cbCustomInput') cbCustomInput: ElementRef;

  getListNativeElement = () => this.list.nativeElement;
  getIsDropdownOpen = () => this.dropdownIsOpen;
  getOptionsListLength = () => this.options.length;
  getOptionDisplayTextAt = (i: number) => {
    if (this.optionText) {
      return this.options[i][this.optionText] + '';
    } else {
      return this.options[i] + '';
    }
  };
  onSelectAt = (i: number) => {
    this.selectOption(this.options[i]);
  };
  onArrowKeyDown = () => {};
  onArrowKeyUp = () => {};
  toggleDropDown = (isOpen: boolean) => {
    // if (this.dropdownIsOpen != isOpen) {
    //   this.cbButton.nativeElement.click();
    // }
    this.focusCustomInput();
  };

  focusCustomInput = () => {
    if (this.enableCustomOptionInput) {
      setTimeout(() => {
        if (this.cbCustomInput) {
          this.cbCustomInput.nativeElement.focus();
          this.cbDropdown.isOpen = true;
        }
      });
    }
  };

  private typingDebouncer: Subject<any> = new Subject();
  private typingString: string = "";

  setHiddenOptions(){
    this.hiddenOptions = (this.options||[]).filter(opt => this.fxHideOption(this, opt));
  }

  onBtnMouseEnter(e:MouseEvent){
    this.mouseOvered = true;
    this.setHiddenOptions();
  }

  addTypingCharacter = (char: string) => {
    this.typingString += char;
    this.typingDebouncer.next();

    return this.typingString;
  }
  hasTypingCharacters = () => {
    return (this.typingString != "");
  }
  private onTypingReset() {
    this.typingString = "";
  }

  public isDisabled: boolean = false;
  @Input() public readonly: boolean = false;
  public mouseOvered: boolean = false;
  public focused = false;

  /**
   * the text inside the dropdown button
   */
  public ngModelDisplayText: string = null;


  ngOnInit() {
    this.sbjWriteOrChangeValue.pipe(
      distinctUntilChanged(),
      untilDestroyed(this)
    ).subscribe(value => this.writeOrChangeValue.emit(value))
  }
  ngOnDestroy(){
    this.cbDropdown = null;
    this.list = null;
    this.cbButton = null;
    this.cbCustomInput = null;
  }

  validate(c: AbstractControl): { [key: string]: any; } {
    let errors = {};
    if(this.required && this.ngModel==null){
      errors["required"] = true;
    }
    return Object.keys(errors).length == 0? null : errors;
  }

  /**
   * if options and ngModel are set,
   * sets the selected index to match ngModel in options
   */
  refreshInitiallySelectedIndex(){
    if (this.options != null){
      this.options.forEach((opt, i) => {
        let compareValue: any = opt;
        if (this.optionValue != null){
          compareValue = opt[this._getOptionValueKey(opt)];
        }
        if (compareValue == this.ngModel) {
          this.keyboard.selectedIndex = i;
          this.keyboard.origSelectedIndex = i;
        }
      });
    }
  }

  propagateChange = (_: any) => { };
  propagateTouched = () => { };

  /**
   * called when user clicks the button to toggle the dropdown
   */
  public buttonClick(event) {
    if(this.enableAutoFocus){
      this.autoFocuList();
    }

    if (this.Nested) {
      event.stopPropagation();
      event.preventDefault();
    }
    this.propagateTouched();
  }

  public autoFocuList() {
    setTimeout(() => {
      if(this.options && this.options.length) {
        let el = document.getElementById(this.ngModelDisplayText);
        if (el) {

          el.scrollIntoView();
          window.scroll({top: 1});
        }
      }
    }, 100);
  }

  @Output() selected = new EventEmitter<any>();

  /**
   * handles users setting the option, either by click or keyboard
   */
  public selectOption(option: any) {
    if (this.enableCustomOptionInput) {
      this.cbDropdown.isOpen = !this.cbDropdown.isOpen;
      this.customOptionInputValue = null;
    }

    if (this.optionValue != null) {
      this.ngModel = option[this._getOptionValueKey(option)];
    } else {
      this.ngModel = option;
    }

    this.propagateChange(this.ngModel);
    this.propagateTouched();
    this.change.emit(this.ngModel);
    this.selected.emit(option);
    this.cbDropdown.hide(); // https://corebridge.atlassian.net/browse/END-5996
  }

  /*
    Handles user clearing the value of cb-select
  */
  public clearSelected() {
    if(this.ngModel !== null){
      this.ngModel = null;
      this.change.emit(this.ngModel);
      this.propagateChange(this.ngModel);
    }
  }

  /**
   * handles user custom option
   */
  public createCustomInput(inputVal: string) {
    if(this.customOptionInputValue == null){
      return;
    }

    if (inputVal && inputVal.trim() != '') {
      this.keyboard.selectedIndex = -1;
      this.ngModel = inputVal;
    } else {
      this.keyboard.selectedIndex = 0;
      this.ngModel = this.options[0][this.optionValue];
    }

    this.customOptionInputValue = null;//reset custominput(needed because of the condition inside `set ngModel`)
    this.customOptionInputValueChange.emit(this.customOptionInputValue);

    this.propagateChange(this.ngModel);
    this.propagateTouched();
    this.change.emit(this.ngModel);
    this.cbDropdown.isOpen = false;
  }

  /**
   * called by blur and focus events
   */
  public handleFocus(isFocused: boolean) {
    if (isFocused) {
      this.focused = true;
      this.focus.emit(this);
    } else {
      setTimeout(() => {
        this.focused = false;
        if (this.dropdownIsOpen)
          this.toggleDropDown(false);
      }, 100);
    }
  }

  /**
   * computes the display text (the text in the button)
   */
  setDisplayText() {
    if ((this.ngModel == null && (!this.allowNullInput)) || this.ngModel === undefined || this.ngModel === "") {
      this._setDisplayTextToDefault();
      return;
    }
    if ((this.options || []).length < 1) {
      this._setDisplayTextToDefault();
      return;
    }

    var currentOption: any;
    if (this.optionValue != null) {
      currentOption = this.options.find(opt => opt[this._getOptionValueKey(opt)] == this.ngModel);
    } else {
      currentOption = this.ngModel;
    }

    if (this.enableCustomOptionInput) {
      this._setDisplayTextToDefault(this.ngModel);
      return;
    } else if (currentOption == null) {
      this._setDisplayTextToDefault(this.notFoundText);
      return;
    }

    if (this.optionText) {
      this.ngModelDisplayText = currentOption[this.optionText];
    } else {
      this.ngModelDisplayText = this.ngModel;
    }
  }

  /**
   * checks if the Option passed has a value using the this.optionvalue key
   * END-5326
   */
  private _getOptionValueKey(option: any): string {
    let key = this.optionValue;
    if ((this.optionValue != null) && (this.useAltOnValueZero) && (option && option[this.optionValue] == 0)) {
      /* 0 is not considered a reliable value, use alt option value instead */
      key = this.altOptionValue;
    }
    return key;
  }

  /**
   * sets the display text to default
   */
  private _setDisplayTextToDefault(adefault: string = ''){
    this.ngModelDisplayText = this.DisplayText || adefault;
  }

  /**
   * checks if the Option should need to have a null first value to clear the dropdown
   * END-5997
   */
  private _updateOptions() {
    if ((this._options && this._options.length) && (this._optionText) && (this._optionValue)) {
      //clean options first to make sure no duplicate null first entries
      let filteredOptions = this._options.filter(option => !((option[this._optionText] == '(None)') && (option[this._getOptionValueKey(option)] == null)));

      if ((this._showNoneOption == true) && (!this._required)) {
        const nullOption = {};
        nullOption[this._getOptionValueKey(nullOption)] = null;
        nullOption[this._optionText] = '(None)';
        filteredOptions.unshift(nullOption);
      }
      this._options = filteredOptions;
    }
  }

  private dropdownIsOpen: boolean = false;
  handlePopoverChange(isOpen: boolean, $event: any) {
    if (isOpen) {
      this.open.emit($event);
    } else {
      this.close.emit($event);
    }
    this.dropdownIsOpen = isOpen;
  }

  writeValue(value: any) {
    this.ngModel = value;
    this.sbjWriteOrChangeValue.next(value);
  }

  registerOnChange(fnChange: any) {
    this.propagateChange = (value)=>{
      fnChange(value);
      this.sbjWriteOrChangeValue.next(value);
    };
  }

  registerOnTouched(fnTouched: any) {
    this.propagateTouched = fnTouched;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}

export interface IDropdownKeyboardNavigatorHost{
  getListNativeElement: () => any;
  getIsDropdownOpen: () => boolean;
  getOptionsListLength: () => number;
  getOptionDisplayTextAt: (i: number) => string;
  onSelectAt: (i: number) => void;
  onArrowKeyDown: () => void;
  onArrowKeyUp: () => void;
  toggleDropDown: (isOpen: boolean) => void;
  addTypingCharacter: (char: string) => string;
  hasTypingCharacters: () => boolean;
}

export class DropdownKeyboardNavigator
{
  constructor(private host: IDropdownKeyboardNavigatorHost) {}

  Destroy(){
    this.host = null;
  }

  /**
   * index of the currently selected option
   * if user has done up/down events, index does not match ngModel
   * otherwise matches ngModel
   * only used for styling
   */
  public selectedIndex: number = -1;
  public origSelectedIndex: number = -1;

  /**
   * sets the scroll position of the dropdown to the selected index
   * if the dropdown is addressable
   */
  private setScrollPosition(){
    let nativeElement = this.host.getListNativeElement();
    if (nativeElement)
    {
      nativeElement.scrollTop = (this.selectedIndex - (nativeElement.offsetHeight / 50)) * 25;
    }
  }

  private onKeyUp() {
    event.preventDefault();
    event.stopPropagation();
    this.host.onArrowKeyUp();
    if (this.selectedIndex == 0) {
      // this.index = this.options.length - 1;
    } else {
      this.selectedIndex--;
    }
    this.setScrollPosition();
  }

  private onKeyDown() {
    event.preventDefault();
    event.stopPropagation();
    this.host.onArrowKeyDown();

    if (this.selectedIndex == this.host.getOptionsListLength() - 1) {
      // this.index = 0;
    } else {
      this.selectedIndex++;
    }
    this.setScrollPosition();
  }

  /**
   * public handler for key events
   * delegates to onKeyUp/onKeyDown
   * handles enter/spacebar events
   * handles text input to jump to option
   */
  public onKeyAny(event, allowText = true, allowEnter = true) {
    //ArrowUp
    if (event.keyCode == 38) {
        if (!this.host.getIsDropdownOpen()) {
            this.host.toggleDropDown(true);
        } else {
            this.onKeyUp();
        }
    }
    //ArrowDown
    else if (event.keyCode == 40) {
        if (!this.host.getIsDropdownOpen()) {
            this.host.toggleDropDown(true);
        } else {
            this.onKeyDown();
        }
    }
    //Esc
    else if (event.keyCode == 27) {
      this.selectedIndex = this.origSelectedIndex;
    }
    //enter or space
    else if (allowEnter && (event.keyCode == 13 || event.keyCode == 9 || (event.keyCode == 32 && !this.host.hasTypingCharacters()) ))  {
      //only allow selecting a new ngModel
      //if the dropdown is open
      //and if the selected index is within the bounds of our options array
      if (this.selectedIndex >= 0 && this.selectedIndex < this.host.getOptionsListLength()) {
        this.host.onSelectAt(this.selectedIndex);
      }
    }
    else if (allowText) {
      //keyboard 0-9, a-z, keypad 0-9
      if ((event.keyCode >= 48 && event.keyCode <= 57) ||
          (event.keyCode >= 65 && event.keyCode <= 90) ||
          (event.keyCode >= 96 && event.keyCode <= 105) ||
          (event.keyCode == 32)) {

        //console.log("ascii");
        event.preventDefault();
        event.stopPropagation();

        let s: string = this.host.addTypingCharacter(event.key.toLowerCase());

        for (let i = 0; i < this.host.getOptionsListLength(); i++) {
          let elementOptionText = this.host.getOptionDisplayTextAt(i).toLowerCase();

          if (elementOptionText.startsWith(s)) {
            this.selectedIndex = i;
            if (this.host.getIsDropdownOpen()) {
              this.setScrollPosition();
            }
            else {
              this.host.onSelectAt(this.selectedIndex);
            }
            break;
          }
        }
      }
    }
  }

}
