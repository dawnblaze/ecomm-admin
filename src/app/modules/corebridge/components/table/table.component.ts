import { Component, Input, Output, EventEmitter, OnInit, ViewChild, ViewChildren, AfterViewInit, QueryList } from '@angular/core';

import { TableToolbarComponent } from './table-toolbar/table-toolbar.component';
import { TablePaginationComponent } from './table-pagination/table-pagination.component';

@Component({
  selector: 'cb-table',
  templateUrl: './table.component.html'
})
export class TableComponent implements OnInit, AfterViewInit {

  @Input() public columns:string[] = [];

  @Input()
  set rows(rows: any[]) {
    this._rows = rows;

    if(typeof rows !== 'undefined' && this.total == 0){
      this.total = rows.length;
    }
  }

  @Output() onLoaded: EventEmitter<boolean> = new EventEmitter<boolean>();

  // use ViewChildren instead of ViewChild to detect element since we are using ngIf
  @ViewChildren(TablePaginationComponent) pagination: QueryList<TablePaginationComponent>;
  @ViewChild(TableToolbarComponent) toolbar: TableToolbarComponent;

  public name: string = "cb-table";
  public _rows: any[] = [];
  public filtered: any[] = [];
  public search: string;
  public searchKey: string = "Name";
  public sortAsc: boolean = true;
  public sortString: string = "";
  public sort: string = "";
  public page: number = 1;
  public count: number = 50;
  public total: number = 0;
  public rememberPage: boolean = true;

  constructor() {
  }

  ngOnInit() {
    if (sessionStorage.getItem(this.name + "-sortString") != null) {
      this.sortString = JSON.parse(sessionStorage.getItem(this.name + "-sortString"));
      this.sortAsc = JSON.parse(sessionStorage.getItem(this.name + "-sortAsc"));
      this.sortBy(this.sortString);
    }

    if (sessionStorage.getItem(this.name + "-searchString") != null) {
      this.search = JSON.parse(sessionStorage.getItem(this.name + "-searchString"));
    }
  }

  ngAfterViewInit() {
    if (sessionStorage.getItem(this.name + '-count') != null) {
      this.count = +sessionStorage.getItem(this.name + '-count');
      if (this.toolbar) {
        // use setTimeout to tell angular we have changes
        setTimeout(() => {
          this.toolbar.count = this.count;
        }, 0);
      }
    }

    if (this.rememberPage && sessionStorage.getItem(this.name + '-page') != null) {
      this.page = +sessionStorage.getItem(this.name + '-page');

      this.pagination.changes.subscribe((pagination) => {
        let p = pagination.first;

        if (p) {
          // use setTimeout to tell angular we have changes
          setTimeout(() => {
            p.changePage(this.page);
          }, 0)
        }
      });
    }

    this.load();
  }

  public load() {
    this.filterRows();
  }

  public filterRows(){
    if(this.search == ""){
      this.filtered = this._rows;
    } else {
      this.filtered = this._rows.filter( item => {
        return this.filterProcess(item);
      });
    }

    this.total = this.filtered == null ? 0 : this.filtered.length;
    this.loaded();
  }

  public filterProcess(item: any): boolean {
    return (String(item[this.searchKey]).toLowerCase().indexOf(this.search.toLowerCase()) !== -1) ? true : false;
  }

  public sortBy(sort: string) {
    if(sort == this.sortString){
      this.sortAsc = !this.sortAsc;
    }
    this.sortString = sort;
    this.sort = (this.sortAsc)? sort : '-'+sort;

    // Store sort preference for this table in the session storage
    sessionStorage.setItem(this.name + "-sortString", JSON.stringify(this.sortString));
    sessionStorage.setItem(this.name + "-sortAsc", JSON.stringify(!this.sortAsc));
  }

  public onCurrentPage(i:number) {
    let min = 1*this.count * (this.page - 1);
    let max = 1*this.count + min;

    return (i >= min && i < max);
  }

  public onCountChange(count: number) {
    this.count = count;
    this.page = 1;
    sessionStorage.setItem(this.name + '-count', this.count + '');
    sessionStorage.setItem(this.name + '-page', this.page + '');
  }

  public onPageChange(page: number) {
    this.page = page;
    sessionStorage.setItem(this.name + '-page', this.page + '');
  }

  public onSearchChange(search: string) {
    this.search = search;

    // Store search query for this table in the session storage
    sessionStorage.setItem(this.name + "-searchString", JSON.stringify(search));

    this.filterRows();
  }

  public loaded(){
    this.onLoaded.emit(true);
  }
}
