import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'cb-table-pagination',
  templateUrl: './table-pagination.component.html'
})
export class TablePaginationComponent implements OnInit {

  public _count: number; // total per page
  public _total: number = 0;
  public maxIndex: number = 0; // largest index on current page
  public pages: number[] = [1]; // array on page numbers
  public page: number = 1; // current page
  private maxPageNum: number;


  @Input() // total number of rows
  set total(total: number) {
    this._total = total;
    this.updateValues();
  }

  @Input()
  set count(count: number) {
    this._count = count;
    this.page = 1;
    this.updateValues();
  }

  @Output() onPageChange = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  updateValues(){
    if(this._total && this._count){
      this.maxPageNum = Math.ceil(this._total / this._count);

      let start = this.page - 5,
        end = this.page + 5;

      if (start < 1) {
        end = end + Math.abs(start) + 1;
        start = 1;
      }

      if (end > this.maxPageNum) {
        start = start - (end - this.maxPageNum);
        end = this.maxPageNum;
      }

      if (this.maxPageNum < 11) {
        start = 1;
        end = this.maxPageNum;

      }

      this.pages = [];

      for(let i = start;i <= end;i++) {
        this.pages.push(i);
      }

      let maxOnPage = this._count * (this.page -1) + this._count;
      this.maxIndex = Math.min(maxOnPage, this._total);
    }
  }

  changePage(page: number){
    page = page > 0 ? page : 1;
    page = page < this.maxPageNum ? page : this.maxPageNum;
    console.log('page updated to ' + page);
    this.page = page;

    this.updateValues();
    this.onPageChange.emit(page);
  }

}
