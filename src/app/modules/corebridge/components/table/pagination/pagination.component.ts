import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';

@Component({
  selector: 'cb-pagination',
  templateUrl: './pagination.component.html'
})
export class PaginationComponent implements OnInit {
  public _count: number = 0;
  public _size: number = 50;
  public _index: number = 1;
  public totalPages: number = 0;
  public pages: number[] = [];

  @Input()
  set count(value: number) {
    this._count = value;
    this.calculatePages();
  }

  @Input()
  set size(value: number) {
    this._size = value;
    this.calculatePages();
  }

  @Input()
  set index(value: number) {
    this._index = value;
    this.calculatePages();
  }

  @Output() pageChange = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
    this.calculatePages();
  }

  changePage(page: number) {
    page = page > 0 ? page : 1;
    page = page < this.totalPages ? page : this.totalPages;

    this.pageChange.emit(page);
  }

  private calculatePages() {
    if(this._count && this._size){
      this.totalPages = Math.ceil(this._count / this._size);
      let start = this._index - 5,
        end = this._index + 5;

      if (start < 1) {
        end = end + Math.abs(start) + 1;
        start = 1;
      }

      if (end > this.totalPages) {
        start = start - (end - this.totalPages);
        end = this.totalPages;
      }

      if (this.totalPages < 11) {
        start = 1;
        end = this.totalPages;
      }

      this.pages = [];

      for(let i = start;i <= end;i++) {
        this.pages.push(i);
      }
    }
  }
}


