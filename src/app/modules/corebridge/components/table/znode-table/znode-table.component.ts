import { Output, EventEmitter, OnInit, ViewChild } from '@angular/core';

import { ZnodeRequest, ZnodeResponse } from '../../../../../services/znode-api.service';
import { TableToolbarComponent } from '../table-toolbar/table-toolbar.component';

export class ZnodeTableComponent implements OnInit {
  public name: string = '';
  public rows: any[] = [];

  public sort: boolean;
  public sortString: string;
  public search: string = '';
  public searchKeys: string[] = [];
  public dataLoaded: boolean = false;
  public request: ZnodeRequest = new ZnodeRequest();
  public response: ZnodeResponse = new ZnodeResponse();

  @Output() onLoaded: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild(TableToolbarComponent) toolbar: TableToolbarComponent;

  constructor() { }

  ngOnInit() {
    this.onPageChange(1, false);
    this.getStorage();
    this.load();
  }

  load() {
  }

  getRows() {
  }

  getStorage() {
    if (sessionStorage.getItem(this.name + '-page')) {
      this.request.index = +sessionStorage.getItem(this.name + '-page');
    }

    if (sessionStorage.getItem(this.name + '-count')) {
      this.request.size = +sessionStorage.getItem(this.name + '-count');
      this.toolbar.count = this.request.size;
    }

    if (sessionStorage.getItem(this.name + '-search')) {
      this.search = sessionStorage.getItem(this.name + '-search');

      this.searchKeys.forEach((str) => {
        this.request.addFilter({ key: str, operator: 'cn', value: this.search });
      });
    }

    if (sessionStorage.getItem(this.name + '-sort')) {
      this.sort = sessionStorage.getItem(this.name + '-sort') == 'true';
      this.request.addSort({ key: this.sortString, asc: this.sort });
    }

    if (sessionStorage.getItem(this.name + '-sortString')) {
      this.sortString = sessionStorage.getItem(this.name + '-sortString');
      this.request.addSort({ key: this.sortString, asc: this.sort });
    }
  }

  loaded() {
    this.dataLoaded = true;
    this.onLoaded.emit(true);
  }

  onPageChange(page: number, reload: boolean = true) {
    sessionStorage.setItem(this.name + '-page', page + '');
    this.request.index = page;

    if(reload)
      this.getRows();
  }

  onCountChange(count: number) {
    sessionStorage.setItem(this.name + '-count', count + '');
    this.request.size = count;
    this.onPageChange(1);
  }

  onSearchChange(search: string) {
    this.search = search;
    sessionStorage.setItem(this.name + '-search', this.search);
    // add debounce
    this.searchKeys.forEach((str) => {
      this.request.addFilter({ key: str, operator: 'cn', value: this.search });
    });

    this.onPageChange(1);
  }

  onSort(sort: string) {
    if (sort == this.sortString) {
      this.sort = !this.sort;
    }
    else {
      this.sort = false;
      this.sortString = sort;
    }

    this.request.addSort({ key: this.sortString, asc: this.sort });

    sessionStorage.setItem(this.name + '-sort', this.sort ? 'true' : 'false');
    sessionStorage.setItem(this.name + '-sortString', this.sortString);

    this.getRows();
  }
}

