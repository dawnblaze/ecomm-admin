import { Component, EventEmitter, Input, Output, OnInit, ViewChild } from '@angular/core';
import { Select2Module, Select2OptionData } from 'ng2-select2';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators'

@Component({
  selector: 'cb-table-toolbar',
  templateUrl: './table-toolbar.component.html',
  styleUrls: ['./table-toolbar.component.less']
})
export class TableToolbarComponent implements OnInit {

  private searchDebouncer: Subject<string> = new Subject<string>();

  public count: number = 50;
  public results: string[] = [];

  public options: Select2Options = {
    multiple: true
  };

  @Input() select: boolean = false;
  @Input() hideSearch: boolean = false;
  @Input() suggestions: any[];
  @Input() search: string = '';
  @Input() filterSearch: any[] = [];
  @Input() hasToolBarMessage: boolean = false;
  @Input() toolbarMessage: string = '';

  @Output() onCountChange = new EventEmitter<number>();
  @Output() onSearchChange = new EventEmitter<string>();
  @Output() onSelect2Update = new EventEmitter<string[]>();

  constructor() { }

  ngOnInit() {
    this.searchDebouncer.pipe(debounceTime(500)).subscribe((value: string) => {
      this.onSearchChange.emit(value);
    });
    //this.searchDebouncer.

    this.results = this.suggestions;
  }

  changeCount(count: number){
    this.onCountChange.emit(count);
    this.count = count;
  }

  changeSearch(search: string){
    this.searchDebouncer.next(search);
  }

  updateSelection(data: {value: string[]}) {
    this.onSelect2Update.emit(data.value);
  }

}
