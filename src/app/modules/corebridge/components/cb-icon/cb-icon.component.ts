import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'cb-icon',
  templateUrl: './cb-icon.component.html',
  styleUrls: ['./cb-icon.component.less']
})

export class IconComponent implements OnInit {
  @Input() iconTypeClass: string = '';

  @Input() icon: string = '';


  constructor() { }

  ngOnInit() {
  }

}
