import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../../services/auth.service';
import { AlertService } from '../../../../services/alert.service';
import { ThemeService } from '../../../../services/theme.service';
import { ATEService } from '../../../../services/ate.service';
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Component({
  selector: 'cb-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  systemVersion:string;
  loadingOpacity = 0;
  login = {username: '', password: ''};
  usernameError:string = '';
  passwordError:string = '';
  rememberMe: boolean;

  auth:any;

  constructor(private router:Router, private authService:AuthService, private alertService: AlertService, public themeService: ThemeService) {
  }

  ngOnInit() {
    if(this.authService.isLoggedIn()){
      this.redirectToDashboard();
    }
    else{
      document.body.className = document.body.className + " cb_login";
      this.systemVersion = this.authService.getSystemVersion();
    }

    this.authService.loggedInStatus.subscribe( status => {
      if( status['loggedIn'] ){
        this.redirectToDashboard();
      }
    });


    var username = this.getCookie('username');
    if(username != ''){
      this.login.username = username;
      this.rememberMe = true;
    }
  }

  checkUsername() {
    if ('' == this.login.username) {
      this.usernameError = 'Username cannot be blank';
      return false;
    }
    else {
      this.usernameError = '';
      return true;
    }
  }

  checkPassword() {
    if ('' == this.login.password) {
      this.passwordError = 'Password cannot be blank';
      return false;
    }
    else {
      this.passwordError = '';
      return true;
    }
  }

  submitLogin(event: Event) {

    event.preventDefault();
    this.usernameError = '';
    this.passwordError = '';

    if (!this.checkUsername()) {
      return false;
    }

    if (!this.checkPassword()) {
      return false;
    }

    this.loadingOpacity = 1;

    if(this.rememberMe){
      this.setCookie('username', this.login.username, 7);
    } else {
      this.deleteCookie('username');
    }
    this.processLogin().subscribe(res => {
      if(res == true) {
        return true;
      } else {
        this.loadingOpacity = 0;
        return false;
      }
    },
      err => {
      this.loadingOpacity = 0;
      return false;
      });
  }

  processLogin() : Observable<boolean> {
    return this.authService.login(this.login.username, this.login.password)
      .pipe(map(
        auth => {
          if (!auth) {
            this.login = {username: this.login.username, password: ''};
            this.usernameError = 'Incorrect Username or Password';
            return false;
          }
          return true;
        },
        error => {
          console.error(error);
          this.usernameError = "Could not verify username. Please try again later.";
          this.loadingOpacity = 0;
        }
      ));
  }

  redirectToDashboard(){
    let classes: string = document.body.className.replace(" cb_login", "");
    document.body.className = classes;
    this.router.navigate(['ecommerce/storefronts']);
  }

  forgotPassword(){
    var content = `Your Ecommerce credentials are the same as your ${this.themeService.currentTheme.name} credentials. If you don't know your credentials please contact your ${this.themeService.currentTheme.name} administrator or ${this.themeService.currentTheme.name} support for assistance.`;
    this.alertService.warning(content, 0);
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
  }

  private deleteCookie(name) {
      this.setCookie(name, "", -1);
  }

  setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

}
