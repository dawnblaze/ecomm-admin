import { untilDestroyed } from 'ngx-take-until-destroy';
import { Component, OnInit, Input, Output, EventEmitter, ElementRef, AfterViewChecked, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

export enum AutoSaveBarState{
  none = "none", //none of the below
  saving = "saving", //save in flight
  saved = "saved", //recent save (transient, transitions to none after a few seconds)
  error = "error", //recent save returned error from server (allows new save in manual save mode)
  invalid = "invalid", //client will not allow save due to validation error (does not allow new save until fixed)
  validating = "validating"
}

@Component({
  selector: 'cb-autosave-bar',
  templateUrl: './autosave-bar.component.html',
  styleUrls: ['./autosave-bar.component.less'],
  host:{
    '[class.has-right-side-panel]':'showSidePanelPropertyEditor'
  }
})
export class AutosaveBarComponent implements OnInit, OnDestroy {

  parentBaseElem:Element = null;
  parentBaseWidth:number = null;

  @Input() isManualSave:boolean = false;
  @Input() isNewEntity:boolean = false;
  @Input() state: string = AutoSaveBarState.none;
  @Input() parentBaseSelector:string = '';
  @Input() showSidePanelPropertyEditor: boolean = false;
  @Input() hideSaveButton: boolean;
  @Input() CustomSaveButtonText: string;

  @Output() clickViewErrors: EventEmitter<any> = new EventEmitter<any>();
  @Output() clickClose: EventEmitter<any> = new EventEmitter<any>();
  @Output() clickCancel: EventEmitter<any> = new EventEmitter<any>();
  @Output() clickSave: EventEmitter<any> = new EventEmitter<any>();

  constructor(private elemRef:ElementRef<HTMLElement>,private cdref:ChangeDetectorRef) { }

  subs:Subscription[] = [];

  public get manualEditSaveHasError(): boolean{
    return (this.state == 'error' && !this.isNewEntity && this.isManualSave);
  }

  ngOnInit() {
  }

  ngAfterViewInit(){
  }

  ngOnDestroy(){
    this.subs.forEach(sub => sub.unsubscribe());
  }

  setHostWidth(cbContent:HTMLElement){
    this.elemRef.nativeElement.style.width = cbContent.offsetWidth+"px";
  }

  public onCancel(ev:Event) {
    this.clickCancel.emit(ev);
  }

  public onClose(ev:Event) {
    this.clickClose.emit(ev);
  }

  public showSavedLabel() {
    return (this.state == 'saved' || (this.state=='none' && !this.isNewEntity))
      && !this.hideSaveButton;
  }

  public showSaveButton() {
    return (
      (this.state=='none' && (this.isNewEntity || this.isManualSave))
      || this.manualEditSaveHasError
    ) && !this.hideSaveButton;
  }
}
