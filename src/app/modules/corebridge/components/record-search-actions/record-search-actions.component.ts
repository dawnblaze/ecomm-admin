import { Component, OnInit, Input, Output, EventEmitter, HostListener, ContentChildren, TemplateRef, QueryList, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { IAtom } from '../../models/iatom';
import { RecordAction, AbstractAction } from '../../models/action';

import { CbTemplateDirective } from '../../directives/cb-template.directive';
import { Observable } from 'rxjs';
import { QueryUtils } from '../../models/queryutils';

@Component({
  selector: 'cb-record-search-actions',
  templateUrl: './record-search-actions.component.html',
  host: { 'class': 'hidden-print' },
  styleUrls: ['./record-search-actions.component.less']
})
export class RecordSearchActionsComponent implements OnInit {
  public borderRadiusCss: string = 'border-radius-0';

  @Input()
  public doBeforeExecute: (enity: any | any[], action: AbstractAction) => Observable<boolean> = null;
  @Input()
  public doAfterExecute: (entity: any | any[], action: AbstractAction) => any = null;
  @Input()
  public showActions: boolean = true;

  @Input()
  public showAddButton: boolean = true;

  @Input()
  public hideGroupActions: boolean = false;

  @Input("selected")
  public selected: any[];

  @Input("disableAddButton")
  public disableAddButton: boolean = false;

  @Input()
  public actions: RecordAction[];

  @Input()
  public entity: string;

  @Input()
  public relativeAddPath: string;

  @Input()
  public isPageActionDropDown: boolean = false;

  @Input()
  public selectedPageAction: number = 0;

  @Input()
  public pageActionOptions: any[] = [];

  @Input()
  public PreventCheckedOutputEmit: boolean = false;

  @Input()
  public isDisabled: boolean = false;

  @Output()
  pageActionSelected: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  public add = new EventEmitter<boolean>();

  @Output()
  checked = new EventEmitter<boolean>();

  @Input()
  ButtonAndDropdown: boolean = false;

  @Input()
  AlwaysShowLabel: boolean = false;

  @Input()
  disabledLabel: boolean = false;

  @Input()
  isAssociate: boolean;

  @Input()
  buttonIcon: string;

  @Output() clickButtonEvent = new EventEmitter();

  @ContentChildren(CbTemplateDirective) templates: QueryList<CbTemplateDirective> = null;
  optionsTemplate: TemplateRef<any> = null;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private elementRef: ElementRef) { }

  public addNewTitle: string;
  public newOptions = [];

  ngOnInit() {
    this.addNewTitle = 'Add New ' + this.entity;
  }

  ngAfterContentInit() {
    if (this.templates) {
      this.templates.forEach(t => {
        switch (t.name.toLowerCase()) {
          case "options":
            this.optionsTemplate = t.template;
            break;
          default:
            break;
        }
      });
    }
  }

  @HostListener('click', ['$event.target'])
  public onClick(target: any): void {
    if (target.classList.contains('ui-chkbox-box') && this.elementRef) {
      setTimeout(function () {
        if (this.elementRef) {
          if (target.classList.contains('ui-chkbox-box')) {
            $(this.elementRef.nativeElement).find('.ui-chkbox-box').addClass('ui-state-focus');
          } else {
            $(this.elementRef.nativeElement).find('.ui-chkbox-box').removeClass('ui-state-focus');
          }
        }
      }, 100);
      $(this.elementRef.nativeElement).find('[tabindex="0"]').focus();
    }
  }

  @HostListener('mouseleave')
  public onMouseLeave() {
    if (this.isPageActionDropDown || this.AlwaysShowLabel)
      return;

    this.resetToDefaultWidth();
  }

  public resetToDefaultWidth() {
    if (this.isDisabled || this.isPageActionDropDown || this.AlwaysShowLabel)
      return;

    let btn = document.querySelector('.addAction') as HTMLElement;
    btn.style.width = !this.buttonIcon ? '30px' : '35px';
  }

  public onSelectAll(event: boolean) {
    this.checked.emit(event);
  }

  public doAdd() {
    if (this.relativeAddPath != null && this.relativeAddPath.length > 0) {
      console.warn("navigation: navigating after add");
      let rawActiveFilterID = QueryUtils.getUriEncodedQueryParams().af;
      let isGUID = true;
      if (rawActiveFilterID != null) {
        isGUID = (rawActiveFilterID + '').indexOf('-') > -1;
      }
      if (!isGUID) {
        this.router.navigate([this.relativeAddPath], { queryParams: { ref: rawActiveFilterID }, relativeTo: this.route });
      }
      else {
        this.router.navigate([this.relativeAddPath], { relativeTo: this.route });
      }
    }
    else {
      // this.add.next(true);
      this.clickButtonEvent.emit(true);
    }
  }

  public hidePopover(pop) {
    setTimeout(() => {
      pop.hide();
    }, 200);
  }

  public onActionSelect(event) {
    if (event.target.value == "")
      return;

    var a = this.actions.find(a => a.text == event.target.value);
    a.DoExecute(this.doBeforeExecute, this.doAfterExecute);

    if (this.PreventCheckedOutputEmit) {
      // <select> element change value to "" which reset the dropdown selected
      $(this.elementRef.nativeElement).find('label.select > select').val("");
    } else {
      this.checked.emit(false);
    }
  }

  public setPageAction(pageAction: number) {
    console.log("setPageAction", pageAction);
    this.selectedPageAction = pageAction;
    this.pageActionSelected.emit(pageAction);
  }

  isActionHidden(action: RecordAction): boolean {
    return !action.IsVisible || (!action.IsEnabled && !action.showIfDisabled);
  }

  hasDisplayedActions(): boolean {
    if (this.actions) {
      for (const action of this.actions) {
        if (!this.isActionHidden(action)) {
          return true;
        }
      }
    }
    return false;
  }
}
