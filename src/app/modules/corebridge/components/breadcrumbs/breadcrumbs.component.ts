import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../../services/auth.service';
import { User } from '../../../ecommerce/models/user';

@Component({
  selector: 'cb-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.less']
})
export class BreadcrumbsComponent implements OnInit {

  public time:string = "";
  public date:string = "";
  public user:User;

  @Input() breadcrumbs:any = [];

  constructor(private router:Router, private authService:AuthService) {
    this.updateTime();

    setInterval(() => {
      this.updateTime();
    }, 1000);
  }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  updateTime() {
    let today = new Date();
    this.time = today.toLocaleTimeString().replace(/:\d+ /, ' ');
    ;

    if (this.date == "" || this.time == "12:00 AM") {
      this.updateDate();
    }
  }

  updateDate() {
    let today = new Date();

    let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    let d = days[today.getDay()];

    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let m = months[today.getMonth()];

    this.date = d + ', ' + m + ' ' + today.getDate();
  }

  goToBreadcrumb(path: string) {
    this.router.navigate([path]).then(response => {
      if(response == null) {
        // Some components navigate with location.go() which doesn't update the router path.
        // If we are trying to navigate to the same page the router thinks we are already on, add a #top tag so
        // components who are listening the path changes can do what they need to do.
        this.router.navigate([path], { fragment: 'top' });
      }
    });
  }
}
