import { Component, Input } from '@angular/core';

@Component({
  selector: 'cb-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.less']
})
export class TabComponent {
  @Input() title: string;
  @Input() active:boolean = false;
  @Input() slug: string;
  @Input() hasError: boolean = false;
}
