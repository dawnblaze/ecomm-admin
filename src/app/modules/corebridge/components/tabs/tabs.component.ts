import { Component, ContentChildren, QueryList, AfterContentInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';

import { TabComponent } from './tab/tab.component';

@Component({
  selector: 'cb-tabs',
  templateUrl: './tabs.component.html'
})
export class TabsComponent implements AfterContentInit {

  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  @Output() onChange: EventEmitter<any> = new EventEmitter();
  @Output() tabChange: EventEmitter<TabComponent> = new EventEmitter<TabComponent>();

  @Input() canDeactivate: any;

  // contentChildren are set
  ngAfterContentInit() {
    // get all active tabs
    let activeTabs = this.tabs.filter((tab)=>tab.active);

    // if there is no active tab set, activate the first
    if(activeTabs.length === 0) {
      this.selectTab(this.tabs.first, false);
    }
  }

  private selectTab(tab: TabComponent, emit: boolean = true){
    if (this.canDeactivate) {
      let res = this.canDeactivate();

      if (res instanceof Observable) {
        res.subscribe((ok: boolean) => {
          if (ok) {
            this.changeTab(tab, emit);
          }
        });
      }
      else {
        if (res) {
          this.changeTab(tab, emit);
        }
      }
    }
    else {
      this.changeTab(tab, emit);
    }
  }

  private changeTab(tab: TabComponent, emit: boolean = true) {
    // deactivate all tabs
    this.tabs.toArray().forEach(tab => tab.active = false);

    // activate the tab the user has clicked on.
    tab.active = true;

    // let parent components know
    if(emit){
      this.onChange.emit(tab.slug);
      this.tabChange.emit(tab);
    }
  }

  public openTab(slug: string) {
    let tab = this.tabs.filter( tab => tab.slug.toLowerCase() == slug.toLowerCase())[0];
    if(tab){
      this.selectTab(tab, false);
      return tab;
    }
  }

  public isActiveTab(slug: string) {
    let activeTab = this.tabs ? this.tabs.find(tab => tab.active) : false;
    return activeTab && activeTab.slug == slug;
  }
}
