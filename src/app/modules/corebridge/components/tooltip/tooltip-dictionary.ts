export const TooltipDictionary = {
  /* STORE */
  /* Store Details */
  /* Store Logo */
  store_details_logo:
    " Upload an image for your logo to display on the store. Only JPG, GIF, and PNG images are supported. The file size should be less than 3MB, and the image aspect ratio shoud be 3:1 (16:9).",
  /* Favicon */
  store_details_favicon:
    "Upload an image for your store favicon. Only JPG, GIF, and PNG images are supported. The file size should be less than 1MB, and the aspect ratio should be 1:1 (square) with a minimum size of 32x32 pixels.",
  /* Store Identity */
  store_details_name: "This is the store name displayed to your customers.",
  store_details_brand:
    "The brand associated with this store to assist in organization.",
  store_details_tagline:
    "A tagline that is shown to your customers on the store.",
  store_details_catalog:
    "A catalog (e.g., Summer Catalog, Winter Catalog) is a hierarchical grouping of Collections to be displayed in a store. Each store can only have one catalog assigned.",
  /* Store Domains */
  store_details_genericDomain:
    "This is the standard URL the store can be accessed from in a browser.",
  store_details_customDomain:
    "This is the custom URL the store can be accessed from in a browser.",

  /* Default Product Image */
  store_details_defaultImage:
    "Upload an image to display if a product image is not available. Only JPG, GIF and PNG images are supported. The file size should be less than 3MB. Your image will automatically be scaled so it displays correctly in the catalog.",
  /* Security */
  store_details_requireLogin:
    "When checked, a visitor must login to view the private store.",
  store_details_disableSignup:
    "When un-checked, store visitors cannot sign up for an account and accounts must be made in the MIS. Usually used in conjucntion with <b>Require User Login</b>.",
  store_details_restrictEmailDomain: `Restricts the email address allowed when store visitors sign up for an account. Only emails matching the allowed domain(s) can be used. The allowed domains are not displayed to the visitor. You can list multiple domains by separating each domain with a semi-colon. <br/> <br/>
        <i>Example:</i> <br/>domainone.com;domaintwo.com`,
  /* Store Settings */
  store_details_showpricing:
    "Show the price of products on the store to logged in users.",
  store_details_enableWholesale:
    "Show the wholesale price of products (if it's different) on the store to logged in users.",
  store_details_enableFileUploads:
    "Enable or disable file uploads for the storefront. Products must also be individually enabled for file upload functionality to be available.",
  /* Store Information */
  store_details_salesPhone:
    "The sales department phone number is displayed on sales transaction related communication.",
  store_details_servicePhone:
    "The customer service phone is displayed on the store.",
  store_details_mailingAddress:
    "The address is displayed on the storefront footer.",
  store_details_adminEmail:
    "The administrator email address is the email address of record for the store.",
  store_details_salesEmail:
    "The sales department email address is used for sales transaction related communication.",
  store_details_serviceEmail:
    "The customer service email address is displayed on the store.",
  store_details_customServiceEmailLabel: `Label shown on the store for the customer service email address. This can be the email address or a descriptive name. <br/> <br/>
        <i>Example:</i> <br/>If the label is set to \"Email Us\" the link will show that instead of the full email address.`,
  store_details_serviceEmailShowInTopNav:
    "When checked, the email link is shown in the top navigation bar.",
  store_details_customLinkUrl: `Optionally enter a URL to create a custom link in the top navigation bar <br/><br/>
        <i>Example:</i> <br/>A link to the company's social media page, or a secondary site.`,
  store_details_customLinkLabel:
    "Label shown on the store for the custom link in the top navigation bar.",

  /* Integration */
  store_integration_section: "",
  store_integration_toggle_corebridge:
    "Enable or disable the order sync to CoreBridge for this store.",
  store_integration_toggle_control:
    "Enable or disable the order sync to Control for this store.",
  store_integration_orderStation_control:
    "Sets default order station when an order is synced to Control.",
  store_integration_lineStation_control:
    "Sets default line item station when an order is synced to Control.",

  /* Pages */
  /* Page Details */
  store_page_title: "Title of the page shown on the store and the browser tab.",

  /* SEO Settings */
  store_page_seo_title:
    "SEO specific title for this page. By default it the same as the page title.",
  store_page_seo_description:
    "SEO specific description for this page. Limited to 160 characters.",
  store_page_urlHandle: `URL handle is used to uniquely identify this page throughout the system and is used in the store's URL structure. By default it is the same as the page title with spaces and special characters replaced by "-". Use only characters a-z and 0-9. Use "-" instead of spaces.`,
  store_page_content: "Content displayed on this page.",

  /* Checkout */
  store_checkout_paymentTypes:
    "Enable or disable the payment types available in checkout. For credit cards, check the card types you accept to show the badges in checkout.",
  store_checkout_address:
    "Enable or disable countries for billing and shipping addresses in checkout. This is especially helpful if you want to restrict billing addresses to one or two countries but allow shipping to more countries.",
  store_checkout_additionalInst:
    "When checked, the optional <strong>Additional Instructions</strong> field is shown in checkout.",
  store_checkout_requirePO:
    "When checked, a PO or Department Code must be entered during checkout to proceed.",
  store_checkout_tAndC:
    "Display additional terms and conditions that the customer must acnkowledge to complete checkout. You can embed links to site pages such as your full Terms & Conditions or Return Policy.",

  /* Advanced */
  store_advanced_javascriptTop:
    "This will be added to the top of every page on the site. If you're using Google Analytics, placing your tracking code here will automatically enable the ecommerce tracking on the receipt page. If you're using Listrak Shopping Cart Abandonment, placing your tracking code here will track customers who have added items to their shopping cart but did not complete their purchase. Only place your code in the <b>Site Wide Javascript (Top)</b> OR <b>Site Wide Javascript (Bottom)</b> but NOT both fields.",
  store_advanced_javascriptBottom:
    "This will be added to the bottom of every page on the site. If you're using Google Analytics , placing your tracking code here will automatically enable the ecommerce tracking on the receipt page. If you're using Listrak Shopping Cart Abandonment, placing your tracking code here will track customers who have added items to their shopping cart but did not complete their purchase. Only place your code in the <b>Site Wide Javascript (Top)</b> OR <b>Site Wide Javascript (Bottom)</b> but NOT both fields.",
  store_advanced_javascriptExceptReceipt:
    "The contents of this field will be placed at the bottom of every page on the site except the receipt page.",
  store_advanced_javascriptReceipt:
    "The contents of this field will be added only to the order receipt page. In your Javascript you can refer to hidden input fields on the Order Receipt page which have the following IDs: OrderID, OrderTotal, ShippingTotal and TaxTotal.",

  /* Approvals */
  store_approvals_toggle:
    "Enable or disable order approvals on the store. When enabled, orders placed on the store will be subject to the Approval Group(s) defined unless the customer is set as an Approval Exempt Customer or is an Approver.",
  store_approvals_orderLimit: `The Order Limit rule applies to a single order at the time it is being placed and does not include a timeframe limit. The amount includes any applicable shipping and taxes. <br/> <br/>
        <i>Example:</i> The Order Limit is set at $500 and is the only rule enabled for the group. The same user can place multiple orders below the $500 without further rules being applied.`,
  store_approvals_timeFrameLimit: `The Time Frame Limit rule applies to all orders placed within the timeframe specified. The amount includes any applicable shipping and taxes. <br/> <br/>
         <i>Example #1: </i> The Time Frame Limit is set at $1,000 per 30 days and is the only rule enabled for the group. A user can place one or more orders totaling up to $1,000 before the rule is applied. <br/> <br/>
         <i>Example #2: </i> The Order Limit is set at $500 and the Time Frame Limit is set at $1,000 per 30 days with both rules enabled. A user can place two $499.99 orders within 30 days of each other, but the next order the user places within that same 30 day timeframe will trigger the Time Frame Limit rule.`,
  store_approvals_limitType:
    "This setting determines the action in the store at the time of checkout if the rule is triggered.",
  store_approvals_approvers: `One or more Approvers can be selected. If a company is selected as an Approver, all contacts under that company will have the Approver role. When there is more than one Approver, it is treated as an OR logic, meaning, any one of the Approvers listed can approve an order for that Group. <br/> <br/>
        An email notification is sent out to all Approvers anytime an order is pending approval or has been approved or rejected by an Approver. <br/> <br/>
        All Approvers are automatically treated as exempt from any Approval Groups and do not have any rules applied when placing their own orders.`,
  store_approvals_customers: `One or more companies or contacts can be selected for a Group. If a company is selected as a Customer, all contacts under that company will be subject to the Group, unless individual contacts are listed as Approvers or Approval Exempt Customers.`,
  store_approvals_exemptCustomers:
    "One or more companies or contacts can be selected to be exempt from Approvals. If a company is selected as an Approval Exempt Customer, all contacts under that company will be exempt from any Approval Groups and will process oders normally.",

  /* CATALOGS */
  /* Catalog Details */
  /* General */
  catalog_general_title:
    "The name of this Catalog. If you have one Catalog per Store, rather than shared Catalogs, it's a good idea to name the Catalog like the Store for ease of association.",

  /* Associated Collections */
  catalog_associated_selectCategory:
    "Select a collection's URL Handle to be included in the catalog.",
  catalog_associated_selectParentCategory:
    "Select a parent collection's URL Handle if you want to nest this collection in a hierarchy. Otherwise use the <b>No Parent - Root Level</b> option.",
  catalog_associated_displayOrder:
    "The lower the display order value, the higher the collection is displayed. If two collections share the same display order value and are in the same hierarchy, it is displayed in alphabetical order.",

  /* Products in Associated Collection */
  catalog_associated_productsInCollection:
    "Summary view of products in this collection.",

  /* Collection Available Dates */
  catalog_associated_availableDates:
    "Specify a store and start date to schedule availability of the collection.",

  /* COLLECTIONS */
  /* Collection Details */
  /* General Settings */
  collection_general_title:
    "Title of the collection shown on the store and the browser tab.",
  collection_general_navLabel: "Navigation label shown in the store menus.",

  /* Display Settings */
  collection_display_displayOrder:
    "The lower the display order value, the higher the collection is displayed. If two collections share the same display order value and are in the same hierarchy, it is displayed in alphabetical order.",
  collection_display_featuredCategory:
    "Enable or disable this collection in the <b>Featured Collections</b> section on the store homepage.",
  collections_display_displayChild:
    "Enable or disable showing child collections within a parent collection on the store. Parent/child hierarchies are created when association a collection with a catalog. ",

  /* Collection Image */
  collection_image:
    "Upload an image to display for this collection in the catalog. Only JPG, GIF and PNG images are supported. The file size should be less than 3MB. Your image will automatically be scaled so it displays correctly in the catalog.",
  collection_image_AltText:
    "Text that appears in place of the collection image if it fails to load on a user's screen. Also facilitates the use of screen-reading tools and allows search engines to better crawl and rank your store.",

  /* SEO Settings */
  collection_seo_title:
    "SEO specific title for this collection. By default it the same as the collection title.",
  collection_seo_description:
    "SEO specific description for this collection. Limited to 160 characters.",
  collection_seo_urlHandle: `URL handle is used to uniquely identify this collection throughout the system and is used in the store's URL structure. By default it is the same as the collection title with spaces and special characters replaced by "-". Use only characters a-z and 0-9. Use "-" instead of spaces.`,
  product_seo_title:
    "SEO specific title for this product. By default it the same as the product title.",
  product_seo_description:
    "SEO specific description for this product. Limited to 160 characters.",
  product_seo_urlHandle: `URL handle is used to uniquely identify this product throughout the system and is used in the store's URL structure. By default it is the same as the product title with spaces and special characters replaced by "-". Use only characters a-z and 0-9. Use "-" instead of spaces.`,

  /* DEPRACATED */
  portal_clear_cache:
    "This forces the server’s cache to be reset. All recent changes will immediately become available to the e-commerce customer, but this could temporarily increase page load time.",
  portal_product_settings: `<p>You can add the following substitution variables in your default meta tag text that will
      be substituted with the appropriate values at runtime when the page is displayed.</p>
      <p>&lt;NAME&gt; - Will substitute the Product Name.</p>
      <p>&lt;PRODUCT_NUM&gt; - Will substitute the Product Number (only for Product Pages).</p>
      <p>&lt;SKU&gt; - Will substitute the Product SKU (only for Product Pages).</p>
      <p>&lt;BRAND&gt; - Will substitute the Product Brand (only for Product Pages).</p>`,
  portal_category_settings: `<p>You can add the following substitution variables in your default meta tag text that will be substituted with the appropriate values at runtime when the page is displayed.</p>
      <p>&lt;NAME&gt; - Will substitute the Collection Name.</p>`,
  portal_info_page_settings: `<p>You can add the following substitution variables in your default meta tag text that will be substituted with the appropriate values at runtime when the page is displayed.</p>
      <p> &lt;NAME&gt; - Will substitute the Store Info Page title.</p>`,
  portal_use_company_id:
    "Requires the Company ID to be entered instead of the company name when signing up for an account on the Storefront. It must match an existing Company ID in Ecommerce. This allows you to limit sign-ups to only users who have a valid Company ID and will automatically add the user as a contact under that company.",
  tbd: "TBD",
  catalogs_profile:
    "Choose a Storefront for which the Collection effective date will apply.",
  catalogs_effective_date:
    "Choose an Effective Date (MM/DD/YY) for the Collection.",
  categories_description_long:
    "currently hidden	The long description is displayed on the collection page. It can include rich text and images.",
  categories_description_additional:
    "currently hidden	The additional description can potentially be positioned to appear at a different section of the collection page using the included templates.",
  categories_description_banner:
    "currently hidden	This collection banner will apply to collection pages",
  categories_seo_keywords:
    "Key words and phrases making it possible for customers to find the Collection via search.",
  portal_page_name:
    "The precise text to be used in the URL to uniquely identify this page. If you would like to seperate two words in your page name use a hyphen for sepreation. (ex: about-us)",
  portal_seo_keywords:
    "Key words and phrases making it possible for customers to find your site via search.",

  footer_column_label:
    "Customize the label shown on the storefront for this footer column.",

  portal_order_status:
    "When a new order is placed, it will be automatically set to this status.",
  portal_product_review_status:
    "The status selected will appear when a product is added by a franchise or mall admin.",
  portal_url:
    'Enter a fully qualified URL (example: http://mysite.com). Note that the "www" and any port number will be ignored.',

  portal_anonymous_user: "This is the default profile for anonymous users.",
  portal_registered_user: "This is the default profile for registered users.",
  portal_max_storefronts:
    "Maximum number of stores has been reached. To add more stores contact your sales rep.",

  products_product_image:
    "Upload a suitable JPG, GIF or PNG image. The file size should be less than 3 Meg and under 5000x5000px at 72dpi. Your image will automatically be scaled to display correctly in the Storefront.",
  products_product_artwork:
    "Custom product design can be configured using the Online Designer.",
  products_sku_storefront:
    "Select the Storefront you want to associate with the SKU effective date.",
  products_effective_date: "Enter the effective date to show this SKU/Product.",
  products_internal_product_code:
    "If different from the SKU, check the box and enter your unique internal or ERP product code.",
  products_SKU:
    "Enter the unique SKU (Stock Keeping Unit) assigned to this product.",
  products_SKU_associate:
    "Associate this SKU with a Profile and an Effective Date.",
  products_wholesale_price:
    "Wholesale price will be applied to profiles that have the wholesale setting enabled.",
  products_tax_class: "Select taxable or non-taxable.",
  products_display_order:
    "A product with lower display order will be displayed first.",
  products_free_shipping:
    "Enable free shipping for this product. All other shipping rules will be ignored.",
  products_search_description:
    "The search description is displayed in product search results. Enter 100 characters or less.",
  products_description:
    "The product description is displayed in the product page below the product title. Enter 300 characters or less.",
  products_feature_description:
    "Enter a list of product features to display. Leave blank to disable this tab.",
  products_product_specifications:
    "Enter product specifications and characteristics. Leave blank to disable this tab.",
  products_shipping_information:
    "Enter information about shipping methods and timelines. Leave blank to disable this tab.",
  products_assets:
    "Here you can upload files such as templates or other assets that are available on the storefront product page for site visitors to download. Any files here will be available to any user able to view this product's product page on the storefront. Files up to 75MB in size are allowed.",
  products_display_product:
    "Check to enable this product for display on the Storefront when associated with a Collection.",
  products_featured_product:
    "Check this box to feature the product on the Storefront homepage.",
  products_home_page_special:
    "Display product as a featured item on the home page.",
  products_call_for_pricing:
    "Prompt customer to call for pricing. Disable the add-to-cart button.",
  products_seo_friendly_page: `Use only characters a-z and 0-9. Use "-" instead of spaces.`,
  products_image_title:
    "Enter the title to be displayed for this product image. Leave blank for no title.",
  products_image_display_order:
    "An image with lower display order will be displayed first.",
  products_price: "Pricing per product for this tier.",
  products_tier_start:
    "Minimum quantity of items that must be ordered for this pricing to apply",
  products_tier_end:
    "Maximum quantity of items that must be ordered for this pricing to apply",
  products_product_name:
    "Name displayed in the search results, product listing, and product page.",
  products_min_selectable_quantity:
    "Enter the minimum quantity that can be selected when adding an item to the cart",
  products_max_selectable_quantity:
    "Enter the maximum quantity that can be selected when adding an item to the cart.",
  products_quantity_increment:
    "Enter the quantity increment that can be selected when adding a product to the cart.",
  products_retail_price: "The price of the product to be sold to the end user.",
  products_sale_price:
    "Enter a sale price to override the retail price in the Storefront. Leave blank to utilize the retail price.",
  products_width:
    "Define the width of the product artwork to be utilized by the Online Designer.",
  products_height:
    "Define the height of the product artwork to be utilized by the Online Designer.",
  products_length:
    "Define the length of the product as needed for reference only.",
  products_weight:
    "Define the weight of the product as needed to calculate shipping by weight.",
  products_shipping_cost:
    "Select a rate to be used in calculating the shipping cost for this product based on the Shipping Method configured in the Global Settings and selected by the end user during checkout.",
  products_integration_type: `Product level integration specifies how to handle a product in an order when it is syncronized with the linked MIS system.
                                <h4>Basic</h4>
                                Passes the product to the MIS on the order level with minimal MIS interaction.
                                <h4>Advanced</h4>
                                Associates the ecommerce product with a Quick Product resulting in streamlined MIS order processing, production and reporting.
                                <h4>Reporting</h4>
                                Results in classification of the product solely for reporting purposes using the Collection and Income Account specified.
                                <br/><br/>
                                The default settings are applied when creating a new product in ecommerce.`,
  products_seo_title: "A short accurate description of the Product.",
  products_seo_keywords:
    "Key words and phrases making it possible for customers to find your site via search.",
  products_seo_description: "160 character snippet summarizing the Product.",
  products_customizable:
    "An Online Designer product to be configured for artwork customization and presented to the end user with an artwork image generated by the Online Designer.",
  products_not_customizable:
    "A standard product with a static featured product image.",
  products_require_customization:
    "Requires end user to customize product prior to adding the product to the cart.",
  products_image_type:
    "Alternate Image - results in a thumbnail displayed below the feature image on the product page. Swatch - results in a small swatch displayed in the product configuration column and labeled with the alt text.",
  products_thumbs_on_product_page:
    "Check to display this alternate image on the product page as a thumbnail under the featured image. If unchecked this alternate image will not appear on the product page.",
  products_thumbs_on_category_page:
    "Check to display the swatch image type on the collection page overtop of the feature image.  If unchecked or if the Image Type is set to Alternate Image View the thumbnail will not show on the Collection page. ",
  products_image_alt_text:
    "Text description utilized as a text label by the Swatch Image Type.",
  products_sku: "The SKU associated with the product.",
  products_inventory_tracking: "Check to enable inventory tracking.",
  products_enable_file_uploads:
    "This setting enables or disables file uploads for this product.",
  products_variables_to_set: `Use to set additional variables in the corresponding Control product. <br />
                                Example: Machine=Mimaki JV-3 <br />
                                Variables are delimited by return line breaks.<br />
                                The following merge fields are also available: <br />
                                {{quantity}}<br />
                                {{productname}}<br />
                                {{description}}<br />
                                {{sku}}<br />
                                {{width}}<br />
                                {{height}}<br />
                                {{length}}<br />
                                For additional information, work with your implementation specialist.`,
  products_shipping_attributes_different:
    "Check if the dimensions of this product are different for shipping, e.g. a rolled up banner.",
  products_new_item:
    'Display the "New" badge on the product thumbnail on the Storefront.',
  addons_name:
    "Enter an internal name for this Add-On (Ex. Model 1234 Color). This is not displayed to the customer.",
  addons_title:
    'Enter a title for this product Add-On (Ex. "Color"). This is the label displayed for your customer.',
  addons_display_order:
    "An Add-On with lower display order will be displayed first.",
  addons_display_type:
    "Select a display type; drop down, radio, check box or text. The display type determines how the values associated with this Add-On will be displayed on the product page.",
  addons_optional:
    "Check to make this Add-On field optional. Uncheck if the field requires a response prior to adding an associated product to the cart.",
  addons_description:
    "The Add-On Tooltip text will appear in a tooltip next to the Add-On title on the product page.",
  addons_label: `Enter the label for this option value (Ex : "Fire engine red"). The label will appear as an option in a drop down, as a hint in a text field, and next to a radio button or check box.`,
  addons_retail_price: "Enter the retail price for this product Add-On.",
  addons_sale_price: "Enter the sale price for this product Add-On.",
  addons_wholesale_price: "Enter the wholesale price for this product Add-On.",
  addons_supplier: "Select where you get this product Add-On from.",
  addons_tax_class: "Select taxable or non-taxable.",
  addons_sku:
    "The SKU or stock keeping unit is the code assigned to your product inventory.",
  addons_quantity: "Quantity On Hand",
  addons_inventory_tracking: "Check to enable inventory tracking.",
  addons_reorder_level: "Re-Order Level",
  addons_free_shipping:
    "This applies only for custom shipping methods. Check this box to enable free shipping for this product. All other shipping rules will be ignored.",
  addons_shipping_type:
    'This setting determines the shipping rules that will be applied to this product. For ex: if you select "Flat Rate" then shipping will be calculated based on a flat rate per item.',
  addons_weight:
    "Leave blank if this does not apply. Note that the weight can be used to determine shipping cost.",
  addons_height:
    "Leave blank if this does not apply. Note that the height can be used to determine shipping cost.",
  addons_width:
    "Leave blank if this does not apply. Note that the width can be used to determine shipping cost.",
  addons_length:
    "Leave blank if this does not apply. Note that the length can be used to determine shipping cost.",
  addons_recurring_billing: "Enable recurring billing for this product.",
  addons_display_selected_item:
    "Check on one Add-On value to have the selected value be the default selected item in a drop down or set of radio/check boxes.",

  profiles_code: "Internal profile code used by your ERP system.",
  profiles_weighting: "Profiles with higher weighting will get precedence.",

  orders_orders_starting_id:
    "Orders higher than the supplied Starting Order ID value will be downloaded.",

  customers_customers:
    "Search customer accounts, view order history, and access service notes.",

  font_embedding_disallowed:
    "Each font has property embedded in the font file allowing or disabling font embedding. If a font doesn't allow embedding an asterisk will be displayed next to the font. If embedding is disabled the selected font will not be embedded in the PDF and the PDF will need to be viewed on a computer utilizing the installed font. Please avoid using fonts with embed disabled. If a non-embeddable font is required please disable PDF preview on artwork utilizing these fonts and test the printing process before proceeding.",

  styles_font_family: "Font family utilized in your Storefront.",
  styles_font_size: "Font size utilized in your Storefront.",
  styles_colors_header_background:
    "Background color in the Storefront's header.",
  styles_colors_header_text: "Color of text based headers.",
  styles_text_hover: "Default link hover color when not otherwise specified.",
  styles_top_navigation_text: "Color of the top navigation text.",
  styles_colors_top_navigation_background: "Color of the top navigation bar.",
  styles_top_navigation_hover: "Top navigation background color on rollover.",
  styles_colors_navigation_background: "Color of the primary navigation bar.",
  styles_navigation_hover: "Primary navigation background color on rollover.",
  styles_colors_input_text:
    "Header text color, verify this color provides sufficent contrast over top of the Header Background Color selected.",
  styles_colors_button_primary: "Primary button color.",
  styles_colors_button_secondary: "Secondary button color.",
  styles_colors_footer_background: "Background color utilized in the footer.",
  styles_colors_footer_text:
    "Footer text color, verify this color provides sufficent contrast over top of the Header Background Color selected.",
  styles_preview: "Dynamic preview demonstrating the colors selected above.",

  shipping_boxes_display_name:
    "Name of the box to describe it, must be unique.",
  shipping_boxes_length: "Define the length of the box.",
  shipping_boxes_weight: "Define the width of the box.",
  shipping_boxes_depth: "Define the height of the box.",
  shipping_boxes_box_weight: "Define the weight of the box itself.",
  shipping_boxes_max_weight:
    "Define the maximum weight the box can hold, this is typically listed as the 'Gross WT LT' on the box certificate seal on the bottom flap.",
  shipping_boxes_flute_direction:
    "Define the direction of the corrugated flutes on the sidewall of the box.",
  shipping_boxes_enable:
    "Enable this Shipping Box to be used in Carrier Rate calculations.",
  shipping_boxes_storefront:
    "Select All Storefronts if this box will be available for all storefronts. Select an individual storefront if this box will only be available for that storefront.",

  shipping_options_display_name:
    "Shipping option display name presented to the end user during checkout.",
  shipping_options_shipping_code:
    "Unique internal shipping code, similar to a product SKU.",
  shipping_options_storefront:
    "Select all Storefronts if this shipping option will be available on all Storefronts. Or a single Storefront if the shipping option will only apply to a single Storefront.",
  shipping_options_handling_charge:
    "Enter a value for the handling charge if applicable. Enter zero if no handling charge is to be charged for this shipping option.",
  shipping_options_destination_country:
    "Select All Countries if this shipping option will be available to all Countries. Or a single Country if the shipping option will only apply to a single Country.",
  shipping_options_enable:
    "Check to enable this Shipping Method during checkout for the applicable Storefront/s.",
  shipping_options_display_order:
    "A shipping option with a lower display order will be displayed first.",
  shipping_options_flat_base: "Enter a base rate if applicable.",
  shipping_options_flat_unit: "Enter the shipping cost per unit.",
  shipping_options_fixed_base:
    "Enter the base rate if applicable. The Fixed Rate per unit cost is applied on the product level in the General tab under Shipping Costs/Shipping Settings.",
  shipping_options_qty_base: "Enter a base rate if applicable.",
  shipping_options_qty_unit: "Enter the shipping cost per unit.",
  shipping_options_weight_base: "Enter a base rate if applicable.",
  shipping_options_weight_unit: "Enter the shipping cost per weight unit.",
  shipping_options_carrier:
    "Select the parcel carrier for this shipping method.",
  shipping_options_service_type:
    "Select the service type for this shipping method.",
  shipping_options_insurance: `Select the insurance option for this shipping rule. Any rate markups are not applied to the insurance portion of the rate.
  <br/>
  <h4><b>No Insurance</b></h4>
  Insurance is NOT included in the rate quote and NO option is presented to the customer at checkout.
  <h4><b>Include Insurance</b></h4>
  Insurance IS included in the rate quote and NO option is presented to the customer at checkout.
  <h4><b>No Insurance - Optional</b></h4>
  Insurance is NOT included in the rate quote by default but the customer can select to add it at checkout.
  <h4><b>Include Insurance - Optional</b></h4>
  Insurance IS included in the rate quote by default but the customer can deselect it at checkout.
  `,
  shipping_options_no_insurance:
    "Insurance is NOT included in the rate quote and NO option is presented to the customer at checkout.",
  shipping_options_inc_insurance:
    "Insurance IS included in the rate quote and NO option is presented to the customer at checkout.",
  shipping_options_no_insurance_opt:
    "Insurance is NOT included in the rate quote by default but the customer can select to add it at checkout.",
  shipping_options_inc_insurance_opt:
    "Insurance IS included in the rate quote by default but the customer can deselect it at checkout.",
  shipping_attributes_width: "Define the shipping width of the product.",
  shipping_attributes_height: "Define the shipping height of the product.",
  shipping_attributes_length: "Define the shipping length of the product.",
  shipping_attributes_weight:
    "Define the shipping weight of the product including additional materials required specifically for this product, e.g. cardboard core for a rolled up banner.",

  email_file_name:
    "The email template file name is defined here, file name must end in .htm or .html.",
  email_template_content:
    "The email template content is displayed and editable in this field.",

  contact_storefront:
    "Selecting a Storefront is required so that the reset password email can link back to the intended storefront for the reset password process.",

  undefined: " "
};
