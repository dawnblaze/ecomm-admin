import { Component, OnInit, Input, ElementRef, ContentChild, TemplateRef } from '@angular/core';
import { TooltipService } from '../../../../services/tooltip.service';
import { TooltipDictionary } from './tooltip-dictionary';
import { TooltipEvent } from '../../../../services/tooltip.service';
import { ThemeService } from '../../../../services/theme.service';

import popper from 'popper.js';

@Component({
  selector: 'cb-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.less'],
  inputs: ["expandLeft"]
})
export class TooltipComponent {
//   @Input() locationId: string;
//   @Input() expandLeft: boolean;
//   @Input() leftOffset: number;
//   @Input() topOffset: number;

//   public open: boolean = false;

  private locationDictionary = TooltipDictionary;
// "categories_feature_category": "Check to feature this Collection within the Catalog on the storefront home page. Uncheck to remove this Collection from the featured Collections list.";

//   constructor(public tooltip: TooltipService, private elRef: ElementRef, private themeService: ThemeService) {}

//   ngOnInit() {}

//   showTooltip() {
//     let bodyRect = document.body.getBoundingClientRect(),
//       elemRect = this.elRef.nativeElement.getBoundingClientRect();

//     // let data = {
//     //   text: this.locationDictionary[this.locationId],
//     //   left: elemRect.left - bodyRect.left,
//     //   top: elemRect.top - bodyRect.top
//     // };

//     this.locationId = this.locationId || 'undefined';

//     let data = new TooltipEvent(
//       this.locationDictionary[this.locationId].replace('MIS', this.themeService.currentTheme.name),
//       elemRect.left - bodyRect.left,
//       elemRect.top - bodyRect.top,
//       null,
//       this.leftOffset,
//       this.topOffset
//     );

//     if (this.expandLeft === true){
//       data.right = 360;
//     }

//     this.tooltip.show(data);
//   }

//   onClick(e) {
//     e.stopPropagation();
//     e.preventDefault();
//   }



// ENDOR TOOLTIP IMPLEMENTTION
  public explanationDiv:HTMLDivElement = null;

  @ContentChild(TemplateRef)
  tooltipTemplate: TemplateRef<any> = null;

  @Input() tooltip: string;
  // <cb-tooltip-string tooltip="Location In-Store Credit"></cb-tooltip-string>

  // //// TEMP CODE
  @Input() locationId: string;
  @Input() leftOffset: number;
  @Input() topOffset: number;
  // //// TEMP CODE
  @Input() tooltipPlacement: any = "top-end"

  @Input() tooltipPositionClass: string = '';

  @Input() computedPosition:boolean = true;

  @Input() infoIcon: boolean = false;

  myPopper:popper = null;
  public open: boolean = false;
  public hostElem:HTMLElement;

  constructor(private elemRef:ElementRef) {
    this.hostElem = elemRef.nativeElement;
  }

  onContainerMouseLeave(ev:MouseEvent){
    if(this.hostElem.className.includes('cb_tooltip-sticky'))
      return;
    this.myPopper && this.myPopper.disableEventListeners();
    this.open = false;
  }

  onContainerMouseEnter(ev:MouseEvent){
    this.myPopper && this.myPopper.enableEventListeners();
    this.schedulePopperUpdate();
    this.open = true;
  }

  onClick(ev:MouseEvent){
    ev.stopPropagation();
    document.querySelectorAll('.cb_tooltip-sticky').forEach(item => {
      item.classList.remove('cb_tooltip-sticky');
    });
    if(!this.hostElem.className.includes('cb_tooltip-sticky')){
      document.querySelectorAll('.cb_tooltip-explanation.open').forEach(item => {
        if(item != this.explanationDiv)
          item.classList.remove('open');
      });
      if(!this.explanationDiv.className.includes('open'))
        this.explanationDiv.classList.add('open');
      this.hostElem.classList.add('cb_tooltip-sticky');
    } else {
      this.onContainerMouseLeave(ev);
    }
  }

  schedulePopperUpdate(){
    this.myPopper && this.myPopper.update();
    this.myPopper && this.myPopper.scheduleUpdate();
  }

  initComputedPopover(){
    this.explanationDiv = this.hostElem.querySelector('.cb_tooltip-explanation') as HTMLDivElement;
    let containerDiv = this.hostElem.querySelector('.cb_tooltip-string-container') as HTMLDivElement;
    document.body.appendChild(this.explanationDiv);
    this.myPopper = new popper(containerDiv, this.explanationDiv, {
      placement: this.tooltipPlacement,
      modifiers: {
        offset: {
          enabled: true,
          offset: '0,8'
        }
      }
    });
    this.myPopper && this.myPopper.disableEventListeners();
    this.schedulePopperUpdate();
  }

  ngAfterViewInit(){
    if(this.computedPosition){
      this.initComputedPopover();
      this.hostElem.ownerDocument.onclick = (event) => {
        document.querySelectorAll('.cb_tooltip-sticky').forEach(item => {
          item.classList.remove('cb_tooltip-sticky');
        });
        document.querySelectorAll('.cb_tooltip-explanation.open').forEach(item => {
          item.classList.remove('open');
        });
      };
    }
  }


  ngOnDestroy(){
    this.myPopper && this.myPopper.destroy();
    this.explanationDiv && this.explanationDiv.remove();
  }
}
