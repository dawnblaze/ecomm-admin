import { Component, OnInit, HostBinding } from '@angular/core';
import { TooltipService } from '../../../../../services/tooltip.service';

@Component({
  selector: 'cb-tooltip-explanation',
  templateUrl: './tooltip-explanation.component.html',
  styles: [
    `:host {
      position:absolute;
    }`
  ]
})
export class TooltipExplanationComponent implements OnInit {
  @HostBinding('style.left.px') leftLocation;
  @HostBinding('style.top.px') topLocation;
  @HostBinding('style.right.px') rightLocation;
  text: string;
  show: boolean;

  constructor(private tooltip: TooltipService) { }

  ngOnInit() {
    this.tooltip.showTooltip.subscribe((e) => {
      if (e) {
        this.text = e.text;

        if (e.right) {
          this.rightLocation = e.right;
        } else {
          this.leftLocation = e.left;
        }
        
        this.topLocation = 73 + e.top;
        this.show = true;
      }
      else {
        this.show = false;
      }
    });
  }
}
