import { Component, Input, EventEmitter, Output, AfterViewInit, HostListener, ElementRef, SimpleChange, SimpleChanges, ViewChild, TemplateRef, ContentChild, QueryList, ContentChildren, OnDestroy, Renderer2 } from '@angular/core';
import { Dialog, DialogModule } from 'primeng/primeng';
import { CbTemplateDirective } from '../../directives/cb-template.directive';
import { BlurAndClickService } from '../../../../services/blurAndClick.service';
import { Subscription } from 'rxjs';
import { EventService } from '../../../../services/event.service';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'base-modal',
  templateUrl: './base-modal.component.html',
  styleUrls: ['./base-modal.component.less']
})

export class BaseModalComponent implements AfterViewInit, OnDestroy {

  @Input() closeDisabled: boolean = false;
  @Input() AllowFullscreen: boolean = false;
  @Input() closable: boolean = true;
  @Input() draggable: boolean = true;

  @Output()
  public onCloseDialog = new EventEmitter<boolean>();

  @Output()
  public onCancelDialog = new EventEmitter<boolean>();

  @Output()
  public onSaveDialog = new EventEmitter<boolean>();

  @Output()
  public onSkipDialog = new EventEmitter<boolean>();

  @Output()
  public onAddDialog = new EventEmitter<void>();

  @Output() onButtonTabKey = new EventEmitter<Event>();

  @Input()
  public cancellable: boolean = false;

  private _displayDialog: boolean = false;

  @Input() isLoading: boolean = false;

  @Input() showSecondIcon: boolean = false;

  @Input() modalContentClasses: string = '';

  @Input()
  public set displayDialog(value: boolean) {
    let wasDisplayed: boolean = this._displayDialog;
    this._displayDialog = value;

    if (wasDisplayed && !value)
      this.onHideModal();
  }
  public get displayDialog() {
    return this._displayDialog;
  }

  @Input()
  public iconClass: string = '';

  @Input()
  public headerText: string = '';

  @Input()
  public btnSaveText: string = '';

  @Input()
  public btnAddText: string = '';

  @Input()
  public btnSkipText: string = '';

  @Input()
  public isDisableSkipBtn: boolean = true;

  @Input()
  public btnSaveDisable: boolean = false;

  @Input()
  public btnCancelText: string = '';

  @Input()
  public btnCancelDisable: boolean = false;

  @Input() showSkip: boolean = false;

  @Input()
  public inputElementRef: ElementRef;

  @Input()
  public loaderClassName: string = 'full-page-loader';

  @Input()
  public width: string = "393";

  @Input()
  public height: string = "663";

  @Input()
  public cloneModalStyling: boolean = false;

  @Input()
  public focusButtonOnShow: boolean = true;

  @Input()
  public EnableNextButton: boolean = false;

  @Output()
  public onNextDialog = new EventEmitter<boolean>();

  @Input()
  public MaxNextCounter: number = 1;

  public NextCounter: number = 1;

  @Input()
  public customClass: string = '';

  @Input()
  public noHeader: boolean = false;

  @Input()
  public noFooter: boolean = false;

  @Input()
  public closeOnXClick: boolean = true;

  @ViewChild('btnSave') vcBtnSave: ElementRef<HTMLButtonElement> = null;
  @ViewChild('btnCancel') vcBtnCancel: ElementRef<HTMLButtonElement> = null;

  // Default to true, so when modal is close we remove the class that disable the body element overflow
  // Set to false to prevent the removing of class that disable the body element overflow
  // Implemented to use on modals within modals
  @Input()
  public onCloseBodyHasScroll: boolean = true;

  @ViewChild('PrimeDialog') dialog: Dialog;

  @ContentChildren(CbTemplateDirective) templates: QueryList<CbTemplateDirective>;

  public headerTemplate: TemplateRef<any> = null;
  public positionTop: number = 0;
  public positionLeft: number = 0;
  public transitionOptions: string = '0.25s'

  windowKeydownHandler(event: KeyboardEvent) {
    if (event.keyCode === 27) {
      if (!this.closeDisabled) {
        this.onCloseDialog.emit(false);
      }

      if (this.cancellable) {
        this.onCancelDialog.emit(false);
      }
    }
  }

  windowResizeHandler() {
    // if (this._displayDialog)
    // this.centerModalOnPage();
  }

  // @HostListener('document:click', ['$event'])
  onClickHandler(event: MouseEvent) {
    if (event &&
      this.closeOnXClick &&
      event.srcElement &&
      event.srcElement['className'] === 'pi pi-times' &&
      event.srcElement['parentElement'] &&
      event.srcElement['parentElement'].classList.contains('ui-dialog-titlebar-close')) {
      this.onCancelDialog.emit(false);
    }
  }

  constructor(blurClickSvc: BlurAndClickService, eventSvc: EventService, private elemRef: ElementRef, private renderer: Renderer2) {

    blurClickSvc.FocusMapStream.pipe(untilDestroyed(this)).subscribe((event) => {
      if (event &&
        this.closeOnXClick &&
        event.click.srcElement &&
        event.click.srcElement['className'] === 'pi pi-times' &&
        event.click.srcElement['parentElement'] &&
        event.click.srcElement['parentElement'].classList.contains('ui-dialog-titlebar-close')) {
        this.onCancelDialog.emit(false);
      }
    });
    eventSvc.KeyDownStream.pipe(untilDestroyed(this)).subscribe((event) => this.windowKeydownHandler(event));
    eventSvc.WindowResizeStream.pipe(untilDestroyed(this)).subscribe(() => this.windowResizeHandler());
  }
  public ngOnDestroy() {
    this.vcBtnCancel = null;
    this.vcBtnSave = null;
    this.dialog = null;
  }

  ngAfterContentInit() {
    this.initializeTemplates();
  }

  ngAfterViewInit() {
    // addClass to all element that has the class.
    $(this.elemRef.nativeElement).find('.ui-dialog-titlebar').addClass('ui-dialog-titlebar-custom noselect');
    $(this.elemRef.nativeElement).find('.ui-dialog-content').addClass('ui-dialog-content-custom');
    $(this.elemRef.nativeElement).find('.ui-dialog-footer').addClass('ui-dialog-footer-custom');
  }

  ngOnChanges(changes: SimpleChanges) {
    let displayDialogChanges: SimpleChange = changes.displayDialog;
    if (displayDialogChanges && displayDialogChanges.currentValue) {
      if (this.inputElementRef) {
        let inputElement = (this.inputElementRef.nativeElement) as HTMLInputElement;
        if (inputElement)
          setTimeout(() => inputElement.focus(), 250);
      }
    }
  }

  initializeTemplates() {
    if (this.templates) {
      this.templates.forEach(cbTemplate => {
        switch (cbTemplate.name.toLowerCase()) {
          case "header":
            this.headerTemplate = cbTemplate.template;
            break;

          default:
            break;
        }
      });
    }
  }

  centerModalOnPage() {
    if (this.dialog) {
      let currentBodyHeight = document.querySelector('#cb').clientHeight;
      let currentBodyWidth = document.querySelector('#cb').clientWidth;
      let currentModalHeight = this.dialog.headerViewChild.nativeElement.parentElement.clientHeight;
      let currentModalWidth = this.dialog.headerViewChild.nativeElement.parentElement.clientWidth;

      this.positionTop = ((currentBodyHeight - currentModalHeight) / 2);
      this.positionLeft = ((currentBodyWidth - currentModalWidth) / 2);

      setTimeout(() => {
        jQuery(this.dialog.headerViewChild.nativeElement.parentElement)[0].style.top = `${this.positionTop}px`;
        jQuery(this.dialog.headerViewChild.nativeElement.parentElement)[0].style.left = `${this.positionLeft}px`;
      }, 1);
    }
  }

  onShowModal() {
    this.transitionOptions = '0ms';
    this.dialog.focusOnShow = this.focusButtonOnShow;

    jQuery(this.dialog.headerViewChild.nativeElement.parentElement).addClass('cb_base-modal'); //add class on modal show
    // this.centerModalOnPage();
    jQuery('body').addClass('cb_no-scroll');
  }

  onHideModal() {
    this.transitionOptions = '0.25s';

    if (this.dialog && this.dialog.headerViewChild)
      jQuery(this.dialog.headerViewChild.nativeElement.parentElement).removeClass('cb_base-modal'); //hide modal container on modal close

    if (this.onCloseBodyHasScroll)
      jQuery('body').removeClass('cb_no-scroll');

    this.onCloseDialog.emit(false); //added for gl-account-modal not being able to be opened again

    // reset next counter on close
    this.NextCounter = 1;
  }

  public OnNextDialog(event: MouseEvent) {
    event.preventDefault();
    event.stopPropagation();

    this.onNextDialog.emit(true);
    this.NextCounter++;
  }

  public OnMaximizeModal() {
    this.dialog.maximize();
    // this.dialog.contentViewChild.nativeElement.style.height = `calc(100vh - 48px)`;
    // this.dialog.contentViewChild.nativeElement.style.width = `calc(100vw - 48px)`;
  }

  public OnRevertMaximizeModal() {
    this.dialog.revertMaximize();
    setTimeout(() => {
      this.dialog.container.style.height = `${this.height}px`;
      this.dialog.container.style.width = `${this.width}px`;
      // this.centerModalOnPage();
    }, 300);
  }
}
