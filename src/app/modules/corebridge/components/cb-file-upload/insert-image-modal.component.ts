import { Component, OnInit } from "@angular/core";
import { ProductInformationInsertImageComponent } from '../../../ecommerce/components/product-information-insert-image/product-information-insert-image.component';
import { ZnodeService } from "../../../../services/znode.service";
import { untilDestroyed } from "ngx-take-until-destroy";
import { ProductImage } from '../../../ecommerce/models/product-image'

@Component({
  selector: 'cb-insert-image-modal',
  templateUrl: '../../../ecommerce/components/product-information-insert-image/product-information-insert-image.component.html',
  styleUrls: ['../../../ecommerce/components/product-information-insert-image/product-information-insert-image.component.less']
})

export class InsertImageModalComponent extends ProductInformationInsertImageComponent implements OnInit{

  public files: any[] = []
  private portalId: number;
  progress: number = 0;
  uploadedImages = []

  constructor(znodeService: ZnodeService) {
    super(znodeService);
  }

  ngOnInit() {
    this.znodeService.getPortals().subscribe(
      portals => {
        console.log('ngOnInit', this.portalId)
        this.portalId = portals[0].PortalId;
      }
    );

  }
  close(saveAndClose: boolean) {
    this.displayDialog = false;
    if (saveAndClose) this.onUploadDone.emit(this.uploadedImages);
    else this.uploadedImages = [];
  }

  onSelect(event: any){
    let files = event.files;
    if (files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        let file: any = files[i]
        // Add an empty ProductAsset object to array
        let image: any = {}
        image["File"] = file;
        image.ProductFile = file.name;
        image.IsUploading = true;
        image.UploadProgress = 0;
        image.FileSize = (file.size / 1024).toFixed(2) + ' KB';
        image["UploadedSize"] = '0 KB';
        this.uploadingFiles.push(image);
      }


      let uploadFiles = this.uploadingFiles.filter(file => file.UploadProgress == 0);
      let remainingUploads = uploadFiles.length;

      for (let i = 0; i < uploadFiles.length && !this.cancel; i++) {
        let productImage = uploadFiles[i];
        this.znodeService.uploadImage(productImage["File"], this.portalId)
        .pipe(untilDestroyed(this))
        .subscribe(
          (asset: any) => {
            let index = uploadFiles.indexOf(productImage);
            if (typeof asset === "number") {
              uploadFiles[index]["UploadProgress"] = asset;
              if (asset === 100) uploadFiles[index]["UploadMessage"] = "Saving";
              else {
                let file = uploadFiles[index]["File"];
                uploadFiles[index]["UploadedSize"] = ((file.size / 1024) * asset / 100).toFixed(2) + ' KB';
              }
            } else {
              if ('readyState' in asset) {
                // If returned value is an XMLHttpRequest
                uploadFiles[index]["XmlHttpRequest"] = asset;
              } else {

                uploadFiles[index]["IsUploading"] = false;
                uploadFiles[index]["UploadProgress"] = 100;
                if(this.cancel) {
                  // this.znodeService.deleteProductAsset(asset.ProductAssetId).pipe(untilDestroyed(this)).subscribe();
                } else {
                  asset.UploadProgress = 100;
                  // this.uploadedFiles.push(asset);
                  // this.uploadingFiles.splice(this.uploadingFiles.indexOf(asset), 1);
                  // console.log('this.uploadedFiles', this.uploadedFiles)
                  this.uploadedImages.push(new ProductImage(asset));
                  remainingUploads--;
                  if (remainingUploads == 0) this.isSubmitValid = true;
                }
              }
            }
            this.isFileUploading = this.uploadingFiles.length > this.uploadedFiles.length;
          }
        );
      }

    }
    // let query = files.map(file => {
    //   return this.znodeService.uploadImage(file, this.portalId).pipe(map((data: any) => {
    //     let { name, size, type, lastModified } = file;
    //     Object.assign(data, { name, size, type, lastModified });
    //     return data
    //   }))
    // })
    // console.log('queru', query)
    // forkJoin(query).subscribe(result => {
    //   console.log('resulttt', result)
    // })

    // forkJoin(query).subscribe((result: any) => {
    //   this.files.forEach((file: any) => {
    //     let findItem: any = result.find((item: any) => item.name == file.name && item.size == file.size)
    //     if (findItem) {

    //       let { ImagePath } = findItem;
    //       Object.assign(file, { ImageFileTmp: ImagePath });
    //     }
    //   })
    // })
  }
}
