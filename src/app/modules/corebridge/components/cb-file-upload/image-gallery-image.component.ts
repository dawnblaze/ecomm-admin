import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgxDropzonePreviewComponent } from 'ngx-dropzone';
import { DomSanitizer } from '@angular/platform-browser';
import { IimageGalleryImageItem } from '../../models/image_gallery_image';

@Component({
  selector: 'app-image-gallery-image',
  templateUrl: './image-gallery-image.component.html',
  styleUrls: ['./image-gallery-image.component.less'],
  // providers: [
  //   {
  //     provide: NgxDropzonePreviewComponent,
  //     useExisting: ImageGalleryImageComponent
  //   }
  // ]
})
export class ImageGalleryImageComponent implements OnInit {

  displayDialog: boolean = false;
  imageThumbnail: string;

  constructor() {
    // super(sanitizer);
  }

  // whether to enable the info button
  @Input() enableInfoButton: number;

  // whether to enable the remove button
  @Input() enableRemoveButton: number;

  // custom data provided by the caller, which is sent back on events
  @Input() data: IimageGalleryImageItem;

  // cb-file-upload
  @Output() onRemoveFile = new EventEmitter<IimageGalleryImageItem>();
  @Output() onDisplayInfo = new EventEmitter<IimageGalleryImageItem>();

  ngOnInit() {
    console.log('DROPZONE LOADED');
    console.log(typeof this.data);
    console.log(this.data);
    if (!this.data) {
      console.error('No data to read. Please provide some data using the [data] Input property.');
      return;
    }
    this.imageThumbnail = this.data.getImageThumbnail();
  }

  ngOnChanges() {
    if (!this.imageThumbnail) {
      this.imageThumbnail = this.data.getImageThumbnail();
    }
  }

  deleteFileCallback(ev){
    console.log('File to be deleted...', this.data);
    ev.stopPropagation();
    this.onRemoveFile.emit(this.data);
  }

  displayInfoCallback(ev) {
    console.log('File to be displayed...', this.data);
    ev.stopPropagation();
    this.onDisplayInfo.emit(this.data);
  }

  
}
