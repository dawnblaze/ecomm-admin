
import { Component, OnInit, OnChanges, Output, EventEmitter, Input, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { untilDestroyed } from "ngx-take-until-destroy";


import { forkJoin } from 'rxjs';
import { ZnodeService } from '../../../../services/znode.service';
import { tap, map } from 'rxjs/operators';
import { InsertImageModalComponent } from "./insert-image-modal.component";
import { IimageGalleryImageItem } from '../../models/image_gallery_image';


import { NgxDropzoneComponent  } from 'ngx-dropzone';
import { ProductImage } from '../../../ecommerce/models/product-image'

// dragula
import { DragulaService } from 'ng2-dragula';


@Component({
  selector: 'cb-file-upload',
  templateUrl: './cb-file-upload.component.html',
  styleUrls: ['./cb-file-upload.component.less'],
})

export class CbFileUploadComponent extends NgxDropzoneComponent implements OnInit, AfterViewInit {


  // TEST
  // public zitems: any[] = [{"name":1}, {"name":2, "image":"../../assets/img/c64-classic.jpg"}, {"name":3}, {"name":4}, {"name":5}, {"name":6}, {"name":7}, {"name":8},
  // {"name":9}];
  /////////

  @Input() items: IimageGalleryImageItem[] = [];
  @Input() files: File[] = [];

  //whether to enable the info button
  @Input() enableInfoButton: number;

  // whether to enable the remove button
  @Input() enableRemoveButton: number;

  @Output() onUploadComplete: EventEmitter<any> = new EventEmitter<any>();
  @Output() onDelete: EventEmitter<IimageGalleryImageItem> = new EventEmitter<any>();
  @Output() onDisplayInfo: EventEmitter<IimageGalleryImageItem> = new EventEmitter<any>();
  portalId: number;


  @ViewChild("manualUpload")
  @ViewChild("selected_item") selected_item: ElementRef;
  manualUpload: InsertImageModalComponent;

  public subs = new Subscription();

  constructor(private znodeService: ZnodeService, private dragulaService: DragulaService) {
    super(null)

    this.dragulaService.createGroup("ITEMS", {
      // Assign options here...
    })


    this.dragulaService.dropModel("ITEMS")
      .subscribe( args => {
          let element = args.el.firstElementChild;
          let tElementStyle = element.getAttribute('style');
          tElementStyle += "width: 96px; height: 96px;";
          // tElementStyle += "width: 96px; height: 96px; background-position: center; background-repeat: no-repeat; background-size: 100%;"
          element.setAttribute('style',tElementStyle);
          console.log('dropModel: ',args);
          // console.log('dropItem: ', element);
        })

    this.dragulaService.drag("ITEMS")
    .subscribe( args => {

      let elementThumbnailHeader = args.el.firstElementChild.firstElementChild;
      let elementItemBox = args.el.firstElementChild;
      // hide header when moving
      // elementThumbnailHeader.setAttribute('style','opacity: 1');

      // set element item-box styling on drag



      let tElement = elementItemBox.getAttribute('style');
      tElement += "width: 40px; height: 40px; cursor: grabbing; "
      elementItemBox.setAttribute('style',`${tElement}`);


      console.log('Dragged Item: ',args, tElement);
    })

    this.dragulaService.drop("ITEMS")
    .subscribe(args => {
      let elementThumbnailHeader = args.el.firstElementChild.firstElementChild;
      let elementItemBox = args.el.firstElementChild;
      //elementThumbnailHeader.setAttribute('style','opacity: 1');

      let tElementStyle = elementItemBox.getAttribute('style');
      tElementStyle += "width: 96px; height: 96px; background-position: center; background-repeat: no-repeat; background-size: 100%;"
      elementItemBox.setAttribute('style',`${tElementStyle}`);
      console.log('Drop args: ', args, tElementStyle);
    })

    this.dragulaService.out("ITEMS")
    .subscribe(args => {
      // reset to original size if dragged out of container
      let elementItemBox = args.el.firstElementChild;
      //elementThumbnailHeader.setAttribute('style','opacity: 1');

      let tElementStyle = elementItemBox.getAttribute('style');
      tElementStyle += "width: 96px; height: 96px; background-position: center; background-repeat: no-repeat; background-size: 100%;"
      elementItemBox.setAttribute('style',`${tElementStyle}`);
    })

  }




  ngOnInit(){



    this.znodeService.getPortals().subscribe(
      portals => {
        this.portalId = portals[0].PortalId;
      }
    );
  }

  ngAfterViewInit(){


    // this.subs.add(this.dragulaService.drag("items"))
  }

  ngOnDestroy(){
    this.subs.unsubscribe();
    this.dragulaService.destroy("ITEMS");
  }



  //ngOnChanges() {}


  onSelect(event) {
    this.files = [];
    this.files.push(...event.addedFiles);

    for (let i = 0; i < this.files.length; i++) {
      this.items.push(new ProductImage())
    }

    let query = event.addedFiles.map(file => {
      return this.znodeService.uploadImage(file, this.portalId).pipe(map((data: any) => {
        let { name, size, lastModified } = file;
        console.log('on select');
        console.log(data);
        Object.assign(data, { name, size, lastModified });
        return data
      }))
    })

    forkJoin(query).subscribe((result: any) => {
      console.log('on uploaded complete');
      console.log(result);
      this.onUploadComplete.emit(result);
    })

    // to remove drag-start class from the body tag when an image is dropped in the dropzone
    document.querySelector('body').classList.remove("drag-start");

  }

  remove(data: IimageGalleryImageItem) {
    console.log('Removing File...');
    this.onDelete.emit(data);
    console.log('data.getUniqueIdentifier()', data.getUniqueIdentifier())
    this.items = this.items.filter((i)=>{
      if(i.getUniqueIdentifier() !== data.getUniqueIdentifier()){
        return true
      }
    })
    console.log('Files update: ', this.files);
  }

  displayInfo(data: IimageGalleryImageItem) {
    console.log('Displaying info...');
    this.onDisplayInfo.emit(data);
  }

  showManualUploadModal(ev) {
    ev.stopPropagation();
    this.manualUpload.open();
  }

  manualUploadDone(uploads) {
    uploads.forEach(item => {
      item.ImageFile = item.ImagePath;
    })
    this.files.push(...uploads);
    this.onUploadComplete.emit(uploads);

  }

  _onClick() {
    // to remove _onClick error when uploader is shown
  }



}
