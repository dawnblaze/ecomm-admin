import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { RouterModule }   from '@angular/router';
import { PopoverModule } from 'ngx-bootstrap';
import { TableModule } from "primeng/table";
import { NgxDropzoneModule } from 'ngx-dropzone';

// components
import { PageComponent } from './components/page/page.component';
import { HeaderComponent } from './components/page/header/header.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { SidebarComponent } from './components/page/sidebar/sidebar.component';
import { AlertsComponent } from './components/page/alerts/alerts.component';
import { ModalsComponent } from './components/page/modals/modals.component';

import { TableComponent } from './components/table/table.component';
import { TableToolbarComponent } from './components/table/table-toolbar/table-toolbar.component';
import { TablePaginationComponent } from './components/table/table-pagination/table-pagination.component';

import { SvgComponent } from './components/svg/svg.component';
import { IconComponent } from './components/cb-icon/cb-icon.component';
import { ErrorComponent } from './components/error/error.component';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { TooltipExplanationComponent } from './components/tooltip/tooltip-explanation/tooltip-explanation.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { TabComponent } from './components/tabs/tab/tab.component';

import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';

import { Select2Module } from 'ng2-select2';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { Ng2FiledropExtendedDirective } from './ng2-file-upload-overridden/ng2-filedrop-extended.directive';

import { RecordSearchActionsComponent } from "./components/record-search-actions/record-search-actions.component";
import { RecordSearchFilterListComponent } from "./components/record-search-filter-list/record-search-filter-list.component";
import { CbSelectComponent } from "./components/cb-select/cb-select.component";
import { CbRowActionMenuComponent } from "./components/cb-row-action-menu/cb-row-action-menu.component";
import { Table2Component } from "./components/table2/table.component";
import { AutosaveBarComponent } from "./components/autosave-bar/autosave-bar.component";

import { BaseModalComponent } from './components/base-modal/base-modal.component';
import { FullSizeModalComponent } from './components/full-size-modal/full-size-modal.component';
import { FlexModalComponent } from './components/flex-modal/flex-modal.component';

import { CbFileUploadComponent } from './components/cb-file-upload/cb-file-upload.component';
import { ImageGalleryImageComponent } from './components/cb-file-upload/image-gallery-image.component';

//services
import { DomainService } from '../../services/domain.service';
import { AuthService } from '../../services/auth.service';
import { AlertService } from '../../services/alert.service';
import { ModalService } from '../../services/modal.service';
import { CanDeactivateGuard } from '../../services/can-deactivate-guard.service';
import { TooltipService } from '../../services/tooltip.service';
import { SupportAccountService } from '../../services/support-account.service';
import { BlurAndClickService } from '../../services/blurAndClick.service';
import { EventService } from '../../services/event.service';

// pipes
import { PhonePipe } from '../../pipes/phone.pipe';
import { OrderByPipe } from '../../pipes/orderby.pipe';
import { ArrayMapPipe } from '../../pipes/array-map.pipe';

// directives
import { CbButtonDirective } from "./directives/cb-button.directive";
import { CbTemplateDirective } from "./directives/cb-template.directive";
import { CbParentClassDirective } from "./directives/cb-parent-class.directive";
import { CbInputRestrictDirective } from "./directives/cb-input-restrict.directive";
import { CbInputCurrencyDirective } from "./directives/cb-input-currency.directive";
import { CbInlineSubjectDirective } from './directives/cb-inline-subject.directive';
// new pagination
import { PaginationComponent } from './components/table/pagination/pagination.component';

// PrimeNG
import {
  ButtonModule,
  CheckboxModule,
  DialogModule,
  FileUploadModule,
  ToolbarModule
} from 'primeng/primeng';

import { InsertImageModalComponent} from "./components/cb-file-upload/insert-image-modal.component"
import { EndNglibraryModule } from "@corebridge/end-nglibrary";

// CDK
import { DragDropModule } from "@angular/cdk/drag-drop";


// Dragula
import { DragulaModule } from 'ng2-dragula';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    Select2Module,
    BsDropdownModule.forRoot(),
    ButtonModule,
    CheckboxModule,
    DialogModule,
    ToolbarModule,
    FileUploadModule,
    PopoverModule.forRoot(),
    TableModule,
    NgxDropzoneModule,
    EndNglibraryModule,
    DragulaModule.forRoot(),
    DragDropModule
  ],
  declarations: [
    PageComponent,
    HeaderComponent,
    BreadcrumbsComponent,
    SidebarComponent,
    AlertsComponent,
    ModalsComponent,
    BaseModalComponent,
    FullSizeModalComponent,
    FlexModalComponent,
    LoginComponent,
    LogoutComponent,
    TableComponent,
    TableToolbarComponent,
    TablePaginationComponent,
    SvgComponent,
    IconComponent,
    ErrorComponent,
    TooltipComponent,
    TooltipExplanationComponent,
    TabsComponent,
    TabComponent,
    AutosaveBarComponent,
    PhonePipe,
    OrderByPipe,
    ArrayMapPipe,
    PaginationComponent,
    RecordSearchActionsComponent,
    RecordSearchFilterListComponent,
    CbButtonDirective,
    CbTemplateDirective,
    CbSelectComponent,
    CbRowActionMenuComponent,
    CbParentClassDirective,
    Table2Component,
    CbInputRestrictDirective,
    CbInputCurrencyDirective,
    CbInlineSubjectDirective,
    Ng2FiledropExtendedDirective,
    CbFileUploadComponent,
    ImageGalleryImageComponent,
    InsertImageModalComponent
  ],
  exports: [
    PageComponent,
    HeaderComponent,
    BreadcrumbsComponent,
    SidebarComponent,
    AlertsComponent,
    ModalsComponent,
    BaseModalComponent,
    FullSizeModalComponent,
    FlexModalComponent,
    LoginComponent,
    LogoutComponent,
    TableComponent,
    TableToolbarComponent,
    TablePaginationComponent,
    SvgComponent,
    IconComponent,
    ErrorComponent,
    TooltipComponent,
    TabsComponent,
    TabComponent,
    AutosaveBarComponent,
    PhonePipe,
    OrderByPipe,
    ArrayMapPipe,
    PaginationComponent,
    RecordSearchActionsComponent,
    RecordSearchFilterListComponent,
    CbButtonDirective,
    CbTemplateDirective,
    CbSelectComponent,
    CbRowActionMenuComponent,
    CbParentClassDirective,
    Table2Component,
    CbInputRestrictDirective,
    CbInputCurrencyDirective,
    CbInlineSubjectDirective,
    Ng2FiledropExtendedDirective,
    CbFileUploadComponent,
    ImageGalleryImageComponent,
    InsertImageModalComponent
  ],
  providers: [ DomainService, AuthService, AlertService, ModalService, CanDeactivateGuard, TooltipService, SupportAccountService, BlurAndClickService, EventService ]
})
export class CorebridgeModule {
}
