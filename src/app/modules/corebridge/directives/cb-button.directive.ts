import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[CbButton]'
})
export class CbButtonDirective {
  btnDefaultWith: string = '30px';
  btnPaddingLeft: number = 20;
  constructor(private el: ElementRef) { }

  @Input()
  public DisableCbButtonHover: boolean = false;

  /***
   * These code can be implemented using css 
   * By using "always-show-label" & "hover" pseudo selector 
   * 
  @HostListener('mouseenter')
  onMouseEnter() {
    if (!this.DisableCbButtonHover)
      this.resizeButtonToTextWidth();
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    if (!this.DisableCbButtonHover) {
      let btn = document.querySelector('.addAction') as HTMLElement;
      btn.style.width = this.btnDefaultWith;
    }
  }

  private resizeButtonToTextWidth() {
    $(this.el.nativeElement).find('.addAction').width($(this.el.nativeElement).find('.addActionHover').width() + this.btnPaddingLeft);
  }
   **/

}