import { Directive, ElementRef, Input } from '@angular/core';

/**
 * Some elements are not reachable by code of ngClass or [class]
 * e.g.
 * <ng-template>
 *    <td>some table data</td>
 *    <td>another table data</td>
 * <ng-template>
 * in this case we dont have access to <tr>
 */
@Directive({
  selector: '[cbParentClass]'
})
export class CbParentClassDirective {

  hostElement:HTMLElement = null;
  currentParentClass:string = 'meow';

  @Input() parentSelector:string = null;

  @Input('cbParentClass') set _parentClass(newParentClass:string){
    if(this.parentSelector){
      let parentHtml = this.hostElement.closest(this.parentSelector);
      jQuery(parentHtml)
        .removeClass(this.currentParentClass)
        .addClass(newParentClass);
      this.currentParentClass = newParentClass;
    }
  }

  constructor(elemRef:ElementRef) { 
    this.hostElement = elemRef.nativeElement;
  }


}
