import { Directive, Input, HostListener, ElementRef, LOCALE_ID, Inject } from '@angular/core';
import { NgModel } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';

@Directive({
  selector: '[ngModel][cbInputCurrency], [attr.value][cbInputCurrency]',
  providers: [NgModel],
  host:{
      'cbInputRestrict':'"currency"'
  }
})
export class CbInputCurrencyDirective {

  constructor(
    private ngModel: NgModel,
    private elemRef: ElementRef<HTMLInputElement>,
    @Inject(LOCALE_ID) locale:string
  ){
    this._currPipe = new CurrencyPipe(locale);
  }

  private _currPipe:CurrencyPipe;
  private _hasFocus:boolean = false;
  private _originalInputType = "text";

  ngAfterViewChecked(){
    this.attachFormatting();
  }

  @HostListener('focus',['$event.target']) handleFocus(target) {
    this._hasFocus = true;
    this.detachFormatting();
  }

  @HostListener('blur',['$event.target']) handleBlur(target) {
    this._hasFocus = false;
    this.attachFormatting();
  }

  private attachFormatting(){
    if(!this._hasFocus){
      var hostInput:HTMLInputElement = this.elemRef.nativeElement;
      this._originalInputType = hostInput.type;
      hostInput.type = "text";
      if ((this.ngModel.value != null) && (hostInput.value.length > 0)) {
        hostInput.value = this._currPipe.transform(this.ngModel.value);
      } else if ((this.ngModel.value == null || this.ngModel.value == undefined) && hostInput.value != null && hostInput.value != undefined && hostInput.value.length > 0) {
        if(!isNaN(parseInt(hostInput.value))) {
          let inputValue: number = parseInt(hostInput.value);
          hostInput.value = this._currPipe.transform(inputValue);
        }
      }
    }
  }

  private detachFormatting(){
    var hostInput:HTMLInputElement = this.elemRef.nativeElement;
    hostInput.type = this._originalInputType;
    if (this.ngModel.value != null && !isNaN(this.ngModel.value)) {
      hostInput.value = this.ngModel.value;
    }
  }

}
