import { Directive, Input, HostListener, ElementRef, EventEmitter, Output } from '@angular/core';
import { NgModel } from '@angular/forms';
import { AlertService } from '../../../services/alert.service';

@Directive({
  selector: '[ngModel][cbInputRestrict]',
  providers: [NgModel]
})
export class CbInputRestrictDirective {

  constructor(
    private ngModel: NgModel,
    private _alertSvc: AlertService
  ) { }

  private usCurrencyRegex: RegExp = /^[$]?[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{0,})?$/;
  private readonly alphaAllowedRegex: RegExp = /^[a-zA-Z ]*$/;
  private readonly alphanumericAllowedRegex: RegExp = /[a-zA-Z0-9 ]/i;
  private readonly numbersOnlyAllowedRegex: RegExp = /[0-9]/;
  private readonly numericAllowedRegex: RegExp = /[0-9.]/;
  private readonly currencyAllowedRegex: RegExp = /[0-9,.$]/;
  private readonly currencyNegativeAllowedRegex: RegExp = /[-0-9,.$]/;
  private readonly decimalRegex: RegExp = /[.]/;
  private readonly decimal: string = '.';
  private readonly negativeRegex: RegExp = /[-]/;
  private readonly negative: string = '-';
  private readonly isFirstCharAlpha = /^[a-zA-Z]{1}/;
  private readonly variableNameRegex: RegExp = /^[a-zA-Z0-9_]+$/;
  private readonly numericAndEqualAllowedRegex: RegExp = /[0-9,=]/;
  private readonly companyNameRegex: RegExp = /[^\\/\[\]{}]/;

  @Input('cbInputRestrict')
  restriction:"alpha"|"numeric"|"numberOnly"|"alphanumeric"|"currency"|"currencyNegative"|"numericMultiplePeriods"|"variableName"|"decimalPlaces"|"numericAndEqual"|"companyName"|null = null;

  @Input('decimalPlacesRestrict')
  public decimalPlaces: number = null;

  @HostListener('keypress',['$event']) restrictInput(e: KeyboardEvent) {

    if (e.key.length > 1) // back tab space
      return true;

    switch (this.restriction) {
      case "alpha":
        return this.isTypedValid(e,this.alphaAllowedRegex);
      case "currency":
        return this.isTypedValid(e,this.currencyAllowedRegex, this.decimalRegex, this.decimal);
        case "currencyNegative":
        return this.isTypedValid(e,this.currencyNegativeAllowedRegex, this.decimalRegex, this.decimal, this.negativeRegex, this.negative);
      case "numeric":
        return this.isTypedValid(e,this.numericAllowedRegex, this.decimalRegex, this.decimal);
      case "numericMultiplePeriods":
        return this.isTypedValid(e,this.numericAllowedRegex);
      case "alphanumeric":
        return this.isTypedValid(e,this.alphanumericAllowedRegex);
      case "numberOnly":
        return this.isTypedValid(e,this.numbersOnlyAllowedRegex);
      case "companyName":
        return this.isTypedValid(e, this.companyNameRegex);
      case "variableName":
        let inputElement = e.target as HTMLInputElement;

        if(inputElement.selectionStart === 0 && !this.isFirstCharAlpha.test(e.key))
          return false;

        return this.isTypedValid(e,this.variableNameRegex);
      case "decimalPlaces":
        if (this.isTypedValid(e,this.numericAllowedRegex) && this.decimalPlaces != null) {
          return this.isNumberKey(e);
        } else {
          return this.isTypedValid(e,this.numericAllowedRegex);
        }
      case "numericAndEqual":
        return this.isTypedValid(e,this.numericAndEqualAllowedRegex);
      default:
        return true;
    }
  }

  @HostListener('paste',['$event']) restrictPaste(e) {
    let inputVal = e.clipboardData.getData('text');

    switch (this.restriction) {
      case "alpha":
        return this.isPasteValid(inputVal,this.alphaAllowedRegex);
      case "currency":
        return this.isPasteCurrency(inputVal);
      case "currencyNegative":
        return this.isPasteCurrency(inputVal);
      case "numeric":
        return this.isPasteValid(inputVal, this.numericAllowedRegex);
      case "alphanumeric":
        return this.isPasteValid(inputVal, this.alphanumericAllowedRegex);
      case "numericAndEqual":
        return this.isTypedValid(e,this.numericAndEqualAllowedRegex);
      default:
        return true;
    }
  }
/**
   * Checks to ensure that the pasted value is valid
   *
   * @param val the string to test
   * @param r the RegExp containing the valid characters
   */

  isPasteValid(val: string, r:RegExp): boolean {
    var outStr:string = '';
    for (var i = 0; i < val.length; i++) {
      if (r.test(val.charAt(i)))
        outStr += val.charAt(i);
    }

    this.ngModel.update.emit(outStr);
    this.ngModel.control.markAsDirty();
    return false;
  }
  /**
   * Checks to ensure that the pressed key is for a valid character
   * optional test for if bound input only contains one of a special character
   *
   * @param e the KeyboardEvent to compare against
   * @param r the RegExp containing the valid characters
   * @param containsOnlyOneCharacter (optional) the single character to test for uniqueness
   */
  isTypedValid(e: KeyboardEvent, r: RegExp, containsJustOneRegex?: RegExp, containsJustOne?:string, containsJustOneNegativeRegex?: RegExp, containsOnlyOneNegative?:string):boolean {
    var firstItem = containsJustOne != null && containsJustOne.length == 1 && containsJustOneRegex.test(e.key);
    var secondItem = containsOnlyOneNegative != null && containsOnlyOneNegative.length == 1 && containsJustOneNegativeRegex.test(e.key);
    if ( firstItem || secondItem){
      var curVal: string = this.ngModel.control.value;

      if (curVal && ((curVal+"").indexOf(containsJustOne) != -1 || (curVal+"").indexOf(containsOnlyOneNegative) != -1))
        return false;
    }

    return r.test(e.key);
  }

  /**
   * Checks to ensure that the pasted value only contains currency related characters
   *
   * @param val the value to compare against
   */
  isPasteCurrency(val: string): boolean {
    if (this.usCurrencyRegex.test(val)) {
      this.ngModel.update.emit(val);
      this.ngModel.control.markAsDirty();
    }
    else {
      this._alertSvc.error('Cannot paste invalid currency.');
    }

    return false;
  }

  isNumberKey(evt: KeyboardEvent) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
        return false;
    else {
        var curVal: string = this.ngModel.control.value;

        if (curVal) {
          var len = curVal.length;
          var index = curVal.toString().indexOf('.');

          if (index > 0 && charCode == 46) {
              return false;
          }
          if (index > 0) {
              var CharAfterdot = (len) - index;
              if (CharAfterdot > parseInt(this.decimalPlaces.toString())) {
                  return false;
              }
          }
        }
    }
    return true;
 }

}
