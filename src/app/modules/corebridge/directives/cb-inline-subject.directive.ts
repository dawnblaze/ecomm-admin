
import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import { Directive, AfterContentInit, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { ReplaySubject, Subscription } from 'rxjs';

@Directive({
  selector: '[cbInlineSubject],[cbSubject]',
  exportAs: 'cbInlineSubject,cbSubject'
})
export class CbInlineSubjectDirective implements AfterContentInit, OnDestroy {

  constructor() { }

  @Output() onSubjectChange:EventEmitter<any> = new EventEmitter<any>();
  @Input() delayMS:number = 500;
  @Input() distinctUntilChanged:boolean = true;
  private sbjModel:ReplaySubject<any> = new ReplaySubject<any>(1);
  private subscr:Subscription = null;

  ngAfterContentInit(): void {  
    let obs = this.sbjModel.pipe(
                        debounceTime(this.delayMS)
                      );
    if(this.distinctUntilChanged){
      obs = obs.pipe(distinctUntilChanged());
    }
    
    this.subscr = obs.subscribe(val=>this.onSubjectChange.emit(val));
  }

  ngOnDestroy(): void {
    if(this.subscr){
      this.subscr.unsubscribe();
    }
  }

  public next(val:any){
    this.sbjModel.next(val);
  }

}
