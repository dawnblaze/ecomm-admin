import { Directive, TemplateRef, Input } from '@angular/core';

@Directive({
  selector: '[cbTemplate]'
})
export class CbTemplateDirective {

  constructor(templateRef: TemplateRef<any>) { 
    this.template = templateRef;
  }
  @Input('cbTemplate') name:string;
  public template: TemplateRef<any>;

}
