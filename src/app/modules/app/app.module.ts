import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { HttpModule, JsonpModule } from "@angular/http";
import { EditorModule } from "@tinymce/tinymce-angular";

import { AppComponent } from "../../app.component";
import { routing } from "../../app.routing";

// imported modules
import { CorebridgeModule } from "../corebridge/corebridge.module";
import { EcommerceModule } from "../ecommerce/ecommerce.module";

//services
import { UserService } from "../../services/user.service";
import { DomainService } from "../../services/domain.service";
import { ThemeService } from "../../services/theme.service";
import { CesaService } from "../../services/cesa.service";
import { ATEService } from "../../services/ate.service";
import { ColorPickerModule } from "primeng/colorpicker";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AuthGuard } from "../../services/app-guard.service";
import { EventService } from "../../services/event.service";
import { BlurAndClickService } from "../../services/blurAndClick.service";
import { MatDialogModule } from '@angular/material';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EditorModule,
    routing,
    HttpModule,
    JsonpModule,
    CorebridgeModule,
    EcommerceModule,
    ColorPickerModule,
    MatDialogModule
  ],
  declarations: [AppComponent],
  providers: [
    DomainService,
    UserService,
    ThemeService,
    CesaService,
    ATEService,
    AuthGuard,
    EventService,
    BlurAndClickService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
