export class TokenInfo {
  access_token: string;
  token_type: string;
  issued: Date;
  expires: Date;
}