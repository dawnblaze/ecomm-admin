export class UserInfo {
  userName: string;
  userType: string;
  rights: any;
  firstName: string;
  lastName: string;
  Id: number;
  emailAddress: string;
  isActive: boolean;
}