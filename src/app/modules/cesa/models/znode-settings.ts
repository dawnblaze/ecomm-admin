export class ZnodeSettings {
  storeLimit: number;
  apiDomain: string;
  apiKey: string;
  apiAddress: string;
  eCodEnabled: boolean;
}