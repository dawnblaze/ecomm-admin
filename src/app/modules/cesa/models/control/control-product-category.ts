export class ControlProductCategory {
  categoryId: number;
  catgoryName: string;
  isActive: boolean;
  parentCategoryId: number;
}