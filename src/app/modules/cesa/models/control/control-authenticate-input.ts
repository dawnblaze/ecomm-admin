export class ControlAuthenticateInput {
  username: string;
  password: string;
  rootpath: string;
  userType: string;
}