import {TokenInfo} from "../token-info";
import { ControlUser } from "../../../ecommerce/models/control-user"

export class ControlAuthenticateResponse {
  token: TokenInfo;
  user: ControlUser;
}