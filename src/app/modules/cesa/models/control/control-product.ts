export class ControlProduct {
  productName: string;
  productId: number;
  isActive: boolean;
  categoryId: number;
}