export class CesaProductMapping {
  eCommID: number;
  atomID: number;
  atomName: string;
  variableValues: string;
}