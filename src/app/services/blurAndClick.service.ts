
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';



@Injectable()
export class BlurAndClickService {

  constructor() {
  }

  private focusMap: Map<Node, boolean>;
  private _focusMapStream = new Subject<{click: Event, map: Map<Node, boolean>}>();
  public FocusMapStream = this._focusMapStream.asObservable();

  public onClick(event: Event){
    this.focusMap = new Map<Node, boolean>();
    let target = (event.target as Node);
    while(target != null){
      this.focusMap.set(target, true);
      target = target.parentNode;
    }
    this._focusMapStream.next({
      click: event,
      map: this.focusMap
    });
  }
}
