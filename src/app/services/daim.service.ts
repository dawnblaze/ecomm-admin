import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable, forkJoin, throwError } from 'rxjs';
import { map, catchError} from 'rxjs/operators';

import {DomainService} from './domain.service';

import { Font } from '../modules/ecommerce/models/font';
import { FontGroup } from '../modules/ecommerce/models/font-group';
import { Color } from '../modules/ecommerce/models/color';
import { ColorGroup } from '../modules/ecommerce/models/color-group';
import { Asset } from '../modules/ecommerce/models/asset';

@Injectable() export class DaimService {
  private _base: string = undefined;

  public get base() {
    if (this._base == undefined) {
      this._base = this.domain.ecod + '/daim/';
    }

    return this._base;
  }

  constructor(private http: Http, private domain: DomainService) { }

  public get(route: string): Observable<any> {
    let url = this.base + route;

    return this.http.get(url)
                    .pipe(map(res => {
                      return (res.text() == '') ? { } : res.json();
                    })
                    ,catchError(this.handleError));
  }

  public post(route: string, object: any): Observable<any> {
    let url = this.base + route;

    let body = JSON.stringify(object);
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(url, body, options)
                    .pipe(map( res => {
                      return ("" == res.text()) ? {} : res.json();
                    })
                    ,catchError(this.handleError));
  }

  public delete(route: string): Observable<boolean> {
    let url = this.base + route;

    let headers = new Headers({
      'Accept': 'application/json'
    });
    let options = new RequestOptions({ headers: headers });

    return this.http.delete(url, options)
                    .pipe(map(res => {
                      return (res.ok) ? true: false
                    })
                    ,catchError(this.handleError));
  }

  public put(route: string, object: any): Observable<any> {
    let url = this.base + route;
    let body = JSON.stringify(object);

    let headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });
    let options = new RequestOptions({ headers: headers });

    return this.http.put(url, body, options)
                    .pipe(map( res => {
                      return ("" == res.text()) ? {} : res.json();
                    })
                    ,catchError(this.handleError));
  }

  /**
   * Copied from ZNode service.
   *
   * @param {any} - Error Response from API
   */
  private handleError (error: any) {
    console.log(error);
    // Log the error to the console for debugging
    if(error.status == 0){
      console.error("Request timed out, check connect and try again");
      console.error(error);

      return throwError("Request timed out, check connect and try again");
    }
    else if(error._body != ""){
      // if there is content in the body, check for a specific ErrorMessage property and return that
      var body = error.json();

      if(body.HasError && body.ErrorMessage != ''){
        console.error("Error Message from Daim");
        console.error(body.ErrorMessage);
        return throwError(body.ErrorMessage);
      }
      else{
        console.error("Unidentified Error From Daim");
        console.log(body);
        return throwError("Unidentified Error From Daim");
      }
    }
    else if(error.status == 500){
      console.error("Error Message from Daim");
      console.log(error.statusText);
      console.error(error);
      // if no error message was sent, but a server error was still detected
      return throwError(error.statusText);
    }

    // otherwise construct a generic server error message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return throwError(errMsg);
  }

  public getFonts(): Observable<Font[]> {
    return this.get('font/info');
  }

  public getFont(fontFamilyName: string, groups: boolean = false): Observable<Font> {
    return this.get(`font/${fontFamilyName}` + (groups ? '?groups=true' : ''));
  }

  public getFontFromAsset(guid: string): Observable<Font> {
    return this.get(`font/asset/${guid}`);
  }

  public addFont(font: Font) {
    font.bid = this.domain.bid;
    return this.post('font', font);
  }

  public uploadFont(file: File): Observable<Asset> {
    let formData = new FormData();
    formData.append('file', file);

    let url = this.base + 'asset?assettype=12';

    let headers = new Headers({
      'Accept': 'application/json'
    });

    let options = new RequestOptions({ headers: headers });

    return this.http.post(url, formData, options)
                    .pipe(map( res => {
                      return ("" == res.text()) ? {} : res.json();
                    })
                    ,catchError(this.handleError));
  }

  public addFontLink(fontFamilyName: string, groupID: number): Observable<any> {
    return this.post(`font/link?fontFamilyName=${fontFamilyName}&groupID=${groupID}`, {});
  }

  public deleteFontLink(fontFamilyName: string, groupID: number): Observable<boolean> {
    return this.delete(`font/link?fontFamilyName=${fontFamilyName}&groupID=${groupID}`);
  }

  public deleteFont(fontFamilyName: string): Observable<boolean> {
    return this.delete(`font/${fontFamilyName}`);
  }

  public updateFont(font: Font) {
    font.bid = this.domain.bid;
    return this.put(`font/${font.fontFamilyName}`, font);
  }

  public getFontGroups(): Observable<FontGroup[]> {
    return this.get('fontgroup');
  }

  public getFontGroup(id: number, fonts: boolean = false): Observable<FontGroup> {
    return this.get(`fontgroup/${id}` + (fonts ? '?fonts=true' : ''));
  }

  public addFontGroup(fontGroup: FontGroup): Observable<FontGroup> {
    fontGroup.bid = this.domain.bid;
    return this.post('fontgroup', fontGroup);
  }

  public deleteFontGroup(id: number): Observable<boolean> {
    return this.delete(`fontgroup/${id}`);
  }

  public updateFontGroup(fontGroup: FontGroup): Observable<FontGroup> {
    fontGroup.bid = this.domain.bid;
    return this.put(`fontgroup/${fontGroup.id}`, fontGroup);
  }

  public getColors(): Observable<Color[]> {
    return this.get('color');
  }

  public getColor(id: number, groups: boolean = false): Observable<Color> {
    return this.get(`color/${id}` + (groups ? '?groups=true' : ''));
  }

  public deleteColor(id: number): Observable<boolean> {
    return this.delete(`color/${id}`);
  }

  public updateColor(color: Color): Observable<Color> {
    color.bid = this.domain.bid;
    return this.put(`color/${color.id}`, color);
  }

  public addColor(color: Color): Observable<Color> {
    color.bid = this.domain.bid;
    return this.post('color', color);
  }

  public addColorLink(groupID: number, associatedColors: any): Observable<any> {
    return this.post(`color/link?groupID=${groupID}`, JSON.stringify(associatedColors));
  }

  public deleteColorLink(colorID: number, groupID: number): Observable<boolean> {
    return this.delete(`color/link?colorID=${colorID}&groupID=${groupID}`);
  }

  public getColorGroups(): Observable<ColorGroup[]> {
    return this.get('colorgroup');
  }

  public getColorGroup(id: number, colors: boolean = false): Observable<ColorGroup> {
    return this.get(`colorgroup/${id}` + (colors ? '?colors=true' : ''));
  }

  public addColorGroup(colorGroup: ColorGroup): Observable<ColorGroup> {
    colorGroup.bid = this.domain.bid;
    return this.post('colorgroup', colorGroup);
  }

  public deleteColorGroup(id: number): Observable<boolean> {
    return this.delete(`colorgroup/${id}`);
  }

  public updateColorGroup(colorGroup: ColorGroup): Observable<ColorGroup> {
    colorGroup.bid = this.domain.bid;
    return this.put(`colorgroup/${colorGroup.id}`, colorGroup);
  }
}
