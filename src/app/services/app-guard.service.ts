import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { CesaService, MisTypes } from './cesa.service';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private cesaService: CesaService,
    private authService: AuthService) { }

  // We are using canActivate(next: ActivatedRouteSnapshot)
  // where ActivatedRouteSnapshot gives us access to the example:
  // data: { supportUsers: ['administrator', 'cbms.support', 'cbms.admin']}
  // property that we specified in the app.routing.ts
  canActivate(next: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // Get the route children.
    const routeChildren = next.routeConfig.children;

    if (routeChildren) {
      // Check if we have a route equal to 'developer'
      const devRoute = routeChildren.find(children => {
        // path is something like /developer, developer/features, developer/eventlog
        return children.path.toString().toLowerCase()
          .substring(0, 9) === 'developer';
      });      
      // Get the misType
      const misType = this.cesaService.getMisType();
      // Get the username
      const userName = this.authService.getUser().UserName;  
      // Check if the devRoute is not NULL which means the route is 
      // from path like /developer, developer/features, developer/eventlog
      if (devRoute) {
        if ((misType === MisTypes.Cyrious
            && (userName === 'administrator'))
          || (misType === MisTypes.CoreBridge
            && (userName === 'cbms.support'
            || userName === 'cbms.admin'))) {
            return true;
        }  
        // Redirect to a non-existing page if none of the above
        // conditions are met.
        this.router.navigate(['/page-not-found']);
        return false;
      }
    }
    // Just activate the route if it does not match
    // any conditions above.
    return true;
  }
}
