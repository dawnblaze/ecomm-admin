import { Injectable } from '@angular/core';
import { CoreBridgeService } from './corebridge.service';
import { CesaService, MisTypes } from './cesa.service';
import { TaxabilityCode } from '../modules/ecommerce/models/taxability-code';
import { DomainService } from './domain.service';
import { ZnodeService } from './znode.service';
import { Observable, forkJoin } from 'rxjs';
//import { connectableObservableDescriptor } from 'rxjs/';
import { ContactsTableComponent } from '../modules/ecommerce/components/contacts-table';
import { TaxClass } from '../modules/ecommerce/models/tax-class';

export enum ATEProcessorTypeId {
    //Temporarily named.
    Four = 4
}

@Injectable()
export class ATEService {

    private _ateEnabled: boolean = false;
    private _ateProcessorTypeId: number;
    private _taxabilityCodes: TaxabilityCode[];
    private readonly ATE_PROCESSOR_TYPE_SESSION_ID = 'ATE_PROCESSOR_TYPE_ID';

    constructor(private corebridgeService: CoreBridgeService,
        private cesaService: CesaService,
        private domain: DomainService,
        private znodeService: ZnodeService) {}
    private setATEProcessorTypeIdInSessionStorage(ateProcessorTypeId: number): void {
        sessionStorage.setItem(this.ATE_PROCESSOR_TYPE_SESSION_ID, ateProcessorTypeId.toString());
    }

    private getAndSessionStoreATEProcessorTypeId(): number {
        this.corebridgeService.getTaxProcessorType().subscribe(
            data => {
                if (data != null) {
                    this._ateProcessorTypeId = data;
                    this.setATEProcessorTypeIdInSessionStorage(this._ateProcessorTypeId);
                    return this._ateProcessorTypeId;
                }
            },
        );
        return this._ateProcessorTypeId;
    }

    public fetchATEProcessorTypeId(): void {
        this.znodeService.getConfigSettingByBID(this.domain.bid).subscribe(
            data => {
                if (data.EnableOnlineTaxLookup && this.isCoreBridgeMisType()) {
                    this.getAndSessionStoreATEProcessorTypeId();
                }
            }
        );
    }

    public isATEProcessorTypeIdEqualToFour(){
        return this.getATEProcessorTypeId() != null && this.getATEProcessorTypeId() == ATEProcessorTypeId.Four ? true : false;
    }

    public getATEProcessorTypeId(): number {
        if (sessionStorage.getItem(this.ATE_PROCESSOR_TYPE_SESSION_ID) != null) {
            return parseInt(sessionStorage.getItem(this.ATE_PROCESSOR_TYPE_SESSION_ID));
        }
        return null;
    }

    private getMisType(): number {
        return this.cesaService.getMisType();
    }

    private isCoreBridgeMisType(): boolean {
        return this.getMisType() == MisTypes.CoreBridge;
    }

    public fetchTaxabilityCodes(): void {
        forkJoin
        (
          this.znodeService.getConfigSettingByBID(this.domain.bid),
          this.corebridgeService.getTaxProcessorType(),
          this.corebridgeService.getAllTaxCodes()
        ).subscribe(data => {
            let [configSetting, taxProcessorType, taxCodes] = data;
            let taxClasses: TaxClass[] = [];

            taxCodes.forEach(function(value){
                let taxClass = new TaxClass();
                let taxabilityCode = value;
                if (taxabilityCode.Name !== 'Shipping') {
                    taxClass.Name = taxabilityCode.Name;
                    taxClass.ActiveInd = true;
                    taxClass.PortalId = null;
                    taxClass.ExternalId = taxabilityCode.ATETaxabilityTypeId;
                    taxClasses.push(taxClass);
                }
            });

            this.znodeService.checkTaxClasses(taxClasses).subscribe(x => {
                console.log('CHECK TAX CLASSES RESULT:');
                console.log(x);
            });

            if (configSetting.EnableOnlineTaxLookup && this.isCoreBridgeMisType()) {
              if (taxProcessorType != null && taxProcessorType == ATEProcessorTypeId.Four) {
                this.setATEProcessorTypeIdInSessionStorage(taxProcessorType);
                this._taxabilityCodes = taxCodes;
              }
            }
        });
    }
}