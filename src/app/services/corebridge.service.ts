/**
 * CoreBridge Service Provider
 *
 * Provides interactions with the CBMS API for
 * the CBMS Software modules. Methods in this class should
 * return observables that can be subscribed to, and results
 * passed back should be typed to the correct class definitions.
 *
 * @author Mike Payne <payne@rivetal.com>
 * @link http://endor.corebridge.net/swagger/ui/index.html
 **/

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable, throwError }     from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { DomainService } from './domain.service';

import { IncomeAccount } from '../modules/ecommerce/models/income-account';
import { User } from '../modules/ecommerce/models/user';
import { ReportSection } from '../modules/ecommerce/models/reports';
import { ReportInfo } from '../modules/ecommerce/models/report-info';

import { CoreBridgeProductSettings } from '../modules/ecommerce/models/corebridge-product-settings';
import { TaxabilityCode } from '../modules/ecommerce/models/taxability-code';


@Injectable()
export class CoreBridgeService {

    /**
     * Uses dependency injection to allow access to Angular's HTTP component
     *
     **/
    constructor(private http:Http, private domainService: DomainService) {}

    private getBase(): string {
        return this.domainService.cbms;
    }

    private getAuth(): string {
        return "Basic " + sessionStorage.getItem('access_token');
    }

    /**
     * Processes errors from ZNode's API
     *
     * @param {any} - Error Response from ZNode's API
     */
    private handleError (error: any) {
        // Log the error to the console for debugging
        console.error('Error Message from ZNode');
        console.error(error);

        if(error._body != ""){
        // if there is content in the body, check for a specific ErrorMessage property and return that
        var body = error.json();

        if(body.HasError && body.ErrorMessage != ''){
            console.error(body.ErrorMessage);
            return throwError(body.ErrorMessage);
        }
        }
        else if(error.status == 500){
        // if no error message was sent, but a server error was still detected
        return throwError(error.statusText);
        }

        // otherwise construct a generic server error message
        let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        return throwError(errMsg);
    }

    /**
     * Makes a _GET HTTP request to the base CBMS API
     *
     * @param {string} route    - The path of the endpoint to be requested
     */
    public get(route: string) {
        let url = this.getBase() + route;

        let headers = new Headers({
            'Authorization': this.getAuth(),
            'Accept': 'application/json'
        });

        let options = new RequestOptions({headers: headers});
        return this.http.get(url, options);
    }

    /**
     * Makes a _POST HTTP request to the base CBMS API
     *
     * @param {string} route    - The path of the endpoint to be requested
     * @param {any} object      - The model to be used in the body of the request
     */
    public post ( route: string, object: any): Observable<any> {
        let url = this.getBase() + route;
        // serialize JSON object to send as body of request
        let body = JSON.stringify(object);
        let headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': this.getAuth()
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url, body, options);
    }

    public getZnodeAddress(): Observable<string> {

        return this.get('ExEComm/GetTenantZnodeSetting/ZnodeAddress')
                    .pipe(map(res => {
                        let body = res.json();
                        return body.result + '/';
                    })
                    ,catchError(this.handleError));
    }

    public getZnodeKey(): Observable<string>  {
        return this.get('ExEComm/GetTenantZnodeSetting/ZnodeAPIKey')
                    .pipe(map(res => {
                        let body = res.json();
                        return body.result;
                    })
                    ,catchError(this.handleError));
    }

    public getZnodeUserName(): Observable<string>  {
        return this.get('ExEComm/GetTenantZnodeSetting/ZnodeUserName')
                    .pipe(map(res => {
                        let body = res.json();
                        return body.result;
                    })
                    ,catchError(this.handleError));
    }

    public getZnodeOnlineDesignerEnabled(): Observable<boolean> {
        return this.get('ExEComm/GetTenantZnodeSetting/EnableOnlineDesigner')
                    .pipe(map(res => {
                        let body = res.json();
                        return body.result == "True";
                    })
                    ,catchError(this.handleError));
    }

    /**
     * Gets the total number of allowed portals on member's account
     *
     */
    public getPortalLimit(): Observable<number> {
        return this.get('ExEComm/GetTenantZnodeSetting/EcommStoreLimit')
                    .pipe(map(res => {
                        let body = res.json();
                        return body.result;
                    })
                    ,catchError(this.handleError));
    }

    public getProducts(): Observable<any[]> {
        return this.get('ExEComm/GetEcommProducts')
                    .pipe(map(res => res.json())
                    ,catchError(this.handleError));
    }

    public getProduct(productId: number): Observable<any> {
        return this.get('ExEComm/GetEcommProducts/'+productId)
                    .pipe(map(res => res.json())
                    ,catchError(this.handleError));
    }


    public getEcommProductCategories(): Observable<any[]> {
        return this.get('ExEComm/GetEcommProductCategories')
                    .pipe(map(res => {
                        let body = res.json();
                        return body.QuickProductCategories;
                    })
                    ,catchError(this.handleError));
    }

    public getProductCategories(isActive: number = 1): Observable<any[]> {
        return this.get('ExEComm/GetProductCategories/'+isActive)
                    .pipe(map(res => {
                        let body = res.json();
                        return body.ProductCategories;
                    })
                    ,catchError(this.handleError));
    }

    public getProductsByCategory(categoryId: number): Observable<any[]> {
        return this.get('ExEComm/GetEcommProductsByCategory/'+categoryId)
                    .pipe(map(res => {
                        let body = res.json();
                        return body.QuickProducts;
                    })
                    ,catchError(this.handleError));
    }

    public getIncomeAccounts(): Observable<IncomeAccount[]> {
        return this.get('ExEComm/GetAccountsByAccountType?GLAccountTypeId=5&IsActive=1')
                    .pipe(map(res => {
                        let body = res.json();
                        return body.AccountList || [];
                    })
                    ,catchError(this.handleError));
    }

    public createProductSettings(settings: CoreBridgeProductSettings): Observable<number> {
        return this.post('ExEComm/CreateExternalZnodeProductIntegrationSetting', settings)
                    .pipe(map(res => {
                        let body = res.json();
                        return body.Id;
                    })
                    ,catchError(this.handleError));
    }

    public getProductSettingsByID(id: number): Observable<any> {
        return this.get('ExEComm/GetZnodeProductSettingsById/' + id)
                    .pipe(map(res => {
                        let body = res.json();
                        delete body.IsSuccess;
                        delete body.Message;
                        delete body.Status;

                        return body || {};
                    })
                    ,catchError(this.handleError));
    }

    public updateProductSettings(id: number, settings: CoreBridgeProductSettings): Observable<boolean> {
        return this.post('ExEComm/UpdateExternalZnodeProductIntegrationSetting/'+id, settings)
                    .pipe(map(res => {
                        let body = res.json();
                        return body.IsSuccess;
                    })
                    ,catchError(this.handleError));
    }

    public getProductSettingsByZnodeProductID(znodeProductId: number): Observable<any> {
        return this.get('ExEComm/GetZnodeProductSettingsByZnodeProductId?znodeProductId=' + znodeProductId)
            .pipe(map(res => {
                let body = res.json();
                delete body.Status;

                return body || {};
            })
            ,catchError(this.handleError));
    }

    public createOrUpdateProductSettings(settings: CoreBridgeProductSettings): Observable<any> {
        return this.post('ExEComm/CreateOrUpdateZnodeProductIntegrationSetting', settings)
            .pipe(map(res => {
                let body = res.json();
                return body || {};
            })
            ,catchError(this.handleError));
    }

    public getContactTaxInfo(externalId: number): Observable<any>{
        return this.get('ExEComm/accounttaxinfo/'+externalId)
                    .pipe(map(res => {
                        let body = res.json();
                        return body || {};
                    })
                    ,catchError(this.handleError));
    }

    public getReports(): Observable<ReportSection[]>{
        return this.get('ExEComm/GetReportNavigation/')
                    .pipe(map(res => {
                        let body = res.json();
                        return body["Sections"] || null;
                    })
                    ,catchError(this.handleError));
    }

    public getReportInfo(reportId: number): Observable<ReportInfo>{
        return this.get('ExEComm/GetReportInfoById/'+reportId)
                    .pipe(map(res => {
                        let body = res.json();
                        return body || {};
                    })
                    ,catchError(this.handleError));
    }

    public getReportURL(reportInfo: ReportInfo): Observable<string>{
        return this.post('ExEComm/GetReportURL/', reportInfo)
                    .pipe(map(res => {
                        let body = res.json();
                        return body || {};
                    })
                    ,catchError(this.handleError));
    }

    public getReportURLEndpoint(): string {
        return this.getBase() + 'ExEComm/GetReportURL/';
    }

    public getToken(): string {
        return this.getAuth();
    }

    public getTenantCssFile(): Observable<string>  {
        return this.get('ExEComm/GetTenantCSSFile')
                    .pipe(map(res => {
                        let body = res.json();
                        return body.result;
                    })
                    ,catchError(this.handleError));
    }

    //For: ROG-211 / ROG-222
    public getIsFileManagementEnabled(): Observable<boolean>  {
        return this.get('ExEComm/GetIsFileManagementEnabled')
                    .pipe(map(res => {
                        let body = res.json();
                        return body;
                    })
                    ,catchError(this.handleError));
    }

    public getAllTaxCodes(): Observable<TaxabilityCode[]>{
        return this.get('ExTaxJar/GetAllTaxCodes')
                    .pipe(map(res => {
                        let body = res.json();
                        return body;
                    })
                    ,catchError(this.handleError));
    }

    public getTaxProcessorType(): Observable<number>{
        return this.get('ExEcomm/GetTenantZnodeSetting/ateprocessortypeid')
                    .pipe(map(res => {
                        if(res != null){
                            return res.json().result;
                        }else{
                            return null;
                        }
                    })
                    ,catchError(this.handleError));
    }
}
