import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable, forkJoin, of, zip, combineLatest } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { catchError, concatMap, flatMap, map, mergeMap, switchMap } from 'rxjs/operators';

import { User } from '../modules/ecommerce/models/user';

import { DomainService } from './domain.service';
import { CoreBridgeService } from './corebridge.service';
import { ZnodeService } from './znode.service';
import { CesaService, MisTypes } from "./cesa.service";
import { ControlAuthenticateInput } from "../modules/cesa/models/control/control-authenticate-input";
import { ControlAuthenticateResponse } from "../modules/cesa/models/control/control-authenticate-response";
import { ControlUser } from "../modules/ecommerce/models/control-user";
import { ATEService } from './ate.service';

@Injectable()
export class AuthService {

  private loggedIn:boolean = false;
  private _loggedInStatus = new BehaviorSubject({loggedIn: !!sessionStorage.getItem('access_token')});
  private systemVersion:string = '1.3.2001.2001';
  private response: Response;
  private user:User;
  private token: any;

  public loggedInStatus = this._loggedInStatus.asObservable();

  constructor(private http:Http, private domainService: DomainService, private corebridgeService: CoreBridgeService, private znodeService: ZnodeService, private cesaService: CesaService
    , private ateService:ATEService) {
    this.loggedIn = !!sessionStorage.getItem('access_token') && this.getSystemVersion() === sessionStorage.getItem('version');
  }

  getSystemVersion(){
    return this.systemVersion;
  }

  getResponse(){
    return this.response;
  }

  getBase(): string {
    return this.domainService.cbms;
  }

  getUser(){
    return JSON.parse(sessionStorage.getItem('user'));
  }

  getToken(){
    return sessionStorage.getItem('access_token');
  }

  getECodEnabled(): boolean{
    return sessionStorage.getItem('ECodEnabled') == 'true';
  }

  login(username: string, password: string): Observable<boolean> {

    if(this.cesaService.getMisType() == MisTypes.Cyrious)
    { //Control Tenant
      let authInput: ControlAuthenticateInput = new ControlAuthenticateInput();
      authInput.username = username;
      authInput.password = password;
      return this.doControlAuthenticate(authInput)
        .pipe(concatMap(val => {
          console.log(val);
          if(!val)
          {//Control Authentication Failure
            return of(false);
          }
          //Continue, pull from CESA and set ZNode Settings in Session
          return this.doGetZNodeSettingsFromCESA()
        }),
          catchError(err => {
            //Control Authenticate Error
            console.error(err);
            return of(false);
          }))
        .pipe(map((x) => {
          //Return Control Authenticate Success or Failure (Boolean)
          return x;
        }),
          catchError(err => {
          console.error(err);
          return of(false);
        }));
    } else { //CoreBridge Tenant
      let url: string = this.getBase() + 'ExToken/Post';
      let body: string = JSON.stringify({'username': username, 'password': password});
      let headers = new Headers({ 'Content-Type': 'text/plain', 'Accept': 'application/json' });
      let options = new RequestOptions({ headers: headers });

      return this.http.post(url, body, options)
        .pipe(map(res => {
          let body = res.json();

          if (typeof body.Token !== "undefined") {
            // Login Successful
            this.response = res;
            this.token = body.Token;
            this.user = body.user;

            sessionStorage.setItem('access_token', this.token.access_token);
            sessionStorage.setItem('user', JSON.stringify(this.user) );
            sessionStorage.setItem('version', this.systemVersion);

            forkJoin(
              this.corebridgeService.getZnodeAddress(),
              this.corebridgeService.getZnodeKey(),
              this.corebridgeService.getZnodeUserName()
            ).subscribe(
              data => {
                sessionStorage.setItem('znode_url', data[0]);

                let auth = btoa(data[2] + "|" + data[1]);
                sessionStorage.setItem('znode_auth', auth);

                this.znodeService.getBID().subscribe( bid => {
                  sessionStorage.setItem('bid', bid.toString());

                  console.log("bid: "+bid);

                  this.loggedIn = true;
                  this._loggedInStatus.next({loggedIn:true});

                  this.ateService.fetchTaxabilityCodes();

                }, err => {
                  this.loggedIn = true;
                  this._loggedInStatus.next({loggedIn:true});
                });
              },
              err => {
                this.logout();

                console.error(err);

                return false;
              });

            return true;
          }
          else {
            // Login Failed
            return false;
          }
        }),
          catchError(err => {
            console.log(err);
            return of(false);
          }));
    }
  }
  doControlAuthenticate(authInput:  ControlAuthenticateInput): Observable<boolean>
  {
    return this.cesaService.controlAuthenticate(authInput)
      .pipe(map(response => {
        if (response != null) {
          if (response.user.usertype !== "Employee") {
            this.logout();
            throw new Error("User is not an employee or does not permission to login.");
          }

          this.token = response.token;

          //Map ControlUser to CoreBridge User
          let cbUser = new User();

          cbUser.FirstName = response.user.firstname;
          cbUser.LastName = response.user.lastname;
          cbUser.EmailAddress = response.user.emailaddress;
          cbUser.Id = response.user.id;
          cbUser.UserName = response.user.username;
          cbUser.UserType = response.user.usertype;
          cbUser.IsActive = response.user.isactive;

          this.user = cbUser;

          sessionStorage.setItem('bid', response.user.ecommbid.toString());
          sessionStorage.setItem('access_token', this.token.access_token);
          sessionStorage.setItem('user', JSON.stringify(this.user));
          sessionStorage.setItem('version', this.systemVersion);
          return true;
        } else {
          this.logout();
          console.error("error logging in to Control");
        }
      }));
  }
  doGetZNodeSettingsFromCESA(): Observable<boolean>
  {
    return this.cesaService.getZnodeSettings()
      .pipe(map(znodeSettings => {
        sessionStorage.setItem('znode_url', znodeSettings.apiAddress + '/');
        sessionStorage.setItem('znode_auth', btoa(znodeSettings.apiDomain + '|' + znodeSettings.apiKey));
        sessionStorage.setItem(CesaService.ECodEnabledKey, znodeSettings.eCodEnabled.toString());
        sessionStorage.setItem(CesaService.ECommStoreLimitKey, znodeSettings.storeLimit.toString());

        this.loggedIn = true;
        this._loggedInStatus.next({loggedIn: true});
        return true;
      }));
  }

  logout() {
    sessionStorage.removeItem('access_token');
    sessionStorage.removeItem('user');
    sessionStorage.removeItem('znode_url');
    this.response = null;
    this.token = null;
    this.user = null;
    this.loggedIn = false;
    this._loggedInStatus.next({loggedIn:false});
    return true;
  }

  isLoggedIn() {
    return this.loggedIn;
  }

}
