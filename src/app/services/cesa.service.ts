import { environment } from '../../environments/environment'
import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable, forkJoin, throwError, pipe } from "rxjs";
import { map, catchError } from 'rxjs/operators';
import { TenantInfo } from "../modules/cesa/models/tenant-info";
import { ControlAuthenticateInput } from "../modules/cesa/models/control/control-authenticate-input";
import { ControlAuthenticateResponse } from "../modules/cesa/models/control/control-authenticate-response";
import { ZnodeSettings } from "../modules/cesa/models/znode-settings";
import { ControlProductCategory } from "../modules/cesa/models/control/control-product-category";
import { ControlProduct } from "../modules/cesa/models/control/control-product";
import { CesaProductMapping } from "../modules/cesa/models/cesa-product-mapping";
import { ControlStation } from "../modules/ecommerce/models/control-station";
import { CesaStationMapping } from "../modules/ecommerce/models/cesa-station-mapping";


export enum MisTypes {
  CoreBridge = 0,
  Cyrious = 1
}
@Injectable()
export class CesaService {
  static BidKey: string = 'bid';
  static MisTypeKey: string = 'mis_type';
  static MisDomainKey: string = 'mis_domain';
  static ECodEnabledKey: string = 'ECodEnabled';
  static ECommStoreLimitKey: string = 'eCommStoreLimit';

  /**
   * Uses dependency injection to allow access to Angular's HTTP component
   *
   **/
  constructor(private http: Http) {
  }

  getAuth(): string {
    let accessToken = sessionStorage.getItem('access_token');
    if(accessToken)
    {
      return "bearer " + accessToken;
    }
    else {
      return "";
    }
  }

  getMisType(): number {
    return parseInt(sessionStorage.getItem(CesaService.MisTypeKey));
  }

  getMisDomain(): string {
    return sessionStorage.getItem(CesaService.MisDomainKey);
  }

  getECodEnabled(): boolean {
    return sessionStorage.getItem(CesaService.ECodEnabledKey) == 'true';
  }

  getEcommStoreLimit(): number {
    return parseInt(sessionStorage.getItem(CesaService.ECommStoreLimitKey));
  }

  private baseUri: string = environment.cesaBaseUri;
  /**
   * Makes a _GET HTTP request to the base CESA API
   *
   * @param {string} route    - The path of the endpoint to be requested
   * @param {boolean} enableAuth  - If false, Auth headers are not sent
   */
  public get(route: string, enableAuth: boolean = true) {
    let url = this.baseUri + route;

    let headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });

    if(enableAuth) {
      headers.append('Authorization', this.getAuth())
    }

    let options = new RequestOptions({headers: headers});
    return this.http.get(url, options);
  }

  /**
   * Makes a _POST HTTP request to the base CESA API
   *
   * @param {string} route        - The path of the endpoint to be requested
   * @param {any} object          - The model to be used in the body of the request
   * @param {boolean} enableAuth  - If false, Auth headers are not sent
   */
  public post(route: string, object: any, enableAuth: boolean = true): Observable<Response> {
    let url = this.baseUri + route;
    // serialize JSON object to send as body of request
    let body = JSON.stringify(object);
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    });

    if(enableAuth) {
      headers.append('Authorization', this.getAuth())
    }

    let options = new RequestOptions({headers: headers});
    return this.http.post(url, body, options);
  }

  /**
   * Makes a _DELETE HTTP request to the base CESA API
   *
   * @param {string} route    - The path of the endpoint to be requested
   */
  public delete (route: string): Observable<boolean> {
    let url = this.baseUri + route;

    let headers = new Headers({
      'Accept': 'application/json',
      'Authorization': this.getAuth()
    });
    let options = new RequestOptions({ headers: headers });

    return this.http.delete(url, options)
                    .pipe(map(res => { return (res.ok) ? true: false})
                    ,catchError(this.handleError));
  }

  /**
   * Processes errors from CESA API
   *
   * @param {any} - Error Response from ZNode's API
   */
  private handleError (error: any) {
    // Log the error to the console for debugging
    console.error('Error Message from CESA');
    if(error)
      console.error(error);

    if(error.statusText)
      console.error(error.statusText);
    if(error._body != ""){
      // if there is content in the body, check for a specific ErrorMessage property and return that
      var body = error.json();

      if(body.HasError && body.ErrorMessage != ''){
        console.error(body.ErrorMessage);
        return throwError(body.ErrorMessage);
      }
    }
    else if(error.status == 500){
      // if no error message was sent, but a server error was still detected
      return throwError(error.statusText);
    }

    // otherwise construct a generic server error message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return throwError(errMsg);
  }

  ///
  ///Gets TenantInfo by domainName
  ///
  public getTenantInfo(): Observable<TenantInfo>
  {
    return this.get('tenant/info/' + location.host, false)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public controlAuthenticate(authInput: ControlAuthenticateInput): Observable<ControlAuthenticateResponse> {
    authInput.rootpath = this.getMisDomain();
    authInput.userType = "Employee";
    return this.post('control/authenticate', authInput, false)
      //.pipe(Map(response=>{}));

      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));

  }

  public getZnodeSettings(): Observable<ZnodeSettings> {
    return this.get('control/znodesettings')
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      }),
      catchError(this.handleError)
      )
  }

  public getDefaultProductMapping(): Observable<CesaProductMapping> {
    let endpoint = `control/defaultproductmapping`;

    return this.get(endpoint)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError)
      )
  }

  public setDefaultProductMapping(productMappings: CesaProductMapping): Observable<any> {
    let endpoint = 'control/defaultproductmapping';

    return this.post(endpoint, productMappings)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public getDefaultShippingMapping(): Observable<CesaProductMapping> {
    let endpoint = `control/defaultshipmentmapping`;

    return this.get(endpoint)
      .pipe(
            map(response =>
          {
            let body = response.json();
            return body || {};
          }),catchError(this.handleError)
        );


  }

  public setDefaultShippingMapping(productMappings: CesaProductMapping): Observable<any> {
    let endpoint = 'control/defaultshipmentmapping';

    return this.post(endpoint, productMappings)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public getProductMapping(id: number): Observable<CesaProductMapping> {
    let endpoint = `control/productmapping/${id}`;

    return this.get(endpoint)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public deleteProductMapping(id: number): Observable<any> {
    let endpoint = `control/productmapping/delete/${id}`;

    return this.delete(endpoint);
  }

  public setProductMapping(productMappings: CesaProductMapping): Observable<any> {
    let endpoint = `control/productmapping/${productMappings.eCommID}`;

    return this.post(endpoint, productMappings)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError))
  }

  public getControlProducts(id?: number, isActiveOnly?: boolean): Observable<ControlProduct[]> {
    let endpoint = 'control/products';
    let params = new URLSearchParams();

    if(id)
      params.set('id', id.toString());
    if(isActiveOnly)
      params.set('isActiveOnly', isActiveOnly.toString());

    endpoint += params.toString();

    return this.get(endpoint)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public getControlProductCategories(id?: number, isActiveOnly?: boolean): Observable<ControlProductCategory[]> {
    let endpoint = 'control/productcategories';
    let params = new URLSearchParams();

    if(id)
      params.set('id', id.toString());
    if(isActiveOnly)
      params.set('isActiveOnly', isActiveOnly.toString());

    endpoint += params.toString();

    return this.get(endpoint)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public getControlOrderStations(id?: number, isActiveOnly?: boolean): Observable<ControlStation[]> {
    let endpoint = 'control/orderstations';
    let params = new URLSearchParams();

    if(id)
      params.set('id', id.toString());
    if(isActiveOnly)
      params.set('isActiveOnly', isActiveOnly.toString());

    endpoint += params.toString();

    return this.get(endpoint)
    .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public getControlOrderLineItemStations(id?: number, isActiveOnly?: boolean): Observable<ControlStation[]> {
    let endpoint = 'control/itemstations';
    let params = new URLSearchParams();

    if(id)
      params.set('id', id.toString());
    if(isActiveOnly)
      params.set('isActiveOnly', isActiveOnly.toString());

    endpoint += params.toString();

    return this.get(endpoint)
    .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public getOrderStationMapping(storefrontId: number): Observable<CesaStationMapping> {
    let endpoint = `control/orderstationmapping/${storefrontId}`;

    return this.get(endpoint)
    .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public setOrderStationMapping(storefrontId: number, stationMapping: CesaStationMapping): Observable<any> {
    let endpoint = `control/orderstationmapping/${storefrontId}`;

    return this.post(endpoint, stationMapping)
    .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public getOrderLineItemStationMapping(storefrontId: number): Observable<CesaStationMapping> {
    let endpoint = `control/itemstationmapping/${storefrontId}`;

    return this.get(endpoint)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public setOrderLineItemStationMapping(storefrontId: number, stationMapping: CesaStationMapping): Observable<any> {
    let endpoint = `control/itemstationmapping/${storefrontId}`;

    return this.post(endpoint, stationMapping)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  //Not yet in use

  public getDefaultOrderStationMapping(): Observable<CesaStationMapping> {
    let endpoint = 'control/defaultorderstationmapping';

    return this.get(endpoint)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public setDefaultOrderStationMapping(stationMapping: CesaStationMapping): Observable<any> {
    let endpoint = 'control/defaultorderstationmapping';

    return this.post(endpoint, stationMapping)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public getDefaultOrderLineItemStationMapping(): Observable<CesaStationMapping> {
    let endpoint = 'control/defaultitemstationmapping';

    return this.get(endpoint)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }

  public setDefaultOrderLineItemStationMapping(stationMapping: CesaStationMapping): Observable<any> {
    let endpoint = 'control/defaultitemstationmapping';

    return this.post(endpoint, stationMapping)
      .pipe(map(response =>
      {
        let body = response.json();
        return body || {};
      })
      ,catchError(this.handleError));
  }
}
