import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable, forkJoin, throwError, pipe, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators'
import { SupportAccountService } from './support-account.service';

@Injectable()
export class ZnodeApiService {

  public get base(): string {
    return  sessionStorage.getItem('znode_url');
  }

  public get auth(): string {
    return 'Basic ' + sessionStorage.getItem('znode_auth');
  }

  constructor(private http: Http, private supportAccountService: SupportAccountService) { }

  public get (route:string, property?:string): Observable<any> {
    let url = this.base + route;

    let headers = new Headers({
      'Accept': 'application/json',
      'Authorization': this.auth
    });

    let options = new RequestOptions({ headers: headers });

    return this.http.get(url, options)
    .pipe(map(res => {
      if(property != null) {
        return this.getResponse(res, property)
      }
      else {
        return ("" == res.text()) ? {} : res.json();
      }
    })
    ,catchError(this.handleError));
  }

  private handleError (error: any) {
    if (error.status == 0) {
      console.error("Request timed out, check connect and try again");
      console.error(error);

      return throwError("Request timed out, check connect and try again");
    }
    else if (error._body != "") {
      var body = error.json();

      if(body.HasError && body.ErrorMessage != '') {
        console.error("Error Message from ZNode");
        console.error(body.ErrorMessage);
        return throwError(body.ErrorMessage);
      }
      else {
        console.error("Unidentified Error From Znode");
        console.log(body);
        return throwError("Unidentified Error From Znode");
      }
    }
    else if (error.status == 500) {
      console.error("Error Message from ZNode");
      console.log(error.statusText);
      console.error(error);

      return throwError(error.statusText);
    }

    let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';

    return throwError(errMsg);
  }

  private getResponse(res: Response, property?: string): any {
    let body = res.json();

    if (property != null) {
      let response = {
        index: body.PageIndex,
        size: body.PageSize,
        count: body.TotalResults,
        data: body[property]
      };

      return response;
    }
    else {
      return body || null;
    }
  }

  public getContacts(userName: string, request?: ZnodeRequest): Observable<ZnodeResponse> {
    if (!request) {
      request = new ZnodeRequest();
    }

    //request.addFilter({ key: 'userId', operator: 'ne', value: '00000000-0000-0000-0000-000000000000' });
    request.addExpand('user');
    request.addExpand('parentcompanyname');

    return this.get('accountcontacts' + request.toString(), 'Accounts')
    .pipe(map( res => {
      let accountUsers = res.data.filter(
        value => {
          let isDevSupportUser = this.supportAccountService.isDevSupportUser(userName);

          if (!isDevSupportUser) {

            const cyriousSupportUser = this.supportAccountService.isCyriousSupportUser(value.UserName);
            const corebridgeSupportUser = this.supportAccountService.isCorebridgeSupportUser(value.UserName);

            if (cyriousSupportUser || corebridgeSupportUser) {
              return false;
            }
          }

          return true;
        }
      );

      // replaced the original data with the filtered accountUsers
      res.data = accountUsers;
      res.count = accountUsers.length;

      return res;
    }));
  }

  public getCompanies(request?: ZnodeRequest): Observable<ZnodeResponse> {
    if (!request) {
      request = new ZnodeRequest();
    }

    //request.addFilter({ key: 'userId', operator: 'eq', value: '00000000-0000-0000-0000-000000000000' });

    return this.get('accountcompanies' + request.toString(), 'Accounts');
  }

  public getCategoryProducts(request?: ZnodeRequest): Observable<ZnodeResponse> {
    if (!request) {
      request = new ZnodeRequest();
    }

    request.addExpand('product');

    return this.get('productcategories' + request.toString(), 'ProductCategories');
  }

  public getProductsByCategory(categoryId: number, request?: ZnodeRequest): Observable<ZnodeResponse> {
    if (!request) {
      request = new ZnodeRequest();
    }

    return this.get(`products/category/${categoryId}` + request.toString(), 'Products');
  }

  public getProductsOutsideCategory(categoryId: number, request?: ZnodeRequest): Observable<ZnodeResponse> {
    if (!request) {
      request = new ZnodeRequest();
    }

    return this.get(`products/outsidecategory/${categoryId}` + request.toString(), 'Products');
  }

  public getProductsBundled(parentProductId: number, request?: ZnodeRequest): Observable<ZnodeResponse> {
    if (!request) {
      request = new ZnodeRequest();
    }

    return this.get(`productsbundled/${parentProductId}` + request.toString(), 'Products');
  }

  public getProductsNotBundled(parentProductId: number, request?: ZnodeRequest): Observable<ZnodeResponse> {
    if (!request) {
      request = new ZnodeRequest();
    }

    return this.get(`productsnotbundled/${parentProductId}` + request.toString(), 'Products');
  }
}

export class ZnodeRequest {
  cache: boolean;
  expands: string[];
  filters: ZnodeRequestFilter[];
  sort: ZnodeRequestSort[];
  index: number;
  size: number;

  constructor() {
    this.cache = false;
    this.expands = [];
    this.filters = [];
    this.sort = [];
    this.index = 0;
    this.size = 50;
  }

  addFilter(filter: ZnodeRequestFilter) {
    let f = this.filters.find((f) => f.key == filter.key);

    if (!f) {
      this.filters.push(filter);
    }
    else {
      f.operator = filter.operator;
      f.value = filter.value;
    }
  }

  addSort(sort: ZnodeRequestSort) {
    this.sort.pop();
    this.sort.push(sort);
    // current implementation is ONE sort at a time
    /*
    let s = this.sort.find((s) => s.key == sort.key);

    if (!s) {
      this.sort.push(sort);
    }
    else {
      s.asc = sort.asc;
    } */
  }

  addExpand(expand: string) {
    let e = this.expands.find((e) => e == expand);

    if (!e) {
      this.expands.push(expand);
    }
  }

  toString() {
    let str = '';

    if (this.cache) {
      str += '&cache=refresh';
    }

    if (this.expands && this.expands.length) {
      str += '&expand=' + this.expands.join(',');
    }

    if (this.filters && this.filters.length) {
      str += '&filter=';

      let filters = this.filters.map((item) => item.key + '~' + item.operator + '~' + item.value);
      str += filters.join(',');
    }

    if (this.sort && this.sort.length) {
      str += '&sort=';

      let sorts = this.sort.map((item) => item.key + '~' + (item.asc ? 'asc' : 'desc'));
      str += sorts.join(',');
    }

    if (this.index || this.size) {
      str += '&page=';
      if (this.index) {
        str += 'index~' + (this.index - 1);
      }

      if (this.size) {
        if (this.index) {
          str += ',';
        }

        str += 'size~' + this.size;
      }
    }

    return str.replace('&', '?');
  }

}

export class ZnodeRequestFilter {
  key: string;
  operator: string;
  value: any;
}

export class ZnodeRequestSort {
  key: string;
  asc: boolean;
}

export class ZnodeResponse {
  data: any[];
  index: number;
  size: number;
  count: number
}
