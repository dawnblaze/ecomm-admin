import { EventEmitter } from '@angular/core'; 

/**
 * Global event or event that can be access globally e.g. window events
 */

export class GlobalEventServiceType { 

  /**
   * window scroll event; no flags has been set yet
   */
  public fireScroll: EventEmitter<number> = new EventEmitter<number>();

  /**
   * All flags has been set
   */
  public onScroll: EventEmitter<number> = new EventEmitter<number>();

  /**
   * True means down - false means up
   */
  public onScrollDown: EventEmitter<boolean> = new EventEmitter<boolean>();

  public scrollYAxis: number = 0; 

  public isScrollingDown: boolean;

  constructor() {
    this.fireScroll.subscribe(scrollYAxis => {
      let isOnBottom = (document.documentElement.offsetHeight + document.documentElement.scrollTop) === document.documentElement.scrollHeight;
      
      // if scrollbar at bottom, isScrollingDown always true;
      if (isOnBottom) this.isScrollingDown = true;
      else this.isScrollingDown = scrollYAxis > this.scrollYAxis;

      this.scrollYAxis = scrollYAxis;

      this.onScrollDown.emit(this.isScrollingDown);
      
      this.onScroll.emit(scrollYAxis);
    });

  }
}

/**
 * This is intended to skip the injection feature and allow SingleTon instance across application
 * especially in generic-results-turbo-table.component.ts wherein it's an abstract class and we wanted this feature injected globally to all child classes
 */
export const GlobalEventService = new GlobalEventServiceType();
