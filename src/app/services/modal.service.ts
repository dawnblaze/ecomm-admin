import { SafeResourceUrl } from '@angular/platform-browser';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Observable, forkJoin } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { Modal } from '../modules/corebridge/models/modal';

@Injectable()
export class ModalService {

  

  private _modals = new Subject<Modal>();

  private _modalResponse = new Subject<boolean>();
  private _modelOpen = new Subject<boolean>();

  public modalAdded = this._modals.asObservable();

  public response = this._modalResponse.asObservable();
  public modelOpen = this._modelOpen.asObservable();

  constructor(private router: Router) { }

  public respond(response: boolean){
    this._modalResponse.next(response);
  }

  public close(){
    this._modelOpen.next(false);
  }

  private add(modal: Modal) {
      this._modals.next(modal);
  }

  public redirect(redirect) {
    this.router.navigate([`${redirect}`]);
  }

  public success(title: string, message: string, confirmText?: string, cancelText?: string): Observable<boolean> {
    var modal: Modal = {
      type: 'success',
      title: title,
      message: message,
      iframeUrl: null,
      confirmText: confirmText ? confirmText : 'Confirm',
      cancelText: cancelText ? cancelText : 'Cancel'
    };
    this.add(modal);
    return this.response;
  }

  public successNoCancel(title: string, message: string, confirmText?: string): Observable<boolean> {
    var modal: Modal = {
      type: 'successNoCancel',
      title: title,
      message: message,
      iframeUrl: null,
      confirmText: confirmText ? confirmText : 'Confirm',
      cancelText: null
    };
    this.add(modal);
    return this.response;
  }

  public warning(title: string, message: string, confirmText?: string, cancelText?: string): Observable<boolean> {
    var modal: Modal = {
      type: 'warning',
      title: title,
      message: message,
      iframeUrl: null,
      confirmText: confirmText ? confirmText : 'Confirm Delete',
      cancelText: cancelText ? cancelText : 'Cancel'
    };
    this.add(modal);
    return this.response;
  }

  public warningNoCancel(title: string, message: string, confirmText: string): Observable<boolean> {
    var modal: Modal = {
      type: 'warningNoCancel',
      title: title,
      message: message,
      iframeUrl: null,
      confirmText: confirmText ? confirmText : 'Confirm Delete',
      cancelText: null
    };
    this.add(modal);
    return this.response;
  }


  public error(title: string, message: string, confirmText?: string, cancelText?: string): Observable<boolean> {
    var modal: Modal = {
      type: 'error',
      title: title,
      message: message,
      iframeUrl: null,
      confirmText: confirmText ? confirmText : 'Continue',
      cancelText: cancelText ? cancelText : 'Cancel'
    };
    return this.response;
  }

  public info(title: string, message: string, confirmText?: string, cancelText?: string): Observable<boolean> {
    var modal: Modal = {
      type: 'info',
      title: title,
      message: message,
      iframeUrl: null,
      confirmText: confirmText ? confirmText : 'Continue',
      cancelText: null
    };
    this.add(modal);
    return this.response;
  }

  public iframe(iframeUrl: SafeResourceUrl): Observable<boolean> {
    var modal: Modal = {
      type: 'iframe',
      title: null,
      message: null,
      iframeUrl: iframeUrl,
      confirmText: null,
      cancelText: null
    };
    this.add(modal);
    return this.response;
  }

  public template(title: string, html: SafeResourceUrl, confirmText?: string, cancelText?: string): Observable<boolean> {
    var modal: Modal = {
      type: 'template',
      title: title,
      message: null,
      iframeUrl: html,
      confirmText: confirmText ? confirmText : 'Ok',
      cancelText: cancelText ? cancelText : 'Cancel'
    };
    this.add(modal);
    return this.response;
  }
}
