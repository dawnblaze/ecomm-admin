import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BehaviorSubject } from 'rxjs';
import { Observable, forkJoin } from 'rxjs';
import * as $ from 'jquery';


@Injectable()
export class ThemeService {
  currentTheme: Theme;

  constructor(private title: Title) {
    this.currentTheme = new Theme();
  }

  applyTheme(theme: Theme) {
    this.currentTheme = theme;
    this.setFavicon(theme.favicon);
    this.setTitle(theme.name);
    this.setCss(theme.css);
  }

  setFavicon(favicon: string) {
    $('#favicon').attr('href', favicon);
  }

  setTitle(title: string) {
    this.title.setTitle(title);
  }

  setCss(css: string) {
    let removeClass = css == 'mistype-1' ? 'mistype-0' : 'mistype-1';
    $('body').removeClass(removeClass);
    $('body').addClass(css);
  }
}

export class Theme {
  favicon: string = '';
  title: string = '';
  name: string = '';
  email: string = '';
  url: string = '';
  css: string = '';
  loginLoader: string = '';
  contentLoader: string = '';
}

export class CyriousTheme extends Theme {
  constructor() {
    super();

    this.favicon = 'assets/img/favicon_cyrious.ico';
    this.name = 'Control';
    this.email = 'ecommerce@cyrious.com';
    this.url = 'cyrious.com';
    this.css = 'mistype-1';
    this.loginLoader = 'assets/img/cyrious_loader.gif';
    this.contentLoader = 'assets/img/cyrious_content_loader.gif';
  }
}

export class CorebridgeTheme extends Theme {
  constructor() {
    super();

    this.favicon = 'assets/img/favicon.ico';
    this.name = 'CoreBridge';
    this.email = 'support@corebridge.net';
    this.url = 'corebridgesoftware.com';
    this.css = 'mistype-0';
    this.loginLoader = 'assets/img/cb_loader.gif';
    this.contentLoader = 'assets/img/content_loader.gif';
  }
}
