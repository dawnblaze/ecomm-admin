import { Injectable } from '@angular/core';

@Injectable()
export class SupportAccountService {

    private readonly coreBridgeSupportUsers = ['cbms.support', 'cbms.admin'];
    private readonly cyriousSupportUsers = ['administrator'];

    constructor() {}

    public isCorebridgeSupportUser(username: string) {
        if (username && this.coreBridgeSupportUsers.indexOf(username) > -1) {
            return true;
        }

        return false;
    }
    public isCyriousSupportUser(username: string) {
        if (username && this.cyriousSupportUsers.indexOf(username) > -1) {
            return true;
        }

        return false;
    }

    public isDevSupportUser(username) {
        let coreBridgeSupportUser = this.isCorebridgeSupportUser(username);
        let cyriousSupportUser = this.isCyriousSupportUser(username);

        if (coreBridgeSupportUser || cyriousSupportUser) {
            return true;
        }

        return false;
    }
}