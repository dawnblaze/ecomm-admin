import { Injectable } from '@angular/core';
import { Observable, forkJoin } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class TooltipService {
  showTooltipSubject: BehaviorSubject<TooltipEvent> = new BehaviorSubject<TooltipEvent>(null);

  constructor() { }

  show(data: TooltipEvent) {
    this.showTooltipSubject.next(data);
  }

  hide() {
    this.showTooltipSubject.next(null);
  }

  get showTooltip(): Observable<TooltipEvent> {
    return this.showTooltipSubject.asObservable();
  }
}

export class TooltipEvent {

  constructor(text:string, left: number, top: number, right:number = null, leftOffset: number = null, topOffset: number= null){
    this.text=text;
    
    console.log(`B4 Left: ${left} Top: ${top} Right: ${right}`)

    leftOffset === null ? this.left = left : this.left = left + leftOffset;
    topOffset === null ? this.top = top : this.top = top + topOffset;
    right ? this.right = right : null;
    
    
    console.log(`Left: ${this.left} Top: ${this.top} Right: ${this.right}`)
  }
  text: string;
  left: number;
  top: number;
  right: number = null;

  
}
