import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';

export interface CanComponentDeactivate {
    // canDeactivate methods need to return boolean in some form
    canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable() 
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
    canDeactivate(component: CanComponentDeactivate) {
        // if component has a canDeactivate method, call it. otherwise allow the deactivation
        return component.canDeactivate ? component.canDeactivate() : true;
    }
}