/**
 * ZNode Service Provider
 *
 * Provides interactions with the ZNode Ecommerce API for
 * the CBMS Ecommerce module. Methods in this class should
 * return observables that can be subscribed to, and results
 * passed back should be typed to the correct class definitions.
 *
 * @author Mike Payne <payne@rivetal.com>
 * @link http://ecommapi.store.corebridge.net/Help
 **/

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable, forkJoin, pipe, throwError, of }     from 'rxjs';
import { BehaviorSubject, Subject } from 'rxjs';
import { switchMap, map, flatMap, catchError, filter } from 'rxjs/operators';

// ZNode Response Definitions
import { Account } from '../modules/ecommerce/models/account';
import { Addon } from '../modules/ecommerce/models/addon';
import { AddonValue } from '../modules/ecommerce/models/addon-value';
import { Bundle } from '../modules/ecommerce/models/bundle';
import { Catalog } from '../modules/ecommerce/models/catalog';
import { Category } from '../modules/ecommerce/models/category';
import { CategoryNode } from '../modules/ecommerce/models/category-node';
import { CategoryProfile } from '../modules/ecommerce/models/category-profile'
import { Contact } from '../modules/ecommerce/models/contact';
import { FluteDirection } from '../modules/ecommerce/models/flute-direction';
import { Product } from '../modules/ecommerce/models/product';
import { ProductCategory } from '../modules/ecommerce/models/product-category';
import { Portal } from '../modules/ecommerce/models/portal';
import { PortalCatalog } from '../modules/ecommerce/models/portal-catalog';
import { PortalProfile } from '../modules/ecommerce/models/portal-profile';
import { Profile } from '../modules/ecommerce/models/profile';
import { Order } from '../modules/ecommerce/models/order';
import { Sku } from '../modules/ecommerce/models/sku';
import { SkuProfile } from '../modules/ecommerce/models/sku-profile';
import { TaxClass } from '../modules/ecommerce/models/tax-class';
import { ShippingMethod } from '../modules/ecommerce/models/shipping-method';
import { ShippingBox } from '../modules/ecommerce/models/shipping-box';

import { ShippingRule } from '../modules/ecommerce/models/shipping-rule';
import { ShippingRuleType } from '../modules/ecommerce/models/shipping-rule-type';
import { Supplier } from '../modules/ecommerce/models/supplier';
import { Manufacturer } from '../modules/ecommerce/models/manufacturer';
import { Facet } from '../modules/ecommerce/models/facet';
import { ProductTag } from '../modules/ecommerce/models/product-tag';
import { ProductTier } from '../modules/ecommerce/models/product-tier';
import { ProductHighlight } from '../modules/ecommerce/models/product-highlight';
import { DigitalAsset } from '../modules/ecommerce/models/digital-asset';
import { EmailTemplate } from '../modules/ecommerce/models/email-template';
import { ContentPage } from '../modules/ecommerce/models/content-page';
import { Theme } from '../modules/ecommerce/models/theme';
import { CSS } from '../modules/ecommerce/models/css';
import { Redirect } from '../modules/ecommerce/models/redirect';
import { Domain } from '../modules/ecommerce/models/domain';
import { Image } from '../modules/ecommerce/models/image';
import { ProductImage } from '../modules/ecommerce/models/product-image';
import { ProductAsset } from '../modules/ecommerce/models/product-asset';
import { ProductImageType } from '../modules/ecommerce/models/product-image-type';
import { Country } from '../modules/ecommerce/models/country';
import { ProductArtwork } from '../modules/ecommerce/models/product-artwork';
import { Review } from '../modules/ecommerce/models/review';
import { CurrencyType } from '../modules/ecommerce/models/currency-type';

import { AlertService } from './alert.service';
import { CoreBridgeService } from './corebridge.service';
import { SupportAccountService } from './support-account.service';
import { OrdersPaged } from "../modules/ecommerce/models/orders-paged";
import { ProductsPaged } from "../modules/ecommerce/models/products-paged";
import { AddOnsPaged } from "../modules/ecommerce/models/addon-paged";
import { EntityIds } from "../modules/ecommerce/models/entityIds";
import { ContentSection } from "../modules/ecommerce/models/content-section";
import { ReportSection } from "../modules/ecommerce/models/reports";
import { ReportInfo } from "../modules/ecommerce/models/report-info";
import { ShippingCarrier } from '../modules/ecommerce/models/shipping-carrier';
import { ShippingCarrierServiceType } from '../modules/ecommerce/models/shipping-carrier-service-type';
import { ShippingInsuranceType } from '../modules/ecommerce/models/shipping-insurance';
import { ShippingCarrierRateRule } from '../modules/ecommerce/models/shipping-carrier-rate-rule';
import { AccountsPaged } from '../modules/ecommerce/models/accounts-paged';
import { ConfigSetting } from '../modules/ecommerce/models/config-setting';
import { PaymentSetting } from '../modules/ecommerce/models/payment-setting';
import { PortalCountry } from '../modules/ecommerce/models/portal-country';
import { ApprovalGroup, OrderApproval, OrderApprovalCustomer } from '../modules/ecommerce/models/approval-group';
import { ApprovalRule } from '../modules/ecommerce/models/approval-rule';
import { ApprovalApprover } from '../modules/ecommerce/models/approval-approver';
import { ApprovalCustomer } from '../modules/ecommerce/models/approval-customer';
import { ApprovalRuleType } from '../modules/ecommerce/models/approval-rule-type';
import { ApprovalRuleLimitType } from '../modules/ecommerce/models/approval-rule-limit-type';
import { AccountSearchParameters } from '../modules/ecommerce/models/approval-account-search-parameters';

import { EventLog } from '../modules/ecommerce/models/eventLog'
import { CodaTokenResponse } from '../modules/ecommerce/models/coda-token-response';
import { EndorBIDResponse } from '../modules/ecommerce/models/endor-bid-response';
@Injectable()
export class ZnodeService {

  private _base: string;
  private _passConfigSetting = new BehaviorSubject<ConfigSetting>(new ConfigSetting());
  public passConfigSetting = this._passConfigSetting.asObservable();

  public get base() {
    if (undefined === this._base) {
      this._base = sessionStorage.getItem('znode_url');
    }

    return this._base;
  }

  isCreditCardActive$: Observable<any>;
  private methodSubject = new Subject<any>();

  /**
   * Uses dependency injection to allow access to Angular's HTTP component
   *
   **/
  constructor(private http:Http, private alertService: AlertService, private corebridgeService: CoreBridgeService, private supportAccountService: SupportAccountService) {
    this.isCreditCardActive$ = this.methodSubject.asObservable();
  }

  isCreditCardActive(data) {
    this.methodSubject.next(data);
  }

  public getAuth(): string {

    return 'Basic ' + sessionStorage.getItem('znode_auth');
  }

  /**
   * Passes the configuration settings in the dev-support page
   * to the sidebar page
   *
   * * @param {ConfigSetting} configSetting - The configuration settings of a tenant
   *
   */
  public setConfigSetting (configSetting: ConfigSetting) {
    this._passConfigSetting.next(configSetting);
  }

  /**
   * Makes a _GET HTTP request to the base ZNode API
   *
   * @param {string} route    - The path of the endpoint to be requested
   * @param {string} property - !Deprecated
   * @return {any}            - JSON object
   */
  public get (route:string, property?:string ): Observable<any> {
    let url = this.base + route;

    let headers = new Headers({
      'Accept': 'application/json',
      'Authorization': this.getAuth()
    });
    let options = new RequestOptions({ headers: headers });

    return this.http.get(url, options)
                    .pipe(map( res => {
                      if(property != null){
                        return this.getResponse(res, property)
                      }else {
                        return ("" == res.text()) ? {} : res.json();
                      }
                    })
                    ,catchError(this.handleError));
  }

  /**
   * Makes a _POST HTTP request to the base ZNode API with JSON content
   *
   * @param {string} route    - The path of the endpoint to be requested
   * @param {any} object      - The model to be used in the body of the request
   * @param {string} property - The property name of the response object that will be returned
   * @return {any} JSON object
   */
  public post ( route: string, object: any, property?: string): Observable<any> {
    let url = this.base + route;
    // condense JSON object into a single string in order to send as body of request
    let body = JSON.stringify(object);
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': this.getAuth()
    });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(url, body, options)
                     .pipe(map( res => {
                      if(property != null){
                        return this.getResponse(res, property)
                      }else {
                        return ("" == res.text()) ? {} : res.json();
                      }
                    })
                    ,catchError(this.handleError));
  }


  /**
   * Makes a _POST HTTP request to the base ZNode API with Form Data Content
   *
   * @param {string} route    - The path of the endpoint to be requested
   * @param {any} object      - The model to be used in the body of the request
   * @param {string} property - The property name of the response object that will be returned
   * @return {any} JSON object
   */
  public formData ( route: string, object: any, property?: string): Observable<any> {
    let url = this.base + route;

    let headers = new Headers({
      'Accept': 'application/json',
      'Authorization': this.getAuth()
    });

    let options = new RequestOptions({ headers: headers });

    return this.http.post(url, object, options)
                     .pipe(map( res => {
                      if(property != null){
                        return this.getResponse(res, property)
                      }else {
                        return ("" == res.text()) ? {} : res.json();
                      }
                    })
                    ,catchError(this.handleError));
  }

  /**
   * Makes a _PUT HTTP request to the base ZNode API
   *
   * @param {string} route    - The path of the endpoint to be requested
   * @param {any} object      - The model to be used in the body of the request
   * @param {string} property - The property name of the response object that will be returned
   * @return {any} JSON object
   */
  public put (route: string, object: any, property?: string): Observable<any> {

    let url = this.base + route;
    // condense JSON object into a single string in order to send as body of request
    let body = JSON.stringify(object);

    let headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': this.getAuth()
    });
    let options = new RequestOptions({ headers: headers });

    return this.http.put(url, body, options)
                     .pipe(map( res => {
                      if(property != null){
                        return this.getResponse(res, property)
                      }else {
                        return ("" == res.text()) ? {} : res.json();
                      }
                    })
                    ,catchError(this.handleError));
  }


  /**
   * Makes a _DELETE HTTP request to the base ZNode API
   *
   * @param {string} route    - The path of the endpoint to be requested
   */
  public delete (route: string): Observable<boolean> {
    let url = this.base + route;

    let headers = new Headers({
      'Accept': 'application/json',
      'Authorization': this.getAuth()
    });
    let options = new RequestOptions({ headers: headers });

    return this.http.delete(url, options)
                    .pipe(map(res => { return (res.ok) ? true: false})
                    ,catchError(this.handleError));
  }

  /**
   * Gets the JSON object of a response object's body
   *
   * @param {Response}
   * @return {any | null}
   */
  private extractData(res: Response) {
    return res.json() || null;
  }

  /**
   * Processes errors from ZNode's API
   *
   * @param {any} - Error Response from ZNode's API
   */
  private handleError (error: any) {
    // Log the error to the console for debugging
    if(error.status == 0){
      console.error("Request timed out, check connect and try again");
      console.error(error);

      return throwError("Request timed out, check connect and try again");
    }
    else if(error._body != ""){
      // if there is content in the body, check for a specific ErrorMessage property and return that

      var body = error.json();

      if(body.HasError && body.ErrorMessage != ''){
        console.error("Error Message from ZNode");
        console.error(body.ErrorMessage);
        return throwError(body.ErrorMessage);
      }
      else{
        console.error("Unidentified Error From Znode");
        console.log(body);
        return throwError("Unidentified Error From Znode");
      }
    }
    else if(error.status == 500){
      console.error("Error Message from ZNode");
      console.log(error.statusText);
      console.error(error);
      // if no error message was sent, but a server error was still detected
      return throwError(error.statusText);
    }

    // otherwise construct a generic server error message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return throwError(errMsg);
  }

  /**
   * Gets a specific property from a Respnse's body
   *
   * @param {Response} res    - Response from server
   * @param {string} property - Name of property to be returned
   * @return {any}            - JSON Object
   */
  private getResponse(res: Response, property?: string): any{
    let body = res.json();
    if(property != null){
      return body[property] || null;
    }
    else {
      return body || null;
    }
  }

  /**
   * Get a list of all Customer Objects (Accounts in ZNode)
   *
   * @return {Account[]} - list of Account Objects
   */
  public getCompanies(): Observable<Account[]> {
    return this.get('accounts?filter=userId~eq~00000000-0000-0000-0000-000000000000','Accounts');
  }

  /**
   * Get a specific Customer Object
   *
   * @param {number}    - AccountId of requested object
   * @return {Account}  - Instance of Account class
   */
  public getCompany(AccountId: number): Observable<Account> {
    return this.get('accounts/' + AccountId, 'Account');
  }

  /**
   * Get a list of all Contact Objects (Customer in ZNode)
   * @TODO: define the customer class in order to type responses
   *
   * @return {any} - Customer Object
   */
  public getContacts(): Observable<any[]> {
    return this.get('accounts?filter=userId~ne~00000000-0000-0000-0000-000000000000&expand=user,parentcompanyname','Accounts')
    .pipe(map( Contacts => {
      return Contacts.map(
        value => {
          if(value.UserName != 'cbms.support'){
            return value;
          }
        }
      ).filter(x=>!!x);
    }));
  }

  public getContactById(id: number): Observable<any> {
    return this.get(`accounts/${id}?expand=user,parentcompanyname,profiles&cache=refresh`, 'Account');
  }

  public getContactsByIds(ids: number[] | string[]): Observable<any> {
    return this.post('accounts/listbyprofileids?filter=userId~ne~00000000-0000-0000-0000-000000000000&expand=user,parentcompanyname&cache=refresh', { Ids: ids.join(',') }, 'Accounts');
  }

  public getContactsByProfileIds(userName: string, ids: number[] | string[], pageIndex: number, pageSize: number, filters: any, sorts: any): Observable<AccountsPaged> {
    let endpoint = 'accounts/listcontactsbyprofileids?expand=user,parentcompanyname&cache=refresh';
    endpoint += `&page=index~${pageIndex},size~${pageSize}`;
    if (filters && filters.length) {
      endpoint += '&filter=';

      let filterString = filters.map((item) => item.key + '~' + item.operator + '~' + item.value);
      endpoint += filterString.join(',');
    }

    return this.post(endpoint, { Ids: ids.join(',') })
    .pipe(map( res => {
      let accountUsers = res.Accounts.filter(
        value => {
          let isDevSupportUser = this.supportAccountService.isDevSupportUser(userName);

          if (!isDevSupportUser) {

            const cyriousSupportUser = this.supportAccountService.isCyriousSupportUser(value.UserName);
            const corebridgeSupportUser = this.supportAccountService.isCorebridgeSupportUser(value.UserName);

            if (cyriousSupportUser || corebridgeSupportUser) {
              return false;
            }
          }

          return true;
        }
      );

      // replaced the original data with the filtered accountUsers
      res.Accounts = accountUsers;
      res.TotalResults = accountUsers.length;

      return res;
    }));
  }

  public getCustomersCanvasUserId(): Observable<string> {
    return this.get(`accounts/getcustomerscanvasuserid`, 'Value');
  }
  /**
   * Get a list of all Catalogs
   *
   * @return {Catalog[]} - list of Catalog objects
   */
  public getCatalogs(): Observable<Catalog[]> {
    return this.get('catalogs','Catalogs');
  }

  /**
   * Get a list of all Catalogs and Expand ProductIds
   *
   * @return {Catalog[]} - list of Catalog objects
   */
  public getCatalogsAndExpandProductIds(): Observable<Catalog[]> {
    return this.get('catalogs/?expand=productIds&cache=refresh', 'Catalogs').pipe(map( Catalogs => {
        var results: Catalog[] = [];
        for(var cat of Catalogs){
          if(cat.DisplayOrder == null){
            cat.DisplayOrder = 99;
          }
          results.push(cat);
        }

        return results;
      }));
  }

  /**
   * Get a list of all Catalogs and Expand ProductIds, CategoryCount
   *
   * @return {Catalog[]} - list of Catalog objects
   */
  public getCatalogsAndExpandProductIdsCategoryCount(): Observable<Catalog[]> {
    return this.get('catalogs/?expand=productIds,categoryCount&cache=refresh', 'Catalogs').pipe(map( Catalogs => {
        var results: Catalog[] = [];
        for(var cat of Catalogs){
          if(cat.DisplayOrder == null){
            cat.DisplayOrder = 99;
          }
          results.push(cat);
        }

        return results;
      }));
  }

  /**
   * Get a specific Catalog object
   *
   * @param {number} - CatalogId of requested object
   * @param {Catalog} - Catalog object
   */
  public getCatalog(CatalogId: number): Observable<Catalog> {
    return this.get('catalogs/'+CatalogId,'Catalog');
  }

  /**
   * Get a list of Catalog Nodes attached to a specific Catalog
   *
   * @param {number}          - Optional CatalogId of catalog by which to filter CategoryNodes
   * @return {CategoryNode[]} - list of CategoryNode objects
   */
  public getCatalogCategoryNodes(CatalogId?: number): Observable<CategoryNode[]> {
    let url = 'categorynodes?expand=catalog';
    if(CatalogId > 0){
      url += '&filter=catalogId~eq~'+CatalogId;
    }
    return this.get(url, 'CategoryNodes');
  }

  /**
   * Get a list of products attached to a specific Catalog
   *
   * @param {number}          - CatalogId of catalog by which to filter CategoryNodes
   * @return {Product[]} - list of Product objects
   */
  public getCatalogProducts(CatalogId: number): Observable<Product[]> {
    let url = 'products/catalog/'+CatalogId;
    return this.get(url, 'Products');
  }

  /**
   * Get a specific CategoryNode
   *
   * @param {number}        - CategoryNodeId of requested object
   * @return {CategoryNode} - CategoryNode object
   */
  public getCategoryNode(CategoryNodeId: number): Observable<CategoryNode> {
    let url = 'categorynodes/'+CategoryNodeId;
    return this.get(url, 'CategoryNode');
  }

  /**
   * Create a new CategoryNode (relationship between a catalog and category)
   *
   * @param {CategoryNode}  - Model of new CategoryNode
   * @return {CategoryNode} - The newly created CategoryNode
   */
  public createCategoryNode(node: CategoryNode): Observable<CategoryNode> {
    if(node.ParentCategoryNodeId == 0){
      delete node.ParentCategoryNodeId;
    }

    return this.post('categorynodes', node, 'CategoryNode');
  }

  /**
   * Updates a CategoryNode
   *
   * Could be used to change parent category, or adjust theme/css, etc.
   *
   * @param {CategoryNode}
   * @return {CategoryNode} - The newly updated CategoryNode
   */
  public updateCategoryNode(node: CategoryNode): Observable<CategoryNode> {
    let route = 'categorynodes/' + node.CategoryNodeId;
    return this.put(route, node, 'CategoryNode');
  }

  /**
   * Delete a CategoryNode
   *
   * Removes the association between a catalog and category
   */
  public deleteCategoryNode(id: number): Observable<boolean> {
    return this.delete('categorynodes/'+id);
  }

  /**
   * Updates a Catalog
   *
   */
  public updateCatalog(catalog: Catalog): Observable<any> {
    let route = 'catalogs/' + catalog.CatalogId;
    return this.put(route, catalog, 'Catalog');
  }

  /**
   * Copies a Catalog (creates exact duplicate in ZNode)
   *
   */
  public copyCatalog(catalog: Catalog): Observable<any> {
    return this.post('copycatalog', catalog);
  }

  /**
   * Deletes a Catalog
   *
   */
  public deleteCatalog(CatalogId: number): Observable<any> {
    return this.delete('catalogs/'+ CatalogId);
  }

  /**
   * Creates a new Catalog
   *
   */
  public createCatalog(catalog: Catalog): Observable<Catalog>{
    return this.post('catalogs', catalog, 'Catalog');
  }

  /**
   * Get a list of Category Objects
   *
   */
  public getCategories(expand: string = null): Observable<Category[]> {

    let route = 'categories?cache=refresh' + (expand ? '&expand=' + expand : '');
    return this.get(route, 'Categories').pipe(map( Categories => {
      var results: Category[] = [];
      for(var cat of Categories){
        if(cat.DisplayOrder == null){
          cat.DisplayOrder = 99;
        }
        results.push(cat);
      }

      return results;
    }));

  }

    /**
     * Get a list of Category Objects and expand Product Ids
     *
     */
    public getCategoriesAndExpandProductIds(): Observable<Category[]> {
      return this.getCategories('productIds');
    }

    public getCategoriesByCatalog(catalogIds: number[]): Observable<Category[]> {
        return this.get('categories/catalog/' + catalogIds.join(',') + '?expand=productIds', 'Categories');
    }
  /**
   * Get a specific Category object
   *
   */
  public getCategory(CategoryId: number): Observable<Category> {

    let url = 'categories/' + CategoryId + '?cache=refresh';

    return this.get(url, 'Category')
                    .pipe(map( result => {
                      if(result.DisplayOrder == null){
                        result.DisplayOrder = 99;
                      }

                      return result;
                    }));
  }

  /**
   * Updates a Category
   *
   */
  public updateCategory(category: Category): Observable<Category> {
    category.IsVisible = true; // force visible by default

    let route = 'categories/' + category.CategoryId;
    return this.put(route, category, 'Category');
  }

  /**
   * Deletes a Category
   *
   */
  public deleteCategory(CategoryId: number): Observable<boolean> {
    return this.delete('categories/' + CategoryId);
  }

  /**
   * Creates a new Category
   *
   */
  public createCategory(category: Category): Observable<Category>{
    category.IsVisible = true; // force visible by default

    return this.post('categories', category, 'Category');
  }

  public copyCategory(CategoryId: number): Observable<boolean> {
    let url = 'copycategory/' + CategoryId;

    return this.get(url);
  }

  /**
   * Get list of Products associated with a Category
   * @TODO: Test that this actually limits the results
   *
   */
  public getCategoryProducts(categoryId: number, productId: number = 0): Observable<ProductCategory[]> {
    var path = 'productcategories?expand=product&filter=categoryId~eq~'+categoryId;
    if(productId > 0){
      path += '&filter=productId~eq~'+productId;
    }

    return this.get(path, 'ProductCategories');
  }

  /**
   * Get list of Products associated with a Category for use in advanced filters
   *
   */
  public getCategoryProductsForFilter(category: Category): Observable<any> {
    let url = 'categories/'+ category.CategoryId +'?expand=productIds';

    return this.get(url)
                .pipe(map( categories => {
                      var productIds: any[] = [];

                      for(var id of categories.Category.ProductIds) {
                        productIds.push(id);
                      }

                      return {
                        Name: category.Name,
                        CategoryId: category.CategoryId,
                        ProductIds: productIds
                      };
                }));
  }


  /**
   * Get a list of products attached to a specific Catalog for use in advanced filters
   *
   */
  public getCatalogProductsForFilter(catalog: Catalog): Observable<any> {
    let url = 'products/catalog/'+catalog.CatalogId;

    return this.get(url, 'Products')
                    .pipe(map( products => {
                      var productIds: any[] = [];

                      for(var product of products) {
                        productIds.push(product.ProductId);
                      }

                      return {
                        Name: catalog.Name,
                        CategoryId: catalog.CatalogId,
                        ProductIds: productIds
                      };

                    }));
  }

  /**
   * Get a paged list of products attached to a specific Catalog for use in advanced filters
   *
   */
  public getCatalogProductsForFilterPage(catalog: Catalog): Observable<any> {
    let url = 'catalogs/'+catalog.CatalogId+'?expand=productIds';

    return this.get(url)
                    .pipe(map( res => {
                      var productIds: any[] = [];

                      for(var id of res.Catalog.ProductIds) {
                        productIds.push(id);
                      }

                      return {
                        Name: res.Catalog.Name,
                        CategoryId: res.Catalog.CatalogId,
                        ProductIds: productIds
                      };

                    }));
  }

  /**
   * Get Product Category by ProductId and CategoryId
   *
   */
  public getProductCategory(productId: number, categoryId: number): Observable<ProductCategory> {

    var path = `productcategories/${productId}/${categoryId}`;

    return this.get(path, 'ProductCategory');
  }

  /**
   * Get list of Products associated with a Product or Category
   *
   */
  public getProductCategories(productId: number = 0, categoryId: number = 0): Observable<ProductCategory[]> {

    var path = 'productcategories?expand=category&expand=product';

    if(productId > 0){
      path += '&filter=productId~eq~'+productId;
    }
    if(categoryId > 0){
      path += '&filter=categoryId~eq~'+categoryId;
    }

    return this.get(path, 'ProductCategories');
  }

  public createCategoryProduct(categoryProduct: ProductCategory): Observable<ProductCategory> {
    categoryProduct.IsActive = true; // force active by default
    return this.post('productcategories', categoryProduct, 'ProductCategory');
  }

  public createCategoryProductsFromIds(categoryId: Number, productIds: Number[]): Observable<ProductCategory> {
    return this.post(`productcategories/${categoryId}`, {Ids: productIds.join(',')}, 'ProductCategories');
  }

  /**
   * Updates an existing Category Product association
   *
   */
  public updateCategoryProduct(categoryProduct: ProductCategory): Observable<ProductCategory> {
    let route = 'productcategories/' + categoryProduct.ProductCategoryId;
    return this.put(route, categoryProduct, 'ProductCategory');
  }

  /**
   * Deletes a Category Product (association between the two)
   *
   */
  public deleteCategoryProduct(categoryProductId: number): Observable<boolean> {
    return this.delete('productcategories/' + categoryProductId);
  }

  /**
   * Get a list of Category/Profiles
   *
   */
  public getCategoryProfiles(categoryId: number): Observable<CategoryProfile[]> {
    return this.get('categoryprofile/getcategoryprofilesbycategoryid/' + categoryId + '?cache=refresh', 'CategoryProfiles');
  }

  /**
   * Deletes a Category Profile (association between the two)
   *
   */
  public deleteCategoryProfile(categoryProfileId: number): Observable<boolean> {
    return this.delete('categoryprofile/' + categoryProfileId);
  }

  public updateCategoryProfile(categoryProfile: CategoryProfile): Observable<CategoryProfile> {
    if(categoryProfile.CategoryProfileID > 0){
      console.log('updating Category Profile');
      console.log(categoryProfile);
      return this.put('categoryprofile/' + categoryProfile.CategoryProfileID, categoryProfile, 'CategoryProfile');
    }
    else {
      console.log('creating new Category Profile');
      console.log(categoryProfile);
      return this.createCategoryProfile(categoryProfile);
    }
  }

  public createCategoryProfile(categoryProfile: CategoryProfile): Observable<CategoryProfile> {
    return this.post('categoryprofile', categoryProfile, 'CategoryProfile');
  }

  /**
   * Get a list of SKU/Profiles
   *
   */
  public getSkuProfiles(skuId: number): Observable<SkuProfile[]> {
    return this.get('skuprofileeffective/getskuprofileeffectivebyskuid/' + skuId + '?cache=refresh', 'SkuProfileEffectives');
  }

  /**
   * Deletes a SKU Profile (association between the two)
   *
   */
  public deleteSkuProfile(skuProfileId: number): Observable<boolean> {
    return this.delete('skuprofileeffective/' + skuProfileId);
  }

  public updateSkuProfile(skuProfile: SkuProfile): Observable<SkuProfile> {
    if(skuProfile.SkuProfileEffectiveID > 0){
      return this.put('skuprofileeffective/' + skuProfile.SkuProfileEffectiveID, skuProfile, 'SkuProfileEffective');
    }
    else {
      return this.createSkuProfile(skuProfile);
    }
  }

  public createSkuProfile(skuProfile: SkuProfile): Observable<SkuProfile> {
    return this.post('skuprofileeffective', skuProfile, 'SkuProfileEffective');
  }

  public findSkuProfileBySku(sku: string): Observable<any> {
    return this.get('attributebysku/' + sku);
  }

  /**
   * Gets a list of Add-Ons
   *
   */
  public getAddons(expand: string = null): Observable<Addon[]> {
    let username = 'cbms.support';
    let route = "addons";
    if(expand) route += '?expand=' + expand;

    return this.get(route,'AddOns');
  }

  /**
   * Gets a specific Add-Ons
   *
   */
  public getAddon(AddOnId: number): Observable<Addon> {
    return this.get('addons/' + AddOnId,'AddOn');
  }

  /**
   * Deletes an Add-On
   *
   */
  public deleteAddon(AddOnId: number): Observable<any> {
    return this.delete('addons/' + AddOnId);
  }

  /**
   * Updates an Add-On
   *
   */
  public updateAddon(addon: Addon): Observable<Addon> {
    let url = 'addons/' + addon.AddOnId;

    return this.put(url, addon, 'AddOn');
  }

  public copyAddon(AddOnId: number): Observable<any> {
    let url = 'copyaddon/' + AddOnId;
    return this.get(url);
  }

  /**
   * Creates a new Add-On
   *
   */
  public createAddOn(addon: Addon): Observable<Addon> {
    return this.post('addons', addon, 'AddOn');
  }

  /**
   * Gets a list of Add-Ons associated with a specific product
   *
   */
  public getProductAddOns(productId: number): Observable<Addon[]> {
    let route = "productaddons/" + productId;
    return this.get(route,'AddOns');
  }

  /**
   * Gets a list of Add-Ons that are not associated with a specific product
   *
   */
  public getUnassociatedProductAddOns(productId: number, portalId: number): Observable<Addon[]> {
    let route = "unassociatedaddons/" + productId + '/' + portalId;
    return this.get(route,'AddOns');
  }

  public createProductAddons(productAddons: Addon[]): Observable<Addon[]> {
    return this.post('associateaddon', productAddons);
  }

  /**
   * Deletes a Product Add-On association
   *
   */
  public deleteProductAddon(AddOnId: number): Observable<boolean> {
    return this.delete('removeproductaddon/' + AddOnId);
  }


  /**
   * Get a list of Add-On Values
   *
   */
  public getAddonValues(AddOnId: number): Observable<AddonValue[]> {
    return this.get('addonValues?filter=AddOnID~eq~' + AddOnId + '&sort=AddOnValueID~asc', 'AddOnValuesList')
      .pipe(map((res) => {
        return res.AddOnValues;
      }));
  }

  /**
   *  Gets the default addon value of an addon
   */
  public getDefaultAddonValue(AddOnId: number): Observable<AddonValue> {
    return this.getAddonValues(AddOnId).pipe(flatMap(
      addonValues => {
        let defaultAddonValue = addonValues.filter(addonValue => {
          return addonValue.IsDefault;
        })[0];

        if (defaultAddonValue) {
          return this.getAddonValue(defaultAddonValue.AddOnValueId);
        }
        else {
          return of(null);
        }
      }));
  }

  /**
   * Gets a specific Add-On Values
   *
   */
  public getAddonValue(AddOnValueId: number): Observable<AddonValue> {
    return this.get('addonValues/'+AddOnValueId, 'AddOnValue');
  }

  /**
   * Updates an Add-On Value
   *
   */
  public updateAddonValue(AddOnValue: AddonValue): Observable<AddonValue> {
    let url = 'addonvalues/' + AddOnValue.AddOnValueId;

    // SupplierId can't be null, since that translates to 0 in ZNode.
    // we will just have to remove it
    var body: any = "";

    if(AddOnValue.SupplierId == null || AddOnValue.SupplierId == 0){
      let val: any = AddOnValue;
      delete val.SupplierId;
      body = val;
    }
    else {
      body = AddOnValue;
    }

    if(!AddOnValue.Name){
      delete AddOnValue.Name;
    }

    return this.put(url, body);
  }

  /**
   * Creates a new Add-On Value
   *
   */
  public createAddonValue(AddOnValue: AddonValue): Observable<AddonValue> {
    if(AddOnValue.SupplierId < 1){
      delete AddOnValue.SupplierId;
    }
    if(AddOnValue.TaxClassId < 1){
      delete AddOnValue.TaxClassId;
    }
    delete AddOnValue.UpdateDate;

    if(!AddOnValue.Name){
      delete AddOnValue.Name;
    }

    return this.post('addonvalues', AddOnValue, 'AddOnValue');
  }

  /**
   * Creates a new Add-On Value
   *
   */
  public copyAddonValue(AddOnValue: AddonValue): Observable<AddonValue> {
    if(AddOnValue.SupplierId < 1){
      delete AddOnValue.SupplierId;
    }
    if(AddOnValue.TaxClassId < 1){
      delete AddOnValue.TaxClassId;
    }
    delete AddOnValue.UpdateDate;

    if(!AddOnValue.Name){
      delete AddOnValue.Name;
    }

    return this.post('copyaddonvalues', AddOnValue, 'AddOnValue');
  }

  /**
   * Deletes an Add-On Value
   */
  public deleteAddonValue(AddOnValueId: number): Observable<boolean> {
    return this.delete('addonvalues/' + AddOnValueId);
  }

  /**
   * Gets a list of Products
   *
   */
  public getProducts(): Observable<Product[]> {
    return this.get('products?sort=value&cache=refresh','Products');
  }

  /**
   * Gets a list of Products using a list of IDs
   *
   */
  public getProductsByIds(ids: number[]): Observable<Product[]> {
    var entityIds: EntityIds = new EntityIds;
    entityIds.Ids = ids.join();

    return this.post('products/listbyproductids?sort=value&cache=refresh',entityIds,'Products');
  }
  private addOptionalName(params: string, name: string|null): string{
    if (name === null || name == "") {
      return params;
    } else {
      return params + '&filter=productId~cn~' + name + ',name~cn~' + name + ',salePrice~cn~' + name + ',retailPrice~cn~' + name +'&junction=or';
    }
  }
  /**
   * Gets a paged list of Products using a list of IDs
   *
   */
  public getProductsByIdsPaged(ids: number[], pageIndex: number, pageSize: number, name: string|null, sort: string, sortAsc: boolean): Observable<ProductsPaged> {
    var entityIds: EntityIds = new EntityIds;
    entityIds.Ids = ids.join();

    sort = sort || 'ProductId';
    sort = sort.charAt(0).toLowerCase() + sort.slice(1);

    if (!sortAsc) {
      sort += '~desc';
    }

    return this.post('products/listbyproductids'+this.addOptionalName(`?page=index~${pageIndex},size~${pageSize}&sort=${sort}&cache=refresh`, name),entityIds);
  }
  /**
   * Gets a paged list of Products
   *
   */
  public getProductsPaged(pageIndex: number, pageSize: number, name: string|null, sort: string, sortAsc: boolean): Observable<ProductsPaged> {
    sort = sort || 'ProductId';
    sort = sort.charAt(0).toLowerCase() + sort.slice(1);

    if (!sortAsc) {
      sort += '~desc';
    }

    return this.get('products'+ this.addOptionalName(`?cache=refresh&sort=${sort}&page=index~${pageIndex},size~${pageSize}`, name));
  }
  /**
   * Gets a specific Product
   *
   */
  public getProduct(ProductId: number): Observable<any> {
    return this.get('products/'+ProductId + '?cache=refresh','Product');
  }

  /**
   * Gets SKUs associated with a specific Product
   *
   */
  public getProductSku(ProductId: number): Observable<Sku[]> {
    return this.get('skus?filter=productid~eq~'+ProductId, 'Skus');
  }

  public getSku(skuId: number): Observable<Sku> {
    return this.get('skus/' + skuId, 'Sku');
  }

  public createSku(sku: Sku): Observable<Sku> {
    let req = this.post('skus', sku, 'Sku');

    return req;
  }

  public updateSku(sku: Sku): Observable<Sku> {
    let url = 'skus/' + sku.SkuId;

    return this.put(url, sku, 'Sku');
  }

  /**
   * Updates a Product
   *
   */
  public updateProduct(product: Product): Observable<Product> {
    let url = 'products/' + product.ProductId;

    product.SeoUrl = product.SeoUrl.replace(/\s+/g, '-')

    return this.put(url, product, 'Product');
  }

  /**
   * Updates a Product's Settings
   */
  public updateProductSettings(product: Product): Observable<Product> {
    let url = 'products/updateproductsettings/' + product.ProductId;

    // SEO URL cannot be NULL, or it throws a generic and hard to trace error
    if(product.SeoUrl == null){
      product.SeoUrl='';
    }

    return this.put(url, product, 'Product');
  }

  /**
   * updates product seo Settings
   *
   */
  public updateProductSEOSettings(product: Product): Observable<Product> {
    let route = 'products/updateproductseoinformation/' + product.ProductId;
    return this.put(route, product, 'Product');
  }

  /**
   * Copies a Product (creates an exact duplicate in ZNode)
   *
   */
  public copyProduct(ProductId: number): Observable<any> {
    return this.get('copyproduct/' + ProductId);
  }

  /**
   * Deletes a Product
   *
   */
  public deleteProduct(ProductId: number): Observable<boolean> {
    return this.delete('products/deletesbyproductid/' + ProductId);
  }

  /**
   * Creates a new Product
   *
   */
  public createProduct(product: Product): Observable<Product> {
    return this.post('products', product, 'Product');
  }

  public isSeoUrlExist(productId: number, seoUrl: string): Observable<any>{
    return this.get('products/isseourlexist/' + productId + '/' + seoUrl);
  }

  /**
   * Gets a list of Product Images
   *
   */
  public getProductImages(productId: number): Observable<ProductImage[]> {
    return this.get('productalternateimage?filter=productid~eq~'+productId,'ProductImages');
  }

  /**
   * Gets a list of Product Assets
   *
   */
  public getProductAssets(productId: number): Observable<ProductAsset[]> {
    return this.get('productasset/'+productId,'ProductAssets');
  }

  /**
   * Creates a new Product Asset
   *
   */
  public createProductAsset(productId: number, file: File): Observable<any> {

    const formData = new FormData();
    formData.append('file', file);
    formData.append('fileSize', String(file.size));
    formData.append('fileExtension', file.name.split('.').pop());
    formData.append('fileName', file.name);
    formData.append('productId', String(productId));

    let url = this.base + 'productasset';

    var xhr = new XMLHttpRequest();

    return Observable.create(observer => {
      observer.next(xhr);
      xhr.upload.onprogress = function(progress) {
        let percentCompleted;
        if (progress.lengthComputable) {
          percentCompleted = Math.round(progress.loaded / progress.total * 100);
          if (percentCompleted < 1) observer.next(0);
          else observer.next(percentCompleted);
        }
      };
      xhr.addEventListener("load", (e) => {
        let status = e.target['status'];
        let response = JSON.parse(e.target['responseText']);
        if (status !== 200 || status !== 201) {
          let productAsset = new ProductAsset();
          productAsset = response.ProductAsset;
          productAsset.HasError = response.HasError;
          productAsset.ErrorMessage = response.ErrorMessage;
          productAsset.IsUploading = false;
          observer.next(productAsset);
          return observer.complete();
        } else {
          return observer.error(response);
        }
      });
      xhr.addEventListener("error", (err) => {
        observer.error('Upload error');
      });
      xhr.addEventListener("abort", (abort) => {
        observer.error('Transfer aborted by the user');
      });
      xhr.open('POST', url, true);
      xhr.setRequestHeader("Accept", "application/json");
      xhr.setRequestHeader("Authorization", this.getAuth());
      xhr.send(formData);
      return () => xhr.abort();
    });
  }

  /**
   * Deletes a Product Asset
   *
   */
  public deleteProductAsset(productAssetId: number): Observable<boolean> {
    return this.delete('productasset/' + productAssetId);
  }

  /**
   * Gets a single Product Image
   *
   */
  public getProductImage(productImageId: number): Observable<ProductImage> {
    return this.get('productalternateimage/'+productImageId,'ProductImage');
  }


  /**
   * Gets a list of available Image Types
   *
   */
  public getProductImageTypes(): Observable<ProductImageType[]> {
    return this.get('productimagetypes','ImageTypes');
  }

  /**
   * Creates a new Product Image
   *
   */
  public createProductImage(image: ProductImage): Observable<ProductImage> {
    return this.post('productalternateimage', image, 'ProductImage');
  }

  /**
   * Updates a Product Image
   *
   */
  public updateProductImage ( productImage: ProductImage ): Observable<ProductImage> {
      return this.put('productalternateimage/' + productImage.ProductImageID, productImage, 'ProductImage');
  }

  /**
   * Deletes a Product Image
   *
   */
  public deleteProductImage(imageId: number): Observable<boolean> {
    return this.delete('productalternateimage/' + imageId);
  }

  /**
   * Gets a list of Tax Classes
   *
   */
  public getTaxClasses(): Observable<TaxClass[]> {
    return this.get('taxclasses', 'TaxClasses');
  }

  public getTaxClass(taxClassId: number): Observable<TaxClass> {
    return this.get('taxclasses/'  + taxClassId, 'TaxClass');
  }

  public checkTaxClasses(taxClasses: TaxClass[]): Observable<TaxClass[]> {
    return this.post('checktaxclasses', taxClasses, 'TaxClasses');
  }

  public createTaxClass(taxClass: TaxClass): Observable<TaxClass[]> {
    return this.post('', taxClass, 'TaxClass');
  }

  /**
   * Get a list of Suppliers
   *
   */
  public getSuppliers(): Observable<Supplier[]> {
    return this.get('suppliers', 'Suppliers');
  }

  /**
   * Get a list of Manufacturers
   *
   */
  public getManufacturers(): Observable<Manufacturer[]> {
    return this.get('manufacturers', 'Manufacturers');
  }

  /**
   * Get Shipping carrier service types
   *
   */
  public getShippingCarrierServiceTypes(): Observable<ShippingCarrierServiceType[]> {
    return this.get('shippingcarrierservicetypes?cache=refresh', 'ShippingCarrierServiceTypes');
  }

  /**
   * Get Shipping carrier service types by carrier id
   *
   */
  public getShippingCarrierServiceTypesByCarrierId(CarrierId: number): Observable<ShippingCarrierServiceType[]> {
    return this.get('shippingcarrierservicetypesbycarrier/' + CarrierId, 'ShippingCarrierServiceTypes');
  }

  /**
   * Get Shipping carrier service type
   *
   */
  public getShippingCarrierServiceType(ServiceTypeId: number): Observable<ShippingCarrierServiceType> {
    return this.get('shippingcarrierservicetypes/' + ServiceTypeId, 'ShippingCarrierServiceType');
  }

 /**
   * Creates a new Shipping carrier service type
   *
   */
  public createShippingCarrierServiceType(ShippingCarrierServiceType: ShippingCarrierServiceType): Observable<ShippingCarrierServiceType> {
    return this.post('shippingcarrierservicetypes', ShippingCarrierServiceType, 'ShippingCarrierServiceType');
  }

  /**
   * Updates a Shipping carrier service type
   *
   */
  public updateShippingCarrierServiceType(ShippingCarrierServiceType: ShippingCarrierServiceType): Observable<ShippingCarrierServiceType> {
    return this.put('shippingcarrierservicetypes', ShippingCarrierServiceType, 'ShippingCarrierServiceType');
  }

  /**
   * Deletes a Shipping carrier service type
   *
   */
  public deleteShippingCarrierServiceType(ServiceTypeId: number): Observable<boolean> {
    return this.delete('shippingcarrierservicetypes/' + ServiceTypeId);
  }

/**
   * Get Shipping carriers
   *
   */
  public getShippingCarriers(): Observable<ShippingCarrier[]> {
    return this.get('shippingcarriers?cache=refresh', 'ShippingCarriers');
  }

  /**
   * Get Shipping carrier
   *
   */
  public getShippingCarrier(CarrierId: number): Observable<ShippingCarrier> {
    return this.get('shippingcarriers/' + CarrierId, 'ShippingCarrier');
  }

  /**
   * Get Shipping Insurance
   */
  public getShippingInsurance(): Observable<ShippingInsuranceType[]> {
    return this.get('shippinginsurances?cache=refresh', 'ShippingInsurances');
  }

 /**
   * Creates a new Shipping carrier
   *
   */
  public createShippingCarrier(ShippingCarrier: ShippingCarrier): Observable<ShippingCarrier> {
    return this.post('shippingcarriers', ShippingCarrier, 'ShippingCarrier');
  }

  /**
   * Updates a Shipping carrier
   *
   */
  public updateShippingCarrier(ShippingCarrier: ShippingCarrier): Observable<ShippingCarrier> {
    return this.put('shippingcarriers', ShippingCarrier, 'ShippingCarrier');
  }

  /**
   * Deletes a Shipping carrier
   *
   */
  public deleteShippingCarrier(CarrierId: number): Observable<boolean> {
    return this.delete('shippingcarriers/' + CarrierId);
  }

  /**
   * Create a Shipping Carrier Rate Rule
   */
  public createShippingCarrierRateRule(ShippingCarrierRateRule: ShippingCarrierRateRule): Observable<ShippingCarrierRateRule> {
    return this.post('shippingcarrierraterule', ShippingCarrierRateRule, 'ShippingCarrierRateRule');
  }

  /**
   * Update the specific Shipping Carrier Rate RUle
   * @param ShippingCarrierRateRule - {ID} is required
   */
  public updateShippingCarrierRateRule(ShippingCarrierRateRule: ShippingCarrierRateRule): Observable<ShippingCarrierRateRule> {
    return this.put('shippingcarrierraterule', ShippingCarrierRateRule, 'ShippingCarrierRateRule');
  }

  /**
   * Get the Shipping Carrier Rate rules
   */
  public getShippingCarrierRateRule(): Observable<ShippingCarrierRateRule[]> {
    return this.get('shippingcarrierraterule?cache=refresh', 'ShippingCarrierRateRules');
  }

  /**
   * Get Shipping Carrier Rate Rule by ShippingID
   */
  public getShippingCarrierRateRuleByShippingID(ShippingID: number): Observable<ShippingCarrierRateRule> {
    return this.get('shippingcarrierraterule/' + ShippingID, 'ShippingCarrierRateRule');
  }

  /**
   * Delete specific Shipping Carrier Rate Rules
   */
  public deleteShippingCarrierRateRuleByShippingID(ShippingID: number): Observable<boolean> {
    return this.delete('deleteshippingcarrierraterulebyshippingid/' +  ShippingID);
  }

  /**
   * Get Flute directions
   *
   */
  public getFluteDirections(): Observable<FluteDirection[]> {
    return this.get('flutedirections', 'FluteDirections');
  }

  /**
   * Get Shipping boxes
   *
   */
  public getShippingBoxes(): Observable<ShippingBox[]> {
    return this.get('shippingboxes?cache=refresh', 'ShippingBoxes');
  }

  /**
   * Get Shipping box
   *
   */
  public getShippingBox(shippingBoxId: number): Observable<ShippingBox> {
    return this.get('shippingboxes/'+shippingBoxId, 'ShippingBox');
  }

  /**
   * Get Shipping boxes by profile ids
   *
   */
  public getShippingBoxesByProfileIds(ids: number[]): Observable<ShippingBox[]> {
    return this.post('shippingboxes/listbyprofileids?cache=refresh', { Ids: ids.join(',') }, 'ShippingBoxes');
  }

  /**
   * Creates a new Shipping box
   *
   */
  public createShippingBox(shippingBox: ShippingBox): Observable<ShippingBox> {
    return this.post('shippingboxes', shippingBox, 'ShippingBox');
  }

  /**
   * Updates a Shipping box
   *
   */
  public updateShippingBox(shippingBox: ShippingBox): Observable<ShippingBox> {
    return this.put('shippingboxes/'+ shippingBox.ShippingBoxId, shippingBox, 'ShippingBox');
  }

  /**
   * Deletes a Shipping box
   *
   */
  public deleteShippingBox(shippingBoxId: number): Observable<boolean> {
    return this.delete('shippingboxes/' + shippingBoxId + '?deleteRules=true');
  }

  /**
   * Copies a Shipping box
   *
   */
  public copyShippingBox(shippingBoxId: number): Observable<any> {
    let url = 'copyshippingboxes/' + shippingBoxId;

    return this.get(url);
  }



  /**
   * Get Shipping options
   *
   */
  public getShippingMethods(): Observable<ShippingMethod[]> {
    return this.get('shippingoptions?cache=refresh', 'ShippingOptions');
  }

  /**
   * Get Shipping options
   *
   */
  public getShippingMethod(ShippingId: number): Observable<ShippingMethod> {
    return this.get('shippingoptions/'+ShippingId, 'ShippingOption');
  }

  /**
   * Get Shipping options by profile ids
   *
   */
  public getShippingMethodsByProfileIds(ids: number[]): Observable<ShippingMethod[]> {
    var entityIds: EntityIds = new EntityIds;
    entityIds.Ids = ids.join();

    return this.post('shippingoptions/listbyprofileids?cache=refresh',entityIds,'ShippingOptions');
  }

  /**
   * Creates a new Shipping option
   *
   */
  public createShippingMethod(ShippingMethod: ShippingMethod): Observable<ShippingMethod> {
    return this.post('shippingoptions', ShippingMethod, 'ShippingOption');
  }

  /**
   * Updates a Shipping option
   *
   */
  public updateShippingMethod(ShippingMethod: ShippingMethod): Observable<ShippingMethod> {
    if(ShippingMethod.CountryCode == 0){
      ShippingMethod.CountryCode = null;
    }

    return this.put('shippingoptions/'+ ShippingMethod.ShippingId, ShippingMethod, 'ShippingOption');
  }

  /**
   * Deletes a Shipping option
   *
   */
  public deleteShippingMethod(shippingId: number): Observable<boolean> {
    return this.delete('shippingoptions/' + shippingId + '?deleteRules=true');
  }

  public copyShippingMethod(ShippingId: number): Observable<any> {
    let url = 'copyshippingoption/' + ShippingId;

    return this.get(url);
  }

  /**
   * Get Shipping rule types
   *
   */
  public getShippingRuleTypes(): Observable<ShippingRuleType[]> {
    return this.get('shippingruletypes', 'ShippingRuleTypes');
  }

  /**
   * Get Shipping Rules
   *
   */
  public getShippingRules(shippingOptionId: number): Observable<ShippingRule[]> {
    return this.get('shippingrules?cache=refresh&filter=shippingOptionId~eq~'+shippingOptionId, 'ShippingRules');
  }

  /**
   * Create a new Shipping Rule
   *
   */
  public createShippingRule(shippingRule: ShippingRule): Observable<ShippingRule> {
    return this.post('shippingrules', shippingRule, 'ShippingRule');
  }

  /**
   * Updates a Shipping Rule
   *
   */
  public updateShippingRule(shippingRule: ShippingRule): Observable<ShippingRule> {
    return this.put('shippingrules/'+shippingRule.ShippingRuleId, shippingRule, 'ShippingRule');
  }

  //#region ApprovalGroup

  /**
   * Gets an Approval Group by ID
   *
   * @param approvalGroupId The Approval Group ID
   */
  public getApprovalGroupById(approvalGroupId: number): Observable<ApprovalGroup> {
    return this.get('orderapproval/group/' + approvalGroupId, 'ApprovalGroup');
  }

  /**
   * Gets the Approval Groups based on the Portal ID
   *
   * @param portalId Portal ID
   */
  public getApprovalGroupsByPortalId(portalId: number): Observable<OrderApproval[]> {
    return this.get('orderapproval/' + portalId, 'OrderApprovals');
  }

  /**
   * Creates a new Order Approval
   *
   * @param approvalGroup The Approval Group to be created
   */
  public createOrderApproval(approvalGroup: OrderApproval): Observable<OrderApproval> {
    return this.post('orderapproval', approvalGroup, 'OrderApproval');
  }

  /**
   * Updates an Order Approval
   *
   * @param approvalGroup The Approval Group to be updated
   */
  public updateOrderApproval(approvalGroup: OrderApproval): Observable<OrderApproval> {
    return this.put('orderapproval', approvalGroup, 'OrderApproval');
  }

  /**
   * Updates the desired Approval Group
   *
   * @param approvalGroup The Approval Group to be updated.
   */
  public updateApprovalGroup(approvalGroup: ApprovalGroup): Observable<ApprovalGroup> {
    return this.put('orderapproval/group', approvalGroup, 'ApprovalGroup');
  }

  /**
   * Deletes the specified Approval Group
   *
   * @param approvalGroup The Approval Group to be deleted.
   */
  public deleteApprovalGroup(groupID: number): Observable<boolean> {
    return this.delete('orderapproval/group/' + groupID);
  }

  //#endregion ApprovalGroup

  //#region ApprovalRule

  /**
   * Gets an Approval Rule by ID
   *
   * @param approvalRuleId The Approval Group ID
   */
  public getApprovalRuleById(approvalRuleId: number): Observable<ApprovalRule> {
    return this.get('orderapproval/rule/' + approvalRuleId, 'ApprovalRule');
  }

  /**
   * Gets the Approval Rules based on the Portal ID
   *
   * @param portalId Portal ID
   */
  public getApprovalRulesByPortalId(portalId: number): Observable<ApprovalRule[]> {
    return this.get('orderapproval/rules/' + portalId, 'ApprovalRules');
  }

  public getApprovalRulesByGroupId(groupId: number): Observable<ApprovalRule[]> {
    return this.get('orderapproval/rulesbygroup/' + groupId, 'ApprovalRules');
  }

  /**
   * Creates a new Approval Rule
   *
   * @param approvalRule The Approval Rule to be created
   */
  public createApprovalRule(approvalRule: ApprovalRule): Observable<ApprovalRule> {
    return this.post('orderapproval/rule', approvalRule, 'ApprovalRule');
  }

  /**
   * Updates the desired Approval Rule
   *
   * @param approvalRule The Approval Rule to be updated
   */
  public updateApprovalRule(approvalRule: ApprovalRule): Observable<ApprovalRule> {
    return this.put('orderapproval/rule', approvalRule, 'ApprovalRule');
  }

  /**
   * Deletes the specified Approval Rule
   *
   * @param approvalRule The Approval Rule to be deleted
   */
  public deleteApprovalRule(approvalRule: ApprovalRule): Observable<boolean> {
    return this.delete('orderapproval/rule/' + approvalRule.ID);
  }

  //#endregion ApprovalRule

  //#region ApprovalRuleType

  /**
   * Gets the list of Approval Rule Types
   */
  public getApprovalRuleType(): Observable<ApprovalRuleType[]> {
    return this.get('orderapproval/ruletypes/', 'ApprovalRuleTypes');
  }

  //#endregion ApprovalRuleType

  //#region ApprovalRuleLimitType

  /**
   * Gets the list of Approval Rule Limit Type
   */
  public getApprovalRuleLimitType(): Observable<ApprovalRuleLimitType[]> {
    return this.get('orderapproval/rulelimittypes/', 'ApprovalRuleLimitTypes');
  }

  //#endregion ApprovalRuleLimitType

  //#region ApprovalApprover

  /**
   * Gets the Approval Approver by ID
   *
   * @param approverId The Approval Approver Id
   */
  public getApprovalApproverById(approverId: number): Observable<ApprovalApprover> {
    return this.get('orderapproval/approver/' + approverId, 'ApprovalApprover');
  }

  /**
   * Gets the Approval Approver by Account ID
   *
   * @param accountId Account ID of the Approval Approver
   */
  public getApprovalApproverbyAccountId(accountId: number): Observable<ApprovalApprover> {
    return this.get('orderapproval/approverbyaccount/' + accountId, 'ApprovalApproverByAccount');
  }

  /**
   * Gets the list of Approval Approvers by Approval Group ID
   *
   * @param groupId Approval Group ID of the Approver
   */
  public getApprovalApproversByApprovalGroupId(groupId: number): Observable<ApprovalApprover[]> {
    return this.get('orderapproval/approvers/' + groupId, 'ApprovalApprovers');
  }

  /**
   * Creates the Approval Approver
   *
   *  @param approvalApprover The Approval Approver to be created
   */
  public createApprovalApprover(approvalApprover: ApprovalApprover): Observable<ApprovalApprover> {
    return this.post('orderapproval/approver/', approvalApprover, 'ApprovalApprover');
  }

  /**
   * Updates the desired Approval Approver
   *
   * @param approvalApprover The Approval Approver to be updated
   */
  public updateApprovalApprover(approvalApprover: ApprovalApprover): Observable<ApprovalApprover> {
    return this.put('orderapproval/approver/', approvalApprover, 'ApprovalApprover');
  }

  /**
   * Deletes the specified Approval Approver
   *
   * @param approvalApprover The Approval Approver to be deleted
   */
  public deleteApprovalApprover(approverID: number): Observable<boolean> {
    return this.delete('orderapproval/approver/' + approverID);
  }

  //#endregin ApprovalApprover

  //#region ApprovalCustomer

  /**
   * Gets the Approval Customer by ID
   *
   * @param customerId The Approval Customer ID
   */
  public getApprovalCustomerById(customerId: number): Observable<ApprovalCustomer> {
    return this.get('orderapproval/customer/' + customerId, 'ApprovalCustomer');
  }

  /**
   * @param accountId The Account ID of the Approval Customer
   */
  public getApprovalCustomerbyAccountId(accountId: number): Observable<ApprovalCustomer> {
    return this.get('orderapproval/customerbyaccount/' + accountId, 'ApprovalCustomerByAccount');
  }

  /**
   * Gets the list of Approval Customers by Approval Group ID
   *
   * @param groupId The Approval Group ID of the Approval Customer
   */
  public getApprovalCustomersByApprovalGroupId(groupId: number): Observable<ApprovalCustomer[]> {
    return this.get('orderapproval/customers/' + groupId, 'ApprovalCustomers');
  }

  /**
   * Gets the list of Exempted Approval Customers
   *
   */
  public getApprovalCustomerExempt(portalID: number): Observable<OrderApprovalCustomer[]> {
    return this.get('orderapprovalcustomersexempt/' + portalID, 'ApprovalCustomers');
  }

  /**
   * Creates the Approval Customer
   *
   * @param approvalCustomer The Approval Customer to be created
   */
  public createApprovalCustomer(approvalCustomer: ApprovalCustomer): Observable<ApprovalCustomer> {
    return this.post('orderapproval/customer/', approvalCustomer, 'ApprovalCustomer');
  }

  /**
   * Updates the desired Approval Customer
   *
   * @param approvalCustomer The Approval Customer to be updated
   */
  public updateApprovalCustomer(approvalCustomer: ApprovalCustomer): Observable<ApprovalCustomer> {
    return this.put('orderapproval/customer/', approvalCustomer, 'ApprovalCustomer');
  }

  /**
   * Deletes the specified Approval Customer
   *
   * @param approvalCustomer The Approval Customer to be deleted
   */
  public deleteApprovalCustomer(customerID: number): Observable<boolean> {
    return this.delete('orderapproval/customer/' + customerID);
  }

  //#endregion ApprovalCustomer

  public orderApprovalSearchAccounts(parameters: AccountSearchParameters){
    /*
    Purposely not providing an argument of the this.post() "property?" parameter.
    Providing a value prevents ngx-bootstrap typeahead in OrdersApprovalApproverFieldComponent
    from properly consuming the returned array response and will not show on the field dropdown.
    */
    return this.post('accounts/orderapproval/searchaccounts/', parameters);
  }

  /**
   * Gets a list of Storefronts (Portals in ZNode)
   *
   */
  public getPortals(compileCss?: Boolean): Observable<Portal[]> {
    compileCss = compileCss || false;
    let route = 'portals?cache=refresh&compileCss=' + compileCss;
    return this.get(route,'Portals');
  }

  /**
   * Gets a specific Storefront (Portal in ZNode)
   *
   */
  public getPortal(PortalId: number): Observable<Portal> {
    return this.get('portals/'+PortalId,'Portal');
  }

  /**
   * Updates a Storefront (Portal in ZNode)
   *
   */
  public updatePortal(portal: Portal): Observable<any> {
    let url = 'portals/' + portal.PortalId;

    return this.put(url, portal, 'Portal');
  }

  /**
   * Copies a Storefront (creates an exact duplicate Portal object in ZNode)
   *
   */
  public copyPortal(PortalId: number): Observable<any> {
    return this.get('copystore/' + PortalId);
  }

  /**
   * Deletes a Storefront (Portal in ZNode)
   *
   */
  public deletePortal(portal: Portal): Observable<boolean> {
    return this.delete('deleteportalbyportalid/' + portal.PortalId);
  }

  /**
   * Creates a new Storefront (Portal in ZNode)
   *
   */
  public createPortal(portal: Portal): Observable<Portal>{
    return this.post('portals', portal, 'Portal');
  }

  /**
   * Gets a list of Portal Catalog (association between a storefront and a catalog)
   *
   */
  public getPortalCatalogs ( portalId: number ): Observable<PortalCatalog[]> {
    return this.get('portalcatalogsbyportalid/'+portalId, 'PortalCatalogs');
  }

  /**
   * Gets a list of Portal Catalog (association between a storefront and a catalog)
   *
   */
  public getPortalCatalog ( portalCatalogId: number ): Observable<PortalCatalog> {
    return this.get('portalcatalogs/'+portalCatalogId, 'PortalCatalog');
  }

  /**
   * Gets a list of Portal Catalog (association between a storefront and a catalog)
   *
   */
  public getPortalCatalogByCatalogID ( catalogID: number ): Observable<PortalCatalog[]> {
    return this.get('portalcatalogs?filter=catalogid~eq~'+catalogID, 'PortalCatalogs');
  }

  /**
   * Creates a new Portal Catalog (association between a storefront and a catalog)
   *
   * @TODO: identify current uses of this method
   */
  public createPortalCatalog ( portalCatalog: PortalCatalog ): Observable<PortalCatalog> {
    return this.post('portalcatalogs', portalCatalog, 'PortalCatalog');
  }

  /**
   * Updates a Portal Catalog (association between a storefront and a catalog)
   *
   * @TODO: identify current uses of this method
   */
  public updatePortalCatalog ( portalCatalog: PortalCatalog ): Observable<PortalCatalog> {
    if(portalCatalog.PortalCatalogId > 0){
      return this.put('portalcatalogs/' + portalCatalog.PortalCatalogId, portalCatalog, 'PortalCatalog');
    }
    else {
      return this.createPortalCatalog(portalCatalog);
    }
  }

  /**
   * Gets a list of Profiles associated with a Storefront
   *
   */
  public getPortalProfiles(PortalId: number = 0): Observable<PortalProfile[]> {
    let filter = PortalId ? '&filter=portalid~eq~'+PortalId : '';
    return this.get('portalprofiles?cache=refresh'+filter, 'PortalProfiles');
  }

  public getPortalCountry(PortalCountryId: number): Observable<PortalCountry>{
    return this.get('portalcountries/' + PortalCountryId, 'PortalCountry');
  }

  public getPortalCountriesByPortalId(PortalId: number): Observable<PortalCountry[]> {
    return this.get('portalcountriesbyportalid/' + PortalId, 'PortalCountries');
  }

  public updatePortalCountry(portalCountry: PortalCountry): Observable<PortalCountry>{
    return this.put('portalcountries/' + portalCountry.PortalCountryId, portalCountry, 'PortalCountry')
  }

  /**
   * Gets a list of PortalProfiles associated with a Profile
   *
   */
  public getPortalProfilesByProfile(profileId: number): Observable<PortalProfile[]> {
    return this.get('portalprofiles?cache=refresh&filter=profileid~eq~'+profileId, 'PortalProfiles');
  }

  /**
   * Gets a single Portal Profile association
   *
   */
  public getPortalProfile(PortalProfileId: number): Observable<PortalProfile> {
    return this.get('portalprofiles/'+PortalProfileId+'?cache=refresh', 'PortalProfile');
  }

  /**
   * Removes a Profile from a Storefront
   *
   */
  public deletePortalProfile(PortalProfileId: number): Observable<boolean> {
    return this.delete('portalprofile/' + PortalProfileId);
  }
    /**
   * Creates a new Portal Profile (association between a storefront and a profile)
   *
   */
  public createPortalProfile ( portalProfile: PortalProfile ): Observable<PortalProfile> {
    return this.post('portalprofile', portalProfile, 'PortalProfile');
  }

  /**
   * Updates a Portal Profile (association between a storefront and a profile)
   *
   */
  public updatePortalProfile ( portalProfile: PortalProfile ): Observable<PortalProfile> {
    if(portalProfile.PortalProfileID > 0){
      return this.put('updateportalprofile/' + portalProfile.PortalProfileID, portalProfile, 'PortalProfile');
    }
    else {
      return this.createPortalProfile(portalProfile);
    }
  }

  /**
   * Gets a list of Profiles
   *
   */
  public getProfiles(): Observable<any> {
    return this.get('profiles?cache=refresh', 'Profiles');
  }

  /**
   * Gets a specific Profile
   *
   */
  public getProfile(ProfileId: number): Observable<any> {
    return this.get('profiles/'+ProfileId, 'Profile');
  }

  /**
   * Deletes a Profile
   *
   */
  public deleteProfile(ProfileId: number): Observable<boolean> {
    return this.delete('profiles/' + ProfileId);
  }

  /**
   * Creates a new Profile
   *
   */
  public createProfile ( profile: Profile ): Observable<Profile> {
    return this.post('profiles', profile, 'Profile');
  }

  /**
   * Updates a Profile
   *
   */
  public updateProfile ( profile: Profile ): Observable<Profile> {
      return this.put('profiles/' + profile.ProfileId, profile, 'Profile');
  }

  /**
   * Gets a paged list of Orders placed through ZNode
   *
   * Does not include orders placed or managed through CBMS
   */
  public getOrdersPaged(pageIndex: number, pageSize: number, searchText: string|null, sort: string, sortAsc: boolean): Observable<OrdersPaged> {
    sort = sort || 'OrderId';
    sort = sort.charAt(0).toLowerCase() + sort.slice(1);

    if (!sortAsc) {
      sort += '~desc';
    }

    return this.get('orders?expand=OrderLineItems,Account,parentcompanyname' + this.addOrderFilter(`&cache=refresh&sort=${sort}&page=index~${pageIndex},size~${pageSize}`, searchText, null));
  }

  private addOrderFilter(params: string, searchText: string|null, filterPortalIds: string|null): string {
    let queryString: string = params;
    let filterString: string = "";

    if (filterPortalIds !== null) filterString = '&filter=' + filterPortalIds;

    if (searchText !== null || searchText != "") {
      let searchString = `companyname~cn~${searchText},firstname~cn~${searchText},lastname~cn~${searchText},orderId~eq~${searchText},total~eq~${searchText},paymenttypename~cn~${searchText}&junction=or`;
      if (filterString == "") { filterString = '&filter=' + searchString; }
      else  filterString += ',' + searchString;
    }

    return queryString + filterString;
  }

  /**
   * Gets a paged list of Orders by Portal ID
   *
   */
  public getOrdersByPortalIds(pageIndex: number, pageSize: number, searchText: string|null, sort: string, sortAsc: boolean, portalIds: number[]): Observable<OrdersPaged> {
    sort = sort || 'OrderId';
    sort = sort.charAt(0).toLowerCase() + sort.slice(1);

    if (!sortAsc) {
      sort += '~desc';
    }

    var portalIdsParam: string[] = [];
    portalIds.forEach(id => { portalIdsParam.push(`portalid~eq~${id}`); });

    var endpoint = 'orders?expand=OrderLineItems,Account,parentcompanyname';
    return this.get(endpoint + this.addOrderFilter(`&cache=refresh&sort=${sort}&page=index~${pageIndex},size~${pageSize}`, searchText, portalIdsParam.join(",")));
  }

  /**
   * Gets a paged list of Orders by Portal ID
   *
   */
  public getOrdersFromSearch(pageIndex: number, pageSize: number, searchText: string|null, sort: string, sortAsc: boolean, portalIds: number[] = []): Observable<OrdersPaged> {
    sort = sort || 'OrderId';
    sort = sort.charAt(0).toLowerCase() + sort.slice(1);

    if (!sortAsc) {
      sort += '~desc';
    }

    var portalIdsParam: string[] = [];
    portalIds.forEach(id => { portalIdsParam.push(`portalid~eq~${id}`); });

    var endpoint = 'ordersearch?expand=OrderLineItems,Account,parentcompanyname';
    return this.get(endpoint + this.addOrderFilter(`&cache=refresh&sort=${sort}&page=index~${pageIndex},size~${pageSize}`, searchText, portalIdsParam.join(",")));
  }

  /**
   * Gets a list of Orders placed through ZNode
   *
   * Does not include orders placed or managed through CBMS
   */
  public getOrders(): Observable<Order[]> {
    return this.get('orders?expand=OrderLineItems,Account,parentcompanyname', 'Orders');
  }

  /**
   * Gets a specific ZNode Order
   *
   */
  public getOrder(OrderId: number): Observable<Order> {
    return this.get('orders/'+OrderId, 'Order');
  }

  /**
   * Get details about a ZNode Order
   */
  public getOrderData(OrderId: number): Observable<any> {
    return this.get('downloadorder?filter=OrderId~eq~'+OrderId, 'OrderDownloadData');
  }

  /**
   * Get line items for a ZNode order
   *
   */
  public getOrderLineItemData(OrderId: number): Observable<any> {
    return this.get('downloadorderlineitemdata?filter=OrderId~eq~'+OrderId, 'OrderDownloadData');
  }

  public getOrderDownloadLink(OrderId: number): Observable<any> {
    return this.get('orders/downloadcustomartwork/' + OrderId, 'OrderLineItemList');
  }

  /**
   * Get HTML Receipt for a ZNode order
   *
   */
  public getOrderReceiptHtml(OrderId: number): Observable<Order> {
    return this.get('orders/'+OrderId+'/HtmlReceipt', 'Order');
  }

  /**
   * Get human readable values for ZNode Order States
   *
   */
  public getOrderStates(): Observable<any[]> {
    return this.get('orderstates', 'OrderStates');
  }

  /**
   * Get human readable values for ZNode Order Payment Types
   *
   */
  public getPaymentTypes(): Observable<any[]> {
    return this.get('paymenttypes', 'PaymentTypes');
  }

  /**
   * Get the Payment Type details by PaymentTypeId
   * @param PaymentTypeId
   */
  public getPaymentType(PaymentTypeId: number): Observable<any> {
    return this.get('paymenttypes/' + PaymentTypeId, 'PaymentType');
  }

  /**
   * Get the list of Payment Setting for the specific Profile
   *
   */
  public getProfilePaymentSettings(ProfileId: number): Observable<any[]>{
    return this.get('paymentsettings/' + ProfileId, 'PaymentSettings');
  }

  /**
   * Get the specific Payment Setting
   */
  public getPaymentSetting(PaymentSettingId: number): Observable<any> {
    return this.get('paymentsetting/' + PaymentSettingId, 'PaymentSetting');
  }

  /**
   * Updates Payment Setting data
   */
  public updatePaymentSetting(PaymentSetting: PaymentSetting): Observable<any> {
    return this.put('paymentsetting/' + PaymentSetting.PaymentSettingID, PaymentSetting);
  }

  /**
   * Get a list of Product Bundles
   *
   */
  public getProductBundles(ParentProductId: number): Observable<Bundle[]>{
    let path = 'productbundles?filter=ParentProductId~eq~' + ParentProductId;

    return this.get(path, 'ProductBundles');
  }

  public createProductBundle(parentProductId: number, childProductIds: number[]): Observable<boolean> {
    let body = {
      ProductId: parentProductId,
      ProductIds: childProductIds.join(',')
    };

    let path = 'productbundles/associatebundleproduct';
    return this.post(path, body, 'HasError');
  }

  /**
   * Deletes a single product bundle child product
   *
   */
  public deleteProductBundle(parentId: number, childId: number): Observable<boolean> {
   return this.delete(`productbundles/${parentId}/${childId}`);
  }
  /**
   * Get a list of Product Facets
   *
   */
  public getFacets(): Observable<Facet[]>{
    let path = 'productfacets';
    return this.get(path, 'FacetGroups');
  }

  /**
   * Get a list of Product Facets for a specific Product
   *
   */
  public getProductFacets(ProductId: number): Observable<Facet[]>{
    let path = 'productfacets?filter=productid~eq~' + ProductId;
    return this.get(path, 'FacetGroups');
  }

  /**
   *
   */
  public getProductTags(id: number): Observable<ProductTag>{
    let path = 'producttags/' + id;
    return this.get(path, 'ProductTag');
  }

  /**
   * Creates a new Product TagId
   *
   */
  public createProductTag(productTag: ProductTag): Observable<any> {
    return this.post('producttags', productTag, 'ProductTag');
  }

  /**
   * Updates a Product Tag
   */
  public updateProductTag(productTag: ProductTag): Observable<any> {
    let path = 'producttags/'+productTag.TagId;
    return this.put(path, productTag, 'ProductTag');
  }

  /**
   * Updates a Product Tag, or create a new one if it does not exist
   *
   */
  public saveProductTag(tag: ProductTag, ProductId: number): Observable<any> {
    if(tag.TagId > 0){
      return this.updateProductTag(tag);
    }
    else{
      tag.ProductId = ProductId;
      return this.createProductTag(tag);
    }
  }

  /**
   * Get a paged list of Product Add-Ons
   *
   */
  public getProductAddonsPaged(productId: number, pageIndex: number, pageSize: number): Observable<AddOnsPaged> {
    return this.get('productaddons/' + productId + `?page=index~${pageIndex},size~${pageSize}`);
  }

  /**
   * Get a list of Product Add-Ons
   *
   */
  public getProductAddons(productId: number): Observable<Addon[]> {
    let path = 'productaddons/' + productId;
    return this.get(path, 'AddOns');
  }

  /**
   * Get a list of Product Tiers
   *
   */
  public getProductTiers(productId: number): Observable<ProductTier[]> {
    let path = 'producttiers/' + productId;
    return this.get(path, 'Tiers');
  }

  /**
   * Creates a new Product Tier
   *
   */
  public createProductTier(productTier: ProductTier): Observable<ProductTier> {
    return this.post('createproducttier', productTier, 'ProductTier');
  }

  /**
   * Updates a Product Tier
   *
   */
  public updateProductTier(productTier: ProductTier): Observable<boolean> {
    return this.put('updateproducttier', productTier);
  }

  /**
   * Deletes a Product Tier
   *
   */
  public deleteProductTier(productTierId: number): Observable<boolean> {
    return this.delete('deleteproducttier/' + productTierId);
  }

  /**
   * Get a list of Product Highlights
   *
   */
  public getProductHighlights(id: number): Observable<ProductHighlight[]> {
    let path = 'producthighlights/' + id;
    return this.get(path, 'Highlights');
  }

  /**
   * Get a list of Product Digital Assets
   *
   */
  public getProductDigitalAssets(id: number): Observable<DigitalAsset[]> {
    let path = 'digitalassets/' + id;
    return this.get(path, 'DigitalAssets');
  }

  /**
   * Get a list of Email Templates
   *
   */
  public getEmailTemplates(): Observable<EmailTemplate[]> {
    return this.get('emailtemplates', 'EmailTemplates');
  }

  /**
   * Get a specific Email Template
   *
   */
  public getEmailTemplate(name: string): Observable<EmailTemplate> {
    // name ~= "templateName.extension"
    var path = name.split('-');
    // required path = /emailtemplates/templateName/extension
    return this.get('emailtemplates/'+path[0]+'/'+path[1], 'EmailTemplate');
  }

  /**
   * Creates a new Email Template
   *
   */
  public createEmailTemplate(template: EmailTemplate): Observable<EmailTemplate> {
    return this.post('EmailTemplates', template, 'EmailTemplate');
  }

  /**
   * Deletes an Email Template
   *
   */
  public deleteEmailTemplate(template: EmailTemplate): Observable<boolean> {

    var path = template.TemplateName.split('.');
    console.log(path);
    return this.delete('emailtemplate/' + path[0]+'/'+path[1]);
  }

  /**
   * Updates an Email Template
   *
   */
  public updateEmailTemplate(template: EmailTemplate): Observable<any> {
    delete template.Key;
    delete template.CustomErrorMessage;
    delete template.DeleteTemplate;

    return this.put('updateemailtemplates', template);
  }

  /**
   * Gets a list of Content Pages
   *
   */
  public getContentPages(portalId: number): Observable<ContentPage[]> {
    return this.get('contentpages?filter=portalid~eq~'+portalId, 'ContentPages');
  }

  /**
   * Gets a single Content Page
   *
   */
  public getContentPage(pageId: number): Observable<ContentPage> {
    return this.get('contentpages/'+pageId, 'ContentPage');
  }

  /**
   * Updates a single Content Page
   *
   */
  public updateContentPage(page: ContentPage): Observable<any> {
    let route = 'contentpages/' + page.ContentPageID;
    if(page.UpdatedUser == null){
      page.UpdatedUser = "";
    }
    return this.put(route, page);
  }

  /**
   * Creates a new Content Page
   *
   */
  public createContentPage(page: ContentPage): Observable<ContentPage> {
    return this.post('contentpages', page, 'ContentPage');
  }

  /**
   * Delete a Content Page
   *
   */
  public deleteContentPage(id: number): Observable<boolean> {
    return this.delete('/contentpages/' + id);
  }

  /**
   * Gets a single Content Section
   *
   */
  public getContentSection(portalId: number, sectionName: string): Observable<ContentSection> {
    return this.get('contentsections/' + portalId + '/' + sectionName);
  }

  /**
   * Gets a list of Content Section
   *
   */
  public getContentSections(portalId: number): Observable<ContentSection[]> {
    return this.get('contentsections/' + portalId, 'ContentSections');
  }


  public updateContentSection(contentSection: ContentSection): Observable<ContentSection> {
    return this.put('contentsections', contentSection);
  }

  /**
   * Gets the specific config setting for specific portal
   */
  public getConfigSettingByBID(BID: number): Observable<ConfigSetting> {
    return this.get('configsetting/' + BID, 'ConfigSetting');
  }

  /**
   * Save the config setting for specific portal
   */
  public requestEndorBID(bid: number): Observable<EndorBIDResponse> {
    return this.get('endor/clonebusiness/' + bid);
  }

    /**
   * Save the config setting for specific portal
   */
  public saveConfigSetting(configSetting: ConfigSetting): Observable<ConfigSetting> {
    return this.post('configsetting/saveconfigsetting/', configSetting, 'ConfigSetting');
  }

  /**
   * Gets a list of Available Themes
   *
   */
  public getThemes(): Observable<Theme[]> {
    return this.get('themes', 'Themes');
  }

  /**
   * Gets a list of Available Stylesheets
   *
   * DEPRECATED: This is no longer supported in Znode
   *
   */
  public getCssList(): Observable<CSS[]> {
    return this.get('csslist','CSSs');
  }

  /**
   * Get a single Stylesheet
   *
   */
  public getCss(portalId: number): Observable<CSS> {
    return this.get('getcss/'+portalId, 'CSS');
  }

  /**
   * Creates a new Stylesheet
   *
   */
  public createCss(css: CSS): Observable<CSS> {
    return this.post('createcss', css, 'CSS');
  }

  /**
   * Update a Stylesheet
   *
   */
  public updateCss(portalId:number, css: CSS): Observable<CSS> {
    return this.put('UpdateCss/'+portalId, css, 'CSS');
  }

  /**
   * Delete a Stylesheet
   *
   */
  public deleteCss(id: number): Observable<boolean> {
    return this.delete('/deletecss/' + id);
  }

  /**
   * Gets a list of 301 URL Redirects
   *
   */
  public getRedirects(): Observable<Redirect[]> {
    return this.get('urlredirects','UrlRedirects');
  }

  /**
   * Gets a single 301 URL Redirect
   *
   */
  public getRedirect(UrlRedirectId: number): Observable<Redirect> {
    return this.get('urlredirects/'+UrlRedirectId,'UrlRedirect');
  }

  /**
   * Updates a 301 URL Redirect
   *
   */
  public updateRedirect(redirect: Redirect): Observable<Redirect> {
    let route = 'urlredirects/' + redirect.UrlRedirectId;
    return this.put(route, redirect, 'UrlRedirect');
  }

  /**
   * Creates a new 301 URL Redirect
   *
   */
  public createRedirect(redirect: Redirect): Observable<Redirect> {
    return this.post('urlredirects', redirect, 'UrlRedirect');
  }

  /**
   * Gets a list of Domains
   *
   */
  public getDomains(): Observable<Domain[]> {
    return this.get('domains?sort=domainid&cache=refresh','Domains');
  }

  /**
   * Gets a list of Storefront Domains
   *
   */
  public getStorefrontDomains(): Observable<Domain[]> {
    return this.get('domains?filter=webapptype~eq~3&sort=domainid&cache=refresh','Domains');
  }

  public getPortalDomains(portalId: number): Observable<Domain[]> {
    return this.get('domains?sort=domainid&cache=refresh&filter=portalid~eq~'+portalId, 'Domains');
  }

  /**
   * Gets a single Domain
   *
   */
  public getDomain(DomainId: number): Observable<Domain> {
    return this.get('domains/'+DomainId,'Domain');
  }

  /**
   * Updates a Domain
   *
   */
  public updateDomain(domain: Domain): Observable<Domain> {
    let route = 'domains/' + domain.DomainId;
    return this.put(route, domain, 'Domain');
  }

  /**
   * Creates a new Domain
   *
   */
  public createDomain(domain: Domain): Observable<Domain> {
    return this.post('domains', domain, 'Domain');
  }

  /**
   * Deletes a Domain
   *
   */
  public deleteDomain(domainId: number): Observable<boolean> {
    return this.delete('domains/' + domainId);
  }

  /**
   * Gets a list of reviews
   *
   */
  public getReviews(): Observable<Review[]> {
    return this.get('reviews','Reviews');
  }

  /**
   * Gets details for a review
   *
   */
  public getReview(reviewId: number): Observable<Review> {
    return this.get('reviews/'+reviewId,'Review');
  }

  /**
   * Updates a review
   *
   */
  public updateReview(review: Review): Observable<any> {
    return this.put('reviews/'+review.ReviewId,review);
  }

  /**
   * Deletes a review
   *
   */
  public deleteReview(reviewId: number): Observable<any> {
    return this.delete('reviews/'+reviewId);
  }

  public getAccount(AccountId: number): Observable<Account> {
    return this.get('accounts/' + AccountId, 'Account');
  }

  public updateAccount(account: Account): Observable<Account> {
    return this.put('accounts/' + account.AccountId, account);
  }

  public getAccountProfiles(accountId: number): Observable<Profile[]> {
    return this.get(`customerprofiles/${accountId}?filter=accountid~eq~${accountId}`, 'Profiles');
  }

  public updateAccountProfiles(accountId: number, profileIds: number[]): Observable<any> {
    let ids = encodeURI(profileIds.join(','));
    return this.post(`accountprofile/accountassociatedprofiles/${accountId}/${ids}`, null);
  }

  public deleteAccountProfile(accountProfileId: number): Observable<any> {
    return this.delete(`accountprofile/${accountProfileId}`);
  }

  public clearCache(): Observable<any> {
    return this.get('clearcache/clearapicache', 'booleanModel');
  }

  public uploadImage(file: File, portalId: number): Observable<Image> {

    const formData = new FormData();

    formData.append( 'Image', file );
    formData.append( 'extension', file.name.split('.').pop() );
    formData.append( 'fileName', file.name.replace(/ /g, '-') );
    formData.append( 'portalId', String(portalId) );
    formData.append( 'accountId', String(0) );

    // return this.formData('image', formData, 'Image');
    let url = this.base + 'image';

    var xhr = new XMLHttpRequest();

    return Observable.create(observer => {
      observer.next(xhr);
      xhr.upload.onprogress = function(progress) {
        let percentCompleted;
        if (progress.lengthComputable) {
          percentCompleted = Math.round(progress.loaded / progress.total * 100);
          if (percentCompleted == 100) percentCompleted = 99;
          if (percentCompleted < 1) observer.next(0);
          else observer.next(percentCompleted);
        }
      };
      xhr.addEventListener("load", (e) => {
        let status = e.target['status'];
        let response = JSON.parse(e.target['responseText']);
        if (status !== 200 || status !== 201) {
          // let productAsset = new ProductAsset();
          let Image: any = {};
          Image = response.Image;
          Image.HasError = response.HasError;
          Image.ErrorMessage = response.ErrorMessage;
          Image.IsUploading = false;
          observer.next(Image);
          return observer.complete();
        } else {
          return observer.error(response);
        }
      });
      xhr.addEventListener("error", (err) => {
        observer.error('Upload error');
      });
      xhr.addEventListener("abort", (abort) => {
        observer.error('Transfer aborted by the user');
      });
      xhr.open('POST', url, true);
      xhr.setRequestHeader("Accept", "application/json");
      xhr.setRequestHeader("Authorization", this.getAuth());
      xhr.send(formData);
      return () => xhr.abort();
    });
  }

  /**
   * Starts a new Lucene Index
   *
   */
  public reindex(): Observable<boolean> {

    let url = this.base + 'managesearchindex';
    let body = "";

    let headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': this.getAuth()
    });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(url, body, options)
                    .pipe(map( res => {
                      console.log(res);
                      return (res.status == 201) ? true : false;
                    }),
                    catchError(this.handleError));
  }

  public CheckEmailAccount(): Observable<any> {
    return this.get('Diagnostics/CheckEmailAccount');
  }

  public getCountries(): Observable<Country[]> {
    return this.get('countries', 'Countries');
  }

  public getProductArtwork(productId: number): Observable<ProductArtwork[]> {
    return this.get('artworks?productId=' + productId, 'Artwork');
  }

  public updateProductArtwork(productArtwork: ProductArtwork): Observable<ProductArtwork> {
      return this.put('artworks/' + productArtwork.ID, productArtwork, 'Artwork');
  }

  public setProductArtwork(productArtwork: ProductArtwork): Observable<ProductArtwork> {
    return this.post('artworks', productArtwork, 'Artwork');
  }

  public deleteProductArtwork(productArtwork: ProductArtwork): Observable<boolean> {
    return this.delete('artworks/'+productArtwork.ID);
  }

  public getBID(): Observable<number> {
    return this.get('bid');
  }

  public getTokenForCODA(): Observable<CodaTokenResponse> {
    return this.post('coda/generatetoken', {});
  }

  public resetEmail(email, profileId): Observable<boolean>{
    return this.post('accounts/resetpassword', {Email: email, ProfileId: profileId});
  }

  public unlockContact(contact: Contact): Observable<boolean>{
      return this.post('accounts/unlockuser', contact);
  }

  public getCurrencyTypes(): Observable<CurrencyType[]> {
    return this.get('currencytypes', 'CurrencyTypes');
  }

  public getReports(): Observable<ReportSection[]> {
    return this.get('Exago/GetReportNavigation').pipe(map(res => {

      return res.Sections || null;
    }),catchError(this.handleError));
  }

  public getReportURL(reportInfo: ReportInfo): Observable<string>{
      return this.post('Exago/GetReportURL/', reportInfo)
          .pipe(map(res => {
              let body = res.json();
              return body || {};
          })
          ,catchError(this.handleError));
  }

  public getReportURLEndpoint(): string {
    return this.base + 'Exago/GetReportUrl/';
  }

  public getReportInfo(reportId: number): Observable<ReportInfo> {
    return this.get('Exago/GetReportInfoById/' + reportId).pipe(map(res => {
        return res.Report || {};
    }),catchError(this.handleError));
  }

  public getReportKey(): Observable<string> {
    return this.get('reportkey');
  }

  public getEventLogs(BID: number, views: number): Observable<EventLog[]> {
    //console.log('base: ', this._base)
    //console.log('key:', sessionStorage.getItem('znode_auth'))
    let data = this.get('eventlogviewer/geteventsbybid/'+BID+"/"+views)
    .pipe(map(Events => {
      console.log("Events: ",Events)
      // Events.forEach((e)=>{
      //   let edate = e.EntryDT.match(/\d+/)[0];
      //   console.log('Edate: ',edate)
      //   e.EntryDT = new Date(parseInt(edate)).toUTCString();
      // })
      return Events;
    }))
    console.log('Data: ', data)
    return data;
  }
}
