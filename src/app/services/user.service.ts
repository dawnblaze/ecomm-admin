import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, throwError }     from 'rxjs';
import { map, catchError}  from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable()
export class UserService {

  constructor(private http:Http, private authService:AuthService) {
  }


  public getUser(ID:number):Observable<any> {

    let headers = new Headers();
    let token = this.authService.getToken();
    let base = this.authService.getBase();

    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + token);

    return this.http
      .get(
        base + 'User/' + ID,
        {headers}
      )
      .pipe(map(this.extractData)
      ,catchError(this.handleError));
  }

  public getUsers():Observable<any[]> {

    let headers = new Headers();
    let token = this.authService.getToken();
    let base = this.authService.getBase();

    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + token);

    return this.http.get(base + 'User', {headers})
      .pipe(map(this.extractData)
      ,catchError(this.handleError));
  }

  private extractData(res:Response) {
    let body = res.json();
    return body || {'error': true};
  }

  private handleError(error:any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return throwError(errMsg);
  }


}
