/**
 * Domain Service Provider
 *
 * Provides central location for calculating domains for
 * tenant specific APIs and image paths.
 *
 * @author Mike Payne <payne@rivetal.com>
 **/

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable }     from 'rxjs';

@Injectable()
export class DomainService {

    private isSecure: boolean = true;

    private domain(): string {
        var base = window.location.hostname;
        var protocol = (this.isSecure) ? 'https://' : 'http://';

        if (base === 'localhost')
        {
            // base = 'ecommdev1.corebridge.net';
            base = 'qa.corebridge.net';
        }
        else if(base === 'cyrsales.ecommadmin.cyrious.com')
        {
            base = 'productiontest.corebridge.net';
        }
        else if(base === 'corebridge.rivetal.com')
        {
            //base = 'rivetal.ecommadmin.corebridge.net';
            base = 'productiontest.ecommadmin.corebridge.net';
        }
        else if(base === 'franklinimaging.ecommadmin.cyrious.com')
        {
            base = 'franklinimaging.ecommadmin.corebridge.net'
        }
        else if(base === 'printingtech.ecommadmin.cyrious.com')
        {
            base = 'printingtech.ecommadmin.corebridge.net'
        }
        else if(base === 'qualityserviceplus.ecommadmin.cyrious.com')
        {
            base = 'qualityserviceplus.ecommadmin.corebridge.net'
        }
        else if(base === 'brandgraphic.ecommadmin.cyrious.com')
        {
          base = 'brandgraphic.ecommadmin.corebridge.net'
        }
        else if(base === 'firehouse.ecommadmin.cyrious.com')
        {
          base = 'firehouse.ecommadmin.corebridge.net'
        }
        else if(base === 'znodeenginev3admin.azurewebsites.net') 
        {
            base = 'cy1.ecommadmin.corebridge.net';
        }
        else if(base === 'ecommadmin.cyrious.com')
        {
            base = 'cy2.ecommadmin.corebridge.net';
        }
        else if(base === 'fstest.ecommadmin.corebridge.net')
        {
            base = 'fstraining19.corebridge.net';
            
        }else if(base === 'jm.ecommadmin.corebridge.net'){
            base = 'jm.corebridge.net';
        }

        return protocol + base;
    }

    public get cbms() {
        let url = this.domain().replace('ecommadmin.corebridge.net', 'corebridge.net');
        url += '/api/public/';
        return url;
    }

    private _bid: number = undefined;
    public get bid(){
        if (this._bid === undefined)
            this._bid = parseInt(sessionStorage.getItem('bid'));

        return this._bid;
    }

    private _ecod: string = undefined;
    public get ecod(){
        if (this._ecod === undefined){
            let environment = "";
            switch(window.location.hostname)
            {
                case "qa.ecommadmin.corebridge.net":
                case "cyrqa.ecommadmin.cyrious.com":
                case "cbsupport.ecommadmin.corebridge.net":
                    environment = "qa";
                    break;
                case "ecommdev1.ecommadmin.corebridge.net":
                case "cyrdev.ecommadmin.cyrious.com":
                    environment = "dev";
                    break;
            }
            this._ecod = `https://bn${this.bid}${environment}.designer.corebridge.net`;
        }

        return this._ecod;
    }

}
