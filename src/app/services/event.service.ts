import { Injectable } from '@angular/core';
import { GlobalEventService, GlobalEventServiceType } from './global-event.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  public global: GlobalEventServiceType = GlobalEventService;

  constructor() { }

  /**
   * @alias
   * @param $event
   */
  stop($event) {
    return this.stopPropagation($event);
  }

  /**
   * @param $event
   */
  stopPropagation($event) {
    if ($event) {
      $event.stopPropagation();
    }
  }

  requirePath($event, knownSource: string): boolean {
    if (!$event) {
      return;
    }
    for (const path of ($event.path || ($event.composedPath && $event.composedPath()))) {
      const source = knownSource.toLowerCase();
      const isRegEx = !!source.match(/[\$\^]/);
      const tag = path.tagName;
      const matched = () => isRegEx ? tag.match(source) : tag.toLowerCase() === source;
      if (tag && matched()) {
        return true;
      }
    }
    this.stopPropagation($event);
    return false;
  }

  requirePaths(paths, $event) {
    for (const path of paths) {
      const matched = this.requirePath($event, path);
      if (matched) {
        return true;
      }
    }
    return false;
  }

  forceStop($event: MouseEvent) {
    if ($event) {
      $event.stopPropagation();
      $event.stopImmediatePropagation();
      $event.preventDefault();
    }
  }

  private readonly _keyUpStream = new Subject<KeyboardEvent>();
  public readonly KeyUpStream = this._keyUpStream.asObservable();

  public onKeyUp(event: KeyboardEvent){
    this._keyUpStream.next(event);
  }

  private readonly _keyDownStream = new Subject<KeyboardEvent>();
  public readonly KeyDownStream = this._keyDownStream.asObservable();
  private readonly _scrollStream = new Subject<Event>();
  public readonly ScrollStream = this._scrollStream.asObservable();

  public onKeyDown(event: KeyboardEvent){
    this._keyDownStream.next(event);
  }

  onScroll(event: Event) {
    this._scrollStream.next(event);
  }

  private readonly _windowResizeStream = new Subject();
  public readonly WindowResizeStream = this._windowResizeStream.asObservable();

  public OnWindowResize(){
    this._windowResizeStream.next();
  }

  private readonly _documentClickStream = new Subject();
  public readonly DocumentClickStream = this._documentClickStream.asObservable();

  public OnDocumentClick(event: Event){
    this._documentClickStream.next(event);
  }
}
