import { Injectable, TemplateRef } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

import { Alert, DetailedError } from '../modules/corebridge/models/alert';
import { IDictionary } from '../modules/corebridge/models/dictionary';

@Injectable()
export class AlertService {

  private _alerts = new Subject<Alert>();
  private _detailedAlerts = new Subject<DetailedError[]>();
  private _detailedAlertNavigationActions = new Subject<any[]>();
  private _detailedAlertHideSummary = new Subject<string[]>();
  private detailedAlertsMap:IDictionary<DetailedError[]> = {};
  private _suppressDetailedErrors = new BehaviorSubject<boolean>(false);
  private _showErrorDetails = new BehaviorSubject<boolean>(false);
  public detailedSummaryTemplate:TemplateRef<any> = null;

  public noteAdded = this._alerts.asObservable();
  public readonly detailedNoteAdded:Observable<DetailedError[]> = this._detailedAlerts.asObservable();
  public readonly detailedAlertNavigationAdded:Observable<any[]> = this._detailedAlertNavigationActions.asObservable();
  public readonly detailedAlertHideSummary:Observable<string[]> = this._detailedAlertHideSummary.asObservable();
  public readonly suppressDetailedErrors:Observable<boolean> = this._suppressDetailedErrors.asObservable();
  public readonly showErrorDetails:Observable<boolean> = this._showErrorDetails.asObservable();

  constructor() { }

  public add(alert: Alert|DetailedError[]) {
    if(!Array.isArray(alert)) {
      this._alerts.next(alert);
    } else {
      this._detailedAlerts.next(alert);
    }
  }

  public navigate(gotoActions: any[]) {
    this._detailedAlertNavigationActions.next(gotoActions);
  }

  public hideSummary(summaryKeyList: string[]) {
    this._detailedAlertHideSummary.next(summaryKeyList);
  }

  public success(message: string, timeout: number = 3000) {
    this.add({type: 'success', message: message, timeout: timeout, pinned: false});
  }

  public warning(message: string, timeout: number = 3000) {
    this.add({type: 'warning', message: message, timeout: timeout, pinned: false});
  }

  public addition(message: string, timeout: number = 3000) {
    this.add({type: 'addition', message: message, timeout: timeout, pinned: false});
  }

  public error(message: string, timeout: number = 8000) {
    if (message.indexOf) {
      if(message.indexOf('; StackTrace:') != -1){
        console.error(message);
        message = message.split('; StackTrace:')[0].replace('Message: ', '').trim();
      }
    }

    this.add({type: 'error', message: message, timeout: timeout, pinned: false});
  }

  public info(message: string, timeout: number = 3000) {
    this.add({type: 'info', message: message, timeout: timeout, pinned: false});
  }

  public detailedError(errorGroup:string, detailedErrors:DetailedError[]){
    this.detailedAlertsMap[errorGroup] = detailedErrors||[];
    const combinedErrors:DetailedError[] = [];
    for (const key in this.detailedAlertsMap) {
      combinedErrors.push(...this.detailedAlertsMap[key]);
    }

    this.add(combinedErrors);
  }

  public notifyDetailedError(){
    const combinedErrors:DetailedError[] = [];
    for (const key in this.detailedAlertsMap) {
      combinedErrors.push(...this.detailedAlertsMap[key]);
    }

    this.add(combinedErrors);
  }

  public clearDetailedError(){
    this.detailedAlertsMap = {};
    this.add([] as DetailedError[]);
  }

  public toggleSuppressDetailedErrors(state: boolean){
    this._suppressDetailedErrors.next(state);
  }

  public toggleErrorDetails(showErrorDetails: boolean) {
    this._showErrorDetails.next(showErrorDetails);
  }

}
