import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'imagePath', pure: false})
export class ImagePathPipe implements PipeTransform {

    transform(path:string, base:string): string {
        if( /^http(s)?:\/\//.test(path) ){
            return path;
        }

        return base + path.replace('~/', '');
    }
}


