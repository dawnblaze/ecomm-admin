import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayMap'
})
export class ArrayMapPipe implements PipeTransform {

  transform(value: any[], predicate:(value: any, index: number, obj: any[]) => boolean, ...args: any[]): any {
    if(!value){
      value = [];
    }
    return value.map(predicate);
  }

}
