import { Pipe, PipeTransform } from '@angular/core';

import { Color } from '../modules/ecommerce/models/color';

@Pipe({
  name: 'color',
  pure: false
})
export class ColorPipe implements PipeTransform {
  transform(value: Color, type: string): string {
    if (type == 'cmyk') {
      return `${value.c}, ${value.m}, ${value.y}, ${value.k}`;
    }
    else if (type == 'rgb') {
      return `rgb(${value.r}, ${value.g}, ${value.b})`;
    }
    else if (type == 'hex') {
      return ('0' + parseInt(value.r + '', 10).toString(16)).slice(-2) +
        ('0' + parseInt(value.g + '', 10).toString(16)).slice(-2) +
        ('0' + parseInt(value.b + '', 10).toString(16)).slice(-2);
    }
    else if (type == 'cmyk-rgb') {
      let r = Math.floor(255 * (1 - (value.c / 100)) * (1 - (value.k / 100))),
        g = Math.floor(255 * (1 - (value.m / 100)) * (1 - (value.k / 100))),
        b = Math.floor(255 * (1 - (value.y / 100)) * (1 - (value.k / 100)));

      return `rgb(${r}, ${g}, ${b})`;
    }
  }
}
