export const environment = {
  production: false,
  theme: 'corebridge',
  cesaBaseUri: 'https://cesaqa.cyrious.com/',
  ssrsUrl: 'http://qa.ecommreporting.corebridge.net/GenericReportViewerPage',
  codaUrl: 'https://coda-dev.corebridge.net',
  customersCanvasBaseUri: 'https://canvas-qa.corebridge.net/'
};
