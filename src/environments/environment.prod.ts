export const environment = {
  production: true,
  theme: 'corebridge',
  cesaBaseUri: 'https://cesa.cyrious.com/',
  ssrsUrl: 'http://prod.ecommreporting.corebridge.net/GenericReportViewerPage',
  codaUrl: 'https://coda-dev.corebridge.net',
  customersCanvasBaseUri: 'https://canvas.corebridge.net/'
};
