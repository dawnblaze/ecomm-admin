export const environment = {
  production: false,
  theme: 'corebridge',
  cesaBaseUri: 'http://localhost:44400/',
  ssrsUrl: 'http://dev.ecommreporting.corebridge.net/GenericReportViewerPage',
  codaUrl: 'https://localhost:44700',
  customersCanvasBaseUri: 'https://localhost:44201/'
};
