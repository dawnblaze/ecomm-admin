export const environment = {
  production: false,
  theme: 'corebridge',
  cesaBaseUri: 'https://cesadev.cyrious.com/',
  ssrsUrl: 'http://dev.ecommreporting.corebridge.net/GenericReportViewerPage',
  codaUrl: 'https://coda-dev.corebridge.net',
  customersCanvasBaseUri: 'https://canvas-dev.corebridge.net/'
};
